FROM nixos/nix

WORKDIR /root
ADD app /root/app
ADD default.nix /root/default.nix

RUN nix-shell --run "echo ok"
RUN rm -rf /root/app /root/default.nix

# docker build -t juliendehos/cicd:mywebpage2 .
# docker push juliendehos/cicd:mywebpage2

