import Text.Pandoc
import Text.Pandoc.JSON

inFramed :: Block -> Block
inFramed x = Div nullAttr 
    [ RawBlock (Format "latex") "\\begin{framed}"
    , x
    , RawBlock (Format "latex") "\\end{framed}"
    ]

main :: IO ()
main = toJSONFilter behead
  where behead HorizontalRule = Null
        behead x@(CodeBlock _ _) = inFramed x
        behead x@(BlockQuote _) = inFramed x
        behead x = x

