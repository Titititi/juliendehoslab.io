{ pkgs
? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/20.03-beta.tar.gz") {}
} :

let drv = pkgs.haskellPackages.callCabal2nix "app" ./. {};
in if pkgs.lib.inNixShell then drv.env else drv

