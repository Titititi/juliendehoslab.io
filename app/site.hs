{-# LANGUAGE OverloadedStrings #-}

--import qualified Codec.Archive.Tar as Tar
import qualified Data.ByteString.Lazy as LBS
import qualified Data.Text.Lazy as L
import qualified System.FilePath as FP

import qualified Control.Foldl as FD
import qualified Turtle as TT

import Codec.Archive.ZTar.GZip (extract)
import Control.Monad (when)
import Data.List (span)
import Data.Monoid ((<>))
import Hakyll
import Lucid
import System.FilePath ((</>))
import Text.Pandoc (bottomUp, nullAttr, Block(Header), Inline(..), Pandoc(..))
import Text.Pandoc.Highlighting
import Text.Pandoc.Options

import qualified GHC.IO.Encoding as E



-------------------------------------------------------------------------------
-- configuration
-------------------------------------------------------------------------------

myContext :: Context String
myContext =  dateField "date" "%Y-%m-%e" <> defaultContext

myConfig :: Configuration
myConfig = defaultConfiguration 
    { providerDirectory = "source"
    , destinationDirectory = "public"
    }

myWriterOptions :: WriterOptions
myWriterOptions = defaultHakyllWriterOptions
    { writerSectionDivs = True
    , writerTOCDepth = 2
    , writerHTMLMathMethod = KaTeX mempty
    , writerHighlightStyle = Just pygments
    }

myWriterOptionsToc :: WriterOptions
myWriterOptionsToc = myWriterOptions
    { writerTableOfContents = True
    , writerTemplate = Just myTocTemplate
    }

-------------------------------------------------------------------------------
-- main Hakyll program
-------------------------------------------------------------------------------

main :: IO ()
main = do

    E.setLocaleEncoding E.utf8
    let siteDstDir = destinationDirectory myConfig
    extract "static/syntax.tar.gz" (siteDstDir </> "css")
    extract "static/katex.tar.gz" siteDstDir
    extract "static/revealjs.tar.gz" siteDstDir
    let glDir = siteDstDir </> "posts" </> "gl" </> "files"
    extract "static/gl-doxygen.tar.gz" glDir
    extract "static/gl-sphinx.tar.gz" glDir
    let siteSrcDir = providerDirectory myConfig
    topicsTT <- TT.fold (TT.cd (TT.decodeString siteSrcDir) >> TT.ls "posts") FD.list
    TT.sh $ TT.cd ".."
    let topics = map TT.encodeString topicsTT
    myHakyll myConfig topics


myHakyll :: Configuration -> [FilePath] -> IO ()
myHakyll config topics = hakyllWith config $ do

    let siteDstDir = destinationDirectory config

    -- copy static files
    match ("files/**" .||. "posts/*/files/**") 
        (route idRoute >> compile copyFileCompiler)

    -- build CSS stylesheets
    match "css/*.hs" $ do
        route $ setExtension "css"
        compile $ getResourceString >>= traverse (unixFilter "runghc" [])

    -- build UML diagrams
    match "posts/*/uml/*.uml" $ do
        route $ setExtension "svg"
        compile $ getResourceLBS
            >>= traverse (unixFilterLBS "plantuml" $ words "-Tsvg -p -charset UTF-8")

    -- build Dot graphics
    match "posts/*/dot/*.dot" $ do
        route $ setExtension "svg"
        compile $ getResourceLBS
            >>= traverse (unixFilterLBS "dot" ["-Tsvg"])

    -- build Tikz graphics
    match "posts/*/tikz/*.tex" $ do
        route $ setExtension "svg"
        compile $ do
            name <- FP.takeBaseName <$> getResourceFilePath
            TmpFile tmpFile <- newTmpFile name
            let tmpDir  = FP.takeDirectory tmpFile
            getResourceLBS
                >>= traverse (unixFilterLBS "xelatex" 
                        [ "--shell-escape"
                        , "--output-directory", tmpDir
                        , "-jobname", name <> "-tikz1"
                        ])
                    >>= traverse (unixFilterLBS "pdfcrop" 
                        [ "--margin", "5"
                        , tmpDir </> name <> "-tikz1.pdf"
                        , tmpDir </> name <> "-tikz2.pdf"
                        ])
                    >>= traverse (unixFilterLBS "pdf2svg" 
                        [ tmpDir </> name <> "-tikz2.pdf"
                        , tmpDir </> name <> "-tikz2.svg"
                        ])
                    >>= traverse (unixFilterLBS "cat" [tmpDir </> name <> "-tikz2.svg"])

    -- build CV
    match "cv/cv-dehos.md" $ do
        route $ setExtension "pdf"
        compile $ do
            TmpFile tmpFileCv <- newTmpFile "tmp-cv-dehos.pdf"
            dstDir <- last . init . FP.splitDirectories <$> getResourceFilePath
            getResourceLBS 
                >>= traverse (unixFilterLBS "pandoc"
                    [ "-o", tmpFileCv
                    , "--pdf-engine", "pdflatex"
                    , "--resource-path", siteDstDir </> dstDir
                    , "-V", "geometry:margin=1in"
                    , "-s"
                    ])
                >>= traverse (unixFilterLBS "cat" [tmpFileCv])

    -- main index page
    match "*.md" $ do
        route $ setExtension "html"
        compile $ do
            thisId <- getUnderlying
            thisNotoc <- getMetadataField thisId "notoc"
            let wopts = if null thisNotoc then myWriterOptionsToc else myWriterOptions
            defaultTpl <- compileTemplateItem (myDefaultItem False)
            myPandocCompiler wopts
                >>= applyTemplate defaultTpl myContext
                >>= relativizeUrls

    -- topic index pages
    match "posts/*/index.md" $ do
        route $ setExtension "html"
        compile $ do
            dstDir <- last . init . FP.splitDirectories <$> getResourceFilePath
            let postsPatt = fromGlob $ "posts" </> dstDir </> "post*.md"
            posts <- loadAll (postsPatt .&&. hasNoVersion)
            let indexCtx = myContext <> listField "posts" myContext (return posts)
            indexTpl <- compileTemplateItem myIndexItem
            defaultTpl <- compileTemplateItem (myDefaultItem True)
            pandocCompiler
                >>= applyTemplate indexTpl indexCtx
                >>= applyTemplate defaultTpl indexCtx
                >>= relativizeUrls

    -- topic pages
    match "posts/*/post*.md" $ do
        route $ setExtension "html"
        compile $ do
            fp <- getResourceFilePath
            let dstDir = last $ init $ FP.splitDirectories fp
            let thisName = FP.takeBaseName fp
            -- find previous/next
            let postsPatt = fromGlob $ "posts" </> dstDir </> "post*.md"
            postsItem  <- loadAll (postsPatt .&&. hasVersion "slides")
            let postsName = map (FP.takeBaseName . toFilePath . itemIdentifier) 
                            (postsItem :: [Item LBS.ByteString])
            let (prevs, nexts) = span (/= thisName) postsName
            let prev = if null prevs then mempty else constField "prev" (last prevs)
            let next = if length nexts < 2 then mempty else constField "next" (nexts !! 1)
            -- find index title (i.e. topic)
            let indexPatt = fromGlob $ "posts" </> dstDir </> "index.md"
            indexAll <- loadAll (indexPatt .&&. hasVersion "slides")
            when (null indexAll) $ error $ "missing index: " ++ show indexPatt
            let indexItem = head indexAll
            let indexId = itemIdentifier (indexItem :: Item LBS.ByteString)
            indexTitle <- getMetadataField' indexId "title"
            -- create context
            let versions = FP.replaceExtension (FP.takeFileName fp) "html"
            let defCtx = myContext 
                        <> constField "topic" indexTitle 
                        <> constField "versions" versions
                        <> prev
                        <> next
            -- write toc ?
            thisId <- getUnderlying
            thisNotoc <- getMetadataField thisId "notoc"
            let wopts = if null thisNotoc then myWriterOptionsToc else myWriterOptions
            -- run compiler
            defaultTpl <- compileTemplateItem (myDefaultItem True)
            myPandocCompiler wopts
                >>= applyTemplate defaultTpl defCtx
                >>= relativizeUrls

    -- topic pages (slides)
    match "posts/*/*.md" $ version "slides" $ do
        route $ setExtension "html.slides.html"
        compile $ getResourceLBS 
            >>= traverse (unixFilterLBS "pandoc" 
                    [ "-s"
                    , "-i"
                    , "--katex=../../katex/"
                    , "--section-divs"
                    , "--slide-level=2"
                    , "--css", "../../css/myrevealjs.css"
                    , "-t", "revealjs"
                    , "-V", "theme=white"
                    , "-V", "center=false"
                    , "-V", "history=false"
                    , "-V", "transition=fade"
                    , "-V", "slideNumber=true"
                    , "-V", "fragments=false"
                    , "-V", "revealjs-url=../../revealjs"
                    , "-A", "source/files/mygoatcounter.js"
                    , "-H", "source/files/favicon.html"
                    ])

    mapM_ (myLatexRoute "html.pdf" [] siteDstDir) topics
    -- mapM_ (myLatexRoute "html.txt" ["-t", "native"] siteDstDir) topics
    -- mapM_ (myLatexRoute "html.tex" [] siteDstDir) topics

myLatexRoute :: String -> [FilePath] -> FilePath -> FilePath -> Rules ()
myLatexRoute ext opts siteDstDir baseDir = do
    dotDeps <- makePatternDependency (fromGlob $ baseDir </> "dot/*.dot")
    tikzDeps <- makePatternDependency (fromGlob $ baseDir </> "tikz/*.tex")
    umlDeps <- makePatternDependency (fromGlob $ baseDir </> "uml/*.uml")
    rulesExtraDependencies [dotDeps, tikzDeps, umlDeps] $ 
        match (fromGlob $ baseDir </> "*.md") $ version ext $ do
            route $ setExtension ext
            compile $ do
                fp <- getResourceFilePath
                TmpFile tmpFile <- newTmpFile (FP.takeBaseName fp ++ "." ++ ext)
                let dstDir = FP.joinPath (siteDstDir : init (tail $ FP.splitPath fp))
                getResourceLBS
                    >>= traverse (unixFilterLBS "pandoc"
                        ([ "-o", tmpFile
                        , "--resource-path", dstDir
                        , "--filter", "latexfilter"
                        , "--include-in-header", "./static/mylatexheader.txt"
                        , "--toc"
                        , "-s"
                        , "-V", "geometry:margin=1in"
                        ] ++ opts)) 
                    >>= traverse (unixFilterLBS "cat" [tmpFile])

-------------------------------------------------------------------------------
-- custom compilers/filters
-------------------------------------------------------------------------------

myPandocCompiler :: WriterOptions -> Compiler (Item String)
myPandocCompiler wopts = 
    pandocCompilerWithTransform defaultHakyllReaderOptions wopts myPandocTransform

myPandocTransform :: Pandoc -> Pandoc
myPandocTransform = bottomUp headerLink 
    where getString (Str s) = s
          getString (Code _ s) = s
          getString _  = " "
          inlinesToString = concatMap getString
          headerLink (Header a (href,b,c) d) = Header a (href,b,c)
              [ Link nullAttr d ("#"++href, "selflink " ++ inlinesToString d) ]
          headerLink x = x

-------------------------------------------------------------------------------
-- HTML templates
-------------------------------------------------------------------------------

myTocTemplate :: String
myTocTemplate = L.unpack $ renderText $ 
    div_ [id_ "sidebar-container"] $ do
        div_ [id_ "sidebar-toc"] "$toc$"
        div_ [id_ "sidebar-content"] "$body$"

myIndexItem :: Item String
myIndexItem = Item "myIndexItem" $ L.unpack $ renderText $ do
    p_ "$body$"
    ul_ $ do
        "$for(posts)$"
        li_ [class_ "post"] $ do
            a_ [href_ "$url$"] "$title$"
            a_ [href_ "$url$.pdf"] $
                img_ [src_ "/files/icon-pdf.svg", class_ "icon-inline"] 
            a_ [href_ "$url$.slides.html"] $
                img_ [src_ "/files/icon-slides.svg", class_ "icon-inline"] 
        "$endfor$"

myPrevTopicNextHtml :: Html () 
myPrevTopicNextHtml = do
    "$if(topic)$"
    p_ mempty
    table_ [class_ "myfloat"] $ tr_ $ do
        td_ [class_ "myfloat-left"] $ do
            "$if(prev)$"
            a_ [href_ "$prev$.html"] $
                img_ [src_ "/files/icon-left.svg", class_ "icon-inline"] 
            "$endif$ "
        td_ [class_ "myfloat-center"] $ 
            a_ [id_ "mytopic", href_ "./index.html"] "$topic$"
        td_ [class_ "myfloat-right"] $ do
            "$if(next)$"
            a_ [href_ "$next$.html"] $
                img_ [src_ "/files/icon-right.svg", class_ "icon-inline"] 
            "$endif$ "
    "$endif$"

myDefaultItem :: Bool -> Item String
myDefaultItem showTitle = Item "myDefaultItem" $ L.unpack $ renderText $ do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            meta_ [name_ "viewport", content_ "width=device-width, initial-scale=1"]
            title_ $ "Julien Dehos' webpage" >> when showTitle " - $title$" 
            link_ [rel_ "stylesheet", href_ "/css/mydefault.css"]
            link_ [rel_ "stylesheet", href_ "/css/syntax/pygments.css"]
            script_ [src_ "/katex/katex.min.js"] L.empty
            script_ [src_ "/files/mykatex.js"] L.empty
            link_ [rel_ "stylesheet", href_ "/katex/katex.min.css"]
            link_ [rel_ "shortcut icon", href_ "/files/favicon.ico"]

        body_ $ do

            header_ $ a_ [id_ "mysite", href_ "/index.html"] "Julien Dehos' webpage"

            main_ $ do

                myPrevTopicNextHtml

                when showTitle $ h1_ [id_ "title"] $ do
                    a_ [href_ "#title"] "$title$ "
                    "$if(versions)$"
                    a_ [href_ "$versions$.pdf"] $
                        img_ [src_ "/files/icon-pdf.svg", class_ "icon-inline"] 
                    a_ [href_ "$versions$.slides.html"] $
                        img_ [src_ "/files/icon-slides.svg", class_ "icon-inline"] 
                    "$endif$"

                "$if(date)$"
                p_ [id_ "mydate"] "Last update: $date$"
                "$endif$"

                "$body$"

                p_ [style_ "margin-top:50px"] mempty
                myPrevTopicNextHtml

            footer_ $ table_ [class_ "myfloat"] $ tr_ $ do
                td_ [class_ "myfloat-left"] $ do
                    a_ [href_ "/index.html"] $ 
                        img_ [src_ "/files/icon-home.svg", class_ "icon"] 
                    a_ [href_ "#"] $ 
                        img_ [src_ "/files/icon-up.svg", class_ "icon"] 
                td_ [class_ "myfloat-center"] $ do
                    "Site generated by "
                    a_ [href_ "https://jaspervdj.be/hakyll"] "Hakyll"
                td_ [class_ "myfloat-right"] $ do
                    a_ [href_ "https://gitlab.com/juliendehos/juliendehos.gitlab.io"] $
                        img_ [src_ "/files/icon-gitlab.svg", class_ "icon"] 
                    a_ [href_ "https://juliendehos_gitlab_io.goatcounter.com/"] $
                        img_ [src_ "/files/icon-analytics.svg", class_ "icon"] 

            script_ [src_ "/files/mygoatcounter.js"] L.empty


{- 
import qualified Data.Text as T

mySearchboxUrl :: T.Text
mySearchboxUrl = 
    "https://duckduckgo.com/search.html?site=julien.dehos.free.fr&prefill=Search"

myIconUp :: String
myIconUp = L.unpack $ renderText $
    a_ [href_ "#"] $ img_ [src_ "/files/icon-up.svg", class_ "icon-inline"]

import Text.Regex

myApplyFilter :: (a -> b) -> Item a -> Compiler (Item b)
myApplyFilter strfilter = return . fmap strfilter

myFilterAddTop :: String -> String
myFilterAddTop input = subRegex patt input ouput
    where patt = mkRegex "(</h[1-3]>)"
          ouput = myIconUp ++ "\\1"
-}

