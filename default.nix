{ config ? {} }:

let

  rev = "20.03-beta";
  #rev = "d5291756487d70bc336e33512a9baf9fa1788faf";  # 19.09
  #rev = "4d403a3a51663f30efd478c8f35f3c5b49c6bc37";  # 2019-12-27
  url =  "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz";
  channel = fetchTarball url;
  pkgs = import channel { inherit config; };

in pkgs.stdenv.mkDerivation {
  name = "juliendehos.gitlab.io";
  src = ./.;

  buildInputs = with pkgs; [

    (haskellPackages.callCabal2nix "app" ./app {})

    (haskellPackages.ghcWithPackages (ps: with ps; [
      clay
      text
    ]))

    gnumake
    graphviz
    gzip
    librsvg
    pandoc
    pdf2svg
    plantuml
    (texlive.combine {
      inherit (texlive)
      scheme-small
      adjustbox
      collectbox
      framed
      pdfcrop
      wasy
      wasysym;
    })

  ];

  installPhase = ''
    mkdir -p $out
    cp -R public $out/
  '';

  shellHook = ''
    export PATH="$PATH:${pkgs.haskellPackages.wai-app-static}/bin"
  '';

}

