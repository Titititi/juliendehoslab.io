let

  config = {
    packageOverrides = pkgz: rec {
      haskellPackages = pkgz.haskellPackages.override {
        overrides = hpNew: hpOld: rec {
          hakyll = hpOld.hakyll.overrideAttrs(old: {
            configureFlags = "-f watchServer -f previewServer";
            preConfigure = "sed -i \"s/warp            >= 3.2   && < 3.3/warp/\" hakyll.cabal";
          });
        };
      };
    };
  };

in import ./default.nix { inherit config; }

