#!/bin/sh

if [ "$#" -lt 2 ] ; then
    echo "usage: $0 dpi files..." >&2
    echo "example: $0 150 foobar.png" >&2
    exit 1
fi

DPI=$1
shift

re='^[0-9]+$'
if ! [[ $DPI =~ $re ]] ; then
    echo "error: dpi must be a number" >&2
    exit 1
fi

DPC=$(bc -l <<< "scale=3; $DPI/2.54")

for FILE in "$@" ; do
    convert "$FILE" -density "$DPC" -units pixelspercentimeter "$FILE"
    echo "$FILE converted"
done

