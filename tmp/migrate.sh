#!/bin/sh

mydir=$1

myhtmls=$(ls *.html)

for myhtml in $myhtmls ; do
    echo $myhtml

    echo "<!doctype html> \
<html> \
<head> \
<meta charset=\"utf-8\"> \
<script type=\"text/javascript\"> \
window.location.replace(\"https://juliendehos.gitlab.io/posts/$mydir/$myhtml\"); \
</script> \
</head> \
<body> \
<a href=\"https://juliendehos.gitlab.io/posts/$mydir/$myhtml\">this page has moved</a> \
</body> \
</html> " > $myhtml


done
