---
title: CM C++, Templates
date: 2019-04-19
---

## Principe du template

- idée : rendre un code générique vis-à-vis d’un type de données
- meta-programmation (code qui écrit du code)
- réduit la quantité de code à écrire, sans surcoût en temps d’exécution
- mécanisme souvent utilisé (STL)
- template de fonction, template de classe

## Mise en oeuvre

- définition du template : patron de code pour un type générique
- instanciation du template : utilisation avec un type concret (génère et compile le code correspondant)

## Tableau de 3 entiers (sans template)

```cpp
#include <iostream>

class Tableau3 {
    private:
        int _donnees [3];
    public:
        Tableau3(int v) : _donnees{v, v, v} {}
        const int & operator[](int i) const {
            return _donnees[i]; }
        int & operator[](int i) { return _donnees[i]; }
};

// opérateur de flux de sortie
ostream & operator<<(ostream & os, const Tableau3 & t) {
    os << t[0] << ' ' << t[1] << ' ' << t[2];
    return os;
}
```

## Tableau de 3 caractères (sans template)

```cpp
#include <iostream>

class Tableau3 {
    private:
        char _donnees [3];
    public:
        Tableau3(char v) : _donnees{v, v, v} {}
        const char & operator[](int i) const {
            return _donnees[i]; }
        char & operator[](int i) { return _donnees[i]; }
};

// opérateur de flux de sortie
ostream & operator<<(ostream & os, const Tableau3 & t) {
    os << t[0] << ' ' << t[1] << ' ' << t[2];
    return os;
}
```

## Tableau de 3 éléments (avec template)

```cpp
#include <iostream>
template <typename T> // définit un template de classe
class Tableau3 {      // on appelle T le type générique
    private:
        T _donnees [3]; // T sera remplacé par un type
    public:
        Tableau3(T v) : _donnees{v, v, v} {}
        const T & operator[](int i) const {
            return _donnees[i]; }
        T & operator[](int i) { return _donnees[i]; }
};

template <typename T> // définit un template de fonction 
ostream & operator<<(ostream & os, const Tableau3<T> & t) {
    os << t[0] << ' ' << t[1] << ' ' << t[2];
    return os;
}
```

* * *

```cpp
int main() {
    Tableau3<int> t(0);
    // instancie le template pour le type int

    Tableau3<char> u('a');
    // instancie le template pour le type char

    Tableau3<int> v(0);
    // réutilisation du template instancié
    // pour un nouveau tableau de int

    return 0;
}
```

## Remarques

- mots-clés (équivalents) : `typename` ou `class`
- on peut spécifier plusieurs types génériques, des valeurs par défaut, des paramètres...
- conseil : mettre tout le template dans un seul fichier `.hpp` (déclaration et implémentation)
- attention : à l’instanciation du template, le type spécifié doit fournir toutes les opérations utilisées dans l’implémentation du template

* * *

```cpp
struct Personne {
    std::string _nom;
};

ostream & operator<<(ostream & os, const Personne & p) {
    os << p._nom;
    return os;
} // obligatoire pour instancier le template Tableau3 

int main() {
    Tableau3<Personne> t({"toto"});
    return 0;
}
```

