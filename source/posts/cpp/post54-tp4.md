---
title: TP4 C++ , Héritage et polymorphisme
date: 2019-04-19
---

## Mise en place du projet

- Copiez le projet de base : `cp -r TP_vide TP4`. 

## Modélisation

On veut gérer des figures géométriques possédant une couleur. Ces figures
peuvent être de deux types : ligne ou polygône régulier. Dans un premier temps,
on propose la modélisation suivante (les héritages sont publiques) :

* * *

![](uml/TP4a.svg){width="80%"}

## Premières structures

- Implémentez un module `Point` et un module `Couleur` (pas besoin de `cpp`) :

![](uml/TP4b.svg){width="30%"}

## Classe mère FigureGeometrique

- Implémentez la classe `FigureGeometrique`.

- Expliquez les droits d'accès de la classe `FigureGeometrique` (en d'autres
  termes : qui a accès aux différents membres de la classe ?).

## Classe dérivée Ligne

- Implémentez la classe `Ligne` (avec héritage public).  La méthode
  `afficher` doit afficher la couleur et les deux points de la ligne, selon
  le format suivant (pour une ligne rouge du point $(0,0)$ au point
  $(100,200)$) :

```text
Ligne 1_0_0 0_0 100_200
```

- Proposez deux façons différentes d'accéder à la couleur, dans la méthode
  `afficher`.

- Quelles sont les méthodes que l'on peut appeler à partir d'une instance de
  `Ligne` ?

- Testez l'affichage dans le `main` et le reste avec des tests unitaires.

## Classe dérivée PolygoneRegulier

- Implémentez la classe `PolygoneRegulier`.  Le constructeur doit allouer
  dynamiquement un tableau et calculer les points correspondants.  La méthode
  `afficher` doit afficher la couleur et les points du polygône, selon le
  format suivant (pour un pentagône vert de centre $(100,200)$ et de rayon
  $50$) :

```text
PolygoneRegulier 0_1_0 150_200 115_247 59_229 59_170 115_152 
```

- Testez l'affichage dans le `main` et le reste avec des tests unitaires.

- Exécutez votre programme avec `valgrind`. Expliquez ce qu'il se passe.
  Modifiez votre classe `PolygoneRegulier` pour corriger le problème.

## Polymorphisme

- On veut désormais pouvoir afficher des ensembles de `FigureGeometrique`, de
  type `Ligne` ou `PolygoneRegulier`. Modifiez votre code pour implémenter
  cela (ne vous préoccupez pas des destructeurs pour l'instant).

- Testez dans le `main` avec un ensemble de type `std::vector`.

- Exécutez votre programme avec `valgrind`. Expliquez ce qu'il se passe.
  Modifiez votre code pour corriger le problème.

## S'il vous reste du temps

- Remplacez le tableau `_points` de `PolygoneRegulier` par un `std::vector` et
  mettez à jour votre code en fonction.

- Vérifiez, avec `valgrind`, que votre gestion mémoire est correcte.


