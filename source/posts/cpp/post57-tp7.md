---
title: TP7 C++, Gestion mémoire
date: 2019-04-19
---

On veut implémenter du code permettant de générer et de visualiser des images
en niveaux de gris.

![](files/image.png)

## Mise en place du projet

- Copiez le projet de base : `cp -r TP_vide TP7`. 

## Module Image de base

- Implémentez le module `Image` suivant. Les pixels de l'image doivent être
  stockées dans un tableau (1D) alloué dynamiquement.

![](uml/TP7a.svg){width="40%"}

* * *

- Testez avec des tests unitaires.  Vous pouvez ajouter la ligne suivante dans
  le programme de tests unitaires pour désactiver la détection de fuites
  mémoire.

```cpp
MemoryLeakWarningPlugin::turnOffNewDeleteOverloads();
```

## Accesseur/modificateur par référence

- Commentez les accesseur/modificateur de pixels et réimplémentez-les en
  "version référence".

- Testez avec des tests unitaires.

## Destructeur

- En utilisant `valgrind`, vérifiez que la mémoire allouée par votre
  `Image` n'est pas libérée.  Implémentez un destructeur et vérifiez que le
  problème est résolu. Vérifiez également que la détection de cpputest passe
  également.

- Implémentez une fonction `void ecrirePnm(const Image & img, const
  std::string & nomFichier)` qui écrit une image dans un fichier au format PNM
  (de type P2).

- Implémentez une fonction `void remplir(Image & img)` qui remplit une image
  avec un dégradé vertical en cosinus (cf image précédente).

## Constructeur par copie

- Implémentez une fonction `Image bordure(const Image & img, int couleur, int
  epaisseur)` qui prend en paramètre une image (entre autres) et retourne une
  copie de l'image avec une bordure.  Vérifiez la gestion mémoire avec
  `valgrind`.

- Implémentez le constructeur par copie pour résoudre le problème.

- Implémentez l'opérateur d'affectation. Testez.

## Interface graphique

- Implémentez une interface graphique qui génère une image de dégradé +
  bordure, l'enregistre dans un fichier puis affiche ce fichier. Pour cela,
  utilisez la méthode `set` du widget
  [Gtk::Image](https://developer.gnome.org/gtkmm/2.24/classGtk_1_1Image.html).

## S'il vous reste du temps

- Réimplémentez le tableau de pixels avec un `std::vector` et mettez à jour
  votre code en fonction.

- Vérifiez, avec `valgrind`, que votre gestion mémoire est correcte.

