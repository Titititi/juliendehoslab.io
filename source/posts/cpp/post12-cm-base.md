---
title: CM C++, Notions de base
date: 2019-04-19
---

## Généralités sur le C++

- langage généraliste
- multi-paradigme : procédural, orienté objet, générique
- compatible avec le C
- normalisé par l’ISO : C++98, C++03, C++11, C++14, C++17
- « réputation » : langage complexe, produit du code rapide
- quelques liens : 
    - <http://en.cppreference.com>
    - <https://isocpp.org>

## Bibliographie

- A tour of C++, Bjarne Stroustrup, 192 pages, Addison-Wesley, 2013

![](files/tour-small.png)

## Quelques langages concurrents

- Le C++ par rapport au C :
    - langage de plus haut niveau

* * *

- Le C++ par rapport à Java/C\# :
    - n’appartient à personne
    - compilation en code natif (pas de machine virtuelle)
    - fonctionnalités bas-niveau également disponibles
    - bibliothèque standard plus restreinte (mais nombreuses bibliothèques tierces)

* * *

- Le C++ par rapport à Rust :
    - plus mature/répandu
    - plus de passif de compatibilité ascendante
    - paradigmes différents (objet vs fonctionnel)

## Exemple

```cpp
// helloworld.cpp
#include <iostream>
int main() {
    std::cout << "Hello world !" << std::endl;
    return 0;
}
```

```text
$ g++ helloworld.cpp

$ ./a.out
Hello world !
```

## Variable 

```cpp
int main() {

    int x; // déclare une variable x

    int y = 2; // déclare et initialise une variable y

    x = y + 1; // fait un calcul utilisant la valeur de y
               // et affecte le résultat dans x

    return 0;
}
```

## Liste d’initialiseurs

- liste des valeurs pour initialiser une variable
- pratique pour initialiser des structures de données composées
- utilisable pour des variables, classes, structures...
- les types doivent correspondre (pas de conversion implicite)

* * *

```cpp
int main() {

    int x = 1; // initialisation classique
    int y {1}; // liste d'initialiseurs

    // liste d'initialiseurs pour des tableaux
    int t[3] {1, 2, 3};
    std::array<int,3> v {1, 2, 3}; 

    int a = 4.2; // conversion implicite de double en int

    int b {4.2}; // erreur : pas de conversion implicite 
                 // avec les listes

    return 0;
}
```

## Déduction de type

- mot-clé `auto`
- laisse le compilateur trouver le type
- mais c’est toujours du typage statique (i.e. à la compilation)
- pratique pour les types fastidieux à écrire
- mais **à utiliser avec parcimonie** (lisibilité du code)

```cpp
int main() {
    auto x = 3.1415;      // x est de type double
    int tab[] {1, 2, 3};  // tableau de int
    auto n = tab[0];      // n est de type int
    auto b = tab[0] == n; // b est de type bool
    return 0;
}
```

## Type booléen

- type : `bool`
- valeurs : `true false`
- operateurs : `and or not && || !`

## Chaîne de caractères

```cpp
#include <iostream>
#include <string>
int main() {
    std::string texte1 {"toto"};
    std::string texte2;
    texte2 = texte1 + "tata";
    std::cout << texte2 << std::endl;
    return 0;
}
```

## Entrées/sorties

```cpp
#include <iostream>
int main() {
    int a;
    std::cout << "Saisissez un entier : ";
    std::cin >> a;
    std::cout << "Vous avez saisi : " << a << std::endl;
    return 0;
}
```

## Fonction 

```cpp
// déclare et implémente la fonction arrondirReel
int arrondirReel(float r) {
    return int(r + 0.5f);
}

// appelle la fonction arrondirReel
int main() {
    int a = arrondirReel(42.7f);
    return 0;
}
```

## Surdéfinition de fonctions

- fonction définie plusieurs fois mais avec des paramètres différents

```cpp
#include <iostream>
void afficher(float x) {
    std::cout << "un float : " << x << std::endl;
}
void afficher(int x) {
    std::cout << "un int : " << x << std::endl;
}
int main() {
    afficher(3);
    afficher(3.14f);
    return 0;
}
```

```text
un int : 3
un float : 3.14
```

## Structures de contrôle 

- `if`:

```cpp
if (x == 42) {
    std::cout << "quarante-deux" << std::endl;
}
else {
    std::cout << "valeur inconnue" << std::endl;
}
```

* * *

- `switch`:

```cpp
switch (x) {
    case 13:
        std::cout << "treize" << std::endl;
        break;
    case 37:
        std::cout << "trente-sept" << std::endl;
        break;
    default:
        std::cout << "valeur inconnue" << std::endl;
}
```

* * *

- `for` :

```cpp
for (int k=0; k<42; ++k) {
    std::cout << "le double de " << k 
              << " est " << 2*k << std::endl;
}
```

* * *

- `while`, `do while` :

```cpp
int x;

while (std::cin >> x and x != 42)
    std::cout << "veuillez entrer 42: ";
}
 
do {
    std::cout << "veuillez entrer 42: ";
} while (std::cin >> x and x != 42);
```

* * *

- `break`, `continue` :

```cpp
while (true) {
    std::cout << "veuillez entrer 42: ";
    int y;
    std::cin >> y;
    if (y == 42) break;
    if (y == 13) continue;
    std::cout << "valeur inconnue" << std::endl;
}
```

## Pointeur 

- variable contenant l’adresse mémoire d’une autre variable
- déclaration : `*`
- adressage : `&`
- déréférencement : `*`
- accès à un membre : `->`
- pointeur nul en C++ : `nullptr`

* * *

```cpp
int main() {
    int a = 9;

    int * pa; // déclare un pointeur

    pa = &a;  // fait pointer pa vers la variable a
              // (adressage)

    *pa = 3;  // affecte 3 dans la variable pointée par pa
              // (déréferencement)

    return 0;
}
```

## Allocation dynamique

- opérateurs : `new delete`
- utilisation : polymorphisme, structures de données complexes
- **à n’utiliser que si on ne peut pas faire autrement** (c’est-à-dire assez
  rarement, cf la section sur la [gestion mémoire](#gestion-mémoire))

* * *

```cpp
#include <iostream>
#include <vector>
int main() {
    int n;
    std::cin >> n;

    // tableau avec allocation dynamique -> PAS BIEN
    int * t1 = new int [n];
    // ...
    delete [] t1 ;

    // tableau de la bibliothèque standard -> BIEN
    std::vector<int> t2(n);
    // ...

    return 0;
}
```

## Référence

- pointeur simplifié (adressage et déréférencement automatiques)
- mais on ne peut pas changer l’adresse pointée
- déclaration : `&`
- utilisation comme une variable classique

```cpp
int main() {
    int a = 12; // crée une variable a

    int & ra = a; // crée une référence vers a

    ra = 42; // modifie la valeur de la 
             // variable référencée par ra

    return 0;
}
```

## Références et paramètres de fonctions

- pour pouvoir modifier la variable passée en paramètre

```cpp
// référence vers une variable modifiable
void initialiserString(std::string & str) {
    str = "toto";
}

int main() {

    // crée une variable "texte"
    std::string texte;

    // texte est modifié lors de l'appel de fonction
    initialiserString(texte);
    ...
}
```

* * *

- pour éviter les copies mémoires des passages par valeur

```cpp
// référence vers une variable non-modifiable
void afficherString(const std::string & str) {
    std::cout << str << std::endl;
}

int main() {
    std::string texte;
    ...
    // texte n'est pas copié ni modifié lors de l'appel
    afficherString(texte);
    ...
}
```

## Espace de noms

- permet de regrouper des symboles et d’éviter des conflits de noms
- l’espace de noms de la bibliothèque standard est `std`

* * *

```cpp
namespace MonEspace {  // declare un nouveau namespace

    void MaFonction() {   // declare et implémente une 
        ...               // fonction dans le namespace
    }
}

int main() {
    // appelle la fonction du namespace
    MonEspace::MaFonction();

    // charge le namespace et appelle la fonction
    using namespace MonEspace;
    MaFonction();

    // std est le namespace de la bibliothèque standard 
    std::cout << "Ceci est un message." << std::endl;
    ...
```

## Énumération

- définit un type et ses valeurs
- à peu près comme en C
- avec quelques fonctionnalités de plus (type sous-jacent, enum class )

* * *

```cpp
// déclare une énumération
enum Couleur { ROUGE, VERT, BLEU };

int main() {

    // utilise l'énumération
    Couleur c = ROUGE;
    // ...

    if (c != Couleur::ROUGE)
        // ...

    return 0;
}
```

## Structure

- en C++, définir une structure définit également un type 
- pas besoin de `typedef`
- **ne pas oublier le point-virgule à la fin de la déclaration**

* * * 

```cpp
struct Point2d {  // déclare une structure Point2d
    float x, y;
};

void afficher(const Point2d & p) { // paramètre de fonction
    std::cout << p.x << " " << p.y << std::endl;
}

int main() {
    Point2d p1;
    p1.x = 13;  // initialisation champ par champ
    p1.y = 37;

    Point2d p2 {13, 37}; // avec une liste d'initialiseurs

    afficher(p2); // appelle la fonction sur la variable p2
    ...
```

## Structure et programmation orientée objet

- en C++, une structure est une classe dont tous les membres sont publics (cf
  la section sur la [POO](#programmation-orientée-objet))
- on peut donc y définir des méthodes (publiques)

* * *

```cpp
struct Point2d {
    float x, y;              // déclare des attributs

    void afficher() const;   // déclare une méthode
};

// implémente la méthode déclarée
void Point2d::afficher() const {
    std::cout << x << " " << y << std::endl;
}

int main() {
    Point2d p2 {13, 37};
    p2.afficher(); // appelle la méthode sur l'objet p2
    return 0;
}
```

## Gestion d’erreurs par assertion 

- **pour les erreurs des développeurs**
- notion de précondition
- simple à utiliser
- désactivable à la compilation (option `-DNDEBUG`)

```cpp
#include <cassert>

char getElement(char * tab, int size, int i) {
    assert(tab != nullptr);
    assert(i>=0 and i<size);
    return tab[i];
}
```

## Gestion d’erreurs par exception

- **pour les erreurs des utilisateurs**
- on exécute du code dans un bloc `try`
- si on détecte une erreur, on lève une exception avec `throw`
- l’exécution est interrompue et la pile d’appels est remontée jusqu’au bloc `catch` correspondant (propagation)
- le bloc `catch` est exécuté (capture et gestion de l’exception)
- si la propagation sort du main, la fonction `std::terminate` est appelée

* * *

```cpp
void FonctionHS() {
    throw int(42); // lève une exception de type int
}
int main(int argc, char ** argv) {
    try { // bloc de code avec gestion d'exceptions
        FonctionHS(); // lève une exception de type int 
        // la suite du bloc n'est pas exécutée
    }
    catch (int v) {  // gère les exceptions de type int
        std::cerr << "exception " << v << std::endl;
    }
    catch (...) {  // gère les autres exceptions
        std::cerr << "exception inconnue" << std::endl;
    }
    // puis l'exécution reprend ici
}
```

