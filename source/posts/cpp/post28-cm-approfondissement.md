---
title: CM C++, Approfondissement
date: 2019-04-19
---

## Site de référence 

Décrit les éléments du langage, la STL, les normes...

- <http://en.cppreference.com/>

## Boost 

Ensemble de bibliothèques très classiques. Exemples de code C++ moderne (ou
pas).

- <http://www.boost.org/>

## Site de la *Standard C++ Foundation*

Regroupe de nombreux liens et informations sur le C++, notamment un brouillon
du livre [A Tour of C++](https://isocpp.org/tour) de Bjarne Stroustrup et
la [C++ Super-FAQ](https://isocpp.org/faq).

- <https://isocpp.org/>

## C++ Core Guidelines

Projet collaboratif initié par Bjarne Stroustrup et Herb Sutter pour diffuser
des bonnes pratiques de programmation en C++ moderne.

- [C++ Core Guidelines](https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md)

## Awesome C++

Liste de bibliothèques C++ pour différents domaines d'application.

- <http://fffaraz.github.io/awesome-cpp>

