---
title: Présentation du module de C++
date: 2019-04-19
---

## Objectifs

- aborder les principales notions de C++ (norme C++14)
- les mettre en pratique
- en utilisant quelques méthodes et outils de développement classiques

## Pré-requis 

- algorithmique
- programmation orientée objet

## Organisation

- 6h de CM 
- 30h de TP
- 12h de projet 

## Évaluation

- contrôle continu 

## Travaux pratiques

- Vous aurez besoin d'un [compte gitlab](https://gitlab.com/users/sign_in).
- Forkez le [dépôt git fourni](https://gitlab.com/juliendehos/L3_CPP_etudiant). 
- **L'utilisation de linux + qtcreator est imposée**.

> Si vous voulez absolument utiliser votre machine perso, installez les paquets
> suivants et **vérifiez que votre installation fonctionne avant les
> séances de TP** :
> `g++` `make` `cmake` `qtcreator` `gdb` `valgrind` `cppcheck`
> `libgtkmm-3.0-dev` `libcpputest-dev` `gnuplot`

