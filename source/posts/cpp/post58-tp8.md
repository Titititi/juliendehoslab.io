---
title: TP8 C++, Architecture Modèle-Vue-Controleur
date: 2019-04-19
---

On veut implémenter un logiciel pour gérer des bouteilles. On veut notamment
pouvoir lire un inventaire de bouteilles à partir d'un fichier et l'afficher
dans une console texte et dans une interface graphique.

## Mise en place du projet

- Utilisez le projet `TP8` fourni. 

## Architecture Modèle-Vue-Contrôleur 

- Qu'est-ce-que l'architecture MVC ? Quel est le rôle des différents composants
  (consultez la page wikipedia) ?  

- Faites un diagramme de classes du code fourni (avec les relations) et
  indiquez à quel composant MVC correspond chaque classe.

## Opérateur de sortie de l'inventaire 

- Implémentez l'opérateur de flux de sortie pour `Inventaire`, en vous
  inspirant du fichier `mesBouteilles.txt`.

- Testez-le avec le test unitaire `TestInventaire_1`.

## Affichage de l'inventaire  

- Dans le contrôleur, ajoutez une méthode `getTexte` qui retourne le texte
  correspondant à l'inventaire. 

- Dans le constructeur du contrôleur, ajoutez une bouteille de test à
  l'inventaire.

- Modifiez la vue pour qu'elle affiche l'inventaire au lieu du texte de test
  initial. 

- Exécutez votre programme et vérifiez que votre bouteille de test est bien
  affichée.

## Interface texte 

- Dérivez une classe `VueConsole` de la classe `Vue`.  Cette classe doit
  simplement permettre d'afficher le texte de l'inventaire dans la sortie
  standard.

- Modifiez le contrôleur pour qu'il utilise une vue console en plus de la vue
  graphique. Attention, le lancement de la vue graphique est bloquant donc
  arrangez-vous pour que la vue console soit lancée avant.

## Chargement de l'inventaire  

- Dans le contrôleur, ajoutez une méthode `chargerInventaire`. À terme, cette
  méthode permettra de charger un inventaire à partir d'un fichier de nom
  donné. Pour l'instant, cette méthode doit juste ajouter une bouteille de test
  supplémentaire dans l'inventaire et demander de mettre à jour les vues.

- Dans le constructeur du contrôleur, remplacez l'ajout de la bouteille de test
  par un appel à `chargerInventaire`.

- Testez.

## Interface graphique 

- Ajoutez un bouton en bas de votre interface graphique et connectez-le à la
  méthode `ouvrirFichier`. 

- Modifiez la méthode `ouvrirFichier` de telle sorte qu'elle demande de charger
  l'inventaire à partir du fichier sélectionné.

- Dans le constructeur de contrôleur, enlevez le chargement de l'inventaire que
  vous aviez mis pour tester.

- Testez.

