---
title: CM C++, Environnement de développement
date: 2019-04-19
---

## Découpage du code source en modules

- déclarations dans les .hpp, implémentations dans les .cpp
- avantages : lisibilité/réutilisation de code, compilation séparée

* * *

```cpp
// Doubler.hpp 
#ifndef DOUBLER_HPP_
#define DOUBLER_HPP_

int doubler(int x);

float doubler(float x);

#endif
```

* * *

```cpp
// Doubler.cpp 
#include "Doubler.hpp"

int doubler(int x) {
    return 2*x;
}

float doubler(float x) {
    return 2.f*x;
}
```

* * *

```cpp
// main_doubler21.cpp
#include "Doubler.hpp"

#include <iostream>

int main() {
    std::cout << doubler(21) << std::endl;
    std::cout << doubler(21.f) << std::endl;
    return 0;
}
```

## Compilation avec g++

- syntaxe basique : `g++ <fichiers cpp>`
- quelques options :
    - `-o <nom fichier>` : spécifie le fichier de sortie
    - `-std=c++14` : norme C++14
    - `-Wall -Wextra` : messages d’avertissement
    - `-g` : symboles de débogage
    - `-O2` : optimisation du code
    - `-I -L -l` ...

* * *

```text
$ g++ main_doubler21.cpp Doubler.cpp

$ ./a.out
42

$ g++ -o main_doubler21.out main_doubler21.cpp Doubler.cpp

$ ./main_doubler21.out
42

$ g++ -std=c++14 -Wall -Wextra -g -o main_doubler21.out \
  main_doubler21.cpp Doubler.cpp

$
```

## Compilation séparée

- principe : compiler chaque .cpp en .o puis édition de lien
- avantage : éviter de recompiler les .cpp non modifiés

![](dot/compilation.svg){width="50%"}

* * *

```text
$ g++ -c Doubler.cpp

$ g++ -c main_doubler21.cpp

$ g++ Doubler.o main_doubler21.o

$ ./a.out
42

$ ... # modification de main_doubler21.cpp

$ g++ -c main_doubler21.cpp

$ g++ Doubler.o main_doubler21.o
```

## Compilation avec cmake

- utilitaire de compilation classique 
- permet d’automatiser la compilation séparée
- permet de gérer des dépendances

```cmake
# CMakeLists.txt
cmake_minimum_required( VERSION 3.0 )
project( MON_PROJET )

add_executable( main_doubler21.out 
    main_doubler21.cpp Doubler.cpp )
```

* * *

- compilation :

```text
$ mkdir build

$ cd build

[build]$ cmake ..
-- The C compiler identification is GNU 4.9.2
-- The CXX compiler identification is GNU 4.9.2
...

[build]$ make
Scanning dependencies of target main_doubler21.out
[ 33%] Building CXX object CMakeFiles/main_doubler21.out.dir/main_doubler21.cpp.o
[ 66%] Building CXX object CMakeFiles/main_doubler21.out.dir/Doubler.cpp.o
[100%] Linking CXX executable main_doubler21.out
[100%] Built target main_doubler21.out

[build]$ ./main_doubler21.out 
42
```

## Édition de code

- besoins : écrire/lire/modifier du code, compiler, déboguer...
- fonctionnalités : coloration syntaxique, complétion, navigation, refactoring...
- outils : IDE (eclipse, visual studio...), éditeurs (emacs, vim...)

* * *

> **Important :**
> 
> Pour les TP, lancez qtcreator et ouvrez un projet en sélectionnant son
> fichier `CMakeLists.txt`. Pour créer un nouveau projet, partez d'une
> copie d'un projet existant (par exemple `TP_vide`).

* * *

![](files/qtcreator.png)

## Débogage avec qtcreator

- intérêt : suivre l’exécution du programme dynamiquement
- mise en oeuvre : compiler en debug, lancer le debugger
- fonctionnalités du debugger:
    - placer un point d’arrêt
    - lancer l’exécution
    - afficher la pile d’appels
    - changer de position dans la pile d’appels
    - afficher la valeur d’une variable
    - aller à l’instruction suivante
    - aller dans l’instruction courante
    - continuer l’exécution jusqu'au prochain point d'arrêt
    - ...

* * *

![](files/qtcreator_debug.png)

## Détection de fuites mémoires avec valgrind

- analyse dynamique de code (à l’exécution)
- permet de détecter la mémoire non libérée, les accès invalides...
- permet également de profiler le code
- penser à utiliser un analyseur statique avant (e.g. cppcheck)

* * *

```cpp
// toto.cpp 
int main() {

    int * tableau = new int [42];

    tableau[0] = 0;

    delete tableau;
    // erreur : il faudrait "delete [] tableau"

    return 0;
}
```

* * *

```text
$ g++ -Wall -Wextra -g toto.cpp

$ valgrind ./a.out
...
==27162== Mismatched free() / delete / delete []
...
==27162== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
```

```text
$ cppcheck --enable=all --inconclusive toto.cpp
Checking toto.cpp...
[toto.cpp:4]: (error) Mismatching allocation and deallocation: tableau
Checking usage of global functions..
```

## Tests unitaires

- principe :
    - un test unitaire permet de vérifier automatiquement qu’une fonction donnée appelée avec un paramètre donné produit bien le résultat attendu.
- utilisation :
    - écrire différents tests unitaires pour tester différentes fonctions et différents paramètres
    - écrire un programme pour lancer tous les tests unitaires.

## Tests unitaires (exemple avec cpputest)

```cpp
// Doubler_test.cpp
#include "Doubler.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupDoubler) { };

TEST(GroupDoubler, test_doubler_1) {  // premier test
    int result = doubler(21);
    CHECK_EQUAL(42, result);
}

TEST(GroupDoubler, test_doubler_2) {  // deuxième test
    int result = doubler(0);
    CHECK_EQUAL(0, result);
}
```

* * *

```cpp
// main_test.cpp
#include <CppUTest/CommandLineTestRunner.h>

int main(int argc, char ** argv) {
   return CommandLineTestRunner::RunAllTests(argc, argv);
}
```

* * *

```cmake
# CMakeLists.txt
cmake_minimum_required( VERSION 3.0 )
project( MON_PROJET )

# programme principal
add_executable( main_doubler21.out 
    main_doubler21.cpp Doubler.cpp )

# dépendances
find_package( PkgConfig REQUIRED )
pkg_check_modules( PKG_CPPUTEST REQUIRED cpputest )
include_directories( ${PKG_CPPUTEST_INCLUDE_DIRS} )

# programme de tests unitaires
add_executable( main_test.out 
    main_test.cpp Doubler.cpp Doubler_test.cpp )
target_link_libraries( main_test.out 
    ${PKG_CPPUTEST_LIBRARIES} )
```

* * *

```text
$ make
...

$ ./main_test.out 
..
OK (2 tests, 2 ran, 2 checks, 0 ignored, 0 filtered out, 0 ms)
```

## Quelques conseils pour coder efficacement

- comprenez ce que vous écrivez
- soignez votre code (noms, conventions de code, indentation...)
- testez régulièrement (compilation, exécution, tests unitaires)
- compilez en `-Wall` `-Wextra` et ne laissez passer aucun warning
- si un algo n’est pas trivial, commencez par réfléchir sur un papier puis écrivez le pseudo-code en commentaire puis traduisez en code
- préférez le débogueur aux affichages temporaires


