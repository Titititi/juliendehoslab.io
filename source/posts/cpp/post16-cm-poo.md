---
title: CM C++, Programmation orientée objet
date: 2019-04-19
---

## Notion de classe

- permet de représenter un concept (abstraction)
- explicite les fonctionnalités fournies tout en masquant le fonctionnement interne (encapsulation)
- trois étapes : déclaration, implémentation, instanciation
- une classe définit également un type
- **ne pas oublier le point-virgule à la fin de la déclaration**

![](uml/personne.svg){width="40%"}

## Déclaration de classe

```cpp
// Personne.hpp
#ifndef PERSONNE_HPP_
#define PERSONNE_HPP_

#include <string>

class Personne {
    private:
        std::string _nom;
        int _age;
    public:
        Personne(const std::string & nom, int age);
        void afficherInfos() const;
};

#endif
```

## Implémentation de classe

```cpp
// Personne.cpp
#include "Personne.hpp"

#include <iostream>

Personne::Personne(const std::string & nom, int age) :
    _nom(nom), _age(age)
{}
void Personne::afficherInfos() const {
    std::cout << _nom << " a " 
              << _age << " ans" << std::endl;
}
```

## Instanciation de classe

```cpp
// main_toto.cpp 
#include "Personne.hpp"

int main() {
    Personne toto("Toto", 42);
    toto.afficherInfos();
    return 0;
}
```

```text
Toto a 42 ans
```

## Accès à un membre

- depuis le même espace de noms :

```cpp
// Personne.cpp
void Personne::afficherInfos() const {

    // accès direct (à privilégier)
    std::cout << _nom;

    // accès par le pointeur this (à éviter)
    std::cout << this->_nom;

    // opérateur de résolution de portée
    std::cout << Personne::_nom;
}
```

* * *

- depuis l’extérieur :

```cpp
// main_toto.cpp
int main() {

    Personne toto("Toto", 42);

    toto.afficherInfos(); // accès par l'objet

    Personne * ptrToto = &toto;
    ptrToto->afficherInfos(); // accès par un pointeur
    (*ptrToto).afficherInfos(); // autre écriture possible

    return 0;
}
```

## Droits d’accès

- contrôlent l’accès aux attributs et méthodes (à la compilation)
- 3 types de droits :
    - `private` : droit d’accès aux membres de la classe uniquement (droit par défaut)
    - `protected` : droit d’accès aux membres de la classe et aux membres des classes dérivées
    - `public` : droit d’accès à tout le monde (membres de classe, membres des classes dérivées, extérieur) private

* * *

- bonne pratique : restreindre au maximum les droits (si besoin, utiliser des accesseurs/modificateurs)
- en C++, la structure ( struct ) est une classe dont tous les membres sont `public`
- autoriser l’accès pour une fonction/classe donnée : `friend` (à éviter)

* * *

```cpp
void Personne::afficherInfos() const {
    std::cout << _nom;  
    // ok : _nom est privé mais on est 
    // dans le même espace de noms
}

int main() {
    Personne toto("Toto", 42);

    toto.afficherInfos(); 
    // ok : afficherInfos est public

    toto._nom = "Tata"; 
    // erreur : _nom est privé et on est dans 
    // un autre espace de noms

    return 0;
}
```

## Accesseur/modificateur (« getter/setter »)

```cpp
// Personne.hpp

class Personne {
    // ...
    public:
        int getAge() const; // accesseur
        void setAge(int age); // modificateur

        const std::string & getNom() const; // accesseur
        void setNom(const std::string & nom); // modificateur
};
```

* * *

```cpp
// Personne.cpp

int Personne::getAge() const {
    return _age;
}

void Personne::setAge(int age) {
    _age = age;
}

const std::string & Personne::getNom() const {
    return _nom;
}

void Personne::setNom(const std::string & nom) {
    _nom = nom;
}
```

* * *

```cpp
// main_toto.cpp

int main() {
    Personne toto("Toto", 42);

    // appelle le modificateur
    toto.setNom("TOTO");

    // appelle l'accesseur
    std::cout << toto.getNom() << std::endl;

    return 0;
}
```

## Accesseur/modificateur (« références »)

```cpp
// Personne.hpp

class Personne {
    // ...
    public:
        int age() const; // accesseur
        int & age(); // modificateur

        const std::string & nom() const; // accesseur
        std::string & nom(); // modificateur
};
```

* * *

```cpp
// Personne.cpp

int Personne::age() const {
    return _age;
}

int & Personne::age() {
    return _age; 
}

const std::string & Personne::nom() const {
    return _nom; 
}

std::string & Personne::nom() {
    return _nom; 
}
```

* * *

```cpp
// main_toto.cpp

int main() {
    Personne toto("Toto", 42);

    // appelle le modificateur
    toto.nom() = "TOTO";

    // appelle l'accesseur
    std::cout << toto.nom() << std::endl;

    return 0;
}
```

## Remarque sur les accesseurs/modificateurs

- permettent de contrôler les accès/modifications des attributs
- mais nuisent au principe d’encapsulation
- proscrire le réflexe « attribut $\Rightarrow$ accesseur + modificateur »

## Constructeur

- méthode particulière : même nom que la classe, pas de retour
- appelée à l’instanciation pour initialiser l’objet
- la liste d’initialisation permet de construire les attributs
- dans le corps du constructeur, les attributs sont déjà construits
- une classe peut définir plusieurs constructeurs (surdéfinition)

* * *

```cpp
// constructeur à 2 paramètres -> version PAS BIEN
// _nom est construit automatiquement (et initialisé à "")
// puis _nom reçoit la valeur de nom (2e initialisation)
Personne::Personne(const std::string & nom, int age) {
    _nom = nom;
    _age = age;
}
```

```cpp
// constructeur à 2 paramètres -> version BIEN
// _nom est construit+initialisé dans la liste
// d'initialisation
Personne::Personne(const std::string & nom, int age) :
    _nom(nom), _age(age)
{}
```

## Surdéfinition de méthodes

- idem surdéfinition de fonctions mais pour des méthodes
- souvent utilisé pour les constructeurs

* * *

```cpp
// Personne.hpp

class Personne {
    // ...
    Personne();
    Personne(const std::string & nom, int age);
};
```

```cpp
// main_toto.cpp

int main() {
    Personne inconnu;
    Personne toto("Toto", 42);
    return 0;
}
```

## Destructeur

- méthode particulière : nom de la classe précédé d'un tilde (~), pas de valeur de retour ni de paramètre
- appelée automatiquement à la destruction de l’objet
- utile pour libérer les ressources dynamiques de l’objet (notamment les allocations mémoires)

* * *

```cpp
// Personne.hpp

class Personne {
    // ...
    public:
        ~Personne();
};
```

```cpp
// Personne.cpp

Personne::~Personne() {} // rien de particulier à 
                         // faire dans cet exemple
```

* * *

```cpp
// main.cpp

int main() {

    Personne toto("Toto", 42);

    return 0;

} // fin de portée de l'objet toto 
  // -> appelle automatiquement le destructeur
```

## Héritage de classes

- création d’une nouvelle classe à partir d’une classe existante
- la classe dérivée possède les caractéristiques de la classe initiale
- et en ajoute de nouvelles
- le C++ permet de l’héritage multiple (mais bon...)

![](uml/objet3d_1.svg){width="60%"}

## Classe mère

```cpp
// Objet3d.hpp 
#ifndef OBJET3D_HPP_
#define OBJET3D_HPP_

class Objet3d {
    private:
        float _densite;
    public:
        Objet3d(float densite);
        float getDensite() const;
};

#endif
```

* * *

```cpp
// Objet3d.cpp
#include "Objet3d.hpp"

Objet3d::Objet3d(float densite) :
    _densite(densite)
{}

float Objet3d::getDensite() const {
    return _densite;
}
```

## Classe dérivée

```cpp
// Cube.hpp 
#ifndef CUBE_HPP_
#define CUBE_HPP_

#include "Objet3d.hpp"

class Cube : public Objet3d {  // dérivation
    private:
        float _cote;
    public:
        Cube(float densite, float cote);
        float volume() const;
};

#endif
```

* * *

```cpp
// Cube.cpp 
#include "Cube.hpp"

Cube::Cube(float densite, float cote) :
    Objet3d(densite), _cote(cote)
    // appel au constructeur de la classe mère
{}

float Cube::volume() const {
    return _cote*_cote*_cote;
}
```

## Utilisation d’une classe dérivée

```cpp
// main_cube.cpp 
#include "Cube.hpp"

#include <iostream>

int main() {
    Cube c(1, 2);

    std::cout << c.getDensite() << std::endl; 
    // méthode héritée de la classe mère

    std::cout << c.volume() << std::endl; 
    // méthode de la classe dérivée

    return 0;
}
```

## Droits d’accès d’un héritage

- permettent de contrôler comment la classe fille peut accéder aux membres de la classe mère
- 3 types d’héritage : `public`, `protected` et `private`
- par défaut héritage privé mais en pratique souvent héritage public

* * *

| membre\\héritage   | public        | protected     | private     |
|--------------------|---------------|---------------|-------------|
| **public**         | public        | protected     | private 
| **protected**      | protected     | protected     | private 
| **private**        | pas d'accès   | pas d'accès   | pas d'accès

* * *

![](uml/objet3d_2.svg){width="60%"}

```cpp
float Cube::masse() const {
    return _densite * volume(); 
    // erreur : Objet3d::_densite est privé
}
```

```cpp
float Cube::masse() const {
    return getDensite() * volume(); 
    // ok : Objet3d::getDensite() est public
}
// autre possibilité : 
// déclarer Objet3d::_densite en protected
```

## Héritage et construction/destruction

- un constructeur de la classe mère est appelé, **avant** le constructeur de la classe fille :
    - soit par appel explicite dans la liste d’initialisation
    - soit par appel implicite du constructeur sans paramètre
- le destructeur de la classe mère est appelé automatiquement, **après** le destructeur de la classe fille

* * *

```cpp
class Objet3d {
    public:
        Objet3d() {
            std::cout << "Objet3d()" << std::endl;
        }
        ~Objet3d() {
            std::cout << "~Objet3d()" << std::endl;
        }
};

class Cube : public Objet3d {
    public:
        Cube() {    // implicitement Cube() : Objet3d()
            std::cout << "Cube()" << std::endl;
        }
        ~Cube() {
            std::cout << "~Cube()" << std::endl;
        }
};
```

* * *

```cpp
int main() {

    // construit un Cube : appelle Objet3d() puis Cube()
    Cube c; 

    return 0;

} // destruction du Cube : appelle ~Cube() puis ~Objet3d()
```

```text
Objet3d()
Cube()
~Cube()
~Objet3d()
```

## Héritage et typage

- un objet de la classe fille est également un objet de la classe mère (mais la réciproque est fausse)

* * *

```cpp
Objet3d objet(1);
objet.getDensite();  // ok (méthode de Objet3d)
objet.volume();      // erreur (méthode de Cube)

Cube cube(1, 2);
cube.getDensite();   // ok (méthode héritée de Objet3d)
cube.volume();       // ok (méthode de Cube)

Objet3d * pObjet;
pObjet = &objet;     // ok (pointe vers un Objet3d)
pObjet = &cube;      // ok (pointe vers un Cube, 
                     //     qui est aussi un Objet3d)

Cube * pCube;
pCube = &objet;      // erreur (pointe vers un Objet3d, 
                     //         qui n'est pas un Cube)
pCube = &cube;       // ok (pointe vers un Cube)
```

## Surdéfinition de méthode dérivée

- une classe fille peut redéfinir une méthode de la classe mère (même signature)
- mot-clé `override`
- la méthode à appeler est déterminée d’après le typage

* * *

![](uml/objet3d_3.svg){width="50%"}

```cpp
class Objet3d {
    // ...
    float volume() const;
};
```

```cpp
class Cube : public Objet3d {
    // ...
    float volume() const override;
};
```

* * *

```cpp
float Objet3d::volume() const {
    return 0;
}
```

```cpp
float Cube::volume() const {
    return _cote*_cote*_cote;
}
```

* * *

```cpp
Objet3d objet(1);
objet.volume(); // appelle Objet3d::volume()

Cube cube(1, 2);
cube.volume(); // appelle Cube::volume()
cube.Objet3d::volume(); // appelle Objet3d::volume()

Objet3d * ptrObjet = &cube;
ptrObjet->volume(); // appelle Objet3d::volume()

Cube * ptrCube = &cube;
ptrCube->volume(); // appelle Cube::volume()
ptrCube->Objet3d::volume(); // appelle Objet3d::volume()
```

## Méthode virtuelle

- un pointeur (ou une référence) référence un objet de la classe spécifiée **ou de ses classes dérivées**
- par défaut, c’est la méthode correspondant au type du pointeur qui est appelée (liaison statique)
- si la méthode est virtuelle, c’est la méthode de l’objet pointé qui est appelée (liaison dynamique)
- mot-clé `virtual`

* * *

![](uml/objet3d_4.svg){width="70%"}

```cpp
class Objet3d {
    // ...
    virtual float volume() const;
};
```

```cpp
class Cube : public Objet3d {
    // ...
    float volume() const override;
};
```

* * *

```cpp
float Objet3d::volume() const {
    return 0;
}
```

```cpp
float Cube::volume() const {
    return _cote*_cote*_cote;
}
```

* * *

```cpp
Objet3d objet(1);
Cube cube(1, 2);
Objet3d * ptrObjet;

ptrObjet = &objet;
    // le pointeur pointe sur un Objet3d
ptrObjet->volume();
    // appelle la méthode Objet3d::volume() de objet

ptrObjet = &cube;
    // le pointeur pointe sur un Cube
ptrObjet->volume();
    // appelle la méthode Cube::volume() de cube
```

## Méthode virtuelle pure

- méthode sans implémentation, à surdéfinir dans les classes dérivées
- rend la classe abstraite (non instanciable)

![](uml/objet3d_5.svg){width="70%"}

```cpp
class Objet3d {
    // ...
    virtual float volume() const = 0;
};
```

## Destructeur virtuel

- par défaut, si un objet est détruit à partir d’un pointeur, c’est le destructeur du type du pointeur qui est appelé
- problème : si l’objet pointé est d’une classe dérivée, son destructeur n’est pas appelé
- solution : déclarer virtuel le destructeur de la classe mère
- en pratique : faire attention si polymorphisme et destruction non-triviale

## Destructeur virtuel et polymorphisme

![](uml/objet3d_6.svg){width="70%"}

* * *

```cpp
// tableau d'objets 3D polymorphes alloués dynamiquement
std::vector<Objet3d*> objets {
    new Cube(1, 2), 
    new Sphere(1, 3)
};

// affiche le volume des objets 3D
for (Objet3d * ptrObjet : objets)
    std::cout << ptrObjet->volume() << std::endl;

// libère la mémoire
for (Objet3d * ptrObjet : objets)
    delete ptrObjet; 
    // ~Objet3d() est virtuel donc appelle bien les 
    // destructeurs des classes dérivées
```

