---
title: CM C++, Gestion mémoire
date: 2019-04-19
---

## La gestion mémoire en C++

- plusieurs possibilités :
    #. variables statiques
    #. classes de la bibliothèque standard (std::vector...)
    #. allocation dynamique avec pointeurs intelligents
    #. allocation dynamique manuelle
- au choix du programmeur $\rightarrow$ toujours faire au plus simple

## Portée d’une variable statique

- une variable est accessible à partir de sa déclaration et jusqu’à la fin du bloc
- à la fin du bloc, la mémoire locale au bloc est libérée

```cpp
int main() {
    int x;

    // nouveau bloc
    {
        int y;  
    } 
    // fin du bloc : y est libéré

    return 0;
} 
// fin du main : x est libéré
```

## Passage d’objets en paramètre

- par défaut, les paramètres de fonction sont passés par copie
- dans le cas d’un objet, cela implique tout le mécanisme d’instanciation (constructeur par copie, destructeur)
- si classe complexe (espace mémoire, héritage), optimiser en passant plutôt une référence constante

```cpp
void fonctionBof(MaClasseCompliquee c);
// construit une copie

void fonctionBien(const MaClasseCompliquee & c);
// utilise une reference
```

## Allocation dynamique

- principe : gérer de la mémoire dynamiquement et explicitement
- mise en oeuvre : opérateurs `new` et `delete`, pointeurs
- cas d’utilisation :
    - structure de données complexe et de taille dynamique (arbres...)
    - polymorphisme

* * *

```cpp
int main() {

    int * n = new int; // alloue dynamiquement un int
    *n = 2;
    delete n; // libère la mémoire : la zone mémoire pointée 
              // par n n'est donc plus valide

    int * t = new int [42]; // alloue dynamiquement un
                            // tableau de 42 éléments
    t[0] = 2;
    delete [] t; // ne pas oublier [] pour libérer un tableau

    return 0;
}
```

## Exemple d’arbre 

```cpp
struct Noeud {
    int _valeur;
    Noeud * _gauche;
    Noeud * _droit;

    Noeud(int valeur) : 
        _valeur(valeur), _gauche(nullptr), _droit(nullptr) {}

    // destruction récursive de l'arbre avec 
    // les libérations mémoires nécessaires
    ~Noeud() {
        if (_gauche) delete _gauche;
        if (_droit) delete _droit;
    }
};
```

* * *

```cpp
class Abr {
    private:
        Noeud * _racine;

    public:
        Abr() : _racine(nullptr) {}

        ~Abr() { if (_racine) delete _racine; }
        // lance la destruction récursive

        bool trouver(int valeur) {
            Noeud * n = _racine;
            while (n) {
                if (n->_valeur > valeur) n = n->_droit;
                else if (n->_valeur < valeur) n = n->_gauche;
                else return true; // valeur trouvée
            }
            return false; // valeur non trouvée
        }

        void ajouter(int valeur) {
            Noeud ** p = &_racine;
            while (*p) {
                if ((*p)->_valeur > valeur)
                    p = &((*p)->_droit);
                else if ((*p)->_valeur < valeur)
                    p = &((*p)->_gauche);
                else return; // valeur trouvée => termine 
                             // sans ré-ajouter la valeur
            }
            *p = new Noeud (valeur); // ajoute la valeur
        }
};
```

* * *

```cpp
#include <iostream>

int main() {

    Abr abr;
    abr.ajouter(12); // allocation dynamique d'un noeud
    abr.ajouter(4);  // idem

    std::cout << abr.trouver(12) << std::endl;
    std::cout << abr.trouver(4) << std::endl;
    std::cout << abr.trouver(2) << std::endl;
    std::cout << abr.trouver(6) << std::endl;
    std::cout << abr.trouver(13) << std::endl;

    return 0;
}
// destruction de l'arbre => libération des noeuds
```

## Exemple de polymorphisme

```cpp
#include <iostream>
#include <vector>

struct Objet3d {
    virtual float volume() const = 0;
    virtual ~Objet3d() {}
};

struct Cube : public Objet3d {
    float volume() const override { return 13; }
};

struct Sphere : public Objet3d {
    float volume() const override { return 37; }
};

```

* * *

```cpp
int main() {
    // tableau d'objets 3D polymorphes alloués dynamiquement
    std::vector<Objet3d*> objets {new Cube, new Sphere};

    // affiche le volume des objets 3D
    for (Objet3d * ptrObjet : objets)
        std::cout << ptrObjet->volume() << std::endl;

    // libère la mémoire
    for (Objet3d * ptrObjet : objets)
        delete ptrObjet;

    return 0;
}
```


## Fuites mémoires

- à chaque `new` doit correspondre **un** `delete`
- si on oublie un delete, on a une fuite mémoire (zone mémoire toujours allouée mais plus accessible par aucun pointeur)
- la mémoire est récupérée par l’OS à la fin du programme mais risque de saturation mémoire, inefficacité

* * *

```cpp
int main() {
    // tableau d'objets 3D polymorphes alloués dynamiquement
    std::vector<Objet3d*> objets {new Cube, new Sphere};
    return 0;
}
// objets est supprimé mais pas les zones mémoires allouées
```

```text
$ valgrind ./a.out
...
==11144==
==11144== LEAK SUMMARY:
==11144==
definitely lost: 16 bytes in 2 blocks
...
```

## Pointeurs intelligents

- permettent d’automatiser la gestion mémoire
- bibliothèque memory :
    - `std::unique_ptr` : pointeur vers une zone unique, avec libération mémoire en fin de portée
    - `std::shared_ptr` : pointeur vers une zone partagée, avec libération mémoire automatique (implique un surcoût de gestion)
    - ...

## Pointeurs intelligents

- exemple 1 :

```cpp
    Objet3d * ptr = new Cube;
    // ...
    delete ptr; // libération du cube

    std::unique_ptr<Objet3d> uptr(new Cube);
    // ...
    // libération automatique du cube

    std::unique_ptr<Objet3d> uptr = std::make_unique<Cube>(); 
    // ...
    // libération automatique du cube
```

* * *

- exemple 2 :

```cpp
int main() {

    // tableau d'objets 3D polymorphes via 
    // des pointeurs intelligents
    std::vector<std::unique_ptr<Objet3d>> objets;
    objets.push_back(std::make_unique<Cube>());
    objets.push_back(std::make_unique<Sphere>());

    for (const auto & uptrObjet : objets)
        std::cout << uptrObjet->volume() << std::endl;

    return 0;
} 
// objets est supprimé ainsi que les zones mémoires allouées
```

## Allocation dynamique et attributs de classe

- on peut avoir besoin d’allouer des attributs dynamiquement (polymorphisme, structures de données complexes)
- l’implémentation de la classe doit s’assurer de la cohérence de la gestion mémoire (encapsulation) :
    - constructeur
    - destructeur (virtuel en cas d’héritage)
    - constructeur par copie
    - opérateur de copie
    - [Rule of three](https://en.wikipedia.org/wiki/Rule_of_three_%28C%2B%2B_programming%29)
    - il existe également une notion de déplacement `std::move` mais pas abordée dans ce cours

## Constructeur par copie

- construit un objet à partir d’un autre objet de la même classe
- par défaut : copie membre à membre
- problème : en cas d’allocation dynamique, on copie le pointeur mais pas la zone mémoire pointée
- solution : définir explicitement un constructeur par copie

## Opérateur de copie

- idem constructeur par copie mais pour un objet déjà existant
- problèmes supplémentaires : autoréférencement, chaînage

* * *

```cpp
class Tableau {
    private:
        int _taille;
        int * _donnees;

    public:
        Tableau(int taille);
        ~Tableau();

        // constructeur par copie
        Tableau(const Tableau & t);

        // opérateur de copie
        const Tableau & operator=(const Tableau & t);

        ...
```

* * *

```cpp
#include <cstring>

Tableau::Tableau(int taille) {
    _taille = taille;
    _donnees = new int [_taille];  // allocation mémoire
}

Tableau::~Tableau() {
    delete [] _donnees;  // libération mémoire
}
```

* * *

```cpp
// constructeur par copie
Tableau::Tableau(const Tableau & t) {
    _taille = t._taille;
    _donnees = new int [_taille];

    // copie les données
    std::memcpy(_donnees, t._donnees, _taille*sizeof(int));
}
```

* * *

```cpp
// opérateur de copie
const Tableau & Tableau::operator=(const Tableau & t) {
    if (this != &t) { // pour l'autoréférencement
        delete [] _donnees;
        _taille = t._taille;
        _donnees = new int [_taille];
        std::memcpy(_donnees, t._donnees, 
                    _taille*sizeof(int));
    }
    return *this; // pour le chaînage
}
```

* * *

```cpp
int main() {

    Tableau t1(42);
    Tableau t2(t1);  // constructeur par copie
    Tableau t3 = t1; // constructeur par copie

    t3 = t2;      // opérateur de copie
    t3 = t3;      // autoréférencement
    t3 = t2 = t1; // chaînage

    return 0;
} 
// destructions avec libérations mémoires
```

