---
title: TP9 C++, Conteneurs, itérateurs et algorithmes
date: 2019-04-19
---

On veut finaliser le logiciel de gestion de bouteilles du TP précédent
(chargement d'un fichier d'inventaire, calcul du stock).

## Mise en place du projet

- Reprenez le projet du TP précédent.
 
- Vérifiez que vous avez bien le diagramme de classes suivant.

* * *

![](uml/TP9a.svg){width="80%"}

## Lecture de fichiers 

- Implémentez l'opérateur de flux d'entrée pour `Inventaire`, en vous inspirant
  du fichier `mesBouteilles.txt`.

- Testez-le avec le test unitaire `TestInventaire_2`.

- Modifiez votre méthode `Controleur::chargerInventaire` pour charger le
  fichier demandé.

- Exécutez le programme et vérifiez que le fichier `mesBouteilles.txt` est
  correctement chargé.

## Itérateurs 

- Vérifiez que l'opérateur de flux de sortie de votre inventaire utilise bien
  une boucle `for` classique (avec un indice de boucle, comme en C) pour
  parcourir l'inventaire.

- Modifiez votre inventaire de manière à utiliser un `std::list` au lieu d'un
  `std::vector`. Que se passe-t-il ? Pourquoi ?

- Modifiez votre opérateur de flux de sortie de façon à utiliser des itérateurs
  et vérifiez que ça va mieux.

- Idem avec une boucle "range-based".

## Lambdas et algorithmes 

- En utilisant l'algorithme `std::sort`, implémentez une méthode
  `Inventaire::trier` pour trier les bouteilles selon leur nom.

- Modifiez votre méthode `Controleur::getTexte` pour afficher également
  l'inventaire trié. Testez.

- Réimplémentez le parcours de la section précédente en utilisant l'algorithme
  `std::for_each`. Indication : capture de variable.

## Conteneur associatif 

- Implémentez la classe `Stock` suivante. Un stock donne le volume total de
  chaque produit.  Par exemple, si j'ai deux bouteilles d'un litre de
  Lysergsäurediethylamid, j'ai un stock de 2l de ce produit.

![](uml/TP9b.svg){width="50%"}

* * *

- Implémentez un opérateur de flux de sortie pour votre `Stock`.  Quelles sont
  les types de parcours possibles ?

- Testez votre classe avec des tests unitaires (inspirez-vous des tests
  unitaires déjà existants).

- Modifiez votre méthode `Controleur::getTexte` pour afficher également le
  stock.

- Testez avec `mesBouteilles.txt`.

