---
title: TP C++, Complexité algorithmique
date: 2019-10-14
---

L'objectif de ce TP est de s'intéresser à la complexité algorithmique de
quelques codes C++.

Le dossier `TP_complexite` fournit du code de base avec `Makefile` et script de
lancement. Pour compiler, lancez simplement `make` dans ce dossier.


## Recherche dans un tableau trié

- Regardez le code `search.cpp`, compilez-le et exécutez-le.

* * *

```text
$ make

...

$ ./search.out 

usage: ./search.out <n0> <n1> <step> <naive|optim> <seed> <nbruns>

$ ./search.out 10 300 20 naive 0 2000000

10	0.0757014	0.499559
30	0.0811769	0.499202
50	0.0871734	0.500004
70	0.0934324	0.500483
90	0.0992639	0.499957
...
```

* * *

- Regardez le script `run-search.sh`, exécutez-le et regardez les fichiers produits.

- Comment évolue le temps d'exécution de l'algorithme de recherche en fonction de la taille des données ?

- Réimplémentez la fonction `optimSearch` de façon plus efficace. Vous devriez obtenir des graphiques semblables aux suivants.

* * *

![Recherches naïve et optimisée dans un tableau trié (temps d'exécution en seconde, en fonction de la taille du tableau.](files/cpp-tp-complexite-search.svg){width="80%"}


## Suite de Fibonacci

- Regardez le code `fibo.cpp` et exécutez-le. 

* * *

```text
$ ./fibo.out 

usage: ./fibo.out <n0> <n1> <step> <naive|optim>

$ ./fibo.out 0 10 1 naive

0	0	0
1	0	1
2	1	1
3	2	2
4	4	3
5	7	5
6	12	8
7	20	13
8	33	21
9	54	34
10	88	55
```

* * *

- Quel est le critère de mesure ici ?

- En vous inspirant de l'exercice précédent, écrivez un script permettant de tracer l'évolution du coût de calculs.

- Implémentez une version plus efficace de l'algorithme. Vérifiez les graphiques générés.

* * *


![Nombre de calculs requis pour calculer le Xieme terme de la suite de Fibonacci.](files/cpp-tp-complexite-fibo.svg){width="80%"}


## Évaluation de polynomes

- On veut comparer deux méthodes d'évaluation de polynomes :

    1. avec un calcul direct des puissances (fonction `std::pow`)
    2. avec la méthode de [Ruffini-Horner](https://fr.wikipedia.org/wiki/M%C3%A9thode_de_Ruffini-Horner)


- Quelles sont les complexités attendues ?

- Écrivez un programme et un script de lancement, implémentant et comparant les deux méthodes sur des entrées générées aléatoirement. Inspirez-vous du fichier `polynome.cpp` fourni et du code de l'exercice sur la recherche dans un tableau trié.

* * *

![Temps d'exécution en fonction du degré du polynome.](files/cpp-tp-complexite-polynome.svg){width="80%"}


## Allocation mémoire

Le programme `allocation.cpp` compare le remplissage d'un tableau avec ou sans préallocation. Écrivez un programme mesurant l'évolution du temps d'exécution en fonction de la taille des données, avec ou sans préallocation.

```text
$ ./allocation.out 

with reserve: 0.220411
w/o reserve: 0.451948
```

* * *

![Temps d'exécution en fonction de la taille du tableau.](files/cpp-tp-complexite-allocation2.svg){width="80%"}


## Somme des entiers de 1 jusqu'à N

Écrivez un programme et un script de lancement, implémentant et comparant le calcul des entiers de 1 à N selon les algorithmes suivants:

- ``rec`` : algorithme récursif non-terminal
- ``tail`` : algorithme récursif terminal
- ``iter`` : algorithme itératif
- ``const`` : calcul direct

Testez sans (`-O0`) et avec (`-O2`) les optimisations du compilateur.

* * *

![Temps de calculs en fonction de N. Sans les optimisations du compilateur.](files/cpp-tp-complexite-sum-O0.svg){width="80%"}

* * *

![Temps de calculs en fonction de N. Avec les optimisations du compilateur.](files/cpp-tp-complexite-sum-O2.svg){width="80%"}



## S'il vous reste du temps

Reprenez le premier exercice et réimplementez les fonctions de recherche (linéaire et dichotomique). Vérifiez que les résultats sur des petites entrées et les performances sur des plus grandes entrées.

