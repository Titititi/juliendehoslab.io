---
title: TP10 C++, Itérateurs et templates
date: 2019-04-19
---

On veut implémenter une liste d'entiers (diagramme ci-dessous) puis en faire
une liste générique.

* * *

![](uml/TP10a.svg){width="90%"}

## Mise en place du projet

- Utilisez le projet `TP10` fourni. 

## Liste d'entiers  

- Implémentez la liste d'entiers en complétant le fichier `Liste.hpp`. Vérifier
  régulièrement avec les tests unitaires.

- Vérifiez votre gestion mémoire avec `valgrind` et `cppcheck`.

- Vérifiez que votre liste fonctionne pour les boucles "range-based".

## Liste générique 

- En réutilisant votre module `Liste`, implémentez un module `ListeGenerique`.

- Testez dans le `main` avec des listes de `int` et de `float`.

## Liste de personnes 

- Implémentez la structure `Personne` suivante.

![](uml/TP10b.svg){width="20%"}

- Dans le `main`, créez une liste de personnes.  Que se passe-t-il ?
  Implémentez ce qu'il faut pour corriger le problème.

