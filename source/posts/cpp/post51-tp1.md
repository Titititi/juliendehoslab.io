---
title: TP1 C++, Notions de base
date: 2019-04-19
---

> **Important :**
> 
> - Si ce n'est pas déjà fait, [configurez votre accès par proxy](../env/post10-proxy.html).
> - Connectez-vous à [gitlab](https://gitlab.com/users/sign_in).
> - Forkez le [dépôt fourni](https://gitlab.com/juliendehos/L3_CPP_etudiant).
> - Clonez votre dépôt.
> - Pensez à committer/pusher votre travail régulièrement <br>(à la fin de chaque
>   exercice, par exemple).

## Premier programme

- Ouvrez un terminal et créez un dossier `TP1`.

- Allez dans votre dossier et ouvrez un fichier `main.cpp` avec geany.

- Dans ce fichier, écrivez un programme principal qui affiche un message.

- Dans le terminal, compilez avec une commande g++ puis exécutez votre programme.

## Premier module

- Toujours dans votre dossier et toujours en utilisant geany, créez un module
  `Fibonacci`.

- Dans votre module, écrivez deux fonctions `fibonacciRecursif` et
  `fibonacciIteratif` qui retournent le nième terme de la suite de
  Fibonacci (avec un algorithme récursif et avec un algorithme itératif,
  respectivement).

- Modifiez votre programme principal pour afficher le 7e terme de la
  suite (avec les deux algorithmes).

- Dans un terminal, compilez avec **une** commande g++ puis exécutez votre programme.

## Compilation séparée 

- Dans un terminal, lancez les trois commandes g++ permettant de compiler votre
  code par compilation séparée.

- Quelle(s) commande(s) faut-il relancer si on modifie `main.cpp` ?

- En vous inspirant du cours, écrivez un `CMakeLists.txt` pour compiler votre
  code.

- Testez la compilation avec cmake et regardez les commandes g++ lancées.

- Testez la compilation dans qtcreator, notamment quand il y a des erreurs.

## Tests unitaires

- Toujours dans qtcreator, ajoutez un programme de tests unitaires dans votre
  dossier (fichier `main_test.cpp`).

- Dans un fichier `Fibonacci_test.cpp`, écrivez un test unitaire pour vérifier
  que `fibonacciRecursif` calcule correctement les cinq premiers termes.
  Compilez et exécutez votre programme de tests unitaires.

- De même, écrivez un deuxième test pour vérifier `fibonacciIteratif`.

## Structure

- Ajoutez un module `Vecteur3`. Que faut-il changer dans le `CMakeLists.txt` ?

- Dans ce module, écrivez une structure permettant de représenter un vecteur 3D
  de `float`. Dans le programme principal, créez un vecteur `(2, 3, 6)` et
  vérifiez que tout compile.

- Dans le module `Vecteur3`, écrivez une fonction `afficher` permettant
  d'afficher un vecteur (selon le format : `(2, 3, 6)`).  Testez-la dans le
  programme principal. Quelle est la meilleure façon de déclarer le paramètre de
  cette fonction ? 

* * *

- Écrivez une méthode équivalente à la fonction `afficher`.  Testez-la dans le
  programme principal.

- Écrivez une méthode `norme` qui calcule la norme du vecteur.  Testez dans le
  programme principal et avec un test unitaire.

- Idem pour une fonction `produitScalaire` (calcule le produit scalaire de deux
  vecteurs).

- Idem pour une fonction `addition` (calcule l'addition de deux vecteurs) .


## S'il vous reste du temps

- [TP Régression polynomiale](post82-tp-regression.html)

- [TP C++, Complexité algorithmique](post80-tp-complexite.html)

