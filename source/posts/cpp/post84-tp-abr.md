---
title: TP C++, Arbre binaire de recherche
date: 2020-11-17
notoc: notoc
---

- Dans le dossier `TP_abr`, implémentez un arbre binaire de recherche (ABR), en
  vous inspirant du diagramme de classes suivant. Écrivez un programme de test
  équivalent à `test-vector.cpp`.

* * *

![](uml/TP-abr.svg){width="60%"}

* * *

- Complétez les fichiers `main.cpp` et `run.sh`. Comparez les temps d'exécution
  des recherches.

* * *

![Temps d'exécution d'un million de recherches dans des `vector`.](files/cpp-tp-abr-vector.svg){width="80%"}

* * *

![Temps d'exécution d'un million de recherches dans des `Tree`.](files/cpp-tp-abr-tree.svg){width="80%"}

* * *

![Comparaison des temps d'exécution.](files/cpp-tp-abr-cmp.svg){width="80%"}

