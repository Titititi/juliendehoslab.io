---
title: TP3 C++, Classes et objets
date: 2019-04-19
---

## Mise en place du projet

- Copiez le projet de base : `cp -r TP_vide TP3`. 

## Modélisation

On veut développer une application de gestion de location de produits.  Un
`Magasin` possède des `Client` et des `Produit` ainsi qu'un ensemble de
`Location`.  Un client peut louer plusieurs produits mais un produit ne peut
être loué que par un seul client. Une `Location` associe un produit à un client
grâce à leur identifiant (nombre entier). En plus de créer et de contenir les
données, le `Magasin` attribue des identifiants uniques pour les clients et
pour les produits (auto-incrémentations à partir de 0). On propose la
modélisation suivante :

* * *

![](uml/TP3a.svg){width="90%"}

## Structure Location

- Écrivez un module implémentant la structure `Location`.  La méthode
  d'affichage doit produire le résultat suivant pour le client 0 et le produit
  2 :

```cpp
Location (0, 2)
```

- Comment pouvez-vous instancier et en même temps initialiser un objet de cette
  structure ?  Si ce n'est pas déjà fait, testez-le dans le `main` ainsi que
  la méthode d'affichage.

## Classes Client et Produit

- Écrivez un module implémentant la classe `Client`.  La méthode d'affichage
  doit produire le résultat suivant pour le client d'identifiant 42 et de nom
  "toto" :


```cpp
Client (42, toto)
```

- Testez l'affichage d'un client dans le `main`.

- Testez le constructeur et les accesseurs avec des tests unitaires.

- Mêmes questions pour la classe `Produit`.

## Début d'implementation de la classe Magasin

- Écrivez un début de classe `Magasin` : pour l'instant, implémentez uniquement
  les attributs et le constructeur (qui doit initialiser les identifiants
  courants à 0).

- Implémentez la méthode `nbClients` (utilisez la méthode `size` de la classe
  `vector`).

- Implémentez la méthode `ajouterClient` (utilisez la méthode `push_back`
  de la classe `vector` et n'oubliez pas d'incrémenter l'identifiant courant
  des clients).

* * *

- Implémentez la méthode `afficherClients` (qui doit simplement afficher tous
  les clients du magasin).

- Implémentez la méthode `supprimerClient` (utilisez la fonction `swap` de
  la bibliothèque standard et la méthode `pop_back` de la classe `vector`).
  Pour bien faire, il faudrait également supprimer les `Location` qui
  mentionnent le client supprimé mais on ne le fera pas dans ce TP.

- Testez l'affichage dans le `main` et les autres méthodes avec des tests
  unitaires.

* * *

- Mêmes questions pour gérer les produits :

![](uml/TP3b.svg){width="50%"}

## Exceptions

- Lorsque l'on supprime un `Client` du `Magasin`, il faut que le `Client`
  existe bien. Modifiez votre code de façon à lever une exception si ce n'est
  pas le cas. Utilisez une exception de type `string` et de valeur "erreur :
  ce client n'existe pas".

- Écrivez des tests unitaires pour vérifier que les exceptions sont
  correctement levées.

- Mêmes questions pour la suppression de produits.

## Fin d'implementation de la classe Magasin

- Implémentez les méthodes ci-dessous en testant les affichages dans le `main`
  et le reste avec des tests unitaires. Pour l'ajout / suppression de
  `Location`, on se contentera de lever une exception si la `Location`
  existe déjà / n'existe pas.

![](uml/TP3c.svg){width="60%"}

