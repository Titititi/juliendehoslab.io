---
title: TP GL, Conception 
date: 2019-04-18
---

## Problème 

On veut écrire un programme qui calcule et affiche le plus court chemin pour
aller entre deux villes. En entrée, on fournit le listing des routes :

```text
$ cat finistere.csv 
Douarnenez Pouldreuzic 17
Douarnenez Plomelin 25
vraiment nimporte quoi
Plouneour Pouldreuzic 9
Plouneour Plomelin 11
LaTorche Plomelin 20
LaTorche Plouneour 10
```

* * *

En sortie, le programme écrit un fichier [graphviz/dot](http://graphviz.org/)
décrivant le graphe à dessiner. Par exemple, pour aller de Douarnenez à
LaTorche :

```text
$ cat finistere.dot 
graph {
    splines=line; 
    Douarnenez -- Pouldreuzic -- Plouneour -- LaTorche [color=red, penwidth=3];
    Douarnenez -- Plomelin [label=25];
    Pouldreuzic -- Douarnenez [label=17];
    Plouneour -- Plomelin [label=11];
    Plouneour -- Pouldreuzic [label=9];
    LaTorche -- Plouneour [label=10];
    LaTorche -- Plomelin [label=20];
}
```

* * *

Il suffit alors d'exécuter `dot` (graphviz) sur ce fichier pour obtenir l'image
voulue :

![](dot/TP_conception_finistere.svg){width="30%"}


## Conception

- Proposez un diagramme de classes permettant de répondre au problème posé.

- Faites un diagramme de séquence d'une exécution classique sans erreur.

- Regardez le code fourni (dossier `TP_conception`) et faites-en un
  diagramme de classes et de séquence.


## Réalisation

On propose le découpage suivant :

| étape  | objectif                  | validation          |
|--------|---------------------------|---------------------|
| 1      | importer les données CSV  | tests unitaires     
| 2      | exporter le fichier dot   | TU + script de run  
| 3      | comparer des routes       | TU                  
| 4      | partitionner un chemin    | TU + script de run  

- Quel est l'intérêt de découper l'import/export en deux étapes (1 et 2) et de
  faire l'import avant l'export ?

- Quels sont les avantages et inconvients de faire l'import/export (étape 1 et
  étape 2) avant le calcul du plus court chemin (étape 3 et étape 4) ?

* * *

- Réalisez le projet selon la démarche suivante :

    - à chaque étape, écrivez le code qui réalise complètement mais uniquement l'objectif prévu
    - effectuez et vérifiez la validation prévue
    - commitez et passez à l'étape suivante

