---
title: TP GL, Documentation 
date: 2019-04-18
---

# Documentation et CI/CD (1)

## Mise en place de la CI/CD

Les forges git modernes peuvent héberger de la documentation, grâce à leurs 
outils d'intégration continue / livraison continue (CI/CD).

* * *

- Connectez-vous sur gitlab et créez un dépôt `cicd`.

- Copiez les fichiers `index.html` et `.gitlab-ci.yml` fournis dans votre dépôt
  `cicd`. Committez et pushez.

- Sur gitlab, vérifiez que le pipeline de CI/CD se déroule correctement et
  crée un artefact contenant le fichier `public/index.html`.

- Gitlab va mettre en place une URL `<login>.gitlab.io/<depot>` vous permettant
  d'accéder à votre artefact. Comme cela prend un peu de temps, continuez avec 
  les exercices suivants; ce dépôt sera repris dans un prochain exercice.
  
# Documentation développeur avec doxygen

## Principe

Une documentation développeur permet de faciliter la maintenance du code ou
l'utilisation d'une bibliothèque. Doxygen permet de générer ce genre de
documentation. Il s'utilise de la façon suivante : 

- dans le code source, écrire en commentaire (avec un formatage spécial) les
  informations à mettre dans la documentation 

- écrire un fichier de configuration `Doxyfile` indiquant comment générer la
  documentation

- exécuter le programme `doxygen` pour générer la documentation

## Exercices

- Dans le dossier `TP_documentation/drunk_player`, créez un dossier
  `doc_doxygen`.  Avec la commande `doxygen -g`, créez un `Doxyfile` par
  défaut et adaptez ce fichier au projet. Générez la documentation en html et
  vérifiez le résultat obtenu.

- Doxygen peut s'intégrer à cmake. Ainsi la documentation est compilée et
  installée sur le système de la même manière que le reste du projet. Pour
  cela, vérifiez la configuration dans le `CMakeLists.txt` et ajoutez un
  fichier `Doxyfile.in` dans le dossier `drunk_player`, contenant notamment :

```text
PROJECT_NAME           = ${CMAKE_PROJECT_NAME}
OUTPUT_DIRECTORY       = ${CMAKE_BINARY_DIR}/doc/
INPUT                  = ${CMAKE_SOURCE_DIR}/src
EXTRACT_ALL            = YES
RECURSIVE              = YES
```

* * *

- Dans le code source, modifiez les commentaires afin d'obtenir une
  documentation ressemblant à cet 
  [exemple de doc doxygen](files/gl-doxygen/index.html). Pensez à utiliser les
  fonctionnalités de formatage des préconditions, exception, description brève,
  todo, bug... (cf la 
  [documentation doxygen](http://www.doxygen.nl/manual/index.html)).

![](files/TP_documentation_doxygen.png)

# Documentation markdown avec gitlab

## Principe

[Markdown](https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/) 
est un format de fichier similaire à ReStructuredText. Il est notamment reconnu
et interprété par gitlab/github/etc, ce qui permet d'écrire des petits fichiers
de documentation qui seront affichés plus visiblement sur la page du projet.

- Dans le dossier `drunk_player`, créez un fichier `README.md` décrivant
  rapidement le projet. Sur gitlab, vérifiez que vous obtenez un formatage
  ressemblant à :

* * *

![](files/TP_documentation_markdown.png)

# Documentation utilisateur avec sphinx

## Principe

Une documentation utilisateur permet d'expliquer comment mettre en place et
utiliser le logiciel correspondant au projet, sans entrer dans les détails du
code. Sphinx permet de générer ce type de documentation (entre autres). Il
s'utilise de la façon suivante : 

- créer divers fichiers de configuration indiquant comment générer la
  documentation

- créer des fichiers contenant la documentation proprement dite, par exemple au
  format [ReStructuredText](https://fr.wikipedia.org/wiki/ReStructuredText)

- générer la documentation

## Exercices

- Dans le dossier `drunk_player`, créez un dossier `doc_sphinx`, allez dans
  ce dossier et initialisez la documentation sphinx avec la commande
  `sphinx-quickstart`. Pour les options, choisissez de séparer `build` et 
  `source` et de générer un `Makefile`.

- Générez la documentation avec `make` et vérifiez le résultat produit.

- Écrivez une documentation utilisateur pour le projet `drunk_player`
  ressemblant à cet [exemple de doc sphinx](files/gl-sphinx/index.html).
  Quelques liens : [aide-mémoire RST](http://docutils.sourceforge.net/docs/user/rst/quickref.html),
  [tutorial sphinx](http://matplotlib.org/sampledoc/index.html), 
  [doc sphinx](http://www.sphinx-doc.org/en/master/contents.html).

* * *
  
![](files/TP_documentation_sphinx.png)

# Documentation et CI/CD (2)

## Mise en place d'une doc sphinx

- Rappelez-vous le dépôt `cicd` du premier exercice.

- Vérifiez que gitlab a déployé votre page web à l'URL
  `<login>.gitlab.io/<depot>`.

- Créez un projet sphinx dans votre dépôt en utilisant l'utilitaire
  `sphinx-quickstart`. Testez en local.

* * *

- Modifiez votre fichier `index.html` pour qu'il redirige vers
  `build/html/index.html` en ajoutant le code suivant dans l'entête :

```html
<script type="text/javascript">
      window.location.replace("build/html/index.html");
</script>
```

- Modifiez votre `.gitlab-ci.yml` pour qu'il déploie votre documentation
  sphinx. Testez sur gitlab.

## Thème HTML

- Changez le thème html de votre documentation en modifiant le fichier
  `source/conf.py` (pour l'instant, ne testez pas en local) :

```python
import sphinx_rtd_theme
html_theme = 'sphinx_rtd_theme'
```

- Mettez à jour votre `gitlab-ci.yml` et vérifiez sur gitlab.

- Testez en local en utilisant les environnements virtuels python.

## Extension Matplotlib

- Ajoutez l'extension `matplotlib.sphinxext.plot_directive` dans le fichier de
  configuration sphinx.

- Écrivez un fichier python qui dessine un graphique et incluez-le dans votre
  documentation (cf la 
  [doc matplotlib sur les extensions sphinx](https://matplotlib.org/sampledoc/extensions.html)).

* * *

![](files/gl-doc-sphinx-rtd-matplotlib.png)

