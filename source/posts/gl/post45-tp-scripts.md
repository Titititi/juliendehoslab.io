---
title: TP GL, Langages de scripts 
date: 2019-12-16
---

Un logiciel n'est pas toujours un gros projet écrit dans un seul langage comme
Java ou C++. Il peut également s'agir d'un ensemble de petits scripts, d'un
projet développé dans un langage de script ou d'un projet combinant
plusieurs langages. L'objectif de ce TP est de voir comment utiliser un
langage de script pour :

- écrire des scripts simples
- développer des projets plus complets
- interfacer du code écrit dans un autre langage


# JavaScript (web)

Javascript est un langage de programmation très connu pour le développement web
côté client. Il est également de plus en plus utilisé côté serveur, via
Node.js. Enfin, Javascript permet d'interfacer du code (C, C++, Rust), côté
client et côté serveur. Attention, Javascript a beaucoup évolué et il est
généralement préférable de suivre au moins la norme ES2015/ES6.

- [doc du MDN](https://developer.mozilla.org/fr/docs/Web/JavaScript)

- [doc du W3C](https://www.w3schools.com/js/)


## Scripts simples (js_web_fibo1)

Le code fourni contient une page web et un fichier JavaScript. 

- Modifiez la
  page web `test_fibo.html` de façon à afficher la suite de Fibonacci en
  utilisant le script `fibo.js` :

- Écrivez un fichier `repeat.js` contenant une fonction `repeatN`. Cette
  fonction doit prendre un nombre `n` et une fonction `f` et appliquer
  la fonction `f` sur tous les nombres de 1 à `n`.

- Écrivez une page web `test_repeat.html` qui affiche la suite de Fibonacci
  en utilisant votre fonction `repeatN`.

* * *

![](files/gl-scripts-js-web-fibo1.png)


## Afficher un graphique (js_web_fibo2)

Le code fourni affiche un graphique de la fonction $\sin$ dans une page 
web.

- Réutilisez le script `fibo.js` de l'exercice précédent et modifiez la page
  web de façon à tracer les 10 premiers termes de la suite de fibonacci.


* * *

![](files/gl-scripts-js-web-fibo2.png)


## Interfacer du code C++ (js_web_fibo3)

JavaScript permet d'utiliser du code WebAssembly, par exemple pour interfacer
du code C++. Cette méthode est souvent utilisée pour améliorer la vitesse d'un
programme tout en gardant un code simple : les codes de calculs sont
implémentés en C++ (plus rapide) et appelés dans des scripts JavaScript (plus
simples). Pour la mettre en place, il faut écrire du code de liaison
C++/JavaScript et compiler tout le code C++ dans un format utilisable en
JavaScript (par exemple, wasm).

> Si ce n'est pas déjà fait, [installez emscripten](../env/post42-emscripten.html).
>

* * *

- Regardez le code fourni (`fibo.cpp`, `Makefile` et `index1.html`). Comment
  est fait le lien entre le code C++ et le code JavaScript ?

- Compilez le code et affichez la page `index1.html`.

- Ajoutez un fichier `sinus.cpp` implémentant la fonction $f(x) =
  \sin(2\pi(ax + b))$ ainsi que son interface JavaScript.  Modifiez la page
  `index1.html` de façon à tracer également votre fonction `sinus` avec
  $a=2$ et $b=0.25.$

- Modifiez le fichier `index2.html` de façon à tracer également votre
  fonction `sinus`, dont on pourra régler les paramètres.

* * *

<video preload="metadata" controls>
<source src="files/gl-scripts-js-web-fibo3.mp4" type="video/mp4" />
![](files/gl-scripts-js-web-fibo3.png)

</video>



## Génération d'image en C++/WebAssembly (js_web_sinus)

Le projet fourni contient une page web avec un canvas calculé
par du code C++/WebAssembly. Pour l'instant, le canvas est rempli en rouge.

- Compilez et testez le projet. Utilisez le `Makefile` pour cela.

- Ajoutez un paramètre à la méthode `Sinus::update` pour indiquer la quantité
  de rouge voulue.

- Ajoutez un `range` dans la page de façon à pouvoir modifier dynamiquement
  la quantité de rouge.

- Modifiez votre projet de façon à afficher un dégradé selon un sinus dont on
  peut modifier la fréquence et la phase.

* * *

<video preload="metadata" controls>
<source src="files/gl-scripts-js-web-sinus.mp4" type="video/mp4" />
![](files/gl-scripts-js-web-sinus.png)

</video>



## Application desktop avec Electron (js_web_electron)

[Electron](https://electronjs.org/) est un framework qui permet de développer
des interfaces graphiques. Il est basé sur Chromium et Node.js, et est utilisé
dans des logiciels comme Atom ou Visual Studio Code. Le cas d'utilisation
typique de Electron est de développer des applications de bureau en utilisant
les technos web classiques (HTML, CSS, JS).

- Regardez le projet fourni et comprenez, dans les grandes lignes, le
  fonctionnement d'une application Electron (voir éventuellement le [tutoriel
  Electron](https://electronjs.org/docs/tutorial/first-app)).

- Modifiez le projet de façon à implémenter une application qui affiche un
  graphique de la suite de Fibonacci jusqu'à un nombre réglable via
  l'interface. Vous pouvez reprendre la fonction JavaScript `fiboIterative` des
  premiers exercices.

* * *

![](files/gl-scripts-js-web-electron.png)



# JavaScript (Node.js)

[Node.js](https://nodejs.org/en/) est un environnement d'exécution JavaScript
autonome (i.e., non intégré dans un navigateur). Node.js est souvent utilisé
pour développer des serveurs web asynchrones.

> Si ce n'est pas déjà fait, [installez Node.js](../env/post40-nodejs.html).
>


## Scripts simples (js_node_fib1)

- Regardez le code fourni, notamment comment définir du code JavaScript et
  comment le réutiliser dans un script Node.js.

- Modifiez le script `test_fibo.js` de façon à afficher la suite de Fibonacci,
  en utilisant le script `fibo1.js` ou `fibo2.js`.

- Reprenez le fichier `repeat.js` des exercices précédents et 
  écrivez un script `test_repeat.js` qui affiche la suite de Fibonacci
  en utilisant la fonction `repeatN`.

* * *

```bash
$ node test_fibo.js 

fiboIterative(0) = 0
fiboIterative(1) = 1
fiboIterative(2) = 1
fiboIterative(3) = 2
fiboIterative(4) = 3
fiboIterative(5) = 5
fiboIterative(6) = 8
fiboIterative(7) = 13
fiboIterative(8) = 21
fiboIterative(9) = 34
```


## Interfacer du code C++ (js_node_fib2)

Le code fourni contient un module C++ (`fibo.cpp`), un script utilisant ce
module (`test_fibo.js`) et un fichier de packaging (`package.json`).

- Regardez, compilez et testez le code fourni.

- Reprenez le module `sinus.cpp` des exercices précédents, écrivez un script
  `test_sinus.js` et ajoutez des règles de construction/exécution dans
  `package.json`.

```bash
$ npm run build

...

$ npm run sinus

sinus.sinus(1, 0, 0.5) = 1.2246467991473532e-16
sinus.sinus(1, 0, 0.25) = 1
```


## Application web (js_node_fiboweb)

Le projet fourni contient un serveur Node/Express (API JSON + fichiers
statiques) ainsi que du code C++ interfacé via emscripten (wasm et asmjs).

- Regardez et testez le projet fourni.

- Ajoutez un fichier `sinus.cpp` avec la fonction `sinus` habituelle.
  Normalement, la configuration fournie devrait l'intégrer directement (wasm + asmjs).

- Modifiez le serveur de façon à fournir également une API pour votre `sinus`.

- Modifiez la page web de façon à afficher des graphiques dynamiques, en
  utilisant le code wasm.

- S'il vous reste du temps, calculez les graphiques en utilisant l'API, via des
  requêtes XHR.


* * *


<video preload="metadata" controls>
<source src="files/gl-scripts-js-node-fiboweb.mp4" type="video/mp4" />
![](files/gl-scripts-js-node-fiboweb.png)

</video>



# Python

Python est un langage de script très utilisé pour l'administration système, le
web côté serveur, le calcul scientifique… Il permet également d'interfacer
facilement du code C ou C++.  Attention cependant : Python peut sembler simple
par sa syntaxe mais c'est un langage complet et assez particulier sur certains
points. Il faut donc vraiment l'apprendre pour l'utiliser efficacement.

- [initiation au langage Python](http://www-lisic.univ-littoral.fr/~teytaud/files/Cours/Apprentissage/tutoPython.pdf)
- [documentation Python officielle](https://docs.python.org/fr/3/)


## Scripts simples (py_fibo1)

Le projet contient un module et un script utilisant ce module.

- Regardez le code fourni et testez leur exécution avec l'interpréteur
  `python3`. Comment fonctionnent les modules en Python ?

- Lancez `python3`, importez le module `fibo` et appeler la fonction
  `fiboIterative` sur la valeur 10.

- Écrivez un module `repeat` contenant une fonction `repeatN`. Cette
  fonction doit prendre un nombre `n` et une fonction `f` et appliquer
  la fonction `f` sur tous les nombres de 1 à `n`.

* * *

- Écrivez un script `test_repeat.py` contenant une fonction `print_fibo`.
  Cette fonction doit prendre un nombre et afficher la valeur correspondante de
  la suite de Fibonacci (utilisez `fiboIterative`). En utilisant,
  `print_fibo` et `repeatN`, complétez votre script de façon à afficher la
  suite de Fibonacci de 1 à 10. 

```text
$ python3 test_repeat.py 

1
1
2
3
5
8
13
21
34
55
```

## Projet en Python (py_fibo2)

Cette fois-ci, le projet est configuré avec Setuptools, un outil très classique
en Python. Les modules sont dans le dossier `src` et les scripts dans le
dossier `scripts`. Le fichier `setup.py` permet de configurer et de construire le projet.
Enfin, pour éviter de travailler dans un environnement global avec un
grand risque de conflits, on utilise un environnement virtuel (`virtualenv`). 


- Tout d'abord, testez le script `test_fibo.py` directement, et vérifiez
  que les modules ne sont pas trouvés :

```text
$ python3 scripts/test_fibo.py 

Traceback (most recent call last):
  File "scripts/test_fibo.py", line 3, in <module>
    import fibo as f
ModuleNotFoundError: No module named 'fibo'
```

* * *


- Créez et activez un environnement virtuel Python3 avec les commandes suivantes.
  Ici `myenv` est le nom de l'environnement (vous pouvez mettre un nom différent).

```text
$ python3 -m virtualenv -p python3 myenv

$ source myenv/bin/activate
```

- Installez/réinstallez votre projet dans l'environnement virtuel courant, en
  utilisant la configuration `setup.py` :

```text
$ python3 setup.py install

$ python3 setup.py build
```

* * *


- Testez le script `test_fibo.py` installé dans l'environnement virtuel
  (remarquez que le script est directement connu et exécutable) :

```text
$ python3 scripts/test_fibo.py

3
```


- Testez également le script `plot_fibo.py` et vérifiez l'image obtenue.

- Intégrer le module `repeat.py` et le script `test_repeat.py` de
  l'exercice précédent. Pour cela, copiez les fichiers dans les dossiers
  correspondant, modifiez le `setup.py` et réinstallez votre projet dans
  l'environnement virtuel.

* * *

- Désactivez l'environnement virtuel et vérifiez que vous retrouvez bien
  votre environnement réel :

```text
$ deactivate

$ python3 scripts/test_fibo.py 

Traceback (most recent call last):
  File "scripts/test_fibo.py", line 3, in <module>
    import fibo as f
ModuleNotFoundError: No module named 'fibo'
```


## Projet en Python et en C++ (py_fibo3)

Python permet d'interfacer facilement du code C++. Cette méthode est souvent
utilisée pour faire du calcul scientifique : les codes de calculs sont
implémentés en C++ (plus rapide) et appelés dans des scripts Python (plus
simples). Pour la mettre en place, il faut écrire du code de liaison C++/Python
(par exemple avec boost_python ou pybind11) et indiquer dans 
le `setup.py` comment compiler le code C++.

- Regardez le code fourni : code C++ avec interface Python (`cpp/fibo.cpp`),
  scripts Python (`scripts/*`) et fichier de configuration (`setup.py`).

- Compilez et exécuter dans un environnement virtuel.

* * *

- Ajoutez un fichier `cpp/sinus.cpp` implémentant la fonction $f(x) =
  \sin(2\pi(ax + b))$ ainsi que son interface Python dans un module `sinus`.
  Intégrez votre module dans le fichier de configuration. Enfin, modifiez le
  script `scripts/plot.py` de façon à calculer également une image
  `plot_sinus.png` avec $a=2$ et $b=0.25$.

* * *

![](files/gl-scripts-plot-sinus.png)



## Exemple d'application web (py_fiboweb)

Le projet fourni est un serveur web classique en Python + Flask (équivalent de
JavaScript + Node + Express). Le dossier `cpp` contient une fonction de
calcul en C++ avec son interface Python. Le dossier `src` contient une
fonction de tracer de courbe utilisant Matplotlib. Enfin, le dossier `site`
contient l'application web avec le script serveur et le fichier de template
HTML.

- Compilez le projet dans un environnement virtuel, lancez le programme serveur
  et vérifiez le résultat dans un navigateur web.

- Ajoutez le module C++ `sinus` de l'exercice précédent et modifiez le projet
  de façon à afficher également une courbe correspondant à cette fonction.

* * *

<video preload="metadata" controls>
<source src="files/gl-scripts-py-fiboweb.mp4" type="video/mp4" />
![](files/gl-scripts-py-fiboweb.png)

</video>

## Programmation système (py_cat)

Le projet fourni contient deux fichiers textes.

```text
$ cat toto.txt 
ligne 1 de toto.txt
ligne 2 de toto.txt
ligne 3 de toto.txt

$ cat tata.txt 
ligne 1 de tata.txt
ligne 2 de tata.txt
ligne 3 de tata.txt
```

* * *

- Écrivez un script python `cat.py` équivalent au script `TP_unix_script2.sh`
  du TP unix, c'est-à-dire qui lit et affiche les fichiers dont les noms sont
  passés en argument de la ligne de commande. 

```text
$ ./cat.py toto.txt tata.txt 
**** toto.txt *****
ligne 1 de toto.txt
ligne 2 de toto.txt
ligne 3 de toto.txt

**** tata.txt *****
ligne 1 de tata.txt
ligne 2 de tata.txt
ligne 3 de tata.txt
```


## Programmation d'interface graphique (py_fibogui)

- À partir du script fourni, implémentez une interface graphique permettant de
  tracer l'évolution de la suite de Fibonacci.

* * *

<video preload="metadata" controls>
<source src="files/gl-scripts-fibogui.mp4" type="video/mp4" />
![](files/gl-scripts-fibogui.png)

</video>




