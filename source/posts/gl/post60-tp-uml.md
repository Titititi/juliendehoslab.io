---
title: TP GL, UML
date: 2019-04-18
---

## Calculette

- Ouvrez un terminal dans le dossier `TP_uml/calculette`.

- Compilez et exécutez le programme `calculette1.out`.

- Faites un diagramme de séquence de l'exécution suivante : 

```text
$ ./calculette1.out 

Entrez l'opération à réaliser (+, -, *, /, quitter) : +
Entrez le premier nombre : 37
Entrez le second nombre : 5
37 + 5 = 42

Entrez l'opération à réaliser (+, -, *, /, quitter) : quitter
Au revoir
```

* * *

- Faites un diagramme d'activité. 

- Faites un diagramme de cas d'utilisation.

- Regardez le code de `calculette1` et faites un diagramme d'activité de conception. 

## Compagnie maritime

- En testant le programme et en regardant l'exécution ci-dessous, faites un diagramme de cas d'utilisation.

* * *

```text
Opération (port, bateau, passager, preparer, reserver, afficher, quitter) ? port
Nom du nouveau port ? Calais

Opération (port, bateau, passager, preparer, reserver, afficher, quitter) ? port
Nom du nouveau port ? Bora-Bora

Opération (port, bateau, passager, preparer, reserver, afficher, quitter) ? bateau
Nom du nouveau bateau ? Pen Duick III
Capacité ? 4

Opération (port, bateau, passager, preparer, reserver, afficher, quitter) ? preparer
Nom du bateau à préparer ? Pen Duick III
Nom du port de départ ? Calais
Nom du port d'arrivée ? Bora-Bora

Opération (port, bateau, passager, preparer, reserver, afficher, quitter) ? preparer
Nom du bateau à préparer ? Pen Duick III
Nom du port de départ ? Sangatte
Nom du port d'arrivée ? Bora-Bora
erreur : port de départ inconnu: Sangatte

Opération (port, bateau, passager, preparer, reserver, afficher, quitter) ? passager
Nom du nouveau passager ? Dehos
Prénom ? Julien

Opération (port, bateau, passager, preparer, reserver, afficher, quitter) ? reserver
Nom du bateau à réserver ? Pen Duick III
Nom du passager ? Dehos
Prénom du passager ? Julien

Opération (port, bateau, passager, preparer, reserver, afficher, quitter) ? afficher
Pen Duick III : Calais -> Bora-Bora, 1/4
  - Dehos Julien
```

* * *

- Regardez le code et faites un diagramme de classes.

- Faites un diagramme de séquence de conception pour l'exécution suivante : 

* * *

```text
Opération (port, bateau, passager, preparer, reserver, afficher, quitter) ? port
Nom du nouveau port ? Calais

Opération (port, bateau, passager, preparer, reserver, afficher, quitter) ? port
Nom du nouveau port ? Bora-Bora

Opération (port, bateau, passager, preparer, reserver, afficher, quitter) ? bateau
Nom du nouveau bateau ? Pen Duick III
Capacité ? 4

Opération (port, bateau, passager, preparer, reserver, afficher, quitter) ? preparer
Nom du bateau à préparer ? Pen Duick III
Nom du port de départ ? Calais
Nom du port d'arrivée ? Bora-Bora

Opération (port, bateau, passager, preparer, reserver, afficher, quitter) ? passager
Nom du nouveau passager ? Dehos
Prénom ? Julien

Opération (port, bateau, passager, preparer, reserver, afficher, quitter) ? reserver
Nom du bateau à réserver ? Pen Duick III
Nom du passager ? Dehos
Prénom du passager ? Julien
```

## Médiathèque

```text
Login (utilisateur, gestionnaire, quitter) ? gestionnaire
Opération (afficher, emprunter, rendre, livre, dvd, deconnexion) ? livre
Titre du livre ? Guerre et Paix
Auteur du livre ? Tolstoi
Nombre de pages ? 1617
Opération (afficher, emprunter, rendre, livre, dvd, deconnexion) ? dvd
Titre du dvd ? Rocky III
Auteur du dvd ? Stallone-Shire
Duree en minutes ? 99
Opération (afficher, emprunter, rendre, livre, dvd, deconnexion) ? deconnexion

Login (utilisateur, gestionnaire, quitter) ? utilisateur
Opération (afficher, emprunter, rendre, deconnexion) ? emprunter
Titre ? La dialectique de la durée
Auteur ? Bachelard
Nom de l'emprunteur ? Fabien
erreur : ce media n'est pas disponible
Opération (afficher, emprunter, rendre, deconnexion) ? emprunter
Titre ? Rocky III
Auteur ? Stallone-Shire
Nom de l'emprunteur ? Fabien
Opération (afficher, emprunter, rendre, deconnexion) ? afficher
Livre "Guerre et Paix" de Tolstoi (1617 pages), disponible
Dvd "Rocky III" de Stallone-Shire (99 minutes), emprunté par Fabien
```

* * *

- Faites un diagramme de cas d'utilisation.

- Faites un diagramme d'activité (sans détailler les opérations). 

- Faites un diagramme d'états.

- Regardez le code et faites un diagramme de classes.

- Faites un diagramme de séquence pour l'exécution suivante (ignorez les saisies) :

* * *

```text
Login (utilisateur, gestionnaire, quitter) ? gestionnaire
Opération (afficher, emprunter, rendre, livre, dvd, deconnexion) ? dvd
Titre du dvd ? Rocky III
Auteur du dvd ? Stallone-Shire
Duree en minutes ? 99
Opération (afficher, emprunter, rendre, livre, dvd, deconnexion) ? deconnexion

Login (utilisateur, gestionnaire, quitter) ? utilisateur
Opération (afficher, emprunter, rendre, deconnexion) ? emprunter
Titre ? Rocky III
Auteur ? Stallone-Shire
Nom de l'emprunteur ? Fabien
Opération (afficher, emprunter, rendre, deconnexion) ? afficher
Livre "Guerre et Paix" de Tolstoi (1617 pages), disponible
Dvd "Rocky III" de Stallone-Shire (99 minutes), emprunté par Fabien
Opération (afficher, emprunter, rendre, livre, dvd, deconnexion) ? deconnexion
```

## Logiciel de "chat"

- Faites un diagramme de séquence de l'exécution suivante :

![](files/TP_uml_chat.png)

- Faites un diagramme de cas d'utilisation côté client.

- Testez le programme avec un serveur et deux clients, en local. Faites un
  diagramme d'états côté serveur et un diagramme d'états côté client.

* * *

- Regardez le code et faites un diagramme de classes côté client.

- Faites un diagramme d'activité côté serveur et un diagramme d'activité
  de conception côté client.

