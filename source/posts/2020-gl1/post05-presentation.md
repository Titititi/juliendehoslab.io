---
title: Présentation du module GL1
date: 2020-02-25
---

## TODO

## Objectifs du module

- avoir un peu de culture générale sur la programmation fonctionnelle
- utiliser un langage fonctionnel (Haskell)
- programmer selon une approche fonctionnelle

## Pré-requis 

- algorithmique

## Organisation

- 3h de CM

- 24h de TP:
    - TP1: découverte d'Haskell, expression, fonction, type, IO
    - TP2: filtrage par motif, gardes
    - TP3: liste, tuple
    - TP4: fonction récursive **+ DS (noté)**
    - TP5: traitements de liste, liste en compréhension
    - TP6: curryfication, évaluation partielle **+ DS (noté)**
    - TP7: projet Haskell, mini-projet 1
    - TP8: **mini-projet 2 (noté)**

## Travaux pratiques

- TP sous Linux.
- [Installez Haskell Stack](../2020-env/post45-stack.html).
- [Installez VSCode](../2020-env/post30-vscode.html).
- Et faites-vous un dépôt gitlab pour versionner vos TP. 

