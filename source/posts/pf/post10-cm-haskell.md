---
title: CM PF, Haskell
date: 2019-04-18
---

# La programmation fonctionnelle 

## Paradigmes de programmation

| paradigme    | langage           | principe                                  |
|--------------|-------------------|-------------------------------------------|
| impératif    | Fortran (1954)    | exécuter des instructions successivement pour modifier l'état courant
| fonctionnel  | Lisp    (1958)    | appliquer des fonctions imbriquées sans effet de bord
| objet        | Smalltalk (1972)  | faire interagir des briques logicielles représentant des concepts
| logique      | Prolog (1972)     | inférer le résultat d'un problème décrit par des règles de logique

$\rightarrow$ souvent un langage utilise plusieurs paradigmes différents

## Langages fonctionnels

- principe : imbrication de fonctions sans effet de bord
- intérêts : expressivité, découplage du code, réduction des sources d'erreurs, preuve de correction, concurrence...
- applications : compilateurs, web back-end, script... 
- caractéristiques : fonctions d'ordre supérieur, récursivité, listes, garbage collector...
- regain d'intérêt : Erlang, XSLT, F#, Scala, C++11
- langages fonctionnels : type Lisp, type ML

$\rightarrow$ peu de langages fonctionnels purs


## Langages fonctionnels de type Lisp

- exemples de langages : Common Lisp, Scheme, Emacs Lisp...
- caractéristiques classiques :
    - expressions symboliques + meta-expressions 
    - syntaxe simple et élégante (si on aime les parenthèses)
    - homoïconicité
    - typage dynamique
- exemple de code en Common Lisp :

```lisp
(defun factorielle (n)
    (if (= n 0) 
        1
        (* n (factorielle(- n 1)))))
```


## Langages fonctionnels de type ML

- exemples de langages : ML, Haskell, OCaml...
- caractéristiques classiques :
    - types algébriques 
    - inférence de types
    - filtrage par motifs
    - typage statique
- exemple de code en OCaml :

```ocaml
let rec factorielle = function
    | 0 -> 1
    | n -> n * factorielle (n - 1) ;;
```


## Notion de fonction

- une fonction est un concept mathématique qui fait correspondre à chaque élément d'un ensemble, un élément d'un autre ensemble
- une fonction peut être définie par une expression dans un langage de programmation
- une fonction peut être appliquée à une valeur (appelée paramètre ou argument) pour produire une valeur (appelée résultat ou image)
- les paramètres/résultats peuvent eux-même être des fonctions
- formalisme du lambda-calcul

## Transparence référentielle

- Propriété de transparence référentielle : on obtient un programme équivalent si on remplace une expression par une expression de même valeur
- Définition d'un « élément pur » : un élément est pur s'il respecte la propriété de transparence référentielle
- Corollaire : un langage fonctionnel pur n'autorise pas les variables mutables, affectations, boucles, entrées/sorties...

## Notion de programmation fonctionnelle

« Functional programming is so called because a program consists entirely of
functions.  The main program itself is written as a function which receives the
program's input as its argument and delivers the program's output as its
result.  Typically the main function is defined in terms of other functions,
which in turn are defined in terms of still more functions until at the bottom
level the functions are language primitives. » 

John Hughes, Why Functional Programming Matters


# Premiers pas avec Haskell 

## Présentation du langage Haskell

- langage de type ML créé en 1990 par un comité dédié
- normes Haskell 98, Haskell 2010
- caractéristiques remarquables :
    - purement fonctionnel
    - évaluation paresseuse
    - monades
- implementations : ghc, hugs...
- applications codées en Haskell : darcs, pugs, yesod, xmonad...
- utilisateurs : galois, silk, facebook, google, intel, nvidia, microsoft...

## Quelques liens 

- pour débuter :
    - [site web officiel](https://www.haskell.org)
    - [wikibook Haskell](http://en.wikibooks.org/wiki/Haskell)
    - [Apprendre Haskell vous fera le plus grand bien !](http://lyah.haskell.fr/)
- pour se perfectionner :
    - [What I Wish I Knew When Learning Haskell](http://dev.stephendiehl.com/hask/)
    - [State of the Haskell ecosystem](https://github.com/Gabriel439/post-rfc/blob/master/sotu.md)
    - [Real World Haskell](http://book.realworldhaskell.org/read/)
    - [Typeclassopedia](https://wiki.haskell.org/Typeclassopedia)

## Exemples de code Haskell 

- fonction de tri rapide :

```haskell
quicksort [] = []
quicksort (p:xs) = 
    (quicksort lesser) ++ [p] ++ (quicksort greater)
    where lesser = filter (< p) xs
          greater = filter (>= p) xs
```


- liste des nombres premiers :

```haskell
primes = sieve [2..] 
    where sieve (p:xs) = p : sieve [x | x <- xs, x `mod` p /= 0]
```


## Exécuter du code Haskell

- compilation dans un fichier binaire (ghc)
- compilation à la volée (runghc)
- mode interactif (ghci)

## Compilation 

- example de fichier source `hello.hs` :

```haskell
main = putStrLn "Hello"
```

- compilation dans un fichier binaire :

```text
$ghc --make hello.hs 
[1 of 1] Compiling Main             ( hello.hs, hello.o )
Linking hello ...
$ ./hello 
Hello
$ 
```


- compilation à la volée :

```text
$ runghc hello.hs 
Hello
$
```


## Mode interactif

- interpréteur haskell :

```haskell
$ ghci
GHCi, version 7.8.3: http://www.haskell.org/ghc/  :? for help
Loading package ghc-prim ... linking ... done.
Loading package integer-gmp ... linking ... done.
Loading package base ... linking ... done.
Prelude> putStrLn "Hello"
Hello
Prelude>
```


- charger un fichier dans l'interpréteur :

```haskell
Prelude> :load hello.hs
[1 of 1] Compiling Main             ( hello.hs, interpreted )
Ok, modules loaded: Main.
*Main> main
Hello
*Main>
```


## Définir des symboles

- associer un nom à une valeur (constante ou fonction)
- un symbole ne peut pas être redéfini (transparence référentielle)
- le nom doit commencer par une minuscule

```haskell
n = 42             -- definit une constante n
f x = 2 * x - 3    -- definit une fonction f
main = do
    print n        -- affiche la valeur de n
    print (f 4)    -- applique f sur 4 et affiche
```


## Définir des symboles localement

- mot-clé `where` :

```haskell
tropMaigre taille poids = imc < 18.5
    where imc = poids / t2
          t2 = taille * taille
```


- mot-clé `let in` :

```haskell
tropMaigre taille poids = 
    let t2 = taille * taille
        imc = poids / t2
    in imc < 18.5
```


## Quelques remarques

- l'indentation compte 

```haskell
...
    where t2 = taille * taille
          imc = poids / t2      -- ok
...
    where t2 = taille * taille
    imc = poids / t2            -- erreur d'indentation
```


- les parenthèses servent uniquement pour indiquer des priorités

```haskell
Prelude> let f x = 2 * (x - 3)
Prelude> f 4 
2
```

* * *

- deux types de commentaires :

```haskell
-- commentaire de fin de ligne
{- commentaire
   multi-ligne {- imbricable -} -}
```


- pas de structure de contrôle au sens impératif (par exemple, le if-then-else correspond à une définition) :

```haskell
...
absX = if x < 0 then -x else x
```

## Mot-clés réservés

```text
case class data default deriving do else if 
import in infix infixl infixr instance let 
module newtype of then type where
```


## Interagir avec les entrée/sortie standards

- `toto.hs` :

```haskell
main = do
    putStr "Entrez votre nom : "
    nom <- getLine
    putStrLn ("Bonjour " ++ nom ++ " !")
```

- terminal :

```text
$ runghc toto.hs 
Entrez votre nom : Roger
Bonjour Roger !
```

**Attention : on ne peut pas "debugger à coup de printf"**


## Lire les paramètres de la ligne de commande

- toto.hs :

```haskell
import System.Environment
main = do
    args <- getArgs 
    print args 
```


- terminal :

```text
$ runghc toto.hs blabla 42
["blabla","42"]
```


# Système de type

## Type de données 

- ensemble des valeurs possibles et opérations autorisées
- toute donnée ou expression possède un type


## Le typage en Haskell

- typage statique fort
- typage par inférence 
- types polymorphes
- classes de types
- types élémentaires de base (Bool, Int, Double...)
- types composés (tuples, listes...)


## Typage faible/fort, statique/dynamique

- typage faible/fort :

    - termes souvent utilisés mais pas de définition unanime
    - traduit l'exigence du compilateur lors de la vérification de type

- typage statique/dynamique :

    - statique : vérification de type à la compilation
    - dynamique : vérification de type à l'exécution

- principales écoles :

    - typage statique fort (haskell, ada...) : fiabilité, performance 
    - typage dynamique (lisp, python...) : programmation interactive


## Typage par inférence

- le compilateur détermine les types automatiquement (algorithme W)
- mais on peut les écrire explicitement (ambiguïtés, documentation), et c'est la convention en Haskell :

```haskell
doubler :: Int -> Int
doubler x = 2 * x
```


- avec ghci, on peut demander le type d'une expression :

```haskell
Prelude> :type abs
abs :: (Ord a, Num a) => a -> a
Prelude> :type 12.0
12.0 :: Fractional a => a
```


## Types polymorphes

- une fonction peut être valide quelque soit le type du paramètre
- Haskell permet de définir une fonction avec des types «polymorphes»
- et d'appliquer cette fonction pour différents types «concrets»
- notion différente du polymorphisme de la POO

```haskell
Prelude> let identite x = x
Prelude> :type identite 
identite :: t -> t  -- fonction d'un type quelconque t vers t
Prelude> identite 4
4
Prelude> identite "blabla"
"blabla"
```


## Classes de types

- une fonction peut être valide quelque soit le type, sous réserve qu'il supporte certaines opérations
- Haskell permet de définir des classes de types, c'est-à-dire des ensembles d'opérations que doivent supporter les types de la classe
- quelques classes prédéfinies : Eq, Ord, Show, Read, Num...
- un type peut appartenir à plusieurs classes
- notion différente des classes de la POO

```haskell
Prelude> let carre x = x * x
Prelude> :type carre
carre :: Num a => a -> a  -- fonction de a vers a où 
                          -- a est un type de classe Num
Prelude> carre 2
4
Prelude> carre 2.1
4.41
```


## Quelques types élémentaires 

- `Bool`
    - booléens : `True  False`
    - opérateurs booléens : `&&  ||  not`
- `Char`
    - caractères uniques : `'a'  '\n'  ...`
- `String`
    - chaines de caractères : `"toto"  "a"  ...`
    - listes de `Char`
- `Int, Integer`
    - nombres entiers : `42  -12  ...`
    - précision fixe, précision arbitraire
- `Float, Double`
    - nombres réels : `42.0  -3.2  ...`
    - simple précision, double précision

* * *

```haskell
Prelude> True || False
True
Prelude> not False
True
Prelude> 14 + 4 * 7 
42
Prelude> 3 + 2 > 7
False
Prelude> 2.0 + 3
5.0
Prelude> 7 / 3
2.3333333333333335
Prelude> 3::Float
3.0
```

## Quelques classes de types 

| classe         | fonctions de la classe           |
|----------------|----------------------------------|
| `Eq`           | `==  /=`
| `Ord`          | `<  <=  >  >=  min  max`
| `Show`         | `show`
| `Read`         | `read`
| `Enum`         | `succ  pred`
| `Bounded`      | `minBound  maxBound`
| `Num`          | `+  -  *  negate  abs  signum`
| `Integral`     | `div  mod`
| `Fractional`   | `/  recip`

* * *

```haskell
Prelude> "toto" == "tata"
False
Prelude> show 12
"12"
Prelude> succ 'a'
'b'
Prelude> minBound::Char
'\NUL'
Prelude> abs (-12.3)
12.3
Prelude> recip 2 
0.5
Prelude> mod 1337 42
35
Prelude> 1337 `mod` 42  -- fonction mod en notation opérateur
35
Prelude> (+) 1 2        -- opérateur + en notation fonction
3
```

## Correspondances types-classes 

|           | `Eq` | `Ord` | `Enum` | `Bounded` | `Num` | `Integral` | `Fractional` | `Show` | `Read` |
|-----------|:----:|:-----:|:------:|:---------:|:-----:|:----------:|:------------:|:------:|:------:|
| `Bool`    |  X   |  X    |  X     |  X        |       |            |              |  X     |  X     |
| `Char`    |  X   |  X    |  X     |  X        |       |            |              |  X     |  X     |
| `String`  |  X   |  X    |        |           |       |            |              |  X     |  X     |
| `Int`     |  X   |  X    |  X     |  X        |  X    |  X         |              |  X     |  X     |
| `Integer` |  X   |  X    |  X     |           |  X    |  X         |              |  X     |  X     |
| `Float`   |  X   |  X    |  X     |           |  X    |            |  X           |  X     |  X     |
| `Double`  |  X   |  X    |  X     |           |  X    |            |  X           |  X     |  X     |


## Listes

- suite d'éléments de même type et de taille quelconque (éventuellement infinie)
- liste vide : `[]`
- opérateur de construction : `:`
- syntaxe simplifiée : accolades + virgules
- fonctions prédéfinies : `head length null reverse elem take drop...`
- opérateurs : `++  !!`

* * *

![Miran Lipovača](files/cm-haskell/listmonster.png)
    

* * *

```haskell
Prelude> 1:2:3:[]           -- construction avec l'opérateur et la liste vide
[1,2,3]
Prelude> [1,2,3]            -- construction avec la notation simplifiée
[1,2,3]
Prelude> [1,2,3] ++ [4,5]   -- concaténation de deux listes
[1,2,3,4,5]
Prelude> :type [2.5, 12]    -- type d'une liste
[2.5, 12] :: Fractional t => [t]
Prelude> ['t','o','t','o']  -- liste de caractères / chaîne de caractères
"toto"
Prelude> :type "toto"       -- type d'une chaîne de caractères
"toto" :: [Char]
Prelude> [1..3]             -- liste de nombres
[1,2,3]
Prelude> [0, -3 .. -10]     -- liste géométrique
[0,-3,-6,-9]
Prelude> take 5 [0,-3..]    -- utilisation d'une liste infinie 
[0,-3,-6,-9,-12]
Prelude> head [1..]         -- tête de liste
1
Prelude> tail [1..4]        -- queue de liste
[2,3,4]
```


## Tuples (n-uplets)

- suite d'éléments de types éventuellement différents mais prédéfinis
- syntaxe : parenthèses + virgules
- fonctions prédéfinies : `fst snd zip...`

```haskell
Prelude> ("toto", 42)
("toto",42)
Prelude> fst ("toto", 42)
"toto"
Prelude> :type ("toto", 42, True)
("toto", 42, True) :: Num t => ([Char], t, Bool)
Prelude> zip ["toto", "tata", "titi"] [42, 13, 37]
[("toto",42),("tata",13),("titi",37)]
```


# Fonctions, fonctions d'ordre supérieur 

## Notion de fonction

- une fonction $f$ d'un ensemble $A$  vers un ensemble $B$, fait correspondre, à chaque élément $x \in A$, un élément $f(x) \in B$ :
    - $A$ est appelé le domaine de $f$ 
    - $B$ est appelé le codomaine de $f$ 
    - $f(x)$ est appelé l'image de $x$ par $f$

* * *

- notation : 

$$\begin{array}{ll} 
    f : & A \rightarrow B \\ 
        & x \mapsto f(x) 
\end{array}$$

- exemple : 

![](tikz/fonction.svg){width="30%"}

## Définir une fonction explicitement 

- en math : 

 $$\begin{array}{lrl}
    f : & \mathbb{Z} & \rightarrow \mathbb{N}^* \\ 
        & 1 & \mapsto 2 \\
        & 2 & \mapsto 5 \\
        & -2 & \mapsto 5 \\
\end{array}$$

- en Haskell : 

```haskell
f :: Int -> Int
f 1 = 2
f 2 = 5
f (-2) = 5
```


## Définir une fonction par une équation 

- en math :

$$\begin{array}{ll}
    f : & \mathbb{Z} \rightarrow \mathbb{N}^* \\ 
        & x \mapsto x^2 + 1
\end{array}$$

- en Haskell :

```haskell
f :: Int -> Int
f x = x^2 + 1
```


## Définir une fonction par composition de fonctions 

- en math : 

$$\begin{array}{ll}
    h : & \mathbb{Z} \rightarrow \mathbb{N} \\ 
        & x \mapsto x^2\\
    g : & \mathbb{N} \rightarrow \mathbb{N}^* \\ 
        & x \mapsto x + 1\\
    f = &g \circ h
\end{array}$$

- en Haskell :

```haskell
h :: Int -> Int
h x = x^2
g :: Int -> Int
g x = x+1
f = g . h
```


## Notation « point-free » 

- définition de fonction sans expliciter les paramètres
- exemple :

```haskell
-- notation classique
f :: Int -> Bool
f x =  2 * x > 42

-- notation point-free
f' :: Int -> Bool
f' = (>42).(2*)
```

- question: pourquoi « point-free » alors qu'on utilise des `.` ?


## Fonction anonyme

- on peut créer une fonction sans lui donner de nom
- exemple en Haskell :

```haskell
Prelude> (\x -> x^2 + 1) 2  -- crée et applique une fonction
5
```

- simplifie l'usage des fonctions (par exemple, pour passer une fonction en paramètre...)
- également appelé «lambda», en référence au lambda-calcul


## Typer une fonction

- `fonction :: domaine -> codomaine` 
- types concrets, types polymorphes, classes de types

```haskell
Prelude> :type not
not :: Bool -> Bool
Prelude> :type (\x -> x^2 + 1)
(\x -> x^2 + 1) :: Num a => a -> a
Prelude> :type succ
succ :: Enum a => a -> a
Prelude> :type 2
2 :: Num a => a
Prelude> :type (+)
(+) :: Num a => a -> a -> a
```


## Appliquer/évaluer une fonction 

- en math : $y = f(2)$
- en Haskell : `y = f 2` 
- transparence référentielle : appliquer une fonction sur un argument donné produit toujours le même résultat (pas d'effet de bord)


## Opérateurs d'évaluation 

- `$` évalue (paresseusement) l'expression qui suit, par exemple :

```haskell
print (abs (negate 2))
print $ abs (negate 2)
print $ abs $ negate 2
print (abs $ negate 2)
```

- évaluation stricte avec `$!` (utile pour optimiser des fonctions récursives) 


## Fonction à plusieurs variables : forme non curryfiée

- en Haskell, une fonction prend 1 argument et produit 1 résultat
- mais on peut utiliser des tuples

```haskell
f1 :: (Int,Int) -> Int
f1 (x,_) = x

f2 :: Int -> (Int,Int)
f2 x = (x,-x)

f3 :: (Int,Int) -> (Int,Int)
f3 (x,y) = (y,x)

main = do
    print $ f1 (13,37)  -- affiche 13 
    print $ f2 42       -- affiche (42,-42)
    print $ f3 (3,7)    -- affiche (7,3)
```


## Fonction à plusieurs variables : forme curryfiée

- une fonction peut retourner une fonction
- ce qui permet de représenter des fonctions «à plusieurs paramètres»

```haskell
add :: Int -> (Int -> Int)
add x = (\y -> x+y)           -- ou "add x y = x + y"
                              -- ou "add = (+)"

main =  print $ (add 2) 3
```

- forme curryfiée classique :

```haskell
add :: Int -> Int -> Int
add = (+)
main =  print $ add 2 3
```

* * *

- autre exemple :

```haskell
f :: Int -> String -> Bool -> Float
-- f :: Int -> (String -> (Bool -> Float)) 
f x s p = if p then x' * s' else - x' * s'
    where x' = fromIntegral x
          s' = read s :: Float
main = do
    print $ f 2 "42" True
    print $ ((f 2) "42") True
```


## Ordre d'une fonction 

- ordre 1 : fonction qui retourne une variable

```haskell
f :: Int -> Int  -- fonction d'ordre 1
...
```

- ordre $n$ : fonction qui retourne une fonction d'ordre $n-1$

```haskell
f :: Int -> (Int -> Int)  -- fonction d'ordre 2
...
```


## Application partielle

- appliquer une fonction «sur une partie des paramètres»
- retourne donc une fonction

```haskell
-- fonction "à 2 paramètres"
add :: Int -> Int -> Int
add x y = x + y          -- ou : add = (+)

-- fonction "à 1 paramètre"
-- obtenue par application partielle de add
plus2 :: Int -> Int
plus2 y = add 2 y        -- ou : plus2 = add 2

main = do
    print $ add 2 3
    print $ plus2 3
```


## Passer une fonction en paramètre

- opération très classique en programmation fonctionnelle :

```haskell
              -- ici, les parenthèses sont obligatoires 
applyTwice :: (Int -> Int) -> Int -> Int
applyTwice f x = f (f x)
main = print $ applyTwice (\x -> x*2) 3
```

- autre implémentation :

```haskell
applyTwice :: (Int -> Int) -> Int -> Int
applyTwice f = (f.f)  
main = print $ applyTwice (*2) 3  
```

* * *

- équivalent C++11 :

```cpp
#include <functional>
#include <iostream>

int applyTwice(std::function<int(int)> f, int x) {
    return f(f(x));
}

int main() {
    std::cout << applyTwice([](int x){return x*2;}, 3)
              << std::endl;
    return 0;
}
```


## Notion de fermeture de fonction 

- ensemble des symboles utilisés pour définir la fonction mais non passés en paramètre
- exemple en Haskell :

```haskell
add :: Int -> Int -> Int
add x y = f y
    where f y' = x + y'  -- définition d'une fonction f 
                         -- x est capturé dans la fermeture
```

- exemple équivalent en C++11 :

```cpp
int add(int x, int y) {
    // en C++, on indique la fermeture des lambda
    // ici, la lambda f capture la variable x par valeur
    std::function<int(int)> f = [x](int yp){return x + yp;};
    return f(y);
}
```


## Fermeture et effets de bord 

- dans un langage à effets de bord, une fermeture peut capturer une variable qui peut devenir invalide :

* * *

```cpp
std::function<int(int)> makeDoubleur() {
    int x = 2;
    // ici on capture une variable locale par référence
    // la lambda utilisera une variable qui ne sera plus valide
    return [&x](int yp) { return x*yp; };
}

int main() {
    auto f1 = makeDoubleur();
    auto f2 = makeDoubleur();
    std::cout << f1(1) << std::endl;
    std::cout << f2(1) << std::endl;
    return 0;
}
```

```text
$ ./a.out 
2
32639
```

- cette erreur est impossible avec un langage fonctionnel pur


# Conditions, gardes, filtrage par motif 

## Expressions conditionnelles: if 

- produit une valeur différente selon 2 cas possibles
- dans les 2 cas, la valeur produite doit avoir le même type 
- contrairement aux langages impératifs, le «else» est obligatoire

```haskell
import System.Environment
main = do
    args <- getArgs
    putStrLn $ if length args == 1 
               then ("Salut "++(head args)++" !")
               else ("Erreur : indiquez votre nom")
```

```text
$ runghc toto.hs 
Erreur : indiquez votre nom
$ runghc toto.hs Fred
Salut Fred !
```


## Expressions conditionnelles : case

- produit une valeur différente selon un nombre quelconque de cas possibles

```haskell
import System.Environment
main = do
    args <- getArgs
    putStrLn $ case length args of 
                1 -> ("Salut "++(head args)++" !")
                0 -> ("Erreur : indiquez votre nom")
                _ -> ("Erreur : trop de parametres")
```

```text
$ runghc toto.hs 
Erreur : indiquez votre nom
$ runghc toto.hs roger
Salut roger !
$ runghc toto.hs roger simone
Erreur : trop de parametres
```


## Définition de fonction par des gardes 

- même principe que le `case` mais pour définir une fonction
- le cas retenu est celui qui correspond à la première condition vérifiée

* * *

```haskell
import System.Environment

formatMessage :: [String] -> String
formatMessage args
    | l == 0 = "Erreur : indiquez votre nom"
    | l == 1 = "Salut "++(head args)++" !"
    | otherwise = "Erreur : trop de parametres"
        -- filtre tous les cas restants
    | l > 3 = "ce cas ne sera jamais atteint"
    where l = length args

main = do
    args <- getArgs
    putStrLn $ formatMessage args
```


## Définition de fonction par pattern matching 

- même objectif que les gardes (définir une fonction en séparant les cas possibles)
- mais en testant si les paramètres correspondent à des motifs particuliers (déconstruction des paramètres)
- filtrage de valeur ou de structure

* * *

```haskell
import System.Environment

formatMessage :: [String] -> String
formatMessage [] = "Erreur : indiquez votre nom"
              -- filtre une structure
formatMessage ["julien"] = "Oh non, pas lui !"
              -- filtre une valeur
formatMessage [x] = "Salut " ++ x ++" !"
              -- filtre une structure
formatMessage _ = "Erreur : trop de parametres"
              -- filtre tous les cas restants
formatMessage (x:xs) = "ce cas ne sera jamais atteint"

main = do
    args <- getArgs
    putStrLn $ formatMessage args
```


## Pattern matching de tuples et de listes

- fonction sur des tuples :

```haskell
f :: (a,b) -> ...
f p = ...
f (x,y) = ...
f (_,y) = ...
f (x,_) = ...
f p@(x,y) = ...
```

- fonction sur des listes :

```haskell
f :: [a] -> ...
f l = ...
f (x:xs) = ...
f (_:xs) = ...
f (x:_) = ...
f l@(x:xs) = ...
f (x1:x2:xs) = ...
```


## Récapitulatif

|                    | type de déclaration | type du test | nombre de cas testés |
|--------------------|---------------------|--------------|----------------------|
| `if`               |  expression         |  booléen     |  2 
| `case`             |  expression         |  type testé  |  $n$
| `gardes`           |  fonction           |  booléen     |  $n$
| `pattern matching` |  fonction           |  type testé  |  $n$


# Fonctions récursives 

## Principe de la récursivité  

- une fonction récursive est une fonction dont la définition utilise la fonction elle-même 
- correspond à une définition par récurrence, par exemple : 

$$n! = 
\left\{ \begin{array}{l}
        1 \text{ si } n=1 \\
        n \times (n-1)! \text{ sinon } \\
\end{array} \right.$$

- permet d'implémenter des répétitions sans utiliser de boucle (et sans effet de bord) 
- permet de prouver la correction (méthode par induction) 


## Définition de fonctions récursives en Haskell 

- expressions conditionnelles :

```haskell
factorielle :: Int -> Int
factorielle n = if n==1 
                then 1 
                else n * factorielle (n-1)
```

* * *

- gardes :

```haskell
factorielle :: Int -> Int
factorielle n 
    | n==1 = 1
    | otherwise = n * factorielle (n-1)
```

- filtrage par motif :

```haskell
factorielle :: Int -> Int
factorielle 1 = 1
factorielle n = n * factorielle (n-1)
```


## Récursivité sur des listes 

- déconstruction de la liste via un filtrage par motif
- exemple (calcul de la taille d'une liste d'entiers) :

```haskell
taille :: [Int] -> Int
taille [] = 0
taille (x:xs) = 1 + (taille xs)

main = print $ taille [1..42] 
```


## Récursivité terminale 

- l'appel récursif fournit directement la valeur de retour
- intérêt : coût mémoire constant (pas d'empilement des appels récursifs)
- exemple de récursivité non-terminale (exemple précédent) :

```haskell
taille :: [Int] -> Int
taille [] = 0
taille (x:xs) = 1 + (taille xs)

main = print $ taille [1..42] 
```

* * *

- exemple équivalent en récursivité terminale :

```haskell
taille' :: [Int] -> Int -> Int
taille' [] n = n
taille' (x:xs) n = taille' xs (n+1)

main = print $ taille' [1..42] 0
```


## Fonction auxiliaire 

- problème : réécrire une fonction en récursivité terminale peut nécessiter un paramètre supplémentaire (et dont l'initialisation dépend de l'implémentation)
- solution : implémenter la fonction récursive terminale comme une fonction auxiliaire d'une fonction principale :

```haskell
taille' :: [Int] -> Int
taille' liste = tailleAux liste 0
    where tailleAux [] n = n
          tailleAux (x:xs) n = tailleAux xs (n+1)

main = print $ taille' [1..42] 
```


## Méthode de programmation d'une fonction récursive (ou pas) 

1. écrire le type de la fonction

```haskell
taille :: [Int] -> Int
```

2. énumérer les cas

```haskell
taille [] =
taille (x:xs) =
```

* * *

3. écrire les cas triviaux

```haskell
taille [] = 0
taille (x:xs) =
```

4. écrire les cas restants

```haskell
taille [] = 0
taille (x:xs) = 1 + (taille xs)
```

5. simplifier et généraliser

```haskell
taille :: Num b => [a] -> b
taille = foldr (\ _ n -> n+1) 0
```

# Traitements de listes 

## La liste en programmation fonctionnelle 

- structure classique pour représenter un ensemble de données
- une liste peut être traitée par une fonction récursive
- mais souvent le traitement est un mapping, un filtrage ou une réduction
- et Haskell propose des fonctions implémentant ces traitements
- en programmation impérative, on utiliserait des boucles


## Mapping de liste 

- appliquer une fonction sur chaque élément d'une liste 

- exemple avec ghci :

```haskell
Prelude> map (*2) [1..5]
[2,4,6,8,10]
Prelude> map (\ x -> "toto " ++ show x) [13, 37, 42]
["toto 13","toto 37","toto 42"]
```

- implémentation de `map` par une fonction récursive :

```haskell
map :: (a -> b) -> [a] -> [b]
map _ [] = []
map f (x:xs) = (f x) : map f xs
```

* * *

- exemple dans du code source :

```haskell
fois2 :: Num a => [a] -> [a]
fois2 = map (*2) 
main = print $ fois2 [1..5]
```

- exemple avec une fonction récursive :

```haskell
fois2' :: Num a => [a] -> [a]
fois2' [] = []
fois2' (x:xs) = (x*2):(fois2' xs) 
main = print $ fois2' [1..5]
```


## Filtrage de liste 

- obtenir les éléments d'une liste qui vérifient un prédicat 

- exemple avec ghci :

```haskell
Prelude> filter (\ x -> x `mod` 2 == 0) [13, 37, 42]
[42]
Prelude> filter even [13, 37, 42]
[42]
Prelude> filter odd [13, 37, 42]
[13,37]
```

* * *

- implémentation de `filter` par une fonction récursive :

```haskell
filter :: (a -> Bool) -> [a] -> [a]
filter _ [] = []
filter f (x:xs) = if f x then x:(filter f xs)
                         else filter f xs
```

- exemple dans du code source :

```haskell
pairs :: Integral a => [a] -> [a]
pairs = filter even
main = print $ pairs [1..5]
```

- exemple avec une fonction récursive :

```haskell
pairs' :: Integral a => [a] -> [a]
pairs' [] = []
pairs' (x:xs) = if even x then x:(pairs' xs) else pairs' xs
main = print $ pairs' [1..5]
```


## Réduction de liste 

- faire un calcul avec les éléments d'une liste 

- exemple avec ghci :

```haskell
Prelude> foldr (+) 0 [1..4]
10
Prelude> foldr (\ x acc -> show x ++ " " ++ acc) "" [1..4]
"1 2 3 4 "
```

- implémentation de `foldr` par une fonction récursive :

```haskell
foldr :: (a -> b -> b) -> b -> [a] -> b
foldr f acc [] = acc
foldr f acc (x:xs) = f x (foldr f acc xs)
```

* * *

- exemple dans du code source :

```haskell
somme :: Num a => [a] -> a
somme = foldr (+) 0 
main = print $ somme [1..5]
```

- exemple avec une fonction récursive :

```haskell
somme' :: Num a => [a] -> a
somme' [] = 0
somme' (x:xs) = x + (somme' xs)
main = print $ somme' [1..5]
```

- réductions prédéfinies : `sum product concat length minimum maximum`


## Réduction depuis la gauche ou depuis la droite

- réduction depuis la droite

```haskell
foldr :: (a -> b -> b) -> b -> [a] -> b
foldr f acc [] = acc
foldr f acc (x:xs) = f x (foldr f acc xs)
```

- réduction depuis la gauche

```haskell
foldl :: (b -> a -> b) -> b -> [a] -> b
foldl f acc [] =  acc
foldl f acc (x:xs) = foldl f (f acc x) xs
```

* * *

- exemple

```haskell
Prelude> foldr (:) [] [1..5]
[1,2,3,4,5]

Prelude> foldl (flip (:)) [] [1..5]
[5,4,3,2,1]

-- flip échange les paramètres de (:)
```


## Réduction optimisée

- souvent, on peut utiliser `foldr` ou `foldl` indifféremment mais... 
- `foldr` :
    - récursif non-terminal 
    - donc risque de saturation de la pile d'appels
- `foldl` :
    - récursif terminal 
    - mais les calculs intermédiaires ne sont pas réduits (pour permettre l'évaluation paresseuse)
    - donc pas de saturation de la pile mais coût mémoire (dans le tas)
- `foldl'` :
    - équivalent `foldl'` mais à évaluation stricte
    - coût mémoire constant (ou pas)


# Listes en compréhension 

## Principe 

- syntaxe pour construire des listes : 

```haskell
[x/2 | x<-[1..4], even x]
```

- inspirée des math : $\{ x/2~|~x \in [1,4], x \text{ pair} \}$
- n'ajoute pas vraiment de fonctionnalité mais très pratique 
- similitudes avec la programmation par contraintes
- trois composants : génération, mapping, filtrage


## Génération 

- listes de base permettant de construire la liste finale
- listes de nombre, listes existantes :

```haskell
Prelude> [x | x<-[0,2..12]]
[0,2,4,6,8,10,12]
```

- produit cartésien :

```haskell
Prelude> [(x,y) | x<-[1..2], y<-[1..2]]
[(1,1),(1,2),(2,1),(2,2)]
```

- reférence vers des variables locales précédentes :

```haskell
Prelude> [(x,y) | x<-[1..4], y<-[1..x]]
[(1,1),(2,1),(2,2),(3,1),(3,2),(3,3),(4,1),(4,2),(4,3),(4,4)]

Prelude>[x | xs<-["toto","tata"], x<-xs]
"tototata"
```


## Mapping 

- application d'une fonction sur chaque élément généré :

```haskell
Prelude> [2*x | x<-[1..6]]
[2,4,6,8,10,12]

Prelude> [product [1..x] | x<-[1..6]]
[1,2,6,24,120,720]

Prelude> [(x,y,x+y) | x<-[1..2], y<-[1..2]]
[(1,1,2),(1,2,3),(2,1,3),(2,2,4)]

Prelude> [x+y | x<-[1..2], y<-[1..2]]
[2,3,3,4]

Prelude> [Data.Char.toUpper x | x<-"toto"]
"TOTO"

Prelude> [(Data.Char.toUpper x):xs | (x:xs)<-["toto","tata"]]
["Toto","Tata"]
```


## Filtrage 

- utilisation d'un prédicat pour filtrer les éléments générés :

```haskell
Prelude> [x | x<-[1..6], even x]
[2,4,6]

Prelude> [x | x<-"tototata", elem x "aeiouy"]
"ooaa"

Prelude> [x | x<-[1..42], 42 `mod` x == 0]
[1,2,3,6,7,14,21,42]

Prelude> [(x,y,z) | x<-[1..7], y<-[x..7], z<-[y..7], x+y+z==7]
[(1,1,5),(1,2,4),(1,3,3),(2,2,3)]
```


## Implémentation de map

- avec une fonction récursive :

```haskell
map :: (a -> b) -> [a] -> [b]
map _ [] = []
map f (x:xs) = (f x) : map f xs
```

- avec une liste en compréhension :

```haskell
map' :: (a -> b) -> [a] -> [b]
map' f l = [f x | x<-l] 
```


## Implémentation de filter 

- avec une fonction récursive :

```haskell
filter :: (a -> Bool) -> [a] -> [a]
filter _ [] = []
filter f (x:xs) = if f x then x:(filter f xs) 
                         else filter f xs
```

- avec une liste en compréhension :

```haskell
filter' :: (a -> Bool) -> [a] -> [a]
filter' p l = [x | x<-l, p x]
```


# Évaluation paresseuse 

## Terminologie 

- une expression peut être :
    - une valeur (alias expression en forme normale)
    - une expression réductible à une valeur (alias redex ou thunk)
- évaluer/calculer une expression consiste à la réduire à une valeur
- exécuter un programme revient donc à évaluer son expression principale
- stratégies d'évaluation : stricte, non-stricte

## Évaluation stricte 

- pour évaluer une expression, on réduit d'abord les sous-expressions en forme normale
- appel par valeur
- exemple :

```haskell
take 2 [x^x | x<-[2..9]]
-- on évalue d'abord les deux paramètres de take
take 2 [4,27,256,3125,46656,823543,16777216,387420489]
-- puis on évalue take
[4,27]
```


## Évaluation non-stricte 

- on évalue uniquement quand c'est nécessaire
- appel par nom
- exemple :

```haskell
take 2 [x^x | x<-[2..9]]
-- on évalue d'abord take, qui demande les deux premiers éléments de la liste
take 2 [4,27]
-- puis on termine l'évaluation de take
[4,27]
```


## Évaluation non-stricte et partage des calculs 

- évaluation non-stricte sans partage :

```text
f (2^8) where f x = x + x
(2^8) + (2^8)  -- évalue f
256 + (2^8)    -- évalue le premier terme de +
256 + 256      -- évalue le second terme de +
512            -- termine l'évaluation de +
```

- évaluation non-stricte avec partage :

```text
f (2^8) where f x = x + x
n + n, avec n = (2^8)  -- évalue f avec partage
n + n, avec n = 256    -- évalue le terme partagé
512                    -- termine l'évaluation de +
```


## Évaluation paresseuse (lazy evaluation) 

- évaluation non-stricte (au plus tard) avec partage
- stratégie utilisée par le langage Haskell
- avantages : efficacité des calculs, structures infinies, expressivité
- inconvénients : surcoût des thunks (c.f. `foldl` et `foldl'`)
- évaluation stricte avec `$!`


# Synonymes de types 

## Principe

- mot-clé `type` 
- permet de déclarer des types simples à partir des types existants :

```haskell
type Point = (Float, Float)
```

- un nom de type doit commencer par une majuscule
- limitations : pas de nouvelle valeur, pas de type récursif


## Exemple de synonymes de types 

```haskell
type Point = (Float, Float)      -- synonymes de types 
type Segment = (Point, Point)

creerSegment :: Float -> Float -> Float -> Float -> Segment
creerSegment x0 y0 x1 y1 = (p0, p1)
    where p0 = (x0, y0)
          p1 = (x1, y1)

longueurSegment :: Segment -> Float
longueurSegment ((x0,y0),(x1,y1)) = sqrt $ dx*dx + dy*dy
    where dx = x1-x0
          dy = y1-y0

changerSens :: Segment -> Segment
changerSens (p0,p1) = (p1,p0)

main = do
    print $ changerSens (creerSegment 0.0 0.0 3.0 4.0)
    print $ longueurSegment (creerSegment 0.0 0.0 3.0 4.0)
```


## Synonymes de types paramétriques 

- les synonymes de types peuvent être paramétriques (polymorphes) :

```haskell
type Point a = (a, a)

-- type Segment non paramétrique
type Segment = (Point Float, Point Float)

-- type Segment paramétrique
type Segment a = (Point a, Point a)
```

- on peut aussi spécifier des classes de types mais on le fait plutôt au niveau des fonctions associées

## Exemple de synonymes de types paramétriques 

```haskell
-- synonymes de types paramétriques
type Point a = (a, a)
type Segment a = (Point a, Point a)

creerSegment :: a -> a -> a -> a -> Segment a
creerSegment x0 y0 x1 y1 = (p0, p1)
    where p0 = (x0, y0)
          p1 = (x1, y1)

-- classe Floating nécessaire pour la fonction sqrt
longueurSegment :: Floating a => Segment a -> a
longueurSegment ((x0,y0),(x1,y1)) = sqrt $ dx*dx + dy*dy
    where dx = x1-x0
          dy = y1-y0

changerSens :: Segment a -> Segment a
changerSens (p0,p1) = (p1,p0)
```


# Types algébriques 

## Principe 

- types de données composés dont les valeurs sont enveloppées dans des constructeurs et manipulées via des filtrages par motifs
- utilité : modéliser des données, implémenter des contraintes, EDSL (Embedded Domain Specific Language)...

## Déclaration 

- mot-clé `data` 
- permet de déclarer des types algébriques :

```haskell
Prelude> data Forme = Carre | Rectangle 
Prelude> :type Carre 
Carre :: Forme
```

- un nom de type ou de valeur doit commencer par une majuscule
- possibilité de dériver des instances de classes pour un type :

```haskell
Prelude> data Forme = Carre | Rectangle deriving (Show)
Prelude> print Carre
Carre
```


## Types énumérations 

- types de données définis par leurs valeurs possibles :

```haskell
data Forme = Carre | Rectangle
```


## Exemple de types énumérations 

```haskell
-- une donnée du type Forme peut prendre les 
-- valeurs Carre et Rectangle
data Forme = Carre 
           | Rectangle

-- fonction à un paramètre de type forme, avec 
-- filtrage par motif
afficherForme :: Forme -> String
afficherForme Carre     = "je suis un carre"
afficherForme Rectangle = "je suis un rectangle"

main = print $ afficherForme Carre
```


## Constructeurs de types 

- les valeurs peuvent contenir des données d'un type pré-existant :

```haskell
data Forme = Carre Float | Rectangle Float Float
```

- une valeur est en fait un constructeur de type qui peut prendre des paramètres

```haskell
Prelude> data Forme = Carre Float | Rectangle Float Float
Prelude> :type Carre
Carre :: Float -> Forme
```

- un type énumération est donc un type algébrique dont les constructeurs ne prennent aucun paramètre


## Exemple de constructeurs de types 

```haskell
data Forme = Carre Float 
           | Rectangle Float Float

afficherForme :: Forme -> String
afficherForme (Carre c)
    = "carre de cote " ++ show c
afficherForme (Rectangle w h)
    = "rectangle de longueur " ++ show w 
    ++ " et de largeur " ++ show h

main = print $ afficherForme (Carre 12)
```


## Types paramétriques 

- on peut utiliser des types paramétriques (polymorphes) :

```haskell
-- type algébrique paramétrique 
data Forme a = Carre a 
             | Rectangle a a

-- ici le type paramétré du type algébrique doit 
-- être de la classe Show
afficherForme :: Show a => Forme a -> String
afficherForme (Carre c)
    = "carre de cote " ++ show c
afficherForme (Rectangle w h)
    = "rectangle de longueur " ++ show w 
    ++ " et de largeur " ++ show h

main = print $ afficherForme (Carre 12)
```


## Exemple de types algébriques  

```haskell
-- inspiré de http://shuklan.com/haskell/

-- définition de types algébriques d'énumération
data UniteSI = Metre | Kilogramme deriving (Show)
data UniteUK = Yard | Pound deriving (Show)

-- définition d'un type algébrique paramétrique
data Mesure a = MesureSI a UniteSI
              | MesureUK a UniteUK
              deriving (Show)
```

* * *

```haskell
-- fonction de conversion entre les systèmes SI et UK
convert :: Floating a => Mesure a -> Mesure a
-- conversion SI vers UK avec un filtrage par motif
convert (MesureSI m Metre)      = MesureUK (1.0936 * m) Yard
convert (MesureSI m Kilogramme) = MesureUK (2.2046 * m) Pound
-- conversion UK vers SI avec un case
convert (MesureUK m u) = case u of
    Yard   -> MesureSI (0.9144 * m) Metre
    Pound  -> MesureSI (0.4536 * m) Kilogramme

main = do
    print $ convert (MesureSI 39 Metre)
    print $ convert (MesureUK 2948 Pound)
```


## Types enregistrements 

- on peut nommer les champs d'un type algébrique :

```haskell
data Personne = Personne
    { nom    :: String
    , age    :: Int
    } deriving (Show)

main = do
    print $ Personne {age = 42, nom = "toto"}
    print $ Personne "toto" 42
```


## Types récursifs 

- un type algébrique peut être défini récursivement 
- c'est-à-dire en s'utilisant lui-même dans sa définition
- ce qui permet de définir des chaînages, arborescences...

```haskell
-- type liste d'entiers
data Liste = Nil | Cons Int Liste

-- fonction d'affichage pour notre Liste
afficherListe :: Liste -> String
afficherListe Nil = ""
afficherListe (Cons i l) = show i ++ " " ++ afficherListe l

main = print $ afficherListe l
    where l = Cons 1 (Cons 2 (Cons 3 Nil))
```


## Exemple de type récursif 


![](dot/adt_arbre.svg){width=30%}

* * *

```haskell
data Arbre a = Nil 
             | Noeud a (Arbre a) (Arbre a)
             deriving (Show)

sommeArbre :: Num a => Arbre a -> a
sommeArbre Nil = 0
sommeArbre (Noeud x xg xd) 
    = x + (sommeArbre xg) + (sommeArbre xd)

main = do
    let arbre 
        = Noeud 42 (Noeud 13 Nil Nil) (Noeud 37 Nil Nil)
    print $ arbre
    print $ sommeArbre arbre
```


## Types algébriques généralisés 

- généralisation des types algébriques 
- permet de spécifier explicitement le type des constructeurs
- Generalized Algebraic Data Type (GADT) 

## Exemple avec un type algébrique 

- DSL de calcul arithmétique
- ici `eval` retourne le même type pour tous les constructeurs 

```haskell
-- type algebrique classique
data Expr = Nb Int
          | Add Expr Expr 

-- eval retourne toujours un Int
eval :: Expr -> Int
eval (Nb n)      = n
eval (Add e1 e2) = eval e1 + eval e2

-- teste le calcul "13 + 37"
main = print $ eval $ (Nb 13) `Add` (Nb 37)
```


## Exemple avec un type algébrique généralisé 

- ajout d'un constructeur `Equ` pour tester l'égalité
- `eval` retourne désormais des types différents selon les constructeurs

* * *

```haskell
{-# LANGUAGE GADTs #-}

-- type algébrique généralisé
data Expr a where
    Nb  :: Int  -> Expr Int
    Add :: Expr Int -> Expr Int -> Expr Int
    Equ :: Expr Int -> Expr Int -> Expr Bool

-- ici eval retourne un Int (constructeurs Nb et Add) 
-- ou un Bool (constructeur Equ)
eval :: Expr a -> a
eval (Nb n) = n
eval (Add e1 e2) = eval e1 + eval e2
eval (Equ e1 e2) = eval e1 == eval e2

-- teste "(13 + 37) == 42"
main = print $ eval $ ((Nb 13) `Add` (Nb 37)) `Equ` (Nb 42)
```


## Exemple avec un type algébrique généralisé et polymorphisme 

- à la place des `Int`, on spécifie un type quelconque
- mais qui respecte certaines classes selon les constructeurs demandés

* * *

```haskell
{-# LANGUAGE GADTs #-}

-- type algébrique généralisé avec polymorphisme
-- le constructeur Add requiert la classe Num (opérateur +)
-- le constructeur Equ requiert la classe Eq (opérateur ==)
data Expr a where
    Nb  :: a -> Expr a
    Add :: Num a => Expr a -> Expr a -> Expr a  
    Equ :: Eq a => Expr a -> Expr a -> Expr Bool

eval :: Expr a -> a
eval (Nb n) = n
eval (Add e1 e2) = eval e1 + eval e2
eval (Equ e1 e2) = eval e1 == eval e2

main = print $ eval $ (Nb 42) `Equ` ((Nb 13) `Add` (Nb 37))
```


# Modules et types abstraits 

## Principe 

- répartir le code dans différents fichiers
- spécifier les fonctions et symboles accessibles depuis l'extérieur du module
- modules standards (c.f. [hackage](http://hackage.haskell.org) ou [hoogle](https://www.haskell.org/hoogle))

## Importer un module 

- import simple :

```haskell
import Data.Char
main = do
    print $ toUpper 'a'            -- ok 
    print $ Data.Char.toUpper 'a'  -- ok 
```

- import avec nommage obligatoire :

```haskell
import qualified Data.Char 
main = do
    print $ toUpper 'a'            -- erreur
    print $ Data.Char.toUpper 'a'  -- ok
```


## Importer un module avec renommage 

- import avec renommage possible :

```haskell
import Data.Char as DC
main = do
    print $ DC.toUpper 'a'         -- ok
    print $ toUpper 'a'            -- ok
    print $ Data.Char.toUpper 'a'  -- ok
```

- import avec renommage obligatoire :

```haskell
import qualified Data.Char as DC
main = do
    print $ DC.toUpper 'a'         -- ok
    print $ toUpper 'a'            -- erreur
    print $ Data.Char.toUpper 'a'  -- erreur
```


## Importer un module totalement ou partiellement 

- importer tout un module :

```haskell
import Data.Char
main = do
    print $ toUpper 'a'
```

* * *

- spécifier des symboles à importer :

```haskell
import Data.Char (toUpper)
main = do
    print $ toUpper 'a'
    print $ toLower 'A'  -- erreur
```

- spécifier des symboles à ne pas importer :

```haskell
import Data.Char hiding (toLower)
main = do
    print $ toUpper 'a'
    print $ toLower 'A'  -- erreur
```


## Définir un module 

- un seul module par fichier
- le nom du module commence par une majuscule
- le module et le fichier doivent avoir le même nom

```haskell
-- Math.hs
module Math where
moitie :: Int -> Int
moitie = (flip div) 2
```

```haskell
-- toto.hs
import Math
main = print $ Math.moitie 42
```

## Spécifier les symboles et fonctions exportés d'un module 

```haskell
-- Math.hs
module Math (double) where  -- le module exporte double

-- fonction interne au module
moitie :: Int -> Int
moitie = (flip div) 2

-- fonction exportée
double :: Int -> Int
double = (*2)
```

```haskell
-- toto.hs
import Math
main = print $ Math.moitie 42  -- erreur
main = print $ Math.double 42  -- ok
```


## Types abstraits 

- type défini par les opérations possibles, indépendamment de sa représentation 
- en Haskell, peut être implémenté par un module spécifiant les symboles et fonctions exportés

* * *

```haskell
-- StackModule.hs
-- module implémentant un type abstrait de pile
module StackModule (Stack, empty, push, pop, top) where

-- type pile (ici on utilise un type algébrique mais 
-- on aurait pu utiliser une liste...)
data Stack a = Nil | Cons a (Stack a)

-- pile vide
empty :: Stack a
empty = Nil

-- empiler un élément
push :: a -> Stack a -> Stack a
push = Cons 
...
```

* * *

```haskell
import StackModule

main = print $ top (push 12 (empty))
    -- utilisation du type abstrait
```


# Classes de types et instances de classes 

## Principe 

- une classe de types spécifie les opérations que doivent supporter les types de la classe
- permet de spécifier des fonctions polymorphes avec des contraintes de types 
- utilisation : 

    - déclarer des classes
    - instancier des types à partir des classes déclarées
    - spécifier des contraintes de types 

## Classes standards

* * *

![](files/cm-haskell/classes.svg){width="60%"}


## Contraintes de types 

- spécifie les classes que doit respecter un type polymorphe 

```haskell
-- calcule la somme des éléments d'une liste;
-- le type des éléments de la liste doit être 
-- de la classe Num (opérateur +)
sommeListe :: Num a => [a] -> a
sommeListe = foldl (+) 0
```


## Déclarer une classe de types 

- mot-clé `class`
- le nom de la class doit commencer par une majuscule
- indiquer les opérations de la classe (avec leur type)
- éventuellement, on peut donner une implémentation par défaut

* * *

```haskell
-- pour éviter les conflits avec les opérateurs de Eq'
import Prelude hiding ((==),(/=))

-- déclare une nouvelle classe Eq' qui demande 
-- deux opérateurs (==) et (/=)
class Eq' a where
    (==) :: a -> a -> Bool
    (/=) :: a -> a -> Bool

    -- implémentation par défaut (optionnelle);
    -- évite d'avoir à écrire les deux 
    -- implémentations lors de l'instanciation
    x /= y = not (x == y)
    x == y = not (x /= y)
```


## Héritage de classes 

- permet d'hériter des fonctions de la classe de base
- et d'en ajouter de nouvelles

```haskell
-- déclaration d'une classe Ord' héritant de Eq'
-- donc un type instanciant Ord' doit également 
-- instancier Eq' (i.e. implémenter == et /=)
class (Eq' a) => Ord' a  where
    compare              :: a -> a -> Ordering
    (<), (<=), (>=), (>) :: a -> a -> Bool
    max, min             :: a -> a -> a
```


## Instances de classes 

- spécifie qu'un type appartient à une classe de types
- et implémente donc les fonctions et opérations de la classe :

```haskell
-- définit un type Animal
data Animal = Panda | Poulpe 

-- instancie la classe Eq' pour le type Animal
instance Eq' Animal where
    Panda == Panda = True
    Poulpe == Poulpe = True
    _ == _ = False

    -- inutile d'implémenter /= 
    -- car il y a déjà une implémentation par défaut
    -- (utilisant ==)
```

* * *

- le type peut ensuite être utilisé dans toutes les fonctions polymorphes
  utilisant la contrainte de type correspondante

```haskell
-- fonction polymorphe avec contrainte de type
-- (type de classe Eq')
tester :: Eq' a => a -> a -> String
tester x y = if x == y then "oui" else "non"

-- applique la fonction sur des valeurs de type Animal
-- (de classe Eq')
main = print $ tester Poulpe Panda
```


## Instances dérivées 

- dans certains cas, Haskell peut déterminer automatiquement comment instancier une (ou plusieurs) classe
- il suffit alors de spécifier les classes à instancier, lors de la définition du type :

```haskell
-- déclare un type qui instancie automatiquement 
-- la classe Show
data Animal = Panda | Poulpe deriving (Show)

-- affiche une donnée du nouveau type 
-- (print utilise la fonction show demandée 
-- dans la classe Show)
main = print Poulpe
```


# Les types Maybe et IO 

## Généralités sur Maybe et IO 

- types fournis par la bibliothèque standard
- très utilisés en Haskell
- et permet d'illustrer des classes classiques (Functor, Monad...)

## Le type Maybe 

- permet de représenter une valeur optionnelle
- utile pour gérer des erreurs
- déclaration (dans la bibliothèque standard) :

```haskell
data Maybe a = Just a | Nothing deriving (Eq, Ord)
```

- bibliothèque `Data.Maybe` : `isNothing` `isJust` `fromJust` ...

## Exemple avec Maybe 

```haskell
import System.Environment
import Data.Maybe

-- fonction qui retourne l'éventuelle tête de liste, avec un Maybe
safeHead :: [a] -> Maybe a
safeHead [] = Nothing
safeHead (x:xs) = Just x

main = do 
    -- recupere les arguments de la ligne de commande
    args <- getArgs
    -- recupere le premier argument, s'il existe
    let h = safeHead args
    -- affiche l'argument ou un message d'erreur
    print $ if isNothing h then "erreur : liste vide" 
                           else fromJust h
```


## Le type IO 

- permet de réaliser des entrées/sorties (Input/Output)
    tout en restant dans le cadre fonctionnel pur
- intuitivement, implémente une action dans le «monde réel» :

```haskell
type IO = World -> World  -- illustration (pas de type World en Haskell)
```

- plus exactement, une action IO est une fonction qui prend un «monde réel» et retourne un «monde réel» modifié et une valeur :

```haskell
type IO a = World -> (a, World)  -- illustration
```

* * *

- un programme Haskell est une action IO (avec ou sans valeur)

```haskell
main :: IO ()
```

- Haskell propose des fonctionnalités pour manipuler les actions IO

- un code bien conçu réduit et regroupe au maximum les actions IO 

## Exemple d'utilisation des actions IO 

```haskell
import Data.Char  -- pour la fonction toUpper
main :: IO ()
main = do
    putStrLn "coucou"
    texte1 <- getLine
    texte2 <- return "toto"
    putStrLn $ map toUpper (texte1++texte2)
    let x = 13
        y = 37
    print x
    (putStrLn.show) y
```


## Quelques fonctions manipulant des actions IO 

- `getLine` : saisit un texte

```haskell
getLine :: IO String
```

- `putStrLn` : affiche un texte

```haskell
putStrLn :: String -> IO ()
```

- `print` : affiche une valeur (de classe Show)

```haskell
print :: Show a => a -> IO ()
```

* * *

- exemple :

```haskell
main = do
    putStrLn "coucou"
    texte1 <- getLine
    print x
```


## Construction et déconstruction d'une action IO 

- `<-` : extrait la valeur contenue dans une action IO :

```haskell
line <- getLine
-- on a : getLine :: IO String
-- or <- permet d'extraire la valeur de l'IO
-- donc line :: String 
```

- `return` : encapsule une valeur dans une action IO (qui ne modifie pas le «monde réel») :

```haskell
return :: a -> IO a
return x = \ world -> (x, world)  -- illustration 
```

* * *

- exemple :

```haskell
main = do
    texte1 <- getLine
    texte2 <- return "toto"  -- en vrai, on ferait : let texte2 = "toto"
    putStrLn $ texte1 ++ texte2
```


## Remarque sur return

- En Haskell, `return` est une fonction comme une autre. 
- C'est donc différent de la structure de contrôle des langages impératifs.

```haskell
main = do
    texte1 <- getLine

    texte2 <- return "toto"
        -- return est une fonction classique, 
        -- qui produit juste une valeur.

    putStrLn $ texte1 ++ texte2
        -- L'exécution continue donc normalement 
        -- après un return.
```


## Notation do 

- permet de définir une séquence d'actions IO
- raccourci syntaxique pratique (mais pas indispensable)
- syntaxe volontairement proche d'un langage impératif
- chaque élément de la séquence doit être une action IO, donc on doit utiliser `let` à la place de `let in`
- exemple :

```haskell
main = do
    let x = 13
        y = 37
    print x
```


## Séquence d'actions IO 

- `>>` : «then», chaîne deux actions IO

```haskell
-- type 
(>>) :: IO a -> IO b -> IO b
-- utilisation
main = (print 13) >> (print 37)
-- code équivalent en notation do
main = do
    print 13
    print 37
```

* * *

- `>>=` : «bind», récupère la valeur d'une action IO et lui applique une fonction qui produit une action IO

```haskell
-- type 
(>>=) :: IO a -> (a -> IO b) -> IO b
-- utilisation
main = getLine >>= putStrLn
-- code équivalent en notation do
main = do
    l <- getLine
    putStrLn l
```


## Exemple d'utilisation des actions IO, sans la notation do 

```haskell
main = putStrLn "coucou"
       >> getLine
          >>= (\texte1 -> getLine 
                          >>= (\texte2 -> putStrLn $ 
                                    map toUpper (texte1++texte2))) 
              >> print x
                 >> (putStrLn.show) y
                    where x = 13
                          y = 37

-- moralité : utiliser la notation do...
```


## Fonctions haut-niveau sur des actions IO : sequence 

```haskell
Prelude> :type sequence
sequence :: Monad m => [m a] -> m [a]

Prelude> sequence [getLine, return "toto"]
blabla
["blabla","toto"]
-- ici, sequence :: [IO String] -> IO [String]

Prelude> sequence [putStrLn "toto", return ()]
toto
[(),()]
-- ici, sequence :: [IO ()] -> IO [()]
```


## Fonctions haut-niveau sur des actions IO : mapM 

```haskell
Prelude> :type mapM
mapM :: Monad m => (a -> m b) -> [a] -> m [b]
Prelude> mapM putStrLn ["toto", "blabla"] 
toto
blabla
[(),()]
-- ici, mapM :: (String -> IO ()) -> [String] -> IO [()]
```


## Fonctions haut niveau sur des actions IO : interact 

- fichier `toto.hs` :

```haskell
import Data.Char
-- interact :: (String -> String) -> IO ()
main = interact (map toUpper)
```

- terminal :

```text
$ runghc toto.hs 
toto
TOTO
panda
PANDA
```


## Fonctions haut niveau sur des actions IO : writeFile/readFile 

- fichier `toto.hs` :

```haskell
import System.IO
main = do
    -- writeFile :: FilePath -> String -> IO ()
    getLine >>= writeFile "output.txt" 
    -- readFile :: FilePath -> IO String
    readFile "output.txt" >>= print.length
```

- terminal :

```haskell
$ runghc toto.hs 
blabla
6
$ cat output.txt 
blabla
```


# Les classes Functor, Monad...

## Généralités 

- en plus des classes de base (Eq, Num...), la bibliothèque standard fournit des classes de plus haut niveau (Functor, Monad...) et instancie les types standard correspondants
- ces classes représentent des concepts bien définis, avec des opérations et des propriétés particulières, qui permettent de structurer le code
- attention : quand on instancie ces classes, le compilateur vérifie que les opérations sont bien implémentées mais pas qu'elles possèdent les propriétés demandées

## Quelques classes 

![<https://wiki.haskell.org/Typeclassopedia>](files/cm-haskell/typeclassopedia_diagram.png)


## La classe Functor 

- classe des types dans lesquels on peut mapper des fonctions $\rightarrow$ généralise la notion d'évaluation de fonction
- déclaration :

```haskell
class  Functor f  where
    fmap :: (a -> b) -> f a -> f b
    -- équivaut à fmap :: (a -> b) -> (f a -> f b)
    -- c'est-à-dire que fmap prend une fonction classique 
    -- et retourne une fonction applicable sur un foncteur 
    -- (cette transformation s'appelle un «lift»)
```

## La classe Functor pour le type liste 

- instanciation :

```haskell
instance  Functor []  where
    fmap = map
```

- utilisation :

```haskell
Prelude> fmap (*2) [1..4]
[2,4,6,8]
```


## La classe Functor pour le type IO 

- instanciation :

```haskell
instance Functor IO where  
    fmap f ioa = ioa >>= (return . f)

    -- ce qui équivaut à :
    fmap f ioa = do  
        result <- ioa  
        return (f result)  
```

* * *

- utilisation :

```haskell
Prelude> fmap (*2) (return 3) 
6  -- de type IO Int (enfin presque)
Prelude> fmap (length) getLine
panda
5  
```

```haskell
-- rappel :
length :: [a] -> Int
getLine :: IO String
fmap (length) getLine :: IO Int
fmap (length) :: Functor f => f [a] -> f Int
```


## La classe Functor pour le type Maybe 

- instanciation :

```haskell
instance  Functor Maybe  where
    fmap _ Nothing  =  Nothing
    fmap f (Just x) =  Just (f x)
```

- utilisation :

```haskell
Prelude> fmap (*2) (Just 3)
Just 6
Prelude> fmap (length) (Just "toto")
Just 4
```

```haskell
-- rappel :
length :: [a] -> Int
Just "toto" :: Maybe [Char]
fmap (length) (Just "toto") :: Maybe Int
fmap (length) :: Functor f => f [a] -> f Int
```


## Les lois de la classe Functor 

1. mapper la fonction identité retourne un foncteur identique :

```haskell
fmap id n == n
```

$\rightarrow$ on peut changer la valeur d'un foncteur mais pas sa structure

2. le mapping d’une composition produit le même resultat que la composition des mappings :

```haskell
fmap (f . g) == (fmap f) . (fmap g)
```

$\rightarrow$ on peut mapper successivement des fonctions ou mapper une fois leur composition

## Vérifier que Maybe respecte la loi 1 des foncteurs 

```text
soit P(n) défini par : fmap id n == n

fmap id Nothing = Nothing, par définition de fmap 
donc P(Nothing) est vrai

fmap id (Just x) = Just (id x), par définition de fmap
                 = Just x, par définition de id
donc P(Just x) est vrai

donc P(n) est vrai pour tout n de type Maybe
```


## Vérifier que Maybe respecte la loi 2 des foncteurs 

```text
soit Q(n) défini par : fmap (f.g) n == fmap f (fmap g n)

fmap (f.g) Nothing = Nothing, par définition de fmap 
fmap f (fmap g Nothing) = fmap f Nothing, par définition de fmap (2nd fmap)
                        = Nothing, par définition de fmap
donc Q(Nothing) est vrai

fmap (f.g) (Just x) = Just ((f.g) x), par définition de fmap
                    = Just (f (g x)), par définition de (.)
fmap f (fmap g (Just x)) = fmap f (Just (g x)), par définition de fmap (2nd fmap)
                         = Just (f (g x)), par définition de fmap
donc Q(Just x) est vrai

donc Q(n) est vrai pour tout n de type Maybe
```


## La classe Functor pour un nouveau type ArbreBinaire 

```haskell
data ArbreBinaire a = Nil 
    | Noeud a (ArbreBinaire a) (ArbreBinaire a)
    deriving Show

instance Functor ArbreBinaire where
    fmap _ Nil = Nil
    fmap f (Noeud x xg xd) 
        = Noeud (f x) (fmap f xg) (fmap f xd)

main = print $ fmap (*2) (Noeud 42 (Noeud 13 Nil Nil)
                                   (Noeud 37 Nil Nil))
```


## Vérifier que ArbreBinaire respecte la loi 1 des foncteurs 

```text
soit P(n) défini par : fmap id n == n

fmap id Nil = Nil, par définition de fmap
donc P(Nil) est vrai

soient xg et xd tels que P(xg) et P(xd), alors, quelque soit x, 
fmap id (Noeud x xg xd) = (Noeud (id x) (fmap id xg) (fmap id xd)), déf. de fmap
                        = (Noeud x (fmap id xg) (fmap id xd)), définition de id
                        = (Noeud x xg xd), hypothèses de récurrence
donc P(Noeud x xg xd) est vrai 

donc, par récurrence, P(n) est vrai pour tout n
```


## Vérifier que ArbreBinaire respecte la loi 2 des foncteurs 

```text
soit Q(n) défini par : fmap (f.g) n == fmap f (fmap g n)

fmap (f.g) Nil = Nil 
fmap f (fmap g Nil) = fmap f Nil = Nil
donc Q(Nil) est vrai

soient xg et xd tels que Q(xg) et Q(xd), alors 
fmap (f.g) (Noeud x xg xd) = (Noeud ((f.g) x) (fmap (f.g) xg) (fmap (f.g) xd)
                      = (Noeud (f (g x)) (fmap (f.g) xg) (fmap (f.g) xd)
                      = (Noeud (f (g x)) (fmap f (fmap g xg)) (fmap f (fmap g xd)) 
                      = fmap f (Noeud (g x) (fmap g xg) (fmap g xd))
                      = fmap f (fmap g (Noeud x xg xd))
donc Q(Noeud x xg xd) est vrai 

donc, par récurrence, Q(n) est vrai pour tout n
```


## Intérêt des foncteurs 

- concept répandu
- gestion uniforme via la classe Functor

```haskell
doubler :: (Num b, Functor f) => f b -> f b
doubler = fmap (*2)

data ArbreBinaire a = ...
instance Functor ArbreBinaire where ...

main = do
    print $ doubler (Noeud 42 (Noeud 13 Nil Nil)
                              (Noeud 37 Nil Nil))
    print $ doubler [1..4]
    print $ doubler (return 12)
    print $ doubler (Just 12)
```


## La classe Monad 

- classe de foncteurs permettant de séquencer des fonctions $\rightarrow$ généralise les notions de fonction, évaluation et composition 
- déclaration :

```haskell
class Monad m where
    return :: a -> m a
    (>>=) :: m a -> (a -> m b) -> m b

    (>>) :: m a -> m b -> m b
    x >> y = x >>= \_ -> y

    fail :: String -> m a
    fail msg = error msg
```

* * *

- lois des monades :

```haskell
return x >>= f   =  f x
m >>= return     =  m 
(m >>= f) >>= g  =  m >>= (\x -> f x >>= g)  
```


## La classe Monad pour le type Maybe 

- permet de chaîner des calculs qui peuvent échouer
- instanciation :

```haskell
instance Monad Maybe where
    return         = Just
    Nothing  >>= f = Nothing
    (Just x) >>= f = f x
    fail           = Nothing
```

- utilisation :

```haskell
Prelude> Just 20 >>= return.(*2) >>= return.(+2)
Just 42
Prelude> let safeSqrt x = 
    if x >= 0 then Just (sqrt x) else Nothing
Prelude> Just (-20) >>= safeSqrt >>= return.(+2)
Nothing
```


## La classe Monad pour le type liste 

- chaîner des calculs pouvant retourner plusieurs valeurs
- instanciation :

```haskell
instance Monad [] where
    return x = [x]
    xs >>= f = concat (map f xs)
    fail     = []

-- rappel :
concat :: [[a]] -> [a]
```

- utilisation :

```haskell
Prelude> [1..4] >>= return.(*2) >>= return.(+1)
[3,5,7,9]
Prelude> let f x = [1..x]
Prelude> [3] >>= f >>= return.(flip mod 2)
[1,0,1]
```


## La classe Monad pour le type IO 

- chaîner des actions IO
- instanciation :

```haskell
instance Monad IO where
    return a = ... 
    m >>= k  = ...
```

- utilisation :

```haskell
Prelude> (return 3) >>= (return.(*2)) >>= print
6
Prelude> getLine >>= return.("hello " ++)
toto
"hello toto"
```


## La classe Monad pour un nouveau type 

```haskell
data ArbreBinaire a = Nil 
    | Noeud a (ArbreBinaire a) (ArbreBinaire a)
    deriving Show

instance Monad ArbreBinaire where
    return x = Noeud x Nil Nil
    Nil >>= _ = Nil
    (Noeud x xg xd) >>= f = (Noeud x' (xg >>= f) (xd >>= f))
        where (Noeud x' _ _) = f x
    fail = Nil

main = do
    print $ ab >>= (return . (*2))
        where ab = (Noeud 42 (Noeud 13 Nil Nil)
                             (Noeud 37 Nil Nil))
```


## Quelques fonctions classiques 

```haskell
import Control.Monad

mapM :: Monad m => (a -> m b) -> [a] -> m [b] 
Prelude Control.Monad> mapM (return . (*2)) [1..4]
[2,4,6,8]

filterM :: Monad m => (a -> m Bool) -> [a] -> m [a] 
Prelude Control.Monad> filterM (return.even) [1..4]
[2,4]

foldM :: Monad m => (a -> b -> m a) -> a -> [b] -> m a 
Prelude Control.Monad> foldM (\ acc x -> Just (acc+x)) 0 [1..3]
Just 6

liftM :: Monad m => (a1 -> r) -> m a1 -> m r 
Prelude Control.Monad> liftM (*2) [1..4]
[2,4,6,8]

sequence :: Monad m => [m a] -> m [a] 
Prelude Control.Monad> sequence [Just 3, Just 4] 
Just [3,4]

(>=>) :: Monad m => (a -> m b) -> (b -> m c) -> a -> m c
Prelude Control.Monad> [1..4] >>= (return.(*2) >=> return.(+1))
[3,5,7,9]
```


## Retour sur la notation do 

```haskell
main = 
    print $ do
        x <- Just 3
        y <- Just 4
        return (x*2)
        Just 12 >>= return.(*2)
        return (x+y)
```


## Intérêt des monades 

- permet de gérer des problèmes classiques (erreurs, dépendances, non-déterminisme, effets de bord...)
- fait parti d'un ensemble de classes de types plus général (avec les foncteurs, foncteurs applicatifs, transformations de monades...)


# Exemple d'application : calculatrice 

## Problématique 

- problème de compilation classique
- «comprendre» une expression arithmétique (ici, des entiers et + * ())
- entrée : un texte (par exemple, `"(1+2)*3"`)
- sortie : la sémantique du texte + traitement

    - arbre syntaxique : `(Mul (Add (Nb 1) (Nb 2)) (Nb 3))`
    - calcul : 9
    - génération de code : `(* (+ 1 2) 3)`

- bon exemple d'application d'Haskell (monade, DSL, parsing...) 

## Implémentation d'un arbre syntaxique abstrait 

- sémantique des données : valeurs, opérations
- permet de représenter et de manipuler des données réelles
- implémentation classique : DSL, types

* * *

```haskell
-- Expr.hs
module Expr where

-- type de donnees d'une expression
data Expr = Nb Int 
          | Add Expr Expr 
          | Mul Expr Expr 
          deriving Show

-- evaluer une expression
evalExpr :: Expr -> Int
evalExpr (Nb n) = n
evalExpr (Add e1 e2) = evalExpr e1 + evalExpr e2
evalExpr (Mul e1 e2) = evalExpr e1 * evalExpr e2

-- formater une expression en langage LISP
formatLispExpr :: Expr -> String
formatLispExpr (Nb n) = show n
formatLispExpr (Add e1 e2) = "(+ " ++ (formatLispExpr e1) ++ " " ++ (formatLispExpr e2) ++ ")"
formatLispExpr (Mul e1 e2) = "(* " ++ (formatLispExpr e1) ++ " " ++ (formatLispExpr e2) ++ ")"
```


## Notion de grammaire formelle 

- ensemble de règles permettant de comprendre le texte d'entrée
- définit comment lire le texte
- si possible sans ambiguïté...
- forme de Backus-Naur

```text
-- grammaire en BNF de notre calculatrice

expr ::= term '+' expr | term

term ::= factor '*' term | factor

factor ::= nb | '(' expr ')'

nb ::= '0' | '1' | ... | '9'
```


## Notion de parser 

- objectif : analyser un texte donné pour construire un arbre syntaxique selon une grammaire prédéfinie
- opération trés courante : compilateur, shell, navigateur web, lecture de fichier...
- implémentation directe : `type Parser = String -> Expr`
- implémentation générique : `type Parser a = String -> a`

* * *

- implémentation «monadique» :

    - idée : parser une partie du texte et traiter le reste au parsing suivant
    - simplifie l'implémentation
    - `type Parser a = String -> (a, String)`
    - correspond à une monade : on implémente la grammaire par composition de `Parser` et on réalise le parsing par application de `Parser`

## La bibliothèque Parsec 

- bibliothèque de parsing pour Haskell
- permet de parser des grammaires LL contextuelles
- implémentée par une transformation de monades (monad transformer)
- bibliothèques alternatives : attoparsec...

## Calculatrice avec Parsec + monades 

- Parsec définit :

    - un type `Parser` pour implémenter la grammaire voulue
    - un opérateur `p1 <|> p2` qui retourne le parser `p1` s'il réussit le parsing, sinon  `p2`
    - des fonctions classiques (analyse lexicale, combinaisons de parsers...)
    - une fonction de parsing (`parse`)

- `parse` retourne un `Either ParseError a`

- `Either` est défini par : `data Either a b = Left a | Right b`

* * *

```haskell
-- calc_parsec_monad.hs
import Expr
import System.Environment
import Text.ParserCombinators.Parsec 

exprP :: Parser Expr             -- expr ::= term '+' expr | term
exprP = do t <- termP            -- lit le premier "term"
           (do (char '+')        -- essaie de lire "+expr"
               e <- exprP
               return (Add t e)  -- si on a lu "term+expr", retourne un Add
            <|> return t)        -- sinon retourne le parsing du premier "term"

termP :: Parser Expr             -- term ::= factor '*' term | factor
termP = do f <- factorP 
           (do (char '*')
               t <- termP
               return (Mul f t)
            <|> return f)

factorP :: Parser Expr           -- factor ::= nb | '(' expr ')'
factorP = do (char '(')
             e <- exprP
             (char ')')
             return e
          <|> nbP

nbP :: Parser Expr               -- nb ::= '0' | '1' | ... | '9'
nbP = do n <- many1 digit
         return $ (Nb . read) n
```

* * *

```haskell
main = do
    args <- getArgs 
    let content = concat args
    case parse exprP "(input)" content of 
      Left e -> putStrLn "Error parsing input:" >> print e
      Right r -> print r >> (print $ evalExpr r) >> (putStrLn $ formatLispExpr r)
```

```text
$ runhaskell calc_parsec_monad.hs "(1+2)*3"
Mul (Add (Nb 1) (Nb 2)) (Nb 3)
9
(* (+ 1 2) 3)

$ runhaskell calc_parsec_monad.hs 1 + 2*3
Add (Nb 1) (Mul (Nb 2) (Nb 3))
7
(+ 1 (* 2 3))

$
```


## Calculatrice avec Parsec + foncteurs applicatifs

- foncteur applicatif :

    - généralise les notions de fonction et d'application
    - toute monade est également un foncteur applicatif
    - tout foncteur applicatif est également un foncteur
    - opérateurs :

```haskell
(<$>) :: Functor f     => (a -> b) -> f a -> f b
(<*>) :: Applicative f => f (a -> b) -> f a -> f b
(*>)  :: Applicative f => f a -> f b -> f b
(<*)  :: Applicative f => f a -> f b -> f a
```

* * *

```haskell
Prelude Control.Applicative> (+) 2 3
5
Prelude Control.Applicative> Just (+) <*> Just 3 <*> Just 2 
Just 5
-- Prelude Control.Applicative> Just (+) <*> Just 3
-- Just (+3) 
Prelude Control.Applicative> (+) <$> Just 3 <*> Just 2 
Just 5
-- Prelude Control.Applicative> (+) <$> Just 3
-- Just (+3)
```

* * *

```haskell
-- calc_parsec_applicative.hs
import Control.Applicative hiding (many, (<|>))
import Expr
import System.Environment
import Text.ParserCombinators.Parsec 

-- expr ::= term '+' expr | term
-- essaie de lire "term+expr" (sans consommer l'entree)
-- sinon, lit "term"
exprP :: Parser Expr
exprP = try (Add <$> termP <*> ((char '+') *> exprP))
     <|> termP

 -- term ::= factor '*' term | factor
termP :: Parser Expr
termP = try (Mul <$> factorP <*> ((char '*') *> termP))
     <|> factorP      

 -- factor ::= nb | '(' expr ')'
factorP :: Parser Expr
factorP = try (between (char '(') (char ')') exprP)
       <|> nbP

-- nb ::= '0' | '1' | ... | '9'
nbP :: Parser Expr
nbP = Nb . read <$> many1 digit

main = do
    -- recupere tous les arguments dans un String
    args <- getArgs 
    let content = concat args
    -- parse le String et affiche l'erreur ou le resultat 
    case parse exprP "(input)" content of 
        Left e -> putStrLn "Error parsing input:" >> print e
        Right r -> print r >> (print $ evalExpr r) >> (putStrLn $ formatLispExpr r)
```


## En résumé : calculatrice basique avec arbre syntaxique 

```haskell
import Control.Applicative hiding (many, (<|>))
import System.Environment
import Text.ParserCombinators.Parsec 

data Expr = Nb Int | Add Expr Expr | Mul Expr Expr 

evalExpr (Nb n)      = n
evalExpr (Add e1 e2) = evalExpr e1 + evalExpr e2
evalExpr (Mul e1 e2) = evalExpr e1 * evalExpr e2

exprP   = try (Add <$> termP <*> ((char '+') *> exprP))   <|> termP
termP   = try (Mul <$> factorP <*> ((char '*') *> termP)) <|> factorP
factorP = try (between (char '(') (char ')') exprP)       <|> nbP
nbP     = Nb . read <$> many1 digit

main = do args <- getArgs 
          case parse exprP "(input)" (concat args) of 
            Left e  -> putStrLn "Error parsing input:" >> print e
            Right r -> print $ evalExpr r
```


## En résumé : calculatrice basique avec évaluation directe 

```haskell
import Control.Applicative hiding (many, (<|>))
import System.Environment
import Text.ParserCombinators.Parsec 

exprP   = try ((+) <$> termP <*> ((char '+') *> exprP))   <|> termP
termP   = try ((*) <$> factorP <*> ((char '*') *> termP)) <|> factorP
factorP = try (between (char '(') (char ')') exprP)       <|> nbP
nbP     = (\x -> read x :: Int) <$> many1 digit

main = do args <- getArgs 
          case parse exprP "(input)" (concat args) of 
            Left e  -> putStrLn "Error parsing input:" >> print e
            Right r -> print r
```


## Pour aller plus loin... 

- <https://wiki.haskell.org/Typeclassopedia>
- <https://wiki.haskell.org/All_About_Monads>
- <http://dev.stephendiehl.com/hask/>
- <https://en.wikibooks.org/wiki/Haskell/Category_theory>

