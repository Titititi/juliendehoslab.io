---
title: Présentation du module PF
date: 2019-04-18
---

## Objectifs du module

- avoir un peu de culture générale sur la programmation fonctionnelle
- utiliser un langage fonctionnel (Haskell)
- programmer selon une approche fonctionnelle

## Pré-requis 

- algorithmique
- notions de C++

## Organisation

- 36h de séances encadrées :
    #. généralités, découverte d'Haskell, typage      
    #. fonctions, gardes, filtrage par motif          
    #. fonctions récursives                           
    #. traitements de listes                          
    #. listes en compréhension                        
    #. programmation fonctionnelle en C++             
    #. mini-projets                                   
    #. mini-projets                                   
    #. types algébriques, types abstraits             
    #. classes de types, foncteurs, monades (ou pas)  
    #. mini-projets                                   
    #. mini-projets                                   

## Évaluation

- contrôle continu 
- annales : 
    - [2015-2016 session 1](files/presentation/M1_PF_EX1_2015-2016.pdf)
    - [2015-2016 session 2](files/presentation/M1_PF_EX2_2015-2016.pdf)


## Travaux pratiques

- Du code de base est fourni sur ce [dépôt git](https://gitlab.com/juliendehos/M_PF_etudiant).  Pensez à le forker/cloner.
- TP sous Nixos. Voir la [page consacrée à Nixos](../env/post30-nixos.html)
- Si vous voulez bossez chez vous sur une autre distrib, vous pouvez utiliser
  directement ghc pour les projets simples ou
  [stack](https://docs.haskellstack.org) pour les projets plus complexes.

