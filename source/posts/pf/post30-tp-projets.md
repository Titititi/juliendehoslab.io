---
title: TP PF, Projets
date: 2019-04-18
---

## Base de données de contacts

- On veut implémenter une base de données de contacts (nom, adresse
  email). Pour cela, complétez le fichier `TP_projets/bd.hs` fourni.

```haskell
type Contact = (String, String)     -- (name, email)
type Base = [Contact]

search :: Base -> String -> Bool
...

insert :: Base -> Contact -> Base
...

update :: Base -> Contact -> Base
...

remove :: Base -> String -> Base
...
```

* * *

- Testez le scénario suivant.

```text
search [("toto","toto@ulco.fr"),("tata","tata@ulca.fr")] tutu
False

search [("toto","toto@ulco.fr"),("tata","tata@ulca.fr")] tata
True

insert [("toto","toto@ulco.fr")] ("tata","tata@ulca.fr")
[("tata","tata@ulca.fr"),("toto","toto@ulco.fr")]

insert [("toto","toto@ulco.fr"),("tata","tata@ulca.fr")] ("tata","tata@nimpe.org")
[("toto","toto@ulco.fr"),("tata","tata@ulca.fr")]

update [("toto","toto@ulco.fr")] ("tata","tata@nimpe.org")
[("tata","tata@nimpe.org"),("toto","toto@ulco.fr")]

update [("toto","toto@ulco.fr"),("tata","tata@ulca.fr")] ("tata","tata@nimpe.org")
[("toto","toto@ulco.fr"),("tata","tata@nimpe.org")]

remove [("toto","toto@ulco.fr"),("tata","tata@ulca.fr")] tata
[("toto","toto@ulco.fr")]
```

* * *

- Ré-implémentez en C++. Vous pouvez utiliser de la POO classique avec effets
  de bord. Indications : 

```cpp
// g++ -std=c++11 -Wall -Wextra -o projet_bd.out projet_bd.cpp 
#include <iostream>
#include <map>
#include <string>
using namespace std;

using Contact = pair<string,string>;

ostream & operator<<(ostream & os, const Contact & c) {
    // ...
}

class Base {
    private:
        map<string,string> _data;

    public:
        bool search(const string & name) const {
            // ...
        }

        // ...

        friend ostream & operator<<(ostream & os, const Base & b);
};

ostream & operator<<(ostream & os, const Base & b) {
    // ...
}
```

* * *

- Testez sur le même scénario.

```text
search [(tata, tata@ulca.fr)(toto, toto@ulco.fr)] tutu
0

search [(tata, tata@ulca.fr)(toto, toto@ulco.fr)] tata
1

insert [(toto, toto@ulco.fr)] (tata, tata@ulca.fr)
[(tata, tata@ulca.fr)(toto, toto@ulco.fr)]

insert [(tata, tata@ulca.fr)(toto, toto@ulco.fr)] (tata, tata@ulca.fr)
[(tata, tata@ulca.fr)(toto, toto@ulco.fr)]

update [(toto, toto@ulco.fr)] (tata, tata@nimpe.org)
[(tata, tata@nimpe.org)(toto, toto@ulco.fr)]

update [(tata, tata@ulca.fr)(toto, toto@ulco.fr)] (tata, tata@nimpe.org)
[(tata, tata@nimpe.org)(toto, toto@ulco.fr)]

remove [(tata, tata@ulca.fr)(toto, toto@ulco.fr)] (tata, tata@ulca.fr)
[(toto, toto@ulco.fr)]
```

## Casser le chiffrement de César

Pour rappel, le chiffrement de César consiste à remplacer chaque lettre d'un
message  par la lettre à distance $n$ (modulo 26), dans l'ordre de
l'alphabet.  Par exemple, "hal" est le chiffrement de "ibm" avec $n=25$.
Déchiffrer revient à chiffrer par $-n$.

* * *

Pour casser le chiffrement de César, il suffit de trouver la valeur de
$n$ du message chiffré.  Une méthode brute-force classique consiste à
générer les 26 (dé)chiffrements possibles et à retenir celui dont la
distribution des fréquences d'apparition des différentes lettres est la plus
proche d'une distribution prédéterminée (par exemple, à partir d'un texte de
référence écrit dans la même langue).  Pour trouver la distribution $\hat
f$ la plus proche de la distribution de référence $f$, on cherche à
minimiser la statistique du $\chi^2$, c'est-à-dire à calculer : 

$$\underset{\hat f}{\arg\min} \quad \sum_i
\frac{(\hat f_i - f_i)^2}{f_i}$$

* * *

- Implémentez le chiffrement de César en Haskell.  Pour cela, reprenez le code fourni (TP_projets/cesar) et écrivez les deux fonctions suivantes.

```haskell
decaler :: Int -> Char -> Char
...
chiffrerCesar :: Int -> String -> String
...
```

Vous pouvez utiliser les fonctions `isLower`, `ord` et `chr` du module
`Data.Char`, et supposer que toutes les lettres sont en minuscules.

* * *

- Implémentez l'attaque décrite précédemment, en écrivant les fonctions
  suivantes.

```haskell
compterOccurences :: Char -> String -> Int
...
compterLettres :: String -> Int
...
frequencesUk :: [Float]
frequencesUk = 
    [ 0.082, 0.015, 0.028, 0.043, 0.127, 0.022, 0.02, 0.061, 0.07
    , 0.002, 0.008, 0.04, 0.024, 0.067, 0.075, 0.019, 0.001, 0.06
    , 0.063, 0.091, 0.028, 0.01, 0.024, 0.002, 0.02, 0.001
    ]

calculerFrequences :: String -> [Float]
...
calculerChi2 :: [Float] -> [Float] -> Float
...
casserCesar :: String -> [Float] -> Int
...
```

* * *

- Déchiffrez `ftq qmsxq zqhqd xaef ea ygot fuyq me itqz tq egnyuffqp fa xqmdz
  ar ftq odai`.

- Exprimez la complexité en temps de votre fonction `casserCesar` pour un
  message de taille $N$.

- Écrivez une implémentation C équivalente (ou reprenez celle du module de
  crypto) et comparez les complexités en temps de l'attaque d'un message de
  taille $N$.

- Écrivez une implémentation C++ équivalente utilisant au maximum les
  fonctionnalités de programmation fonctionnelle de la STL. 

## Parcours en spirale d'une matrice 

On veut parcourir une matrice en spirale, du coin en haut à gauche jusqu'au
centre et dans le sens horaire. Par exemple, avec la matrice 

$$\left[
\begin{matrix}
a & b & c & d \\
e & f & g & h \\
i & j & k & l \\
m & n & o & p \\
\end{matrix}
\right]$$

le parcours en spirale donne la liste `[a,b,c,d,h,l,p,o,n,m,i,e,f,g,k,j]`.

Ce genre de parcours est utilisé, par exemple, en synthèse d'images pour
commencer le calcul au centre de l'image (zone la plus intéressante) et ainsi
afficher cette zone plus rapidement.

* * *

Vous aurez bien sûr remarqué que le parcours en spirale peut être implémenté en
répétant les opérations suivantes :

- prendre la première ligne, ici `[a,b,c,d]`, matrice résultat : 

$$\left[
\begin{matrix}
e & f & g & h \\
i & j & k & l \\
m & n & o & p \\
\end{matrix}
\right]$$

- prendre la dernière colonne, ici `[h,l,p]`, matrice résultat : 

$$\left[
\begin{matrix}
e & f & g \\
i & j & k \\
m & n & o \\
\end{matrix}
\right]$$

* * *

- prendre la dernière ligne dans l'ordre inverse, ici `[o,n,m]`, matrice
  résultat : 

$$\left[
\begin{matrix}
e & f & g \\
i & j & k \\
\end{matrix}
\right]$$

- prendre la première colonne dans l'ordre inverse, ici `[i,e]`, matrice
  résultat : 

$$\left[
\begin{matrix}
f & g \\
j & k \\
\end{matrix}
\right]$$

- etc...

* * *

En Haskell, on peut représenter une matrice comme la liste de ses lignes,
chaque ligne étant la liste des colonnes correspondantes. Par exemple, notre
matrice d'exemple peut s'écrire :

```haskell
m = [['a','b','c','d'],
     ['e','f','g','h'],
     ['i','j','k','l'],
     ['m','n','o','p']]`
```

Ainsi, on récupère une ligne très facilement, par exemple la première ligne de
`m` est `head m`.  Pour récupérer une colonne, il suffit de passer par la
matrice transposée.

* * *

Écrivez un programme en Haskell qui calcule le parcours en spirale d'une
matrice.  Pour simplifier, vous pouvez écrire la matrice en dur dans le code
mais on doit pouvoir la remplacer par une autre. Indication : utilisez les
fonctions `transpose`, `splitAt` et `cycle` du module `Data.List`.

S'il vous reste du temps, réimplémentez votre programme en C++ ou autre
langage.

## Trier les colonnes d'une matrice 

On veut trier une matrice par colonne. Par exemple, pour la matrice initiale :

$$\left[
\begin{matrix}
-3 & 29 & -3 \\
-17 & 69 & -17 \\
44 & 3 & 8 \\
\end{matrix}
\right]$$

on veut obtenir la matrice triée :

$$\left[
\begin{matrix}
-3 & -3 & 29 \\
-17 & -17 & 69 \\
8 & 44 & 3 \\
\end{matrix}
\right]$$

* * *

De plus, la matrice initiale doit être lue dans un fichier et la matrice triée
correspondante doit être affichée à l'écran. Le format utilisé pour cela est la
valeur des coefficients dans l'ordre des lignes; les coefficients étant séparés
par des `" "` et les lignes par des `" | "`. Par exemple, la matrice initiale
précédente est donnée par : `"-3 29 -3 | -17 69 -17 | 44 3 8"` et doit donner,
après le tri : `"-3 -3 29 | -17 -17 69 | 8 44 3"`

Écrivez un programme Haskell permettant de répondre à ce problème.  S'il vous
reste du temps, réimplémentez votre programme en C++ ou autre langage.

* * *

**Indications :**

```haskell
import System.Environment (getArgs)
import Data.List (intercalate,sortBy,transpose,unwords,words)
import Data.List.Split (splitOn)

type Row = [Int]
type Matrix = [Row]
...

parseMatrix :: String -> Matrix
...

formatMatrix :: Matrix -> String
...

cmpRows :: Row -> Row -> Ordering
...

sortMatrix :: Matrix -> Matrix
...

main = getArgs 
    >>= readFile.head 
    >>= putStrLn.formatMatrix.sortMatrix.parseMatrix
```

## Calculatrice

On veut implémenter une calculatrice gérant les additions et multiplications
d'entiers, en notation préfixée. La calculatrice doit, en boucle, saisir une
expression au clavier et afficher le résultat. Le programme termine quand on
saisit une ligne vide.

```text
$ ./calculator
Enter expression: 
* 21 2
42
Enter expression: 
* + 10 11 2
42
Enter expression: 

$
```

* * *

- Implémentez cette calculatrice en C++, en complétant le fichier `calculator.cpp`
  fourni.

- Implémentez cette calculatrice en Haskell. Indications:
    - `forever`, `exitSuccess`, `words`
    - définissez un type algébrique `Expr` permettant de représenter une expression
    - écrivez une fonction `parse` qui parse une expression dans les données
      d'entrée et retourne le reste des données

## Recherche de plus court chemin

On veut calculer le chemin le plus court pour aller de Douarnenez à la pointe
de la Torche.

![](files/tp-projets/projet_pcc.png)

* * *

Pour cela, on modélise le réseau routier par un graphe.  Les villes sont
représentées par des noeuds et les routes par des arêtes pondérées par la
distance.  Pour notre problème, la liste des arêtes suffit.

```haskell
[("Douarnenez", "Pouldreuzic", 17),
 ("Douarnenez", "Plomelin",    25),
 ("Plouneour",  "Pouldreuzic",  9),
 ("Plouneour",  "Plomelin",    11),
 ("La Torche",  "Plomelin",    20),
 ("La Torche",  "Plouneour",   10)]
```

* * *

Trouver le plus court chemin entre deux noeuds se résume donc à trouver la
liste d'arêtes permettant de relier les deux noeuds et dont la somme des poids
est minimale.

```haskell
([("Douarnenez","Pouldreuzic",17),
  ("Plouneour","Pouldreuzic",9),
  ("La Torche","Plouneour",10)],
 36)
```

Dans la vraie vie réelle, on utiliserait un algorithme un peu intelligent style
Dijkstra mais ici on se contentera de générer tous les chemins reliant les deux
noeuds puis de retenir le plus court. 

* * *

**Indications :**

```haskell
type Noeud = ... 
type Arete = ...
type Graphe = ...
type Chemin = ...

-- retourne le sous-graphe accessible depuis le noeud
aretesAccessiblesGraphe :: Noeud -> Graphe -> Graphe

-- retourne le noeud suivant de l'arête
noeudSuivantArete :: Noeud -> Arete -> Noeud

-- retourne le graphe privée de l'arête
enleverAreteGraphe :: Arete -> Graphe -> Graphe

-- ajoute l'arête dans le chemin
ajouterAreteChemin :: Arete -> Chemin -> Chemin

-- retourne tous les chemins du graphe reliant les deux noeuds
-- fonction mutuellement récursive avec cheminsSuivants
trouverChemins :: Noeud -> Noeud -> Graphe -> [Chemin]

-- retourne tous les chemins du graphe reliant les deux noeuds, à partir de l'arête
cheminsSuivants :: Noeud -> Noeud -> Graphe -> Arete -> [Chemin]

-- calcule tous les chemins du graphe reliant les deux noeuds et retourne le plus court
plusCourtChemin :: Noeud -> Noeud -> Graphe -> Chemin
```

## Programmation réseau

On veut implémenter un programme client-serveur fonctionnant de la façon suivante :
    - le client se connecte au serveur
    - le client envoie des messages au serveur; à chaque fois, le serveur répond en renvoyant le message 
    - l'échange termine lorsque le client envoie un message vide
    - le serveur peut gérer plusieurs clients simultanément (mais indépendamment)

* * *

<video preload="metadata" controls>
<source src="files/tp-projets/echohs.mp4" type="video/mp4" />
![](files/tp-projets/echohs.png)

</video>

* * *

- Modifiez le projet `echohs` fourni de façon à implémenter le programme en Haskell.

- Idem en C++ avec le projet `echocpp` fourni.

- S'il vous reste du temps, implémentez un programme client-serveur permettant à un client d'interroger une base de données de contacts sur le serveur.

## Lire des fichiers iCalendar

Le format de fichier iCalendar (ICS) permet d'échanger des données de
calendrier.  Par exemple, le fichier ICS suivant définit trois événements, avec
des sommaires, horaires de début et horaires de fin.

* * *

```text
BEGIN:VCALENDAR
PRODID:-//Google Inc//Google Calendar 70.9054//EN
VERSION:2.0
X-WR-CALNAME:M1 info ULCO annee 2013 - 2014
X-WR-TIMEZONE:Europe/Paris
X-WR-CALDESC:Emploi du temps des M1 info annee 2013 - 2014
BEGIN:VEVENT
DTSTART:20150917
DTEND:20150919
SUMMARY:Recherche operationnelle
END:VEVENT
BEGIN:VEVENT
SUMMARY:GAL - E. RAMAT
DTSTART:20150910T070000
DTEND:20150910T100000
END:VEVENT
BEGIN:VEVENT
DTSTART:20150909T083000Z
DTEND:20150909T093000Z
SUMMARY:Reunion de pre-rentree
END:VEVENT
BEGIN:VEVENT
DTSTART;TZID=Europe/Paris:20150326T090000
DTEND;TZID=Europe/Paris:20150326T120000
SUMMARY:Anglais (A. Wagner) en salle 5
END:VEVENT
END:VCALENDAR
```

* * *

Écrivez un programme permettant d'extraire la liste des événements d'un fichier
ICS.  Pour chaque événement, on veut connaître le sommaire de l'événement ainsi
que ses dates et horaires de début et de fin.  On considère que les horaires du
fichier ICS sont des horaires UTC.  Votre programme devra produire les sorties
types suivantes.

```text
$ runhaskell projet_parser.hs 
usage: projet_parser.hs <ICS file>
$ runhaskell projet_parser.hs edt.ics 
("Recherche operationnelle",(2015,9,17,0,0),(2015,9,19,0,0))
("GAL - E. RAMAT",(2015,9,10,7,0),(2015,9,10,10,0))
("Reunion de pre-rentree",(2015,9,9,8,30),(2015,9,9,9,30))
("Anglais (A. Wagner) en salle 5",(2015,3,26,9,0),(2015,3,26,12,0))
```

* * *

**Indications :**

- Définissez un premier type pour représenter une date et un horaire, et un
  second type pour représenter un événement.  L'objectif principal se résume
  alors à récupérer une liste d'événements. 

- Utilisez les fonctions `getArgs` et `readFile` pour récupérer le nom du
  fichier ICS et son contenu.

- Utilisez les fonctions `splitRegex` et `mkRegex` du module `Text.Regex` pour
  récupérer les événements (voir la doc sur
  [Hackage](https://hackage.haskell.org)).

* * *

- Utilisez les expressions régulières Posix pour le parsing interne des
  lignes :

```haskell
Prelude> import Text.Regex.Posix
Prelude Text.Regex.Posix> "toto42" =~ "[0-9]{2}" :: Bool
True
Prelude Text.Regex.Posix> "toto;1984;titi" =~ "toto;([0-9]{2})[0-9]{2};(.*)" :: [[String]]
[["toto;1984;titi","19","titi"]]
```

Pensez à bien gérer les différents formatages possibles de date/horaire du
standard iCalendar.

S'il vous reste du temps, écrivez un programme équivalent en C++ (vous pouvez
vous en sortir assez facilement avec : `getline`, `string::find`,
`string::substr`, `sscanf` et `mktime`).

## Interface graphique

Haskell permet de développer des interfaces graphiques, notamment grâce au
binding gtk2 :

- http://projects.haskell.org/gtk2hs/docs/current/

- http://www.muitovar.com/gtk2hs/

* * *

Écrivez un programme permettant de visualisez graphiquement un fichier ICS (le
nom du fichier est donné dans la ligne de commande).

![](files/tp-projets/projet_gui_screenshot.png)

* * *

Planning de développement conseillé :

- Créer un module permettant de lire un fichier ICS en reprenant le code du mini-projet précédent.
- Créer, dans un autre fichier, un programme qui charge un fichier ICS et l'affiche dans la console.
- Modifier le programme de façon à afficher une fenêtre gtk vide.
- Ajouter un `Calendar` et un `Label` dans la fenêtre.
- Marquer les jours du mois qui correspondent à des événements du fichier ICS.
- Afficher les informations des événements actifs au jour sélectionné.

* * *

Vous pourrez avoir besoin d'écrire les fonctions suivantes, pour comparer des
dates et gérer les événements gtk du `Calendar` :

```haskell
compareDate :: (Int -> Int -> Bool) -> Int -> Int -> Int -> Int -> Int -> Int -> Bool
handleMonthChanged :: Calendar -> Label -> [Event] -> IO ()
handleDaySelected :: Calendar -> Label -> [Event] -> IO ()
```

## Développement web

Yesod est un framework pour développer des applications web en Haskell.  Il
fournit notamment un EDSL (Embedded Domain Specific Language) permettant de
faire du développement web tout en profitant des fonctionnalités classiques du
langage Haskell.  Pour le reste, Yesod est assez comparable aux autres
frameworks modernes comme Rails ou Django : architecture MVC, moteur de
templates, système de routage REST, gestion de données persistantes...

* * *

L'objectif de ce mini-projet est de développer un site web fournissant :

- une page d'accueil, avec un entête et un menu communs à toutes les pages du site
- une page affichant les événements d'un calendrier à partir d'un fichier iCalendar sur le serveur
- une page permettant de chiffrer un message par la méthode de César

* * *

![](files/tp-projets/projet_web_screenshot1.png)

* * *

![](files/tp-projets/projet_web_screenshot2.png)

* * *

![](files/tp-projets/projet_web_screenshot3.png)

* * *

**Indications :**

- En récupérant vos projets précédents, écrivez un module pour lire des
  fichiers ICS et un autre pour chiffrer un texte avec la méthode de César.
- Testez le code suivant et comprenez-le en vous aidant de la documentation de
  [Yesod](http://www.yesodweb.com/book/) (pour ce mini-projet, la partie
  "basics", jusqu'aux formulaires, devrait suffire).

* * *

```haskell
{-# LANGUAGE TypeFamilies, QuasiQuotes, MultiParamTypeClasses, 
    TemplateHaskell, OverloadedStrings #-}

import Control.Applicative
import Data.Text
import System.IO
import Yesod

-- cree un type de classe Yesod pour implementer un site web
data MonSuperSite = MonSuperSite
instance Yesod MonSuperSite

-- definit le routage du site
mkYesod "MonSuperSite" [parseRoutes| 
    /       HomeR   GET
    |]

-- implemente la route "/"
getHomeR = defaultLayout $ do
    -- definit quelques styles
    toWidget [cassius| 
        p
            border: 1px solid black  
            background: rgb(255,220,150)
        |]
    -- lit un fichier du serveur et recupere ses lignes
    contenu <- liftIO $ readFile "/etc/passwd"
    let lignes = Prelude.lines contenu
    -- contenu de la page web, affiche les lignes du fichier
    [whamlet| 
        <h1> Mon super site
        <p>
            $forall l <- lignes
                #{l}<br>
        |]

-- lance un serveur http pour le site, sur le port 3000
main = warp 3000 MonSuperSite
```

* * *

- Ajoutez deux pages à votre site avec un routage et un menu permettant
  d'accéder à toutes les pages.

- Modifiez la page de calendrier pour y afficher un fichier iCalendar prédéfini
  du serveur.

- Modifiez la page sur le chiffrement de César pour y implémenter un formulaire
  qui va bien.

