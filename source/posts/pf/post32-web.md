---
title: Haskell Web 101
date: 2019-10-10
---

# Introduction

## Haskell pour le web

- peu répandu
- mais des bibliothèques et outils intéressants (surtout backend)
- avantages : 
    - quelques serveurs web matures
    - gestion de la concurrence
    - expressivité du langage
    - système de type
    - ...

## Objectif dans ce module

- implémenter un serveur web basique : html, css/bootstrap, formulaire
- mais pas : base de données, frontend...


# Lucid

## Générer du code HTML

- DSL + fonction de rendu
- voir la [doc de lucid](https://hackage.haskell.org/package/lucid)

```haskell
{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text.Lazy.IO as L
import Lucid

main :: IO ()
main = L.putStrLn $ renderText $ h1_ "hello"
```

```text
$ runghc lucid-1.hs 
<h1>hello</h1>
```

## Domain Specific Language

```haskell
main :: IO ()
main = L.putStrLn $ renderText myPage

myPage :: Html ()
myPage = 
    doctypehtml_ $ do
        head_ $ meta_ [charset_ "utf-8"]
        body_ $ do
            h1_ "Ma page"
            p_ $ do
                "aller à "
                a_ [href_ "https://www.haskell.org"] "la page haskell"
            img_ [src_ "https://www.haskell.org/img/haskell-logo.svg"]
```

* * *

![](files/web/lucid-2.png)

## Utiliser Bootstrap

- très similaire à du dev web classique
- voir le [cours du fanboy du web](https://florian-lepretre.herokuapp.com/teaching/web/bootstrap)


```haskell
myPage :: Html ()
myPage = doctypehtml_ $ do
    head_ $ do
        meta_ [charset_ "utf-8"]
        meta_ [name_ "viewport", content_ "width=device-width, initial-scale=1, shrink-to-fit=no"]
        link_ [rel_ "stylesheet", href_ "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"]
    body_ $ div_ [class_ "container"] $ do
        div_ [class_ "row"] $ do
            h1_ "My page"
            p_ "It uses bootstrap !"
        div_ [class_ "row"] "Wait, but I don't like bootstrap !"
```

* * *

![](files/web/bootstrap-1.png)



# Scotty

## Routage d'URL

- serveur de routage léger (idem nodejs + express)
- voir la [doc de scotty](https://hackage.haskell.org/package/scotty)

```haskell
{-# LANGUAGE OverloadedStrings #-}

import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Web.Scotty

main :: IO ()
main = scotty 3000 $ do
    middleware logStdoutDev
    get "/" $ text "Hello !"
    get "/about" $ html "<h1>This is my page !</h1>"
    get "/dumb" $ redirect "/"
```

* * *

<video preload="metadata" controls>
<source src="files/web/pf-web-scotty-1.mp4" type="video/mp4" />

</video>


## Gestion de formulaire

- GET/POST
- gestion des paramètres...

```haskell
{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text.Lazy as L
import Web.Scotty

main :: IO ()
main = scotty 3000 $ do
    get "/" $ text "Hello !"
    get "/echo" $ do
        who <- param "who" `rescue` (\_ -> return "nobody")
        what <- param "what" `rescue` (\_ -> return "nothing")
        text $ L.concat [who, " said ", what]
```

* * *

<video preload="metadata" controls>
<source src="files/web/pf-web-scotty-2.mp4" type="video/mp4" />

</video>

## La monade Scotty

- `liftIO` pour "faire des IO à l'intérieur du serveur"

```haskell
{-# LANGUAGE OverloadedStrings #-}

import Control.Monad.IO.Class (liftIO)
import Web.Scotty

main :: IO ()
main = scotty 3000 $
    get "/" $ do
        liftIO $ putStrLn "we have a request"
        text "Hello !"
```

* * *

<video preload="metadata" controls>
<source src="files/web/pf-web-scotty-3.mp4" type="video/mp4" />

</video>



# MVar

- donnée avec accès atomique (pour les accès concurrents) 
- voir la [doc de MVar](https://hackage.haskell.org/package/base-4.12.0.0/docs/Control-Concurrent-MVar.html)

```haskell
import Control.Concurrent.MVar

model :: [Int]
model = [37]

main :: IO ()
main = do
    modelVar <- newMVar model
    modifyMVar_ modelVar (\ m -> return (13 : m))
    model' <- readMVar modelVar
    print model'
```

```text
$ runghc mvar-1.hs 
[13,37]
```


# Projet d'application

## Problématique

Implémenter un site web permettant d'afficher une gallerie d'images. Une image
est représentée par un nom et une url vers le fichier image sur le web.  La
liste des images de la gallerie est stockée sur le serveur et est donc partagée
par tous les clients. Pour simplifier, le serveur stocke les images (nom + url)
en mémoire.  Côté client, la page contient trois sections : Home, Gallery et
Add.  Elle utilise bootstrap et envoie les données du formulaire au serveur via
la méthode POST.

* * *

<video preload="metadata" controls>
<source src="files/web/pf-web-projet.mp4" type="video/mp4" />
![](files/web/pf-web-projet.png)

</video>

## Planning possible

1. implémenter un serveur scotty tout simple, avec juste une page d'accueil
2. implémenter un module `View`, avec juste une page HTML basique
3. modifier la page pour avoir les 3 sections (Home, Gallery, Add) 
4. modifier la section Gallery pour afficher une image de test
5. intégrer bootstrap et implémenter le menu et les liens internes
6. implementer un module `Model` et l'utiliser dans le code : créer un modèle dans le main, le passer à la vue pour le rendu...
7. implémenter le formulaire dans la section Add
8. implémenter la route côté serveur pour récupérer et afficher les données d'un formulaire
9. utiliser un MVar pour gérer complètement l'ajout d'images dans la gallerie

## Autres indications

- utiliser les documentations indiquées dans le cours, notamment celles sur Hackage
- la fonction `toStrict` permet de convertir un `Text.Lazy` en `Text`
- utiliser le style CSS `flex-wrap` pour rendre la section Gallery responsive


