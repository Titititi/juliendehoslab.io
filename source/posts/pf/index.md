---
title: Programmation Fonctionnelle
---

Introduction à la programmation fonctionnelle (PF) avec Haskell. Module du
Master Informatique de l’ULCO. 
[Dépôt de code.](https://gitlab.com/juliendehos/M_PF_etudiant)

