---
title: TP PF, Haskell
date: 2019-04-18
---

> **Important :**
> 
> - Testez vos fonctions dans un `main` ou avec ghci.
> - Écrivez le type de vos fonctions. 
> - Les exercices marqués d'une étoile (*) <br> sont un peu plus difficiles.

# Outils à utiliser

## hasker2

- installer [hasker2](https://gitlab.com/juliendehos/hasker2)

- créer un nouveau projet : `hasker2 init myproj` puis `cd myproj`


## stack

- compiler : `stack build`

- exécuter `myproj` : `stack run myproj`

- exécuter les tests : `stack test`


## vscode

- aller dans le dossier du projet et lancer vscode : `code .`

- ouvrir un terminal dans vscode et lancer les commandes stack habituelles


# Découverte d'Haskell

## Exécuter du code Haskell

- Lancer ghci et écrivez une expression qui affiche un hello-world.

- Écrivez, dans un fichier, un programme de hello-world. Exécutez-le avec une compilation classique et avec une compilation à la volée.

## Interagir avec les entrée/sortie standards

- Écrivez un programme qui demande le nom de l'utilisateur et répond par un
  message de bienvenue.

- Modifiez votre programme pour faire la même chose mais en boucle.
  Indication : utilisez la fonction `forever` du module `Control.Monad`.

- Modifiez votre programme de façon à quitter quand l'utilisateur saisit
  `"quit"`.  Indication : utilisez la fonction `exitSuccess` du module
  `System.Exit`.

```text
$ runghc toto.hs
Entrez votre nom : Roger
Bonjour Roger !
Entrez votre nom : quit
$
```



## Utiliser les paramètres de la ligne de commande

- Écrivez un programme affiche la liste des paramètres de la ligne de commande.  

```text
$ runghc exo_decouverte_loc.hs toto tata titi
["toto","tata","titi"]
```

- Modifiez votre programme pour que chaque élément de la liste affichée soit un
  tuple composé d'un paramètre et de son numéro.

```text
$ runghc exo_decouverte_loc.hs toto tata titi
[("toto",1),("tata",2),("titi",3)]
```



# Types et opérations de base


## Évaluer des expressions

Donnez le résultat des expressions suivantes. Vérifiez-les **ensuite**
avec ghci.

- `'Z' < 'a'`

- `"abc" <= "ab"`

- `"abc" >= "ac"`

- `1 + 2 * 3`

- `5.0 - 4.2 / 2.1`

- `3 > 4 || 5 < 6 && not (7 /= 8)`

- `if 6 < 10 then 6.0 else 10.0`


## Trouver des erreurs dans des expressions

Toutes les expressions suivantes comportent des erreurs. Trouvez-les.

- `18 mod 7 / 2`

- `if 2 < 3 then 3`

- `1 < 2 and 5 > 3`

- `6 + 7 div 2`

- `4. + 3.5`

- `1.0 < 2.0 or 3 > 4`

- `1.0 = 3`

- `if 4 > 4.5 then 3.0 else 'a'`


## Typer des expressions

Donnez le type des expressions suivantes.

- `2 < 1`

- `4.2 + 2.0`

- `4.2 + 2`

- `4 + 2`

- `(4::Int) + 2`

- `floor 2.3`

- `fromIntegral 3`

* * *

- `succ 2.2`

- `succ 2`

- `succ (2::Int)`

- `succ 'b'`

- `show 2`

- `show 2.4`



# Listes, tuples


## Évaluer des expressions

Donnez le résultat des expressions suivantes.

- `[1,2,3] !! ([1,2,3] !! 1)`

- `head [1,2,3]`

- `tail [1,2,3]`

- `"a":["b","c"]`

- `"abc" ++ "d"`

- `tail "abc" ++ "d"`

- `head "abc" : "d"`

- `([1,2,3] !! 2 : []) ++ [3,4]`

- `[3,2] ++ [1,2,3] !! head [1,2] : []`


## Trouver des erreurs dans des expressions

Toutes les expressions suivantes comportent des erreurs. Trouvez-les.

- `head []`

- `tail []`

- `["n"]:["o","n","!"]`

- `1 ++ 2`

- `head "abc" ++ "d"`


## Typer des expressions

Donnez le type des expressions suivantes.

- `['a', 'b', 'c']` 

- `('a', 'b', 'c')` 

- `[(False, 0), (True, 1)]` 

- `([False, True], [0, 1])` 

- `[tail, init, reverse]` 

- `(1.5,("3",[4,5]))` 

- `[[1,2],[]]` 

- `(['a','b'],[[],[1,2,3]])` 


## Manipuler des listes avec les fonctions prédéfinies

- Définissez une liste `l1` des entiers de 37 à  42.

- Écrivez une expression donnant la taille le `l1`.

- Écrivez une expression donnant la première moitié `l1`.

- Écrivez une expression donnant la seconde moitié `l1`.

- Écrivez une expression donnant un tuple composé des deux moitiés de `l1`.



# Fonctions, fonctions d'ordre supérieur


## Typer des fonctions

Donnez le type des fonctions suivantes.

- `second xs = head (tail xs)`

- `swap (x, y) = (y, x)`

- `pair x y = (x, y)`

- `double x = x*2`

- `palindrome xs = reverse xs == xs`

- `twice f x = f (f x)`


## Calculs de périmètres

- Écrivez une fonction `perimetreDisque` qui calcule périmètre d'un disque de rayon
  $r$.

- Écrivez une fonction `perimetreTriangle` qui calcule périmètre d'un triangle
  $(x_A, y_A, x_B, y_B, x_C, y_C).$


## Prédicats

- Écrivez une fonction `estPair` qui détermine si un entier est pair.

- Écrivez une fonction `estMemeSigne` qui détermine si deux entiers sont de même signe.

- Écrivez une fonction `estTripletPyth` qui détermine si trois entiers forment un triplet pythagoricien.


## Minimum, maximum

- Écrivez une fonction `mini2` qui calcule le minimum de deux entiers.

- Écrivez une fonction `mini3` qui calcule le minimum de trois entiers (en utilisant `mini2`).

- Écrivez une fonction `minimaxi` qui calcule (dans un tuple) le minimum et le maximum de deux entiers.


## Fonctions sur des listes

- Écrivez une fonction `getElem2` qui retourne le 2e élément d'une liste
  donné.  On suppose que la liste contient au moins deux éléments.

- Écrivez une fonction `rmElem2` qui retourne une liste donnée sans son 2e
  élément.  On suppose que la liste contient au moins deux éléments.

- Écrivez une fonction `rotateLeft` qui réalise une rotation cyclique vers la
  gauche d'une liste donnée.


## Lambda-expressions

- Écrivez une lambda-expression `mul42` qui multiplie un entier par 42.

- Écrivez une fonction `foisTroisPlusUn` qui calcule $3x+1$ en
  composant deux lambdas.

- Écrivez une fonction `fSyracuse` qui prend en paramètre un entier $x$
  et retourne une lambda qui divise par 2 si $x$ est pair, ou une lambda
  qui multiplie par 3 et ajoute 1 sinon.


## Évaluation partielle  

- Écrivez une fonction `mul42` qui multiplie un entier par 42 en utilisant
  l'évaluation partielle. 

- Écrivez une fonction `null'` qui teste si un entier vaut zéro. 

- Quel est le type de l’opérateur `(!!)` ? Écrivez une fonction `getElem2'`
  qui retourne le deuxième élément d’une liste en utilisant l’évaluation
  partielle et la fonction `flip`.

## Composition de fonctions 

- Écrivez une fonction `doublePlusUn` qui calcule $2x+1$, en utilisant
  une composition de fonctions.

- Écrivez une fonction `plus42Positif` qui teste si un entier incrémenté de
  42 est positif, en utilisant une composition de fonctions.

- Écrivez une fonction `getElem2''` qui retourne le deuxième élément d'une
  liste, en utilisant la composition.

- Écrivez une fonction `doubleMoinsUn` qui calcule $2x-1$, en utilisant
  une composition de fonctions.  Attention, le symbole '-' a plusieurs
  significations et la soustraction n'est pas commutative...


## Notation «point-free» 

Réécrivez les définitions suivantes en notation «point-free».  Écrivez
également les types correspondants.

- `doubler x = x * 2`

- `tete xs = head xs`

- `fTete xs = 2 * sqrt (head xs)`

- `twice f x = f (f x)`


## Forme curryfiée 

- Écrivez une fonction `fois` qui multiplie deux entiers contenus dans un
  tuple.

- Écrivez une fonction `foisCurry` équivalente mais sous forme curryfiée.

- Écrivez une fonction `fois42` qui multiplie un entier par 42 en utilisant
  la fonction `fois`.

- Écrivez une fonction `fois42Curry` qui multiplie un entier par 42 en
  utilisant la fonction `foisCurry`.


## Curryfication et décurryfication 

- Écrivez une fonction `plus` qui calcule la somme des deux nombres d'un
  tuple.  Testez votre fonction dans un main.

- Écrivez une fonction `curryfier` qui convertit en forme curryfiée, une
  fonction prenant paramètre un tuple de deux nombres.  Testez sur votre
  fonction `plus`.

- Écrivez une fonction `decurryfier` qui convertit une fonction prenant deux
  nombres en paramètre en une fonction prenant un tuple.  Testez sur
  l'opérateur `+`.


## Équation du second degré

- Écrivez une fonction `resoudreEquation` qui résoud une équation du second
  degré.  Indications : retournez un `Maybe (Float,Float)`.

- Testez sur $x^2 + x + 12=0$, $x^2 - 1=0$ et $x^2 - 2x
  +1=0$.



# Filtrages par motif, gardes


## Pattern-matching simples

- Écrivez une implémentation des fonctions de listes `null`, `head` et `tail`
  utilisant le filtrage par motif. 

- Écrivez une implémentation des fonctions de tuples `fst` et `snd` utilisant
  le filtrage par motif. 


## Gardes

- Écrivez une fonction `analyseTemp` qui prend en paramètre un entier
  $t$ et retourne `"frisquet"` si $t<-20$, `"nirvana"` si
  $-20 \le t < 20$ et `"canicule"` si $t \ge 20$.  Votre fonction
  doit utiliser des gardes.

- Même question mais en utilisant des expressions conditionnelles
  (`if-then-else`).




## Fonction safetail

La fonction `tail` produit une erreur si on l'appelle sur une liste vide.  On
voudrait une fonction équivalente mais qui retourne une liste vide dans ce cas.
On considère des listes de `Char` uniquement.

- Écrivez une fonction `safetail1` utilisant une expression conditionnelle.

- Écrivez une fonction `safetail2` utilisant des gardes.

- Écrivez une fonction `safetail3` utilisant un filtrage par motif.


## Opérateurs logiques

*Testez vos fonctions dans un main, en notation préfixe et en notation infixe.*

- Écrivez une fonction `ou` qui implémente l'opérateur disjonction en utilisant un filtrage par motif.

- Écrivez une fonction `ou'` qui implémente l'opérateur disjonction en utilisant une conditionnelle.

- Écrivez une fonction `ou''` qui implémente l'opérateur disjonction en utilisant des gardes.

* * *

- Écrivez une fonction `et` qui implémente l'opérateur conjonction en utilisant un filtrage par motif.

- Écrivez une fonction `et'` qui implémente l'opérateur conjonction en utilisant une conditionnelle.

- Écrivez une fonction `et''` qui implémente l'opérateur conjonction en utilisant des gardes.



# Fonctions récursives


## PGCD

- Écrivez une fonction récursive qui calcule le pgcd.  

Rappel : 

$$P(a,b) = \left\{ \begin{array}{l} a \text{ si } b = 0 \\ P(b, \text{reste de } a \text{ par } b ) \text{ sinon } \end{array} \right.$$



## Factorielle

- Écrivez une fonction récursive qui calcule la factorielle. 

- Écrivez une version récursive terminale.


## Puissance

- Écrivez une fonction récursive qui calcule la fonction puissance sur deux entiers. 

- Écrivez une version récursive terminale.


## Recherche de zéros

- Écrivez une fonction récursive qui résout numériquement l'équation $f(x) = 0$
  par dichotomie.  Votre fonction doit prendre en paramètre la fonction $f$,
  les bornes de l'intervalle de recherche initial et la précision demandée.

- Testez votre fonction avec $f(x) = x^2 - 2x - 8$, l'intervalle $[0, 10]$ et
  la précision $0.1$.


## Sommes

- Écrivez une fonction récursive `sommeEntiers` qui calcule la somme des
  entiers de 1 à $n$.

- Écrivez une fonction récursive `sommeEntiersPairs` qui calcule la somme des
  entiers pairs de 1 à $n$.


## Suite de Fibonacci *

- Écrivez une fonction récursive `fibonacciRecNaif` qui calcule le
  nieme-terme de la suite de Fibonacci en implémentant directement la
  définition : 

$$\begin{array}{l}
 F(0) = 0 \\
 F(1) = 1 \\
 F(n) = F(n-1) + F(n-2)
\end{array}$$

- Écrivez une fonction récursive `fibonacciRecMieux` équivalente mais de complexité linéaire.

* * *

- Comparez les temps de calculs. 

```text
$ time runghc fiboRecNaif.hs 33
3524578
real    0m6.234s
user    0m6.132s
sys 0m0.032s
$ time runghc fiboRecMieux.hs 33
3524578
real    0m0.148s
user    0m0.116s
sys 0m0.028s
```


# Fonctions récursives sur des listes


## Fonctions de base sur des listes

Pour les questions suivantes, pensez bien à écrire les types (génériques) des
fonctions.

- Écrivez une fonction récursive `taille` qui retourne le nombre d'éléments
  d'une liste.

- Écrivez une fonction récursive `element` qui retourne si une valeur est
  dans une liste.

- Écrivez une fonction récursive `mini` qui retourne la valeur minimale
  contenue dans une liste.  On suppose la liste non vide.

* * *

- Écrivez une fonction récursive `replicate'` qui retourne une liste de
  $n$ éléments d'une valeur $x$.

- Écrivez une fonction récursive `at` qui prend en paramètre une liste
  $l$ et un entier $n$ et retourne le $n^e$ élément de
  $l$.



## Fonctions de listes

- Écrivez une fonction récursive `positions` qui prend en paramètres une
  valeur et une liste et retourne les positions où la valeur apparait dans la
  liste.

- Écrivez une fonction récursive `miniMaxi` qui retourne le plus petit et le
  plus grand élément d'une liste.  On suppose que la liste donnée contient au
  moins un élément.

- Écrivez une fonction récursive `rotationListe` qui effectue $n$
  rotations à  gauche d'une liste.

* * *

- Écrivez une fonction récursive `concat2`, équivalente à  l'opérateur
  `++`, qui concatène deux listes. 

- Écrivez une fonction récursive `appliquerFonctions` qui applique une liste
  de fonctions à un entier.

- Écrivez une fonction récursive `extraire` qui extrait les éléments de la
  position $n$ inclue à la position $m$ exclue d'une liste.


## Réimplémentation de fonctions prédéfinies

- Écrivez une fonction récursive `all'` qui teste si tous les éléments d'une
  liste vérifie un prédicat.

- Écrivez une fonction récursive `any'` qui teste si au moins un élément
  d'une liste vérifie un prédicat.

- Écrivez une fonction récursive `take'` qui retourne les $n$ premiers
  éléments d'une liste.

- Écrivez une fonction récursive `drop'` qui «supprime» les $n$
  premiers éléments d'une liste.

* * *

- Écrivez une fonction récursive `takeWhile'` qui retourne les premiers
  éléments d'une liste vérifiant un prédicat.

- Écrivez une fonction récursive `dropWhile'` qui «supprime» les premiers
  éléments d'une liste vérifiant un prédicat.

- Écrivez une fonction récursive `init'` qui retourne une liste privée de son
  dernier élément.

- Écrivez une fonction récursive `last'` qui retourne le dernier élément
  d'une liste.


## Filtrage de liste

- Écrivez une fonction `dupliqueListe` qui duplique tous les éléments d'une
  liste.  Par exemple, `dupliqueListe [42 13 37]` doit retourner `[42, 42,
  13, 13, 37, 37]`.

- Écrivez une fonction `precederListe` qui ajoute un élément donné au début
  de chaque liste d'une liste de liste donnée.  Par exemple, `precederListe 42
  [[1], [], [7, 3]]` doit retourner `[[42, 1], [42], [42, 7, 3]]`.

- Écrivez une fonction `zip'` qui prend deux listes de même taille et
  retourne une liste de tuples.  Par exemple, `zip' ['a', 'b', 'c'] [1, 2,
  3]` doit retourner `[('a',1),('b',2),('c',3)]`.


## Deuxième maximum d'une liste

- Écrivez une fonction `max2` qui retourne le deuxième plus grand élément
  d'une liste.  Indication : utilisez une fonction auxiliaire avec des gardes. 


## Tris *

- Écrivez une fonction récursive `triRapide` qui calcule une liste d'entiers
  selon l'algorithme de tri rapide.  Indication : érivez deux fonctions
  auxiliaires récursives `lesser` (resp. `greater`) qui filtrent les
  éléments inférieurs (resp. strictement supérieurs) à  un entier. 

- Écrivez une fonction récursive `triFusion` qui calcule une liste d'entiers
  selon l'algorithme de tri fusion.  Indication : écrivez une fonction
  auxiliaire récursive `fusion` qui fusionne deux listes triées et utilisez
  la sur les deux moitiés (triées) de la liste à trier. 



# Mapping/filtrage/réduction de listes


## Mapping simple

- Écrivez une fonction `mappingDoubler` qui retourne le double de chaque entier
  d'une liste, en utilisant un mapping.

- Écrivez une fonction récursive `mappingDoubler'` équivalente.


## Filtrage simple

- Écrivez une fonction `filtragePairs` qui retourne les éléments pairs d'une
  liste d'entiers, en utilisant un filtrage.

- Écrivez une fonction récursive `filtragePairs'` équivalente.


## Réduction simple

- Écrivez une fonction `reductionSomme` qui retourne la somme des éléments
  d'une liste d'entiers, en utilisant une réduction.

- Écrivez une fonction récursive `reductionSomme'` équivalente.


## Calculs par réduction

- Écrivez une fonction `factorielleReduction` qui calcule la factorielle, en
  utilisant une réduction.

- Écrivez une fonction `fibonacciReduction` qui calcule le nieme~terme de la
  suite de Fibonacci, où $n \ge 2$, en utilisant une réduction sur des tuples.


## Mapping/filtrage/réduction divers

- Écrivez une fonction `absList` qui retourne les valeurs absolues d'une liste
  d'entiers.

- Écrivez une fonction `maxList` qui retourne le plus grand élément d'une liste
  d'entiers quelconque non vide.

- Écrivez une fonction `first3` qui prend une liste de String et retourne les
  trois premiers caractères de chaque éléments.

- Écrivez une fonction `short` qui prend une liste de String et retourne les
  éléments ayant moins de trois caractères.



# Listes en compréhension


## Entiers pairs, entiers alternés

- Écrivez une expression donnant la liste de tous les entiers pairs de 0 à 42.
  Donnez plusieurs versions différentes.

- Écrivez une expression donnant la liste
  `[2,-3,4,-5,6,-7,8,-9,10,-11,12,-13]` en utilisant une liste en
  compréhension.

- Écrivez le code équivalent en C++.


## Triplets pythagoriciens

- Écrivez une fonction `multiples` qui prend en paramètre un entier $n$
  et retourne la liste des paires multiples de $n$ (sans doublon).

- Écrivez une fonction `tripletsPyth` qui retourne la liste des triplets
  pythagoriciens inférieurs à un entier donné (sans doublon).

- Écrivez le code équivalent en C++ (`multiples` et `tripletsPyth`).


## Quicksort en compréhension

- Écrivez une fonction récursive `quicksort` qui trie une liste par la
  méthode du tri rapide.  Indication : utilisez deux listes en compréhension
  contenant respectivement les éléments inférieurs et les éléments supérieurs
  au pivot.

- Modifiez votre fonction de façon à trier dans l'ordre décroissant.

- Modifiez votre fonction de façon à enlever les doublons.


## Nombres parfaits, nombres premiers

- Écrivez une fonction `multiples` qui retourne la liste des multiples d'un
  entier.  Utilisez une liste en compréhension.

- Écrivez une fonction `premier` qui teste si un nombre est premier en
  utilisant la fonction précédente.  Utilisez la notation «point-free»
  (composition de fonctions).

- Écrivez une fonction `premiers` qui retourne la liste des nombres premiers
  inférieurs à un entier donné (2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37...).

- Écrivez une fonction `parfaits` qui retourne la liste des nombres parfaits
  inférieurs à un entier donné.  Rappel : un entier est parfait s'il est égal
  à la somme de ses multiples sans l'entier lui-même (6, 28, 496).


## Calculs via des listes en compréhension *

- Écrivez une fonction `taille` qui calcule la taille d'une liste en sommant
  une liste en compréhension.

- Écrivez une fonction `produitScalaire` qui calcule le produit scalaire de
  deux listes données.  N'oubliez pas de tester votre fonction...

- Implémentez la fonction `zip` en utilisant la récursivité.  Votre fonction
  doit gérer des listes de tailles éventuellement différentes.

- Implémentez la fonction `zip` en utilisant une liste en compréhension
  (éventuellement très moche).


## Fibonacci en compréhension *

- Écrivez une définition de la suite de fibonacci en utilisant une liste en
  compréhension (infinie et récursive).

- Écrivez une fonction `main` pour tester votre liste.



# Récapitulatif sur les listes


## Réimplémentation de fonctions de traitement 

On rappelle que les fonctions prédéfinies `map` et `filter` ont les types
suivants.

```haskell
map :: (a -> b) -> [a] -> [b]
filter :: (a -> Bool) -> [a] -> [a]
```

- Écrivez une fonction `map1` qui implémente `map` en utilisant la
  récursivité.

- Écrivez une fonction `map2` qui implémente `map` en utilisant une liste
  en compréhension.

* * *

- Écrivez une fonction `filter1` qui implémente `filter` en utilisant la
  récursivité.

- Écrivez une fonction `filter2` qui implémente `filter` en utilisant une
  liste en compréhension.


## Doubler une liste d'entiers

- Écrivez une fonction `doubler1` qui multiplie par 2 les entiers d'une
  liste, en utilisant la récursivité.

- Écrivez une fonction `doubler2` équivalente mais qui utilise une liste en
  compréhension.

- Écrivez une fonction `doubler3` équivalente mais qui utilise des
  traitements de liste (map, filter, fold).


## Premiers éléments d'une liste de paires

- Écrivez une fonction récursive `premiers1` qui prend en paramètre une liste
  de 2-tuples et retourne la liste des premiers éléments de tous ces tuples. 

- Écrivez une fonction `premiers2` équivalente mais qui utilise une liste en
  compréhension. 

- Écrivez une fonction `premiers3` équivalente mais qui utilise des
  traitements de liste.


## Implémentations de concat

- Écrivez une fonction `concat1` qui retourne la liste de tous les éléments
  d'une liste de listes, en utilisant la récursivité (plusieurs implémentations
  possibles).

- Écrivez une fonction `concat2` équivalente mais qui utilise une liste en
  compréhension.

- Écrivez une fonction `concat3` équivalente mais qui utilise des traitements
  de liste.


## Supprimer un élément d'une liste 

- Écrivez une fonction `supprimer1` qui «supprime» un élément d'une liste, en
  utilisant la récursivité.  Que faut-il modifier pour ne supprimer que la
  première occurrence ?

- Écrivez une fonction `supprimer2` équivalente mais qui utilise une liste en
  compréhension.

- Écrivez une fonction `supprimer3` équivalente mais qui utilise des
  traitements de liste.


## Dupliquer chaque élément d'une liste 

On veut écrire une fonction `dupliquer` qui duplique chaque élément d'une
liste.  Par exemple, `dupliquer 3 [1..3]` doit retourner
`[1,1,1,2,2,2,3,3,3]`.

- Écrivez une fonction `dupliquer1` qui utilise la récursivité et l'opérateur
  de concaténation `(++)`.

- Écrivez une fonction `dupliquer1'` qui utilise la récursivité et
  l'opérateur de construction `(:)`.

* * *

- Écrivez une fonction `dupliquer2` qui utilise une liste en compréhension.

- Écrivez une fonction `dupliquer3` qui utilise `foldl` et `replicate`.

- Écrivez une fonction `dupliquer3'` qui utilise `map`, `replicate` et
  `concat`.


## Égalité de listes 

- Écrivez une fonction récursive `egalite1` qui teste si deux listes sont
  égales. 

- Écrivez une fonction `egalite2` équivalente mais qui utilise une liste en
  compréhension.  Indication : utilisez un `zip` et comparez les tailles de
  liste.

- Écrivez une fonction `egalite3` équivalente mais qui utilise des
  traitements de liste.

- Écrivez une fonction `egalite4` équivalente mais qui utilise l'opérateur
  d'égalité sur des listes...


## Liste de taille paire 

- Écrivez une fonction récursive `taillePaire1` qui teste si une liste a un
  nombre pair d'éléments.

- Écrivez une fonction `taillePaire2` équivalente mais qui utilise des
  traitements de liste.

- Écrivez une fonction `taillePaire3` équivalente mais qui utilise les
  fonctions prédéfinies (`even`...).


## Renverser une liste 

- Écrivez une fonction récursive `renverser1` qui renverse une liste
  (équivalent de la fonction prédéfinie `reverse`).

- Écrivez une fonction `renverser2` équivalente mais qui utilise un
  `foldr`.

- Écrivez une fonction `renverser3` équivalente mais qui utilise un
  `foldl`.




# Types algébriques


## Jours de la semaine 

- Écrivez un type de données `Jour` énumérant les jours de la semaine.

- Écrivez une fonction `estWeekend` qui retourne si un jour donné correspond au
  week-end.

- Écrivez une fonction `compterOuvrables` qui retourne le nombre de jours
  ouvrables d'une liste de jours.  Utilisez la notation «point-free».


## Figures géométriques 

- Écrivez un type enregistrement `Figure` permettant de réprésenter des carré
  (côté), rectangle (largeur, hauteur) et disque (rayon).

- Écrivez une fonction `showFigure` qui retourne un texte correspondant à une
  figure, selon le format suivant.

```text
Carre de cote 12.0
Rectangle de largeur 2.0 et de hauteur 12.0
Disque de rayon 2.0
```

- Écrivez une fonction `calculerAire` qui retourne l'aire d'une figure
  donnée.


## Nombres complexes 

- Écrivez un synonyme de type `Complexe` permettant de réprésenter des
  nombres complexes.

- Écrivez une fonction `modComp` qui retourne le module d'un complexe.

- Écrivez une fonction `conjComp` qui retourne le conjugué d'un complexe.

- Écrivez une fonction `addComp` qui retourne l'addition de deux complexes.


## Liste 

- Écrivez un type `Liste` permettant de réprésenter des listes d'entiers.
  Pour cela, définissez un type récursif à deux constructeurs `Cons` et
  `Nil`.

- Écrivez une fonction `showListe` qui retourne un texte décrivant une liste
  (i.e. les entiers de la liste séparés par des espaces).

- Écrivez une fonction `convertListe` qui retourne la `Liste` correspondant
  à une liste Haskell classique.

- Écrivez une fonction `sumListe` qui retourne la somme des éléments d'une
  `Liste`.

* * *

- Écrivez un type `ListeA` permettant de réprésenter des listes génériques,
  ainsi que les fonctions `showListeA`, `convertListeA` et `sumListeA`
  correpondantes.

- Implémentez `ListeA`, `showListeA`, `convertListeA` et `sumListeA` en
  C++ (sans réutilisez les structures de données de la bibliothèque standard).


## Arbre binaire 

Soient les arbres binaires suivants.

![](dot/exo_adt_arbre1.svg){width="70%"}

* * *

- Écrivez un type algébrique `Arbre` modélisant un arbre binaire d'entiers.
  Pour cela, définissez les constructeurs `Noeud0`, `Noeud1` et `Noeud2`
  représentant des noeuds à zéro, un ou deux fils.  Dérivez votre type de la
  classe `Show` pour pouvoir l'afficher.

- Dans un `main`, définissez et affichez les trois arbres précédents.  Quel
  est l'intérêt des constructeurs proposés ci-dessus, par rapport aux
  constructeurs `Nil` et `Noeud` utilisés en cours ?

* * *

- Écrivez une fonction `taille` qui retourne le nombre d'éléments d'un arbre.

- Écrivez une fonction`profondeur` qui retourne la profondeur d'un arbre.

- Écrivez une fonction `estEquilibre` qui teste si un arbre est équilibré.
  Indication : arbre binaire complet, taille, profondeur.

- Réimplémentez tout ça en C++.


## Liste de gens 

Voici une partie de l'arbre généalogique d'Alan Turing :

![](dot/exo_adt_gengens.svg){width="25%"}

- Écrivez un type algébrique `Gengens` permettant de représenter `Kekun` ou
  `Personne`. `Kekun` est défini par son prénom, son père, sa mère et la
  liste de ses enfants.

* * *

- Écrivez une fonction `showPrenom` qui retourne le prénom d'un `Gengens`
  (ou `"personne"` le cas échéant).

- Écrivez une fonction `showPere` qui retourne le prénom du père d'un
  `Gengens`. Idem pour une fonction `showMere`.

- Écrivez une fonction `showEnfants` qui retourne le prénom des enfants d'un
  `Gengens`. Utilisez la fonction `intercalate` du module `Data.List`
  pour séparez les prénoms par `"&"`.

* * *

- Dans un `main`, écrivez la liste des `Gengens` de la famille Turing et
  affichez les prénom, père, mère et enfants de chacun.

- Pourquoi ne peut-on pas afficher un `Gengens` en dérivant le type de la
  classe `Show` ?




# Classes de types


## Angles 

- Écrivez un type `Angle` permettant de réprésenter des angles en degré ou en
  radian.

- Écrivez un `main` qui essaie d'afficher un angle en radian et un angle en
  degré.  Que se passe-t-il ? Pourquoi ? Écrivez le strict nécessaire pour
  résoudre ce problème.
    
- Écrivez une fonction `convert` qui convertit un angle en degré en un angle
  en radian et réciproquement...

- On veut pouvoir comparer l'égalité de deux angles.  Pourquoi ne peut-on pas
  juste dériver de la classe `Eq` ?  Vérifiez-le.

- Instanciez la classe `Eq` pour votre type `Angle`.  Vérifiez que vous
  n'avez pas le problème précédent.


## Arbre binaire de recherche 

Pour rappel, un arbre binaire de recherche est un arbre binaire pour lequel
tout noeud a une valeur supérieure aux noeuds de son sous-arbre gauche et
inférieure aux noeuds de son sous-arbre droit.  Par exemple, si on construit un
ABR en ajoutant successivement les valeurs 13, 2, 42, 37, 12 et 51, on obtient
l'arbre suivant.

![](dot/exo_classes_abr.svg){width="40%"}

* * *

- Recopiez le type algébrique suivant.

```haskell
data Arbre a = Nil | Noeud a (Arbre a) (Arbre a)
```

- Instanciez la classe `Show` pour le type `Arbre` de façon à afficher les
  éléments selon un parcours en profondeur.  Par exemple, la fonction `show`
  appliquée sur l'arbre ci-dessus doit retourner :

```haskell
" 2  12  13  37  42  51"
```

* * *

- Écrivez une fonction `ajouterArbre` qui «ajoute» un élément dans un arbre
  en respectant les propriétés des ABR.

- Écrivez une fonction `construireArbre` qui construit un ABR à partir d'une
  liste d'éléments.

- Écrivez un `main` qui construit et affiche un ABR avec les arguments de la
  ligne de commande (convertis en `Int`).

- Écrivez une fonction `appartient` qui retourne si un élément appartient à
  un arbre.


## Classe EstNul

On veut écrire une classe représentant des types pouvant avoir une valeur nulle
(par exemple, `[]` pour une liste, `0` pour un entier...).

- Écrivez une classe `EstNul` spécifiant une fonction `estNul`.

- Instanciez votre classe pour les types liste, entier et booléen. Testez dans
  un `main`.

- Écrivez un type `Arbre` (binaire) et instanciez la classe `EstNul` pour
  ce type.


## Classe de types empilables * 

On veut écrire une classe représentant des types empilables (c'est-à-dire que
l'on peut utiliser comme une pile) ainsi que deux types de cette classe.

- Écrivez deux implémentations d'une pile d'entiers : l'une par un synonyme de
  type de liste (`PileListe`), l'autre par un type algébrique (`PileTad`).
  Pour l'instant, écrivez uniquement les types (pas de fonction).  Utilisez les
  pragmas suivants.

```haskell
{-# LANGUAGE TypeSynonymInstances, FlexibleInstances #-}
```

* * *

- Écrivez une classe de types `Empilable` et instanciez-la pour `PileListe`
  et `PileTad`.  Votre classe doit spécifier les fonctions `empilerPile`,
  `depilerPile`, `sommetPile` et `creerPile`.  Indications : implémentez
  ces fonctions progressivement et testez-les dans un main.

- Modifiez votre code pour gérer des piles génériques (pas uniquement
  d'entiers).  Indications :
  https://en.wikibooks.org/wiki/Haskell/Advanced_type_classes


## Type Peutetre 

- Écrivez un type algébrique `Peutetre` permettant de représenter `Rien` ou
  `Juste` un réel (`Float`). Ce type permet de représenter le résultat d'un
  calcul ou l'absence de résultat (cas d'erreur).

- Écrivez une fonction `racine` qui prend en paramètre un réel et retourne sa
  racine carrée. Pour rappel, la racine d'un nomble négatif n'est pas définie
  donc peut-être qu'un `Peutetre` peut être utile.

- Écrivez une fonction `peutetreFois2` qui prend en paramètre peut-être un
  réel et retourne peut-être son double.

* * *

- Instanciez la classe `Show` pour votre type `Peutetre` de façon à
  afficher peut-être le réel ou `"rien"`.

- Dans un `main`, calculez et affichez la liste des doubles des racines
  carrées des nombres de -4 à 4, peut-être.

- Réimplémentez tout ça en C++ à partir du code suivant.

* * *

```cpp
enum peutetre_t {RIEN, JUSTE};
struct Peutetre {
    peutetre_t type;
    float valeur;
};
std::string show(const Peutetre & p) {
    // ...
}
Peutetre racine(float x) {
    // ...
}
Peutetre peutetreFois2(const Peutetre & p) {
    // ...
}
int main() {
    // ...
}
```

* * *

- item Idem mais avec le code suivant.

```cpp
struct Peutetre {
    virtual std::string show() const = 0;
    virtual void peutetreFois2() = 0;
};
struct Rien : Peutetre {
    // ...
};
struct Juste : Peutetre {
    // ...
};
std::unique_ptr<Peutetre> racine(float x) {
    // ...
}
int main() {
    // ...
}
```

* * *

- Écrivez une classe `Doublable` représentant les types qu'on peut
  `doubler`.

- Instanciez la classe `Doublable` pour votre type `Peutetre` ainsi que
  pour le type liste.

- Écrivez une fonction `fois4` permettant de multiplier par 4 toute donnée de
  classe `Doublable`. Testez dans le `main`.

- N'implémentez pas ça en C++.



# Foncteurs, monades


## Foncteurs 

- En profitant qu'une liste est un foncteur, écrivez une expression qui
  renverse chaque texte d'une liste (par exemple, avec la liste
  `["hello","world"]`, on doit obtenir `["olleh","dlrow"]`).

- Écrivez une expression équivalente mais pour un texte contenu dans un
  `Maybe`.

- Recopiez le type suivant et faites-en un foncteur.  Testez-le avec le
  renversement de texte.

```haskell
data Arbre a = Nil | Noeud a (Arbre a) (Arbre a) deriving Show 
```

- Écrivez une fonction `fDoubler` qui multiplie par 2 un foncteur. Testez
  votre fonction sur une liste, un `Maybe` et un `Arbre`.


## Monade IO 

- En utilisant la notation `do`, écrivez un `main` qui affiche les
  arguments de la ligne de commande.  Testez avec les arguments `1 2 3`.

- Même question mais sans utiliser la notation `do`.

- Quel est le type retourné par la fonction qui récupère les arguments de la
  ligne de commande ?  En utilisant la notation `do`, écrivez un `main` qui
  affiche chaque argument sur une ligne.

- Même question mais sans utiliser la notation `do`.

* * *

- En utilisant la notation `do`, écrivez un `main` qui multiple par 2 puis
  affiche chaque argument sur une ligne.  On suppose que les arguments
  correspondent à des entiers.  Utilisez au maximum la notation «point-free».

- Même question mais sans utiliser la notation `do`.

- Modifier vos fonctions `main` (avec et sans la notation `do`) de façon à
  afficher le message `fin du programme`, à la fin du programme.

- Modifier vos fonctions de façon à afficher le message `début du programme`,
  au début du programme.



## Fonctions monadiques

On veut pouvoir réaliser le calcul de la racine carrée en gérant les cas
d'erreur (nombres négatifs).  Pour cela, on peut utiliser des monades, pour
lesquelles on va écrire des fonctions, dites fonctions monadiques.

- Dans le code suivant, quelles sont les deux monades utilisées ?

```haskell
main = print $ liste1 >>= \ x -> if x>0 then [x] else [0]
where liste1 = [-2..2]
```

- Réécrivez-les en notation `do`.

* * *

- Écrivez une fonction monadique `positifM` qui seuille à 0 les valeurs
  négatives.  En d'autres termes, cela revient à réécrire la lambda du «bind»
  (`>>=`) en une fonction utilisable pour n'importe quelle monade.
  Indications : le type à seuiller doit être de classe `Ord` et de classe
  `Num`.

- Testez votre fonction dans le `main` sur `liste1` (en notation bind
  plutôt que `do`).

- Écrivez une fonction monadique `sqrtM` qui retourne, dans une monade, la
  racine carrée après seuillage.  Testez dans le `main`.

- Testez vos fonctions avec la monade `Maybe`.

* * *

- Dans tous ces tests, que se passe-t-il en cas d'erreur (nombres négatifs) ?
  Quel autre comportement serait plus judicieux ?

- Écrivez une fonction `positifMP` équivalente à `positifM` mais pour la
  monade `MonadPlus`.  Cette dernière est définie dans le module
  `Control.Monad` et propose un élément nul appelé `mzero`.

- Si ce n'est pas déjà fait, testez votre fonction `positifMP` dans le
  `main`.  À quoi correspond l'élément nul pour les deux monades testées ?



# Évaluation paresseuse


## Le compte est bon 

On veut écrire un programme qui, étant donnés un entier $x$ et une liste
d'entiers $l$, détermine s'il existe une combinaison d'éléments de
$l$ dont la somme vaut $x$.  Pour cela, on part du code suivant:

* * *

```haskell
import Data.Time

combiner :: [Int] -> [[Int]]
combiner = aux . map (:[])
    where aux [[x]] = [[x]] ++ [[]]
          aux ([x]:xs) = (map (x:) (aux xs)) ++ (aux xs)

testerCombiner :: ([Int] -> [[Int]]) -> String -> Int -> IO()
testerCombiner fonctionTest nom nbMax = do
    putStr $ nom ++ " " 
    let listeNombres = [1..nbMax]
    start <- getCurrentTime
    putStr.show $ fonctionTest listeNombres
    stop <- getCurrentTime
    putStr $ " " ++ (show nbMax) ++ " "  
    putStrLn.init.show $ diffUTCTime stop start

main = do
    mapM_ (testerCombiner combiner "combiner") [2..4] 
```

* * *

- Exécutez le code précédent et comprenez à peu près ce qu'il se passe.

- Faites tourner le programme pour un plus grand nombre de valeurs et observez
  le temps de calcul.  Pour avoir une sortie plus lisible, supprimez la colonne
  résultat en "pipant dans awk" :

```text
runghc countdown.hs | awk '{print $1 " " $3 " " $4}'
```

- Comparez avec une compilation optimisée :

```text
ghc -O2 --make countdown.hs
./countdown | awk '{print $1 " " $3 " " $4}'
```

* * *

- Écrivez une fonction `combiner'` qui optimise le calcul de `(aux xs)`.
  Ajoutez un test de votre fonction dans le `main` et comparez les temps de
  calculs, avec compilation à la volée et avec compilation optimisée.

- Écrivez une fonction `chercherSomme1` qui retourne les combinaisons des
  éléments d'une liste donnée dont la somme vaut un entier donné.

- Écrivez une fonction `testerSomme` qui permet de tester votre fonction, à
  la manière de `testerCombiner`.  Ajoutez un test dans le `main`.

- Écrivez une fonction `chercherSomme2` qui retourne un booléen indiquant
  s'il existe au moins une combinaison.  Pour cela, utilisez la fonction
  `chercherSomme1`.  Ajoutez un test dans le `main`.

- Exécutez les tests et expliquez les temps de calculs observés.

