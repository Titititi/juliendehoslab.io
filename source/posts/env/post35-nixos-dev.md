---
title: NixOS (environnements de développement)
date: 2019-04-14
---

# Généralités

## Rappels des commandes nix

- `nix-shell` : lancer un shell
- `nix-build` : construire une dérivation
- `nix-env` : installer/supprimer/… des paquets

## Lancer un shell "ad-hoc"

- lancer un shell avec des dépendances :
    `nix-shell -p python3Packages.numpy`
- lancer un shell et exécuter une commande :
    `nix-shell -p python3Packages.numpy --run python`
- lancer un shell dans un environnement isolé :
    `nix-shell -p python3Packages.numpy --pure`
- lancer un shell d'après un fichier de config (`shell.nix` ou `default.nix`) :
    `nix-shell`
- lister les paquets Python 3 disponibles :
    `nix-env -qaP -A nixos.python3Packages`
- voir les [exemples fournis](https://gitlab.com/juliendehos/nixos_config)
- voir la [documentation Nixpkgs](https://nixos.org/nixpkgs/manual)

# Exemples en Haskell

## Projet Haskell classique, avec Cabal (hs_randmat1)

- code Haskell (`Main.hs`) :

```haskell
import System.Random

main :: IO ()
main = do
   gen <- newStdGen
   let ns = randoms gen :: [Double]
   mapM_ print (take 10 ns)
```

* * *

- configuration Cabal (`hs_randmat1.cabal`) :

```haskell
name:                hs-randmat1
version:             0.1
build-type:          Simple
license:             BSD3
cabal-version:       >=1.10

executable hs-randmat1
  main-is:             Main.hs
  ghc-options:         -Wall
  default-language:    Haskell2010
  build-depends:       base, random
```

* * *

- configuration Nix (`default.nix`) :

```nix
{ pkgs ? import <nixpkgs> {} }:
let 
  drv = pkgs.haskellPackages.callCabal2nix "hs_randmat1" ./. {};
  # hs_randmat1 doit correspondre au fichier .cabal
in
if pkgs.lib.inNixShell then drv.env else drv
```

* * *

- installation système :

```text
nix-env -f . -i
hs-randmat1
```

- construction locale :

```text
nix-build
./result/bin/hs-randmat1
```

- construction dans un shell :

```text
nix-shell
cabal run
…
exit
```

## Scripts Haskell simples, avec Ghc (hs_randmat2)

- configuration Nix (`shell.nix`) :

```nix
with import <nixpkgs> {};

pkgs.stdenv.mkDerivation {
  name = "hs-randmat2";
  buildInputs = [ 
    (haskellPackages.ghcWithPackages (ps: with ps; [
      random
      # indiquer les paquets Haskell à charger
    ]))
  ];
}
```

* * *

- utilisation dans un nix-shell :

```text
nix-shell
runghc hs_randmat2a.hs
runghc hs_randmat2b.hs
…
exit
```

# Exemples en C++

## Projet CMake classique (cpp_randmat1)

- code C++ (`cpp_randmat1.cpp`) :

```cpp
#include <Eigen/Dense>
#include <iostream>
int main() {
    std::cout << Eigen::MatrixXd::Random(2, 3) << std::endl;
    return 0;
}
```

* * *

- configuration CMake (`CMakeLists.txt`) :

```cmake
cmake_minimum_required( VERSION 2.8 )
project( cpp_randmat1 )

find_package( PkgConfig REQUIRED )
pkg_check_modules( PKG_EIGEN REQUIRED eigen3 )
include_directories( ${PKG_EIGEN_INCLUDE_DIRS} )

add_executable( cpp_randmat1 cpp_randmat1.cpp )
target_link_libraries( cpp_randmat1 ${PKG_EIGEN_LIBRARIES} )

install( TARGETS cpp_randmat1 DESTINATION bin )
```

* * *

- configuration Nix (`default.nix`) :

```nix
{ pkgs ? import <nixpkgs> {} } :

with pkgs; stdenv.mkDerivation {
  name = "cpp_randmat1";
  src = ./.;
  buildInputs = [
    cmake
    pkgconfig
    eigen
  ];
}
```

* * *

- installation système :

```text
nix-env -f . -i
cpp_randmat1
```

- construction locale :

```text
nix-build
./result/bin/cpp_randmat1
```

* * *

- construction dans un shell :

```text
nix-shell
mkdir mybuild
cd mybuild
cmake ..
make
./cpp_randmat1
…
exit
```

## Codes C++ simples, avec un Makefile (cpp_randmat2)

- `Makefile` :

```makefile
CXXFLAGS = -O2 -Wall -Wextra `pkg-config --cflags eigen3`
LDFLAGS = `pkg-config --cflags eigen3`
SRC = $(shell find . -name "*.cpp")
BIN = $(SRC:.cpp=.out)

.PHONY: all
all: $(BIN)

.PHONY: clean
clean:
    rm -f $(BIN)

%.out: %.cpp
    $(CXX) -o $@ $(CXXFLAGS) $(LDFLAGS) $<
```

* * *

- configuration Nix (`default.nix`) :

```nix
with import<nixpkgs> {};

stdenv.mkDerivation {
  name = "cpp_randmat2";
  src = ./.;

  buildInputs = [
    pkgconfig
    eigen
    # mettre ici les dépendances nécessaires
  ];

  installPhase = ''
    mkdir -p $out/bin
    mv *.out $out/bin
  '';
}
```

* * *

- utilisation dans un nix-shell :

```text
nix-shell
make
./cpp_randmat2a.out
./cpp_randmat2b.out
…
exit
```

- construction et installation comme d'hab avec `nix-build` et `nix-env`

# Exemples en Python

## Script Python interprété directement par Nix (py_randmat1)

- code Python (`py_randmat1.py`) :

```python
#! /usr/bin/env nix-shell
#! nix-shell -i python3 -p python3Packages.numpy

import numpy as np

print(np.random.random((2, 3)))
```

- inconvénient : rend le script dépendant de Nix

* * *

- exécution (dans un nix-shell) :

```text
./py_randmat1.py
```

- exécution classique toujours possible :

```text
nix-shell -p python3Packages.numpy --run "python3 ./py_randmat1.py"
```

## Configuration d'un nix-shell avec dépendances Python (py_randmat2)

- code Python classique (`py_randmat2.py`) :

```python
#! /usr/bin/env python3

import numpy as np

print(np.random.random((2, 3)))
```

* * *

- configuration Nix (`shell.nix`) :

```nix
with import <nixpkgs> {};

(python3.withPackages (ps: with ps; [ numpy ])).env
```

* * *

- exécution dans un nix-shell :

```text
nix-shell --run ./py_randmat2.py
```

- utilisation dans un nix-shell :

```text
nix-shell
./py_randmat2.py
…
exit
```

## projet Python avec Setuptools (py_randmat3)

- configuration Setuptools (`setup.py`) :

```python
from setuptools import setup

setup(name='py_randmat3',
      version='0.1.0',
      scripts=['py_randmat3.py'],
      install_requires=['numpy'])
```

* * *

- configuration Nix (`default.nix`) :

```nix
{ pkgs ? import <nixpkgs> {} }:

pkgs.python3Packages.buildPythonPackage {
  name = "py_randmat3";
  src = ./.;
  propagatedBuildInputs = [
    pkgs.python3Packages.numpy
  ];
}
```

* * *

- installation :

```text
nix-env -f . -i
py_randmat3.py
```

- construction :

```text
nix-build
./result/bin/py_randmat3.py
```

- exécution dans un nix-shell :

```text
nix-shell --run py_randmat3.py
```

