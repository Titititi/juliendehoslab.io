---
title: NixOS (installation)
date: 2019-09-17
---

## Motivation

Pour les modules HPC, PF et PFW de master, vous aurez besoin d'un certain
nombre de logiciels, compilés avec des options particulières et de gérer des
services systèmes.

![](files/logo-nix.png)

## Installer le système de base

#. **Branchez le disque-dur prévu pour les TP**.
#. Bootez sur la clé USB contenant l'installeur Nixos (F12 pour
   sélectionner le device à booter). Si la machine bloque sur un message "disk
   ... slow" ou autre, alors éteignez la machine, débranchez/rebranchez le
   disque-dur et recommencez.
#. Une console root doit s'afficher. Si des notifications USB apparaissent
   aléatoirement, débranchez la souris le temps de l'installation.
#. Suivez les instructions données avec la [configuration fournie](https://gitlab.com/juliendehos/ulco-nixos).

* * *

<video preload="metadata" controls>
<source src="files/nixos.mp4" type="video/mp4" />

</video>


## Gérer des paquets systèmes

**À faire en tant qu'utilisateur (non root)**

- chercher le paquet `hello` :

```text
nix search hello
```

- pour installer/supprimer des paquets, modifier le fichier
  ``~/.config/nixpkgs/packages.nix`` puis :

```text
home-manager switch
```


## Utilisation dans des projets 

On peut configurer un projet en écrivant un fichier de configuration
`default.nix` ou `shell.nix` puis en lançant un :

```text
nix-shell
```

L'environnement se charge et vous pouvez travaillez comme sur un système
classique. Entrez `exit` ou `ctrl-d` pour quitter.


## Autres fonctionnalités

- lister les paquets installés : `nix-env -q`
- installer le paquet `hello` : `nix-env -iA nixos.hello`
- supprimer le paquet `hello` : `nix-env -e hello`
- lister les canaux de paquets : `sudo nix-channel --list`
- mettre à jour les canaux de paquets : `sudo nix-channel --update`
- mettre à jour les paquets : `nix-env -u --always`
- nettoyer le *store* : `sudo nix-collect-garbage -d`
- …

