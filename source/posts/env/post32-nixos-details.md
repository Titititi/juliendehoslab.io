---
title: NixOS (présentation)
date: 2019-04-14
---

# Généralités

## Présentation de Nix

- gestionnaire de paquets et de services respectant les principes de la
  programmation fonctionnelle 
- Eelco Dolstra. **The Purely Functional Software Deployment Model**. PhD
  thesis, Faculty of Science, Utrecht, The Netherlands. January 2006.
- [site officiel](https://nixos.org/)
- [dépôt git](https://github.com/NixOS)
- a inspiré [GNU Guix](https://www.gnu.org/software/guix/)

## Intérêts 

- environnements logiciels légers
- reproductibles
- composables
- isolés…

# Principe de fonctionnement

## Notion de dérivation

- dérivation = description de paquet :

```nix
# hello.nix 
with import <nixpkgs> {};
stdenv.mkDerivation {
  name = "hello-2.10";
  src = fetchurl {
    url = "mirror://gnu/hello/hello-2.10.tar.gz";
    sha256 = "…";
  };
}
```

* * *

- construction :

```text
$ nix-build hello.nix 
these derivations will be built:
  /nix/store/iclkh6vn27jga8xvwpyv3qx1crjcrw8i-hello-2.10.drv
these paths will be fetched (37.73 MiB download, 153.59 MiB unpacked):
  /nix/store/1hdv6h68f7xy9k0lhxqf26saz0w0r39i-patch-2.7.5
…
/nix/store/2zsigvwimvl6j6wi5azkn87jnmlbjfyc-hello-2.10
```

## Packaging fonctionnel

- entrées/sorties, sans d'effet de bord

![](files/guix-fp.png)

* * *

```text
$ tree /nix/store/2zsigvwimvl6j6wi5azkn87jnmlbjfyc-hello-2.10
/nix/store/2zsigvwimvl6j6wi5azkn87jnmlbjfyc-hello-2.10
|-- bin
|   |-- hello
|-- share
    |-- info
    |   |-- hello.info
    …
```

## Mise en œuvre

- dossier  `/nix/store/` 
- calculs de hash codes uniques
- liens symboliques + profils
- pas d'arborescence unix classique (FHS)

* * *

![](files/user-environments.png)

## Graphe de dépendances

- par exemple, pour `python-2.7.9` :

* * *

![](files/python-deps.dot.png){width="60%"}

# L'écosystème Nix

## Les outils Nix

- Nix
- Nixpkgs
- Nixos
- Nixops
- Hydra, Disnix…

## Nix

- langage de description de paquets et de services
- programmes pour gérer des *nix-expressions* (`nix-shell`, `nix-build`, `nix-env`…)
- [documentation nix](https://nixos.org/nix/manual/)
- [a tour of nix](https://nixcloud.io/tour)

## Nixpkgs

- collection de paquets (logithèque)
- arborescence de *dérivations* 
- [documentation nixpkgs](https://nixos.org/nixpkgs/manual/)
- [liste des paquets](https://nixos.org/nixos/packages.html)
- [dépôt de code](https://github.com/NixOS/nixpkgs)

## Nixos

- distribution linux basée sur Nix et Nixpkgs (paquets + services)
- [site web](https://nixos.org/)
- [documentation nixos](https://nixos.org/nixos/manual/)
- fichier de configuration : `/etc/nixos/configuration.nix`

* * *

```nix
{ config, pkgs, ... }: {
  boot.loader.grub = {
    enable = true;
    version = 2;
    device = "/dev/sda"; 
  };
  services = {
    xserver = {
      enable = true;
      desktopManager.xfce.enable = true;
    };
  };
  networking.hostName = "toto"; 
  …
}
```

