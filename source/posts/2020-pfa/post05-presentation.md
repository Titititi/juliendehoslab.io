---
title: PFA - Présentation du module
date: 2020-04-13
---

## Objectifs du module

- rappeler les notions de base de PF, en Haskell
- voir quelques fonctionnalités plus avancées du système de types
- réaliser quelques projets concrets (web, compilation...)

## Pré-requis 

- module de programmation fonctionnelle de la L3
- notions de base d'algo
- notions de base de développement web

## Organisation

- 14 séances de 3h :
    1. Rappels (expressions, variables, fonctions, récursivité)
    1. Rappels (fonctions d'ordres supérieurs, système de types)
    1. Types algébriques
    1. Classes de types
    1. Environnement de développement + **DS1 (noté)**
    1. Compilation, parsing
    1. Outils de parsing
    1. Génération de code
    1. Frameworks backend + **DS2 (noté)**
    1. Bases de données
    1. Frameworks frontend
    1. Websockets
    1. **Projet noté**
    1. Typage d'API


## Travaux pratiques

- TP sous NixOS (cf la VM fournie).
- [dépôt gitlab](https://gitlab.com/juliendehos/ulco-pfa-etudiant)

