---
title: PFA 7 - Outils de parsing
date: 2020-04-13
---

Pour cette séance, il n'y a pas vraiment de cours. L'objectif est de découvrir
quelques outils Haskell, en rapport avec le parsing, à partir de leur
documentation.


# Quelques outils

## Types de texte 

- Haskell a 5 types classiques pour implémenter du texte : String,
  [Text](https://hackage.haskell.org/package/text) (strict + lazy) et
  [ByteString](https://hackage.haskell.org/package/bytestring) (strict + lazy).

- Comment choisir ?

    - si test/proto/osef : `String`
    - dans tous les autres cas, préférer `Text` ou `ByteString`
    - pour du texte, `Text` est généralement plus adapté mais à voir selon les libs utilisées à côté

* * *

- Strict ou lazy ?

    - si grosses données, plutôt lazy
    - sinon, prendre le plus simple par rapport aux libs utilisées

- Quelques fonctionnalités qui sauvent la vie :

    - `pack`, `unpack`
    - `{-# LANGUAGE OverloadedStrings #-}`
    - `decodeUtf8`, `encodeUtf8`...
    - `fromStrict`, `toStrict`
    - `showt`, `showtl`


## Aeson

- permet d'encoder et de décoder du JSON

- lib très classique en Haskell

- illustre bien les ADT, type-class...

- [doc de Aeson](https://hackage.haskell.org/package/aeson)


## Optparse

- permet de parser les arguments de la ligne de commande

- plusieurs variantes :
  [optparse-generic](https://hackage.haskell.org/package/optparse-generic),
  [optparse-applicative](https://hackage.haskell.org/package/optparse-applicative)...


## Scalpel

- permet de faire du web-scraping (extraire des données depuis des pages HTML)

- [doc de Scalpel](https://hackage.haskell.org/package/scalpel)


## Yaml

- permet d'encoder et de décoder du YAML

- inspiré de Aeson

- bien pratique pour gérer des fichiers de configuration

- [doc de Yaml](https://hackage.haskell.org/package/yaml)



## Pandoc

- permet de convertir des documents textuels depuis et vers plein de formats

- bibliothèque + application

- assez connu, même hors du monde Haskell

- très puissant : nombreuses options, système de templates, système de filtrage de l'AST...

- [doc de Pandoc](https://hackage.haskell.org/package/pandoc), [site web de
  Pandoc](https://pandoc.org/)





# Travaux pratiques

Pour les exercices suivants, allez dans le dossier `tp7/exos` de votre dépôt,
lancez un `nix-shell` et exécutez vos programmes avec des `runghc -Wall`.


## Conversions ByteString, String, Text

- Écrivez un programme `text1.hs` qui lit le fichier `text1.hs` dans un
  `ByteString`, convertit en `String` et affiche à l'écran. Pour cela, utilisez
  le module `Data.ByteString.Char8`.

- Écrivez un programme `text2.hs` équivalent mais avec un `Text`.

- Écrivez un programme `text3.hs` équivalent mais qui lit un `ByteString` et
  convertit en `Text`.

- Écrivez un programme `text4.hs` équivalent mais qui lit un `Text` et
  convertit en `ByteString`.

- Écrivez un programme `text5.hs` équivalent mais qui lit un `Text` strict et
  convertit en `Text` lazy.


## OverloadedStrings

- Exécutez le fichier `overloadedstrings.hs`.

```sh
[nix-shell]$ runghc -Wall overloadedstrings.hs 
[Person "John",Person "Haskell"]
```

- Modifiez le type `Person` de façon à utiliser un `Text` à la place du
  `String`. Utilisez l'extension `OverloadedStrings`.

- Enlevez l'extension `OverloadedStrings` et modifiez la variable `persons`
  en conséquence.


## Aeson

- Le fichier `aeson1.hs` définit un type `Person` qui correspond directement au
  fichier `aeson-test1.json`. En utilisant `Generic` et la bibliothèque
  `aeson`, implémentez le parsing d'une `Person` depuis un fichier JSON. 
  Modifiez le `main` pour tester avec `aeson-test1.json`.

```sh
[nix-shell]$ runghc -Wall aeson1.hs 
Person {firstname = "John", lastname = "Doe", birthyear = "1970", speakenglish = False}
Right (Person {firstname = "John", lastname = "Doe", birthyear = "1970", speakenglish = False})
```

- Modifiez le `main` de façon à parser également `aeson-test2.json` et
  `aeson-test3.json`. Remarquez que ce dernier doit donner une erreur.

* * *

- Écrivez un fichier `aeson2.hs` équivalent mais sans `Generic`, en écrivant
  l'instance explicitement, avec `withOject`.

- Complétez le fichier `aeson3.hs` de façon à implémenter un programme
  équivalent mais avec le nouveau type `Person` proposé.

```sh
[nix-shell]$ runghc -Wall aeson3.hs 
Person {first = "John", last = "Doe", birth = 1970}
Right (Person {first = "John", last = "Doe", birth = 1970})
Right [Person {first = "John", last = "Doe", birth = 1970},Person {first = "Haskell", last = "Curry", birth = 1900}]
Left "Error in $[1].birthyear: expected String, but encountered Number"
```


## Optparse-applicative

- Exécutez le programme `optparse0.hs` sans argument, puis en demandant l'aide,
  puis avec des arguments suffisants.

- Écrivez un programme `optparse1.hs` équivalent mais avec une option
  supplémentaire, `val2`, de type `Double`. 

- Modifiez votre programme de façon à prendre un argument positionnel (un nom
  de fichier de sortie). Indication : ajoutez un champ dans `Args` et utilisez
  la fonction `strArgument`.

* * *

- Modifiez votre programme de façon à prendre un argument positionnel
  supplémentaire (un nom de fichier d'entrée) mais sans modifier `Args`.
  Indication : implémentez un parser `fileP` en utilisant la fonction
  `strArgument`, puis implémentez un parser `fullP` combinant `fileP` et `argsP`.

```sh
$ runghc optparse1.hs in.txt out.txt --hello foo --val2 4.3
("in.txt",Args {outfile = "out.txt", hello = "foo", val1 = 1, val2 = 4.3})
```


## Scalpel

- En vous inspirant des exemples donnés dans la doc de Scapel, implémentez un
  programme qui affiche toutes les images d'une page donnée en argument. 

```sh
$ runghc scalpel1.hs "https://juliendehos.gitlab.io/index.html"
files/livre-haskell-jd-small.jpg
./files/icon-home.svg
./files/icon-up.svg
./files/icon-gitlab.svg
./files/icon-analytics.svg
```


## Mini-projet : scrapimjson

- Écrivez un programme `scrapimjson` qui prend des urls de pages web, scrape
  les images de ces pages et sort le tout dans un fichier JSON également passé
en paramètre.  Pour cela, utilisez `optparse-applicative`, `scalpel` et
`aeson`, et faites un projet propre (par exemple avec le template stack
`gitlab:juliendehos/full`).

* * *

```sh
$ stack run -- --help
This is scrapimjson!

Usage: scrapimjson OUTFILE URLS...

Available options:
  OUTFILE                  output json file
  URLS...                  input urls to scrape
  -h,--help                Show this help text
```

* * *

```sh
$ stack run -- out.json "https://haskell.org" "https://juliendehos.gitlab.io"
scraping https://haskell.org...
scraping https://juliendehos.gitlab.io...
```

* * *

![](files/pfa-tp-scrapimjson.png){width="50%"}


## Yaml

- Écrivez un programme qui lit les données du fichier `yaml-test1.yaml`.

```sh
$ runghc yaml1.hs 
Right (Person {first = "John", last = "Doe", birth = 1970,
               sites = ["https://fr.wikipedia.org/wiki/John_Doe",
                        "https://juliendehos.gitlab.io"]})
```


## Pandoc

- En utilisant le programme `pandoc`, convertissez le fichier `pandoc-test1.md`
  en HTML.

- Écrivez un programme Haskell équivalent. Indication : regardez 
  l'[introduction à l'API pandoc](https://pandoc.org/using-the-pandoc-api.html).

```sh
$ runghc pandoc1.hs pandoc-test1
pandoc-test1.html written
```

