---
title: Prog Fonctionnelle Avancée
---

Introduction à la Programmation fonctionnelle avancée (PFA). M1 Informatique de
l'ULCO. Voir également le
[dépôt gitlab](https://gitlab.com/juliendehos/ulco-pfa-etudiant) et les
[vidéos de cours](https://video.ploud.fr/videos/watch/playlist/a5e42d0e-d2a1-4f90-af58-0be8cb412547?videoId=3f5e012b-8d05-4676-aa08-0c84e1c9bffa).

