---
title: PFA 6 - Compilation, parsing
date: 2020-04-13
---

# Généralités

## Notion de compilateur

- lit un texte en entrée

- le "comprend", selon des règles prédéfinies

- fait quelque chose avec le résultat

![](files/compiler.svg){width="60%"}

## Notion de langage

- grammaire formelle

- contextuelle, non-contextuelle

- ambigüe, non-ambigüe


## Pourquoi utiliser Haskell ?

- fonctionnel pur

- pattern matching

- système de types

- ADT, type-class, higher-kinded types...

## Outils de compilation en Haskell

- compilateur de compilateurs (à la lex/yacc) : Alex/Happy

- combinateurs de parseurs monadiques : ReadP, Parsec, Megaparsec... 


# Exemple

## Projet mycalc

- calculatrice basique : `+`, `*`, entiers positifs
- en sortie : code LISP, évaluation directe

```sh
12+3
123
2+5*8
2*5+8
```

## "Abstract Syntax Tree"

```haskell
data Expr
    = ExprVal Int
    | ExprAdd Expr Expr
    | ExprMul Expr Expr
    deriving (Eq, Show)
```

![AST pour `2*5+8`](dot/pfa-ast.svg){width="40%"}

## Génération de code LISP

```haskell
toLisp :: Expr -> String
toLisp (ExprVal v) = show v
toLisp (ExprAdd e1 e2)
    = "(+ " ++ toLisp e1 ++ " " ++ toLisp e2 ++ ")"
toLisp (ExprMul e1 e2)
    = "(* " ++ toLisp e1 ++ " " ++ toLisp e2 ++ ")"
```

## Évaluation directe

```haskell
eval :: Expr -> Int
eval (ExprVal v) = v
eval (ExprAdd e1 e2) = eval e1 + eval e2
eval (ExprMul e1 e2) = eval e1 * eval e2
```

## Application principale

```sh
> 12+3
ExprAdd (ExprVal 12) (ExprVal 3)
(+ 12 3)
15

> 123
ExprVal 123
123
123

> 2+5*8
ExprAdd (ExprVal 2) (ExprMul (ExprVal 5) (ExprVal 8))
(+ 2 (* 5 8))
42

> 2*5+8
ExprAdd (ExprMul (ExprVal 2) (ExprVal 5)) (ExprVal 8)
(+ (* 2 5) 8)
18
```

## Grammaire

- régles de grammaire de mycalc :

```sh
Additive  ::= Multitive '+' Additive
            | Multitive

Multitive ::= Decimal '*' Multitive
            | Decimal

Decimal   ::= nat
```

* * *

- on pourrait encore décomposer `nat` :

```sh
nat       ::= Digits

Digits    ::= Digit Digits
            | Digit

Digit     ::= '0' | '1' | ... | '9'
```


# Recursive descent parsers

## Définir un type Result

```haskell
type Result v = Either String (v, String)
```

- quand le parsing échoue : `Left` avec un message d'erreur
- quand le parsing réussit : `Right` avec valeur parsée + texte restant


## Parser les primitives

- parser un chiffre :

```haskell
pDigit :: String -> Result Char
pDigit "" = Left "no input"
pDigit (c:cs) =
    -- Digit ::= '0' | '1' | ... | '9'
    if isDigit c
        then Right (c, cs)
        else Left "not a digit"
```

```haskell
*Mycalc.Parser.Recursive> pDigit "12"
Right ('1',"2")
```

* * *

- parser un nombre :

```haskell
pDigits :: String -> Result String
pDigits s0 =

    -- Digits ::= Digit Digits
    case pDigit s0 of
        Right (v1, s1) -> case pDigits s1 of
            Right (v2, s2) -> Right (v1:v2, s2)

            -- Digits ::= Digit
            Left _ -> Right ([v1], s1)

        Left err -> Left err
```

```haskell
*Mycalc.Parser.Recursive> pDigits "12"
Right ("12","")
```

* * *

- parser un nombre (en `Int`) :

```haskell
pNat :: String -> Result Int
pNat s0 =
    -- nat ::= Digits
    case pDigits s0 of
        Right (v, s1) -> Right (read v, s1)
        Left err -> Left err
```

## Parser les règles de grammaire

- parser la règle "Multitive" :

```haskell
pMultitive :: String -> Result Expr
pMultitive s0 = alt1 where

    -- Multitive ::= Decimal '*' Multitive
    alt1 = case pDecimal s0 of
             Right (vleft, s1) -> case s1 of
                 ('*':s2) -> case pMultitive s2 of
                     Right (vright, s3) ->
                            Right (ExprMul vleft vright, s3)
                     _ -> alt2
                 _ -> alt2
             _ -> alt2

    -- Multitive ::= Decimal
    alt2 = pDecimal s0 
```

- idem pour "Decimal" et "Additive"


## Fonction de parsing principale

```haskell
parseExpr :: String -> Either String Expr
parseExpr str = case pAdditive str of
    Right (v, _) -> Right v
    Left err -> Left err
```

## En résumé

- on définit un type (`Result v`) pour représenter le résultat d'un parsing

- on définit des fonctions de parsing (`String -> Result v`), progressivement,
  jusqu'à la fonction qui parse complètement la grammaire

- les fonctions de parsing se réutilisent entre elles, et c'est vérifié par le 
  système de types

- idée suivante : définir un type représentant un parser 


# Parser combinators

## Définir un type Parser

```haskell
type Result v = Either String (v, String)

newtype Parser v = Parser { runParser :: String -> Result v }
```

- En français : le type `Parser v` représente une valeur `Parser` comportant un
  champ `runParser`; ce champ est une fonction qui prend un `String` et
  retourne un `Result v`.


## Exemple: parser le Char '0'

- si on a une fonction qui lit un `String` et retourne un `Result` :

```haskell
zeroF :: String -> Result Char
zeroF ('0':xs) = Right ('0', xs)
zeroF str      = Left ("no parse", str)
```

- alors on peut en faire un `Parser` :

```haskell
zeroP :: Parser Char
zeroP = Parser { runParser = zeroF }
```

- et ensuite faire du parsing :

```haskell
> runParser zeroP "0foobar"
Right ('0', "foobar")

> runParser zeroP "foobar"
Left ("no parse", "foobar")
```

* * *

- écriture équivalente du `Parser` :

```haskell
zeroP :: Parser Char
zeroP = Parser zeroF
```

- écriture équivalente, directement avec une lambda :

```haskell
zeroP :: Parser Char
zeroP = Parser (\str ->
    case str of
        ('0':xs) = Right ('0', xs)
        _        = Left ("no parse", str))
```


## Primitives de parsing de mycalc

- parser de base, qui lit un caractère :

```haskell
itemP :: Parser Char 
itemP = Parser $ \s0 ->
    case s0 of
        (x:xs) -> Right (x, xs)
        [] -> Left "no input"
```

* * *

- parser un chiffre :

```haskell
digitP :: Parser Char
digitP = Parser $ \s0 ->
    case runParser itemP s0 of
        Right (c, cs) ->
            if isDigit c
                then Right (c, cs)
                else Left "not a digit"
        Left err -> Left err
```

- ...

## Mais, c'est plus compliqué !

```haskell
-- recursive descent parser:
pDigit :: String -> Result Char
pDigit (c:cs) =
    if isDigit c
        then Right (c, cs)
        else Left "not a digit"
pDigit _ = Left "no input"
```

```haskell
-- parser combinator:
digitP :: Parser Char
digitP = Parser $ \s0 ->
    case runParser itemP s0 of
        Right (c, cs) ->
            if isDigit c
                then Right (c, cs)
                else Left "not a digit"
        Left err -> Left err
```


# Monadic parser combinators

## Classes instanciables pour le type Parser

- Functor ("appliquer une fonction à l'intérieur du type"):

```haskell
(<$>) :: Functor f => (a -> b) -> f a -> f b
```

```haskell
digitP :: Parser Char
digitP = ...

intP :: Parser Int
intP = digitToInt <$> digitP
```

* * *

- Applicative functor ("application de fonction, à l'intérieur du type"):

```haskell
(<*>) :: Applicative f => f (a -> b) -> f a -> f b

pure :: Applicative f => a -> f a
```

```haskell
intP :: Parser Int
intP = ...

addP :: Parser Int
addP = (+) <$> intP <*> intP
```


* * *

- Alternative ("foncteurs avec aussi une structure de monoïde"): 

```haskell
(<|>) :: Alternative f => f a -> f a -> f a

empty :: Alternative f => f a
```

```haskell
zeroP :: Parser Char
zeroP = ...

unP :: Parser Char
unP = ...

zeroOuUnP :: Parser Char
zeroOuUnP = zeroP <|> unP
```

* * *

- Monad ("abstract datatype of actions"): 

```haskell
(>>=) :: Monad m => m a -> (a -> m b) -> m b

return :: Monad m => a -> m a

fail :: Monad m => String -> m a
```

```haskell
addP :: Parser Int
addP = do
    i1 <- intP
    i2 <- intP
    return (i1 + i2)
```


## Maintenant, c'est plus simple !

- digit :

```haskell
-- recursive descent parser:
pDigit :: String -> Result Char
pDigit (c:cs) =
    if isDigit c
        then Right (c, cs)
        else Left "not a digit"
pDigit _ = Left "no input"
```

```haskell
-- monadic parser combinators:
digitP :: Parser Char
digitP = do
    c <- itemP
    if isDigit c
        then return c
        else fail "not a digit"
```

* * *

- digits :

```haskell
-- recursive descent parser:
pDigits :: String -> Result String
pDigits s0 = case pDigit s0 of
    Right (v1, s1) -> case pDigits s1 of
        Right (v2, s2) -> Right (v1:v2, s2)
        Left _ -> Right ([v1], s1)
    Left err -> Left err
```

```haskell
-- monadic parser combinators:
digitsP :: Parser String
digitsP = do
    c <- digitP
    cs <- (digitsP <|> return "")
    return (c:cs)
```

* * *

```haskell
-- or (using applicative/alternative):
digitsP :: Parser String
digitsP = (:) <$> digitP <*> (digitsP <|> return "")
```

```haskell
-- or (using Control.Applicative):
digitsP :: Parser String
digitsP = some digitP
```

* * *

- multitive :

```haskell
-- recursive descent parser:
pMultitive :: String -> Result Expr
pMultitive s0 = alt1 where

    -- Multitive ::= Decimal '*' Multitive
    alt1 = case pDecimal s0 of
             Right (vleft, s1) -> case s1 of
                 ('*':s2) -> case pMultitive s2 of
                     Right (vright, s3) ->
                            Right (ExprMul vleft vright, s3)
                     _ -> alt2
                 _ -> alt2
             _ -> alt2

    -- Multitive ::= Decimal
    alt2 = pDecimal s0 
```

* * *

```haskell
-- monadic parser combinators:
multitiveP :: Parser Expr
multitiveP
    =   (do e1 <- decimalP
            _ <- charP '*'
            e2 <- multitiveP
            return $ ExprMul e1 e2)
    <|> decimalP
```

* * *

```haskell
-- or (using applicative):
multitiveP :: Parser Expr
multitiveP
    =   (ExprMul <$> decimalP <*> (charP '*' *> multitiveP))
    <|> decimalP
```


## Et les instances ?

... pour information, car pas complètement trivial...

* * *

```haskell
instance Functor Parser where

    fmap fct (Parser p) = 
        Parser $ \s0 -> do
            (x, s1) <- p s0
            return (fct x, s1)
```

* * *

```haskell
instance Applicative Parser where

    pure x = Parser $ \s -> Right (x, s)

    (Parser p1) <*> (Parser p2) =
        Parser $ \s0 -> do
            (fct, s1) <- p1 s0
            (x, s2) <- p2 s1
            return (fct x, s2)
```

* * *

```haskell
instance Alternative (Either String) where

    empty = Left "empty"

    Left _ <|> e2 = e2
    e1 <|> _ = e1

instance Alternative Parser where

    empty = Parser $ const empty

    (Parser p1) <|> (Parser p2)
        = Parser $ \input -> p1 input <|> p2 input
```

* * *

```haskell
instance Monad Parser where

    (Parser p) >>= fct = 
        Parser $ \s0 -> do
            (x1, s1) <- p s0
            (x2, s2) <- runParser (fct x1) s1
            return (x2, s2)

    fail msg = Parser $ \_ -> Left msg
```


# Megaparsec

## Généralités

- bibliothèque de combinateurs de parseurs monadiques

- permet d'implémenter un type `Parser` (avec les instances)

- fournit des parseurs de base, combinateurs, gestion d'erreurs...

- remarque : le backtracking n'est pas automatique, il faut utiliser `try`

## Utilisation

- définir un type `Parser` :

```haskell
type Parser = Parsec Void String
```

- éventuellement, adapter la fonction de parsing principale :

```haskell
parseExpr :: String -> Either String Expr
parseExpr s0 = case parse additiveP "" s0 of
    Left bundle -> Left (errorBundlePretty bundle)
    Right v -> Right v
```

- définir les `Parser`, en utilisant les parseurs et combinateurs fournis :

```haskell
natP :: Parser Int
natP = decimal
```




# Travaux pratiques

- Travaillez dans le dossier `tp6` de votre dépôt.

- **Codez progressivement et avec des tests unitaires en "file-watch" !**

* * *

<video preload="metadata" controls>
<source src="files/pfa-parsing-filewatch.mp4" type="video/mp4" />
![](files/pfa-parsing-filewatch.png){width="80%"}

</video>

## Mycalc

Le projet `tp6/mycalc` est le projet de calculatrice basique présenté
ci-dessus.  Il contient toute la chaine Frontend-AST-Backend. L'objectif de
l'exercice est de comprendre le processus classique d'un compilateur et de
terminer l'implémentation des parsers (recursive, parsec, monadic et
megaparsec).

* * *

```bash
$ stack run megaparsec

megaparsec

> 20+11*2
ExprAdd (ExprVal 20) (ExprMul (ExprVal 11) (ExprVal 2))
(+ 20 (* 11 2))
42

> +12
error: 1:1:
  |
1 | +12
  | ^
unexpected '+'
expecting integer
```

* * *

- Regardez les fichiers `src/Mycalc/Syntax`, `src/Mycalc/Codegen.hs` et
  `app/Main.hs`. 

- Exécutez l'application principale. Pour l'instant, seule
l'entrée "0" est gérée correctement.

- Regardez et exécutez les tests.

* * *

- Lancez les tests en "file-watch" et terminez l'implémentation de
  `src/Mycalc/Recursive.hs` (recursive descent parser). Décommentez et validez
  les tests correspondants. Une fois terminé, faites quelques tests avec
  l'application principale.

- Idem avec `src/Mycalc/Parsec.hs` (parser combinators).

- Idem avec `src/Mycalc/Monadic.hs` (monadic parser combinators).

- Idem avec `src/Mycalc/Megaparsec.hs` (bibliothèque Megaparsec). Pensez à
  consulter les docs de
  [Megaparsec](https://hackage.haskell.org/package/megaparsec-7.0.5)) et des 
  [combinateurs monadiques](https://hackage.haskell.org/package/parser-combinators-1.0.2/docs/Control-Monad-Combinators.html).
  Faites attention à regarder la bonne version des docs.


## JSON

On veut parser/générer du [JSON](https://www.json.org/json-fr.html) simplifié.

```json
value                          member
    object                         string ':' element
    string                     
    number                     element
    "true"                         value
    "false"                    
    "null"                     string
                                   '"' characters '"'
object                         
    '{' members '}'            number
                                   integer
members                        
    member                     
    member ',' members         
```

* * *

- Créez un projet `myjson` avec le template `gitlab:juliendehos/full`.
  Copiez-y le dossier de test `tp6/data` fournis.

* * *

- Ajoutez l'AST suivant et implémentez quelques tests unitaires.

```haskell
data Value
    = ValueObject Object
    | ValueString String
    | ValueNumber Int
    | ValueTrue
    | ValueFalse
    | ValueNull
    deriving (Eq, Show)

data Object
    = Object [Member]
    deriving (Eq, Show)

data Member
    = Member String Value
    deriving (Eq, Show)
```

* * *

- Implémentez une classe `ToJson`, qui définit la fonction `toJson :: a ->
  String`.

- Instanciez `ToJson` pour votre AST. Testez.

* * *

- Implémentez le parser en utilisant Megaparsec. Pour cela, vous pouvez définir
  les `Parser` suivants. Et travaillez avec des tests en "file-watch".

```haskell
stringP :: Parser String

numberP :: Parser Int

valueP :: Parser Value

memberP :: Parser Member

membersP :: Parser [Member]

objectP :: Parser Object
```


* * *

- Implémentez l'application principale et testez avec les fichiers du dossier `data`.

```bash
$ stack run data/test2.json 
syntax: ValueObject (Object [Member "foo" (ValueNumber 42)])
codegen: {"foo":42}
```


