---
title: CM PFW, Websockets
date: 2019-04-16
---

# Généralités

## Websockets

- protocole de communication
- alternative à HTTP
- standardisé en 2011

## Intérêts

- performant/réactif (communication bi-directionnelle à long TTL)
- facile à déployer (ports 80/443/...)

## Principe de fonctionnement

- client-serveur classique
- nombreuses implémentations :
    - API client des navigateurs, en ECMAScript
    - API client des frameworks frontend (elm…)
    - API client/serveur en Node, Haskell…

![](files/websockets/dessin_ws.png){width="90%"}

# API client des navigateurs

## Description

- une API websocket est implémentée dans les navigateurs
- permet d'utiliser des websockets dans une page web
- classe `WebSocket` + méthodes `onopen`, `onmessage`, `send`...
- [doc mozilla websocket](https://developer.mozilla.org/fr/docs/Web/API/WebSocket)
- **attention : l'API websocket n'est pas normalisée par le w3c**

## Exemple de client web (echo)

```html
<!DOCTYPE html>
<html>
<body>
<input id="prompt" disabled="disabled" />
<p id="reply"> </p>

<script>
  const my_prompt = document.getElementById('prompt');
  const my_reply = document.getElementById('reply');
  //const my_ws = new WebSocket("wss://echo.websocket.org");
  const my_ws = new WebSocket("ws://localhost:9000");

  my_ws.onopen = function(e) {
    my_prompt.disabled = false;

    my_prompt.onkeydown = function (evt) {
      if (evt.keyCode == 13) {
        my_ws.send(my_prompt.value);
        my_prompt.value = "";
      }
    };

    my_ws.onmessage = msg => my_reply.innerHTML = msg.data;
  };
</script>
</body>
</html>
```

# API client/serveur Node

## Bibliothèque npm `ws` 

- implémentation des websockets (connexions, échanges de message…)
- côté client + côté serveur
- [doc ws sur npm](https://www.npmjs.com/package/ws)


## Exemple de client Websocket Node (echo)

```javascript
"use strict";

const WS = require("ws");
const READLINE = require('readline');
const SOCKET = new WS('wss://echo.websocket.org');

const RI = READLINE.createInterface({
    input: process.stdin,
    output: process.stdout
});

RI.on('line', function (msgToServer) {
    msgToServer === "" ? process.exit() : SOCKET.send(msgToServer);
});

SOCKET.on('message', function incoming(msgFromServer) {
    console.log(msgFromServer);
});
```

## Exemple de serveur Websocket Node (echo)

```javascript
"use strict";

const WS = require("ws");
const PORT = 9000;
const SERVER = new WS.Server({ port: PORT });

SERVER.on("connection", function connection(ws) {
    ws.on("message", function incoming(msgFromClient) {
        ws.send(msgFromClient);
    });
});

console.log(`listening on port ${PORT}...`);
```

# API client/serveur Haskell

## Bibliothèque Haskell Websockets

- implémentation Haskell du protocole websocket (connexions, échanges de message…)
- côté client + côté serveur
- [doc Haskell Websockets](https://hackage.haskell.org/package/websockets-0.12.2.0/docs/Network-WebSockets.html)

## Exemple de client Websockets Haskell (echo)

```haskell
import           Control.Monad (forever, when)
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as BC
import qualified Network.WebSockets as WS
import           System.Exit (exitSuccess)

main :: IO ()
main = WS.runClient "echo.websocket.org" 80 "" clientApp

clientApp :: WS.ClientApp ()
clientApp conn = forever $ do
    msgToSrv <- getLine
    when (null msgToSrv) exitSuccess
    WS.sendTextData conn (BC.pack msgToSrv)
    WS.receiveDataMessage conn >>= BC.putStrLn . WS.fromDataMessage 
```

## Exemple de serveur Websockets Haskell (echo)

```haskell
import           Control.Monad (forever)
import qualified Data.ByteString as B
import qualified Network.WebSockets as WS

main :: IO ()
main = putStrLn "running server..."
    >> WS.runServer "0.0.0.0" 9000 serverApp

serverApp :: WS.PendingConnection -> IO ()
serverApp pc = WS.acceptRequest pc >>= forever . handleConn

handleConn :: WS.Connection -> IO ()
handleConn c = do
    m <- WS.receiveDataMessage c 
    WS.sendTextData c (WS.fromDataMessage m :: B.ByteString)
```

