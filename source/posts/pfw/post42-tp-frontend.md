---
title: TP PFW, Frameworks frontend
date: 2019-04-16
---

## Application Elm (frontend/bmxriders)

On veut implémenter une application client-serveur permettant de chercher et
d'afficher des données via une API JSON.

- Regardez le code du projet `bmxriders-server`. Compilez et testez dans un
  navigateur ou avec `curl`.

- Regardez le code du projet d'exemple `test-elm`. Compilez et testez dans un
  navigateur.

- Copiez le projet d'exemple dans un dossier `bmxriders-client-elm` et
  implémentez un client qui affiche les « bmxriders » dont le nom commence par
  le texte entré dans la zone de saisie.

* * *

<video preload="metadata" controls>
<source src="files/frontend/pfw-frontend-bmxriders.mp4" type="video/mp4" />
![](files/frontend/pfw-frontend-bmxriders.png)

</video>

## Autres frameworks

- Implémentez des clients `bmxriders-client-miso`,
  `bmxriders-client-purs` et `bmxriders-client-reflex`.

