---
title: CM PFW, API REST
date: 2019-04-16
---

# Généralités

## REST

- architecture de service web : requête HTTP + réponse XML ou JSON
- protocole sans état (« Representational State Transfer ») : API fonctionnelle
- intérêts : HTTP facile à mettre en œuvre, peu de couplage client-serveur

* * *

![](files/api/rest-api.jpg)

* * *

- implémentation :
    - côté serveur : recevoir des requêtes HTTP et envoyer du XML/JSON
    - côté client : envoyer des requêtes HTTP et recevoir du XML/JSON

* * *

- exemple de requête HTTP + réponse JSON :

```text
$ curl "https://www.omdbapi.com/?apikey=PlzBanMe&t=Batman"

{ "Title": "Batman",
  "Year": "1989",
  "Rated": "PG-13",
  "Released": "23 Jun 1989",
  "Runtime": "126 min",
  "Genre": "Action, Adventure",
  "Director": "Tim Burton",
  ...
  "Production": "Warner Bros. Pictures",
  "Website": "N/A",
  "Response": "True" }
```

# En JavaScript

## Généralités

- format JSON natif (JavaScript Object Notation)
- architecture classique : 
    - serveur HTTP avec export JSON (quasi direct avec Node)
    - client web (navigateur) : code JS dans la page HTML

## Serveur (Node)

```javascript
"use strict";

const myMusics = [
    { musicSong: "Paranoid android", musicBand: "Radiohead", musicYear: 1997 },
    { musicSong: "Just", musicBand: "Radiohead", musicYear: 1995 },
    { musicSong: "Take the power back", musicBand: "Rage against the machine", musicYear: 1991 },
    { musicSong: "How I could just kill a man", musicBand: "Rage against the machine", musicYear: 2000 },
    { musicSong: "La porte bonheur", musicBand: "Ibrahim Maalouf", musicYear: 2014 }
];

const express = require("express");
const cors = require("cors");
const app = express();

app.use(cors())

app.get("/api", function (req, res) {
    res.json(myMusics);
});

const port = 3000;

const server = app.listen(port, function () {
    console.log(`Listening on port ${port}...`);
});
```

## Client (Node)

```javascript
"use strict";

const fetch = require('node-fetch');

fetch('http://localhost:3000/api')
    .then(res => res.json())
    .then(json => console.log(json))
    .catch(err => console.error(err));
```

* * *

```text
$ node client.js 

[ { musicSong: 'Paranoid android',
    musicBand: 'Radiohead',
    musicYear: 1997 },
  { musicSong: 'Just', musicBand: 'Radiohead', musicYear: 1995 },
  { musicSong: 'Take the power back',
    musicBand: 'Rage against the machine',
    musicYear: 1991 },
  { musicSong: 'How I could just kill a man',
    musicBand: 'Rage against the machine',
    musicYear: 2000 },
  { musicSong: 'La porte bonheur',
    musicBand: 'Ibrahim Maalouf',
    musicYear: 2014 } ]
```


## Client (HTML)

```html
<!DOCTYPE html>
<html>
    <head> 
        <meta charset="UTF-8"/> 
    </head>
    <body>
        <h1>myMusics</h1>
        <pre id="my_pre"> </pre>
        <script>
            const my_pre = document.getElementById("my_pre");
            const my_url = "http://localhost:3000/api";
            const formatMusic = m => `${m.musicSong}, by ${m.musicBand} (${m.musicYear})`;
            fetch(my_url)
                .then(res => res.json())
                .then(obj => my_pre.innerHTML = obj.map(formatMusic).join("\n"))
                .catch(err => my_pre.innerHTML = `error: ${err}`);
        </script>
    </body>
</html>
```

* * *

![](files/api/pfw_api_myMusics.png)


# En Haskell

## Conversion JSON/Haskell avec Aeson

- Aeson : bibliothèque Haskell pour encoder/décoder du JSON
- nombreuses fonctionnalités de parsing
- parsing automatique si le type de données correspond au JSON

* * *

```haskell
{-# LANGUAGE DeriveGeneric #-}

module Music where

import Data.Aeson (FromJSON, ToJSON)
import Data.Text.Lazy (Text)
import GHC.Generics (Generic)

data Music = Music 
    { musicSong :: Text 
    , musicBand :: Text 
    , musicYear :: Int 
    } deriving (Show, Generic)

instance FromJSON Music
instance ToJSON Music

myMusics :: [Music]
myMusics = 
    [ Music "Paranoid android" "Radiohead" 1997
    , Music "Just" "Radiohead" 1995
    , Music "Take the power back" "Rage against the machine" 1991
    , Music "How I could just kill a man" "Rage against the machine" 2000
    , Music "La porte bonheur" "Ibrahim Maalouf" 2014
    ]

findFromBand :: Text -> [Music]
findFromBand b = filter (\ m -> b == musicBand m) myMusics
```

## Serveur (scotty)

```haskell
{-# LANGUAGE OverloadedStrings #-}

import Network.Wai.Middleware.Cors (simpleCors)
import Web.Scotty (get, json, middleware, scotty)

import Music

main :: IO ()
main = scotty 3000 $ do
    middleware simpleCors
    get "/api" $ json myMusics
```

## Client (http-conduit)

```haskell
{-# LANGUAGE OverloadedStrings #-}

import Data.Aeson (decode)
import Network.HTTP.Conduit (simpleHttp)

import Music

main :: IO ()
main = do
  req <- simpleHttp "http://localhost:3000/api"
  let musicsM = decode req :: Maybe [Music]
  case musicsM of Nothing  -> putStrLn "error"
                  Just res -> mapM_ print res
```

* * *

```text
$ cabal run musicapi-http-client 

Music {musicSong = "Paranoid android", musicBand = "Radiohead", musicYear = 1997}
Music {musicSong = "Just", musicBand = "Radiohead", musicYear = 1995}
Music {musicSong = "Take the power back", musicBand = "Rage against the machine", musicYear = 1991}
Music {musicSong = "How I could just kill a man", musicBand = "Rage against the machine", musicYear = 2000}
Music {musicSong = "La porte bonheur", musicBand = "Ibrahim Maalouf", musicYear = 2014}
```

## Bibliothèque Servant

- bibliothèque pour définir et servir des API web
- principe : définir l'API comme un type Haskell
- [doc servant](https://haskell-servant.readthedocs.io/)

## Serveur (Servant)

```haskell
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
import Data.Text.Lazy (Text)
import Music
import Network.Wai.Handler.Warp (run)
import Servant ((:>), (:<|>)(..), Application, Capture, Get, Handler(..), 
                JSON, Proxy(..), Raw, serve, serveDirectoryWebApp, Server)

type API = "api" :> Get '[JSON] [Music]
    :<|> "api" :> "band" :> Capture "band" Text :> Get '[JSON] [Music]
    :<|> Raw

myServer :: Server API
myServer = musics :<|> musicsFromBand :<|> serveDirectoryWebApp "static"
    where musics :: Handler [Music]
          musics = return myMusics
          musicsFromBand :: Text ->  Handler [Music]
          musicsFromBand = return . findFromBand

myApi :: Proxy API
myApi = Proxy

myApp :: Application
myApp = serve myApi myServer

main :: IO ()
main = run 3000 myApp
```

## Client (Servant)

```haskell
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

import Data.Proxy (Proxy(..))
import Music
import Network.HTTP.Client (defaultManagerSettings, newManager)
import Servant.API ((:>), Get, JSON)
import Servant.Client (BaseUrl(..), client, ClientEnv(..), ClientM, Scheme(Http), runClientM)

type API = "api" :> Get '[JSON] [Music]

myApi :: Proxy API
myApi = Proxy

myQueries :: ClientM ([Music])
myQueries = client myApi

main :: IO ()
main = do
    manager <- newManager defaultManagerSettings
    res <- runClientM myQueries (ClientEnv manager (BaseUrl Http "localhost" 3000 ""))
    case res of Left err -> putStrLn $ show err
                Right m  -> print m
```

* * *

```text
$ curl "http://localhost:3000/api/band/Ibrahim%20Maalouf"

[{"musicSong":"La porte bonheur","musicYear":2014,"musicBand":"Ibrahim Maalouf"}]
```

