---
title: TP PFW, Environnement Haskell/Nix
date: 2019-04-16
---

## Installation

Si ce n'est pas déjà fait :

- [Installez Nixos](../env/post30-nixos.html) (et la configuration utilisateur fournie).

- Forkez/clonez le [dépôt git fourni](https://gitlab.com/juliendehos/M_PFW_etudiant).

## Utilisation de Cabal/Nix/Stack (haskell/mad)

Le projet `haskell/mad` fourni est un programme en Haskell qui calcule une
marche aléatoire 2D et la représente dans une image PNM. On veut tester la
compilation et l'exécution de ce projet avec Nix et avec Stack.

* * *

- Regardez le fichier `mad.cabal` et trouvez comment est organisé le projet.

- Regardez le fichier `default.nix` et testez les compilation/exécution/installation avec nix-shell, nix-build et nix-env.

- Regardez le fichier `stack.yaml` et testez les compilation/exécution avec Stack.
  N'oubliez pas d'utiliser l'option `--nix` sur Nixos.

## Rappels de Haskell, lecture fichier (haskell/hpack)

On veut écrire un programme `hpack` qui lit et affiche les lignes d'un
fichier dont le nom est passé en argument. 

<video preload="metadata" controls>
<source src="files/haskell/pfw_haskell_hpack.mp4" type="video/mp4" />
![](files/haskell/pfw_haskell_hpack.png)

</video>

* * *

- Écrivez des fichiers de configuration Cabal et Nix pour compiler le code 
  fourni.

- Réécrivez le `main` en notation do au lieu de la notation monadique fournie.

- Modifiez le code de façon à lire le fichier dans un `Data.Text` au lieu
  d'un `String`. 

- Modifiez le code de façon à afficher un message d'usage si les arguments
  de la ligne de commande ne sont pas donnés correctement.


## Rappels de Haskell, saisie clavier 

- Créez un projet `somme` qui affiche un message de test.
 
- Écrivez une fonction `somme :: String -> Maybe Int` qui récupère les deux
  éléments d'un texte, les convertit en entiers et retourne leur somme. Cette
  fonction doit retourner `Nothing` si le texte ne contient pas deux éléments
  mais on considère que les éléments représentent bien des entiers.

- Modifiez la fonction `main` de façon à saisir une ligne au clavier et à
  afficher la somme des deux entiers qu'elle représente. La saisie doit
  recommencer tant qu'on n'a pas saisi deux éléments. Utilisez le module
  `Data.Maybe`.

* * *

<video preload="metadata" controls>
<source src="files/haskell/pfw_haskell_somme.mp4" type="video/mp4" />
![](files/haskell/pfw_haskell_somme.png)

</video>

