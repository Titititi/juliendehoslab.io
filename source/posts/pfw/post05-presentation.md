---
title: Présentation du module PFW
date: 2019-04-16
---

## Objectifs du module

Ce module aborde le développement et le déploiement d'applications web à
travers la programmation fonctionnelle (Haskell, Nix, JavaScript).

## Intérêts de la programmation fonctionnelle pour le web

- approche fonctionnelle (pas d'effet de bord) :
    - réduit les sources d'erreur
    - reproduction, composition, isolation

- efficace en backend :
    - exemple d'application : détection de spam avec [facebook sigma](https://code.facebook.com/posts/745068642270222/fighting-spam-with-haskell/)
    - [nombreux frameworks web](https://github.com/Gabriel439/post-rfc/blob/master/sotu.md#server-side-programming) : happstack, snap, yesod...
    - parsing, programmation concurrente…

- intérêts du typage pour le web :
    - validation d'API, de routage
    - génération de code html/css valide
    - détection de failles de sécurité (injection SQL…)

## Organisation

- 24h de séances encadrées :
    #. Environnement Haskell/Nix 
    #. Génération de HTML/CSS et accès à des bases de données
    #. Frameworks web backend
    #. Déploiement et DevOps
    #. JavaScript et dev fullstack 
    #. API REST
    #. Frameworks web frontend
    #. Websockets, CGI… (ou pas)

## Évaluation

- contrôle continu

## Travaux pratiques

- TP sous Nixos. Voir la [page consacrée à Nixos](../env/post30-nixos.html)
- Utilisez les PC de la fac (Batiment Poincaré).
- Forkez le [dépôt git fourni](https://gitlab.com/juliendehos/M_PFW_etudiant).
- Vous aurez besoin d'un compte [Gitlab](https://gitlab.com/users/sign_in) 
  et d'un compte [Heroku](https://www.heroku.com/home).

