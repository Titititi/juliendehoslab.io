---
title: CM PFW, Environnement Haskell/Nix
date: 2019-04-16
---

# Configurer un projet en Haskell

## Cabal

- système de compilation de projets Haskell
- utilisation :
    - fichier de configuration `.cabal`
    - `cabal build` pour compiler
    - `cabal run` pour exécuter (compile si nécessaire)
    - `cabal haddock` pour générer la documentation
    - `cabal test` pour lancer les tests

* * *

- exemple de configuration (`annuaire.cabal`) :

```haskell
name:                annuaire
version:             0.1
build-type:          Simple
license:             MIT
cabal-version:       >=1.10

library
  hs-source-dirs:      src
  exposed-modules:     Annuaire
  ghc-options:         -Wall
  default-language:    Haskell2010
  build-depends:       base

executable chercher-annuaire
  main-is:             chercher-annuaire.hs
  hs-source-dirs:      app
  ghc-options:         -Wall
  default-language:    Haskell2010
  build-depends:       annuaire, base
  …
```

* * *

- exemple d'application (`app/chercher-annuaire.hs`) :

```haskell
import Annuaire
import System.Environment

annuaire :: Annuaire
annuaire = 
    [ Entree "toto" "toto@example.org"
    , Entree "tata" "tata@example.org"
    ]

main :: IO ()
main = getArgs >>= mapM_ (print . (chercher annuaire))
```

* * *

- exemple de module (`src/Annuaire.hs`) :

```haskell
-- |
-- Module: Annuaire
-- Permet de gérer des personnes d'après leur nom + email
module Annuaire where

-- | Entree de l'annuaire.
data Entree = Entree 
    { nomEntree   :: String -- ^ le nom
    , emailEntree :: String -- ^ le mail
    } deriving Show

-- | Annuaire.
type Annuaire = [Entree]

-- | cherche un nom dans l'annuaire
chercher :: Annuaire -> String -> Annuaire
chercher annuaire nom = filter ((==nom) . nomEntree) annuaire
```

* * *

- exemple de documentation générée :

![Documentation générée pour le module `Annuaire`](files/haskell/screenshot_haddock.png)

* * *

- cabal peut aussi gérer les dépendances, télécharger des bibliothèques, etc...
- mais en pratique ça ne marche pas $\rightarrow$ Nix ou Stack

## Cabal + Nix

- Nix fonctionne bien avec Haskell
- mise en oeuvre :
    - fichier de configuration Cabal classique
    - fichier de configuration Nix, qui appelle le fichier Cabal avec `callCabal2nix`
    - puis utiliser nix-env, nix-build ou nix-shell+cabal 

* * *

- exemple de `default.nix`, qui appelle `annuaire.cabal` :

```nix 
{ pkgs ? import <nixpkgs> {} }:
let 
  drv = pkgs.haskellPackages.callCabal2nix "annuaire" ./. {};
in
if pkgs.lib.inNixShell then drv.env else drv
```

- possibilité de fixer la version des paquets (dont les bibliothèques Haskell) :

```nix 
{ pkgs ? import (fetchTarball 
    "https://github.com/NixOS/nixpkgs/archive/18.03.tar.gz") {} }:
...
```

* * *

- développer dans un nix-shell :

```text
$ nix-shell
[nix-shell]$ cabal build
...
[nix-shell]$ cabal repl
...
[nix-shell]$ exit
```

- exécuter dans un nix-shell (avec compilation incrémentale) :

```text
$ nix-shell --run "cabal run"
$ nix-shell --run "cabal run chercher-annuaire toto"
```

* * *

- (re)compilation complète :

```text
$ nix-build
$ tree result
```

- installation :

```text
$ nix-env -f . -i
```

## Cabal + Stack

- outil classique pour construire des projets Haskell, quand Nix n'est pas
  disponible
- principe :
    - snapshots des bibliothèques Haskell sur [stackage](https://www.stackage.org/)
    - outil de construction [stack](https://docs.haskellstack.org)
- Stack complète ou remplace Cabal
- Stack peut fonctionner avec Nix
- sur Nixos, utiliser l'option `--nix` dans les commandes `stack`

* * *

- configuration Stack (`stack.yaml`) :

```yaml
resolver: lts-11.22
nix:
    enable: true
    pure: false
    packages: [zlib.dev, postgresql]
```

- utilisation :

```text
stack build --exec chercher-annuaire toto
stack ghci
stack test
stack haddock
```

# Rappels de Haskell

## Avertissement

La programmation fonctionnelle en général et Haskell en particulier peuvent
sembler déroutants...

* * *

![X as seen by Y](files/haskell/as_seen_by.jpg){width="90%"}

* * *

... apparemment c'est normal, il faut juste persévérer...

* * *

![[programming language learning curves](https://github.com/Dobiasd/articles/blob/master/programming_language_learning_curves.md)](files/haskell/learning_curves.png){width="80%"}


## Rappel sur les modules

- importer des modules :

```haskell
-- importe tout dans l'espace global (bof -> source de conflits)
import Data.Maybe
maybe ...

-- importe uniquement les éléments indiqués
import Data.ByteString.Lazy (fromStric, toStrict)
toStrict ...

-- importe via un alias obligatoire
import qualified Data.ByteString as B
B.intercalate ...
```

## Rappel sur les fonctions

- appel de fonction :

```haskell
print 42
print (21 * 2)
print $ 21 * 2
```

- définition de fonction :

```haskell
ajouter :: Int -> Int -> Int
ajouter x y = x + y
-- ajouter = \ x y -> x + y
-- ajouter = (+)
```

- opérateur/fonction : 

```haskell
2 + 3
(+) 2 3
mod 12 2
12 `mod` 2
```

* * *

- évaluation partielle :

```haskell
plus42 :: Int -> Int
plus42 = ajouter 42
-- plus42 y = ajouter 42 y
```

- composition :

```haskell
appliquer2fois f = f . f
-- appliquer2fois f x = (f . f) x
-- appliquer2fois f x = f (f x)

plus1positif = (>0) . (+1)
-- plus1positif x = (x + 1) > 0
```

## Rappel sur les classes de type

- classe de type : ensemble de fonctions utilisables sur tout type de la classe
- instance de classe : type implémentant les fonctions demandées
- exemple : un type de la classe `Num` doit implémenter les fonctions `+`, `-`, `*`, `negate`, `abs` et `signum`

```haskell
ajouter :: Num a => a -> a -> a
ajouter x y = x + y
-- ajouter = (+)

appliquer2fonctions :: (b -> c) -> (a -> b) -> a -> c
appliquer2fonctions g f x = g (f x)
-- appliquer2fonctions = (.)
```

## Rappel sur les monades

- monoïde : ensemble muni d'une loi de composition interne associative et d'un élément neutre
- monade : monoïde dans la catégorie des endofoncteurs (loi = composition de foncteurs, élément neutre = foncteur identité)

- en Haskell :

```haskell
class Monad m where
    return :: a -> m a
    (>>=) :: m a -> (a -> m b) -> m b    -- "bind"

    (>>) :: m a -> m b -> m b            -- "then"
    x >> y = x >>= \_ -> y
```

* * *

- intuition : design pattern implémentant la notion de composition/séquence
  (par exemple : entrées/sorties, gestion d'erreurs, accès concurrents...) 
- fonctions/opérateurs de IO : `return`, `>>=`, `>>` 
- notation `do`
- [transformateurs de monade](post20-cm-backend.html#transformateurs-de-monade)

## IO

- monade pour les entrées/sorties en Haskell
- une valeur de type IO est un calcul qui retourne une valeur (ou rien) : 
    `IO :: * -> *`
- quelques fonctions utilisant IO :
    - `putStr :: String -> IO ()`
    - `getLine :: IO String`
    - `getArgs :: IO [String]`
    - `getEnv :: String -> IO String`
- [doc sur System.IO](https://hackage.haskell.org/package/base/docs/System-IO.html)
- [doc sur System.Environment](http://hackage.haskell.org/package/base/docs/System-Environment.html)

* * *

- exemple (notation `do`) :

```haskell
main :: IO ()
main = do
  putStr "Entrez votre nom : "
  nom <- getLine
  let prefix = "Bonjour "
  putStrLn $ prefix ++ nom
```

- exemple (notation monade) :

```haskell
main = putStr "Entrez votre nom : " >> getLine >>= putStrLn . ((++) prefix)
    where prefix = "Bonjour "
```

```haskell
main = return "Entrez votre nom : " 
  >>= putStr
  >> getLine 
  >>= putStrLn . ((++) "Bonjour ")
```

## Maybe

- monade pour les valeurs optionnelles (par exemple, gestion d'erreur)
- `data Maybe a = Just a | Nothing`
- équivalent du `std::optional` en C++17
- quelques fonctions :
    - `isJust :: Maybe a -> Bool`
    - `fromJust :: Maybe a -> a`
    - `fromMaybe :: a -> Maybe a -> a`
    - `maybe :: b -> (a -> b) -> Maybe a -> b`
- [doc sur Maybe](https://hackage.haskell.org/package/base/docs/Data-Maybe.html)

* * *

- exemple :

```haskell
f1 :: Maybe String -> String
f1 Nothing = ""
f1 (Just s) = s

f2 :: Maybe String -> String
f2 ms = if isJust ms then fromJust ms else ""

f3 :: Maybe String -> String
f3 ms = fromMaybe "" ms
```

* * *

- exemple (notation monade) :

```haskell
maybeSqrt :: Double -> Maybe Double
maybeSqrt x = if x < 0 then Nothing else Just $ sqrt x

maybeDoubler :: Double -> Maybe Double
maybeDoubler x = Just $ 2.0*x

maybeCalculer :: Double -> Maybe Double
maybeCalculer x = maybeSqrt x >>= maybeDoubler

main = do
  print $ maybeCalculer 16.0
  print $ maybeCalculer (-9.0)
```

## Programmation concurrente avec la bibliothèque MVar

- monade pour les variables mutables synchronisées
- permet de gérer simplement des accès concurrents à une donnée :
    - `newMVar`
    - `modifyMVar_`
    - `readMVar`
- [doc sur MVar](https://hackage.haskell.org/package/base/docs/Control-Concurrent-MVar.html)

* * *

```haskell
import Control.Concurrent (MVar, newMVar, modifyMVar_, readMVar)

update :: MVar Int -> IO ()
update v = modifyMVar_ v (\ n -> return $ n + 1)

main = do
  myVar <- newMVar (0 :: Int)
  update myVar
  update myVar
  x <- readMVar myVar
  print x
```

## Data.Text

- implémentation efficace des chaines de caractères (unicode)
- [doc sur Data.Text](https://hackage.haskell.org/package/text)

```haskell
{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T

main = do
  let t1 = "toto" :: T.Text
  let t2 = "tata" :: T.Text
  print $ T.concat [t1, t2]
  print $ T.intercalate "; " [t1, t2]
```

## Data.ByteString

- implémentation efficace des chaines de caractères (8-bit)
- [doc sur Data.ByteString](https://hackage.haskell.org/package/bytestring)

```haskell
{-# LANGUAGE OverloadedStrings #-}

import qualified Data.ByteString as B

main = do
  let t1 = "toto" :: B.ByteString
  let t2 = "tata" :: B.ByteString
  print $ B.concat [t1, t2]
  print $ B.intercalate "; " [t1, t2]
```

## Récapitulatif sur les chaines de caractères en Haskell

- **5 types possibles** :
    - String
    - Text  
    - Text.Lazy
    - ByteString  
    - ByteString.Lazy
- comment choisir :
    - String en première approche
    - ByteString ou Text pour du texte efficace
    - faire au plus simple, en fonction des bibliothèques utilisées


