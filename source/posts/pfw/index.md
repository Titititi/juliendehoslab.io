---
title: Programmation Fonctionnelle pour le Web
---

Programmation fonctionnelle pour le web (PFW) avec Haskell, NixOS… Module du
Master Informatique de l'ULCO.
[Dépôt de code.](https://gitlab.com/juliendehos/M_PFW_etudiant)

