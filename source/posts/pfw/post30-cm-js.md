---
title: CM PFW, JavaScript et dev fullstack
date: 2019-04-16
---

# Le langage JavaScript

## Généralités

- « JavaScript » : marque déposée par Oracle
- langage ECMAScript (standard ECMA-262)
- dernière version majeure : ES2015 (ou ES6)
- docs : [normes officielles](https://www.ecma-international.org/publications/standards/Ecma-262.htm), [docs MDN](https://developer.mozilla.org/fr/docs/Web/JavaScript)
- nombreuses implémentations : navigateurs web, Node.js/V8…
- quelques caractéristiques :
    - orienté objet par prototype
    - typage dynamique faible
    - format JSON (JavaScript Object Notation)
    - fonctionnalités de programmation fonctionnelle

## Pattern matching, déconstruction

- JavaScript permet de déconstruire des objets
- mais pas de définition de fonction par pattern matching

```javascript
const data1 = [ "hello", "world" ];
const [ str1, str2 ] = data1;
console.log(str1);    // hello
console.log(str2);    // world

const data2 = [ 7, 42, 13, 37 ];
const [ x, ...xs ] = data2;
console.log(x);     // 7
console.log(xs);    // [ 42, 13, 37 ]

const data3 = { key: 42, value: "toto", other: {} };
const { value: v, key } = data3;
console.log(key);    // 42
console.log(v);      // toto
```

## Immuabilité

- **« const » ne signifie pas « immuable »**
- `const` indique juste une liaison nom-valeur constante
- si liaison non constante, utiliser `let` (éviter `var`)
- transparence référentielle à la charge du programmeur (ou Immutable.js)

```javascript
"use strict";

const data1 = [7];
data1 = [42];         // erreur
data1[0] = 42;        // ok

let data2 = [7];
data2 = [42];         // ok
data2[0] = [42];      // ok
```

* * *

- les références partagent les objets :

```javascript
const data1 = [7];
const data2 = data1;
data2[0] = 42;
console.log(data1);    // [ 42 ]
```

- la déconstruction réalise une copie :

```javascript
const data1 = [7];
const data2 = [...data1];
data2[0] = 42;
console.log(data1);    // [ 7 ]
```

## Fonctions, lambdas

- les fonctions sont des valeurs classiques (« first-class citizen »)
- 3 notations possibles : déclaration, liaison, « fonction flêchée »
- les fonctions flêchées définissent des expressions (donc pas besoin de `return` final)

```javascript
function ajouter_v1(x, y) { return x + y; }
console.log(ajouter_v1(1, 2));

const ajouter_v2 = function (x, y) { return x + y; };
console.log(ajouter_v2(1, 2));

const ajouter_v3 = (x, y) => x + y;
console.log(ajouter_v3(1, 2));
```

## Fonctions d'ordre supérieur

- une fonction peut prendre des fonctions en paramètre ou retourner une fonction

```javascript
function afficher(n, formater) {
  console.log(formater(n));
}

afficher(42, x => `x = ${x}`);      // affiche "x = 42"
afficher(7, x => `x vaut ${x}`);    // affiche "x vaut 7"
```

* * *

- utilisation classique : callback

```html
<!DOCTYPE html>
<html>
    <head> <meta charset="UTF-8"> </head>
    <body>
        <button id="my_button"> say hello </button>
        <script>
            const my_button = document.getElementById("my_button");
            my_button.onclick = function () { alert("hello"); };
                                // callback
        </script>
    </body>
</html>
```

## Mapping, filtrage, réduction

- fonctions `map`, `filter` et `reduce`
- applicables sur des objets itérables
- prennent des fonctions en paramètres
- possibilité de chaîner des traitements

* * *

```javascript
const v = [7, 42, 13, 37];

const v_double = v.map(x => x*2);
// [ 14, 84, 26, 74 ]

const v_impair = v.filter(x => x%2 == 1);
// [ 7, 13, 37 ]

const v_somme = v.reduce( (acc, x) => acc+x );
// 99

const v_sup20_neg = v.filter(x => x>20).map(x => -x);
// [ -42, -37 ]
```

## Curryfication, évaluation partielle

- possible mais peu utilisé

- exemple avec la notation classique :

```javascript
const add = function(x, y) { return x+y; };
console.log(add(1, 2));

const add_curry = function(x) { return function(y) { return x+y; } };
console.log(add_curry(3)(4));

const add1 = add_curry(1);
console.log(add1(5));
```

* * *

- exemple avec la notation flêchée :

```javascript
const add_f = (x, y) => x+y;
console.log(add_f(1, 2));

const add_curry_f = x => y => x+y;
console.log(add_curry_f(3)(4));

const add1_f = add_curry_f(1);
console.log(add1_f(5));
```

## Fonctions pures

- les effets de bord sont souvent un choix d'implémentation laissé au développeur

* * *

```javascript
function ajouter_impur(data, n) {
  data.push(n);
  return data;
}
const impur1 = [7, 42];
const impur2 = ajouter_impur(impur1, 23);
console.log(impur1);    // [ 7, 42, 23 ]
console.log(impur2);    // [ 7, 42, 23 ]

function ajouter_pur(data, n) {
  return [...data, n];
}
const pur1 = [7, 42];
const pur2 = ajouter_pur(pur1, 23);
console.log(pur1);    // [ 7, 42 ]
console.log(pur2);    // [ 7, 42, 23 ]
```

## Fermetures

- en JavaScript, les fermetures sont lexicales et dynamiques : une variable est cherchée d’après son nom, à l’évaluation
- lors de sa déclaration, une fonction peut utiliser une variable définie à l'extérieur, voire pas encore définie

* * *

```javascript
function addN(x) { return x + n; }
    // utilise une variable n extérieure à la fonction

function makeAddN(n) { return x => x+n; }
    // construit une fermeture qui inclut n

let n = 1;
const add1 = makeAddN(n);
console.log(addN(10));  // 11
console.log(add1(10));  // 11

n = 2;
console.log(addN(10));  // 12
console.log(add1(10));  // 11
```

* * *

> **Attention :**
> 
> Notion de fermeture avec `this` :
> 
> - `function` crée une nouvelle fermeture pour `this`
> - avec `=>`, c'est la fermeture englobante qui est utilisée pour `this`

* * *

```javascript
"use strict";

const obj = { 
    f1 : function() { return function () { return this; }; },
    f2 : function() { return () => this; }
};

const o1 = obj.f1()();
const o2 = obj.f2()();

console.log(o1 === obj);  // false
console.log(o2 === obj);  // true
```

## Monades

- `Promise` permet de chaîner des fonctions avec traitement d'erreur
- équivalent à la monade `Either` en Haskell
- exemple avec `fetch` :

* * *

```html
<!DOCTYPE html>
<html>
    <head> <meta charset="UTF-8"> </head>
    <body>
        <p id="my_p"> </p>
        <script>
            const my_p = document.getElementById("my_p");
            const url = "https://maps.googleapis.com/maps/api/timezone/json?location=38,140&timestamp=0";
            fetch(url)
                .then(response => response.json())
                .then(function(data) {
                    if (typeof(data.timeZoneId) === "undefined") throw "error";
                    else return `${data.timeZoneId}`})
                .then(tz_id => my_p.innerHTML = tz_id)
                .catch(err => my_p.innerHTML = err);
        </script>
    </body>
</html>
```

# Implémentations d'JavaScript

## Dans les navigateurs web

- raison d'être initiale de JavaScript
- accès au DOM + API W3C 
- nombreuses évolutions : DOM virtuel, WebAssembly…
- nombreux moteurs JavaScript :
    - SpiderMonkey (Firefox)
    - V8 (Chrome)
    - Chakra (Edge)
    - JavaScriptCore (Safari)…

* * *

```html
<!DOCTYPE html>
<html>
    <head> <meta charset="UTF-8"> </head>
    <body onload="startTime()">
        <div id="time_div"></div>
        <script>
            const time_div = document.getElementById('time_div');
            function startTime() {
                const d = new Date();
                time_div.innerHTML = d;
                setTimeout(startTime, 500);
            }
        </script>
    </body>
</html>
```

## Node.js/V8

- Node.js : 
    - JavaScript côté serveur
    - boucle événementielle asynchrone
- architecture : 
    - V8 (moteur JavaScript) 
    - libuv (IO asynchrones non-bloquantes) 
    - npm (gestionnaire de paquets)
- intérêt : développement d'application web côté serveur avec bonne montée en charge

* * *

```javascript
"use strict";

const express = require("express");
const app = express();

app.get("/hello", function (req, res) {
    res.send("hello");
});

app.get("/", function (req, res) {
    const d = new Date();
    res.send(`<html> <body> ${d} </body> </html>`);
});

const port = 5000;
const server = app.listen(port, function () {
    console.log(`Listening on port ${port}...`);
});
```

# Web fullstack

- développement complet d'une application : frontend, backend, BD, OS, gestion de projet…
- répartition d'une application côté-serveur / côté-client : serveur lourd +
  client léger, …, serveur léger + « Single-Page-Application »
- approche serveur lourd : meilleure maitrise du code
- approche serveur léger : moins de charge serveur
- JavaScript est utilisable côté-client et côté-serveur (avec Node), mais en
  pratique c'est plutôt un détail (problématiques + API assez différentes)

## Exemple d'application  

<video preload="metadata" controls>
<source src="files/js/pfw_ecmascript_fullstack.mp4" type="video/mp4" />
![](files/js/pfw_fullstack_1.png)

</video>

## Exemple d'application

- frontend (static/index.html) :

* * *

```html
<!DOCTYPE html>
<html>
    <head> <meta charset="UTF-8"/> </head>
    <body background="background.jpg">
        <input id="my_x" onkeyup="my_update()" onkeypress="my_update()"/>
        <p id="my_p"> </p>
        <script>
            const my_p = document.getElementById('my_p');
            const my_x = document.getElementById('my_x');
            function my_update(evt) {
                const x = my_x.value;
                if (x === "") {
                        my_p.innerHTML = "";
                }
                else {
                    const url = `square/${x}`;
                    fetch(url)
                        .then(function(resp) {
                            if (resp.ok) return resp.text();
                            else throw resp.statusText;})
                        .then(data => my_p.innerHTML = `the square of ${x} is ${data}`)
                        .catch(err => my_p.innerHTML = err);
                }
            }
        </script>
    </body>
</html>
```


* * *

- backend en Haskell :

```haskell
{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text.Lazy as L
import           Data.Maybe (fromJust, isNothing)
import           Web.Scotty (get, middleware, param, redirect, scotty, status, text)
import           Text.Read (readMaybe)
import           Network.HTTP.Types.Status (badRequest400)
import           Network.Wai.Middleware.Static (addBase, staticPolicy)

main :: IO ()
main = scotty 3000 $ do

    middleware $ staticPolicy $ addBase "static"

    get "/" $ redirect "index.html"

    get "/square/:x" $ do
        xMaybe <- readMaybe <$> param "x"
        let x = fromJust xMaybe :: Double
        if isNothing xMaybe
        then status badRequest400
        else text $ L.pack $ show $ x*x
```


* * *


- backend en Node (fichier `app.js`) :

```javascript
"use strict";

const express = require("express");
const app = express();

app.use("/", express.static(__dirname + "/static"));

app.get("/square/:x", function (req, res) {
    const xStr = req.params.x;
    const x = Number(xStr);
    if (isNaN(x)) res.status(400).send();
    else res.send((x*x).toString());
});

const port = 3000;
const server = app.listen(port, function () {
    console.log(`Listening on port ${port}...`);
});
```

* * *

- backend en Node (fichier `package.json`) :

```javascript
{
  "name": "fullstacknode",
  "version": "0.0.1",
  "private": "true",
  "scripts": {
    "start": "node app.js"
  },
  "dependencies": {
    "express": "4.*"
  }
}
```

- exécution avec Nixos :

```text
node2nix -8 --development
nix-shell -A shell
npm start
```

