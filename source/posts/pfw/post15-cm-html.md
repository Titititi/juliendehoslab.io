---
title: CM PFW, HTML/CSS, bases de données
date: 2019-04-16
---

## Généralités

- HTML/CSS/BD : éléments de base pour le dev web
- nombreuses bibliothèques Haskell :
    - HTML : lucid, blaze-html, xhtml
    - CSS : clay
    - BD : sqlite-simple, postgresql-simple, persistent
- également, bibliothèques de templating : yesod (hamlet, cassius…)
- intérêt :
    - expressivité
    - génération de code valide
    - réduction des failles de sécurité

## Lucid

- bibliothèque pour générer du HTML
- "notation do"
- [doc Lucid](https://hackage.haskell.org/package/lucid)

* * *

```haskell
{-# LANGUAGE OverloadedStrings #-}

import Lucid

myPage :: Html ()
myPage = do
  doctype_
  html_ $ do
    head_ $ do
      meta_ [charset_ "utf-8"]
    body_ $ do
      h1_ "my page"
      p_ $ toHtml $ concat ["toto", " ", "tata"]
      img_ [src_ "toto.png", width_ "200px"]

main = do
  print $ renderText myPage           -- sortie à l'écran
  renderToFile "my_page.html" myPage  -- sortie dans un fichier
```

## Clay

- bibliothèque pour générer du CSS
- [doc de Clay](https://hackage.haskell.org/package/clay)
- [site web de Clay](http://fvisser.nl/clay/)

* * *

```haskell
{-# LANGUAGE OverloadedStrings #-}

import qualified Clay as C
import           Data.Text.Lazy (toStrict)
import           Lucid

myCss :: C.Css
myCss = C.div C.# C.byClass "myCss" C.? do
  C.backgroundColor  C.beige
  C.border           C.solid (C.px 1) C.black

main = print $ renderText $ do
  doctype_
  html_ $ do
    head_ $ do
      meta_ [charset_ "utf-8"]
      style_ $ toStrict $ C.render myCss
    body_ $ do
      h1_ "test clay"
      div_ [class_ "myCss"] $ "bloublou"
```

## Configurer une base SQLite

- installer sqlite
- écrire un script SQL d'initialisation (`mybase.sql`) :

```sql
CREATE TABLE mytable (
  id INTEGER PRIMARY KEY,
  name TEXT
);
INSERT INTO mytable VALUES(0, 'John Doe');
```

- provisionner la base en utilisant le script :

```text
$ sqlite3 mybase.db < mybase.sql
```

## Sqlite-simple

- bibliothèque pour accéder à une base sqlite
- conçue pour être simple, efficace et sûre (notamment limiter les injections SQL)
- [doc sur sqlite-simple](https://hackage.haskell.org/package/sqlite-simple)
- pas mal de subtilités : `query`, `query_`, `execute`, `execute_`, `Only`, typage…

* * *

```haskell
{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import qualified Database.SQLite.Simple as SQL

main :: IO ()
main = do
  conn <- SQL.open "mybase.db"
  res <- SQL.query_ conn "SELECT * FROM mytable" :: IO [(Int,T.Text)]
  SQL.close conn
  print res
```

* * *

- `query_` : pas de paramètre mais des résultats

```haskell
res <- SQL.query_ conn
        "SELECT * FROM mytable"
        :: IO [(Int, T.Text)]
```

- `query` : des paramètres et des résultats

```haskell
res <- SQL.query conn
        "SELECT * FROM mytable WHERE id=? AND lower(name) LIKE ?"
        (1::Int, "t%"::T.Text)
        :: IO [(Int, T.Text)]
```

* * *

- `execute_` : pas de paramètre ni de résultat

```haskell
SQL.execute_ conn
        "INSERT INTO mytable VALUES (2, 'toto')"
```

- `execute` : des paramètres mais pas de résultat

```haskell
SQL.execute conn
    "INSERT INTO mytable VALUES (?, ?)"
    (3::Int, T.pack "tata")
```

* * *

- si un seul paramètre : `Only`

```haskell
SQL.execute_ conn
        "INSERT INTO mytable (name) VALUES (?)"
        (SQL.Only $ T.pack "titi")
```

- si résultat de type élémentaire : liste

```haskell
res <- SQL.query conn
        "SELECT name FROM mytable"
        :: IO [[T.Text]]
```

* * *

- si type algébrique pour les résulats : `FromRow`

```haskell
import Database.SQLite.Simple.FromRow (FromRow, fromRow, field)

data Name2 = Name2 T.Text T.Text

instance FromRow Name2 where
    fromRow = Name2 <$> field <*> field

main = do
    -- …
    res <- SQL.query conn "SELECT name, name FROM mytable" :: IO [Name]
```

## Configurer une base PostgreSQL

- éditer la configuration Nixos (`/etc/nixos/configuration.nix`) :

```nix
service = {
  postgresql = {
    enable = true;
    package = pkgs.postgresql;
    authentication = "local all all trust";
  };
  #...
};
```

- mettre à jour

```text
sudo nixos-rebuild switch
```

* * *

- écrire un script SQL d'initialisation (`mybase.sql`) :

```sql
DROP DATABASE IF EXISTS mydb;
CREATE DATABASE mydb;
\connect mydb

SELECT current_database();
SELECT current_user;

CREATE TABLE mytable (
 id INTEGER PRIMARY KEY,
 name TEXT
);
INSERT INTO mytable VALUES(0, 'John Doe');

DROP ROLE IF EXISTS toto;
CREATE ROLE toto WITH LOGIN PASSWORD 'toto';
GRANT SELECT ON TABLE mytable TO toto;
```

* * *

- provisionner la base en utilisant le script :

```text
$ psql -U postgres -f mybase.sql
```

- tester la base dans une console sql :

```text
$ psql "host='localhost' port=5432 dbname=mydb user=toto"
=> SELECT * FROM mytable;
```

## Postgresql-simple

- idem que sqlite-simple mais pour Postgresql
- [doc sur postgresql-simple](https://hackage.haskell.org/package/postgresql-simple)

```haskell
{-# LANGUAGE OverloadedStrings #-}
import qualified Data.Text as T
import qualified Database.PostgreSQL.Simple as SQL

main :: IO ()
main = do
  conn <- SQL.connectPostgreSQL "host='…' port=… dbname=… user=… password='…'"
  res <- SQL.query_ conn "SELECT * FROM mytable" :: IO [(Int,T.Text)]
  SQL.close conn
  print res
```


