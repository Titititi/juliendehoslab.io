---
title: TP PFW, Websocket 
date: 2019-04-16
---

## Serveur echo Haskell avec client Javascript (websockets/echows)

Le projet fourni contient une page web qui envoie un message à un programme
Haskell via un websocket.

- Testez l'application avec les trois méthodes suivantes :
    - nix-build
    - nix-shell + cabal run
    - nixops

- Complétez le code de façon à implémenter une application d'echo.

* * *

<video preload="metadata" controls>
<source src="files/websockets/echows.mp4" type="video/mp4" />
![](files/websockets/echows.png)

</video>


## Client echo Haskell (websockets/echo_client)

Le code fourni implémente un client websocket qui communique avec le serveur
d'echo `ws://echo.websocket.org`.

- Complétez l'implémentation du client de façon à envoyer des messages en
  boucle.  Le programme doit terminer lorsque l'utilisateur saisit un message
  vide.

* * *

```text
$ ./result/bin/echo-client 
to server: 
hello
from server: 
hello

to server: 
toto
from server: 
toto

to server: 

goodbye
```

## Chiffrement de César (websockets/echocaesar)

On veut implémenter une application websocket où le client envoie un message et
le serveur lui répond par le message criffré selon la méthode de César. Le
programme serveur en Haskell est fourni.

- Terminez l'implémentation du client (`index.html`).

- Modifiez l'implémentation du serveur de façon à utiliser la "notation do".

* * *

<video preload="metadata" controls>
<source src="files/websockets/echocaesar.mp4" type="video/mp4" />
![](files/websockets/echocaesar.png)

</video>

* * *

- Modifiez l'application (client + serveur) de façon à pouvoir changer la clé de chiffrement.

<video preload="metadata" controls>
<source src="files/websockets/echocaesarkey.mp4" type="video/mp4" />
![](files/websockets/echocaesarkey.png)

</video>




## Base de données (websockets/musicws et websockets/musicwspg)

On veut écrire une application websocket pour faire des recherches dans les
titres d'une base de données de musiques.

- Testez le projet `websockets/musics`.

- Terminez l'implémentation du serveur (`musicws.hs`).

* * *

<video preload="metadata" controls>
<source src="files/websockets/musicws.mp4" type="video/mp4" />
![](files/websockets/musicws.png)

</video>

* * *

- Testez le projet `websockets/musicwspg` en le déployant avec nixops.

- Modifiez le client de façon à pouvoir spécifier l'adresse du serveur WS.

* * *

<video preload="metadata" controls>
<source src="files/websockets/musicwspg.mp4" type="video/mp4" />
![](files/websockets/musicwspg.png)

</video>

## Chat (websockets/chat)

On veut implémenter une application de chat (client Javascript + serveur
Haskell + websockets). Le projet fourni contient une première version où les
clients sont gérés indépendamment. Modifiez le projet (client + serveur), de
façon à :

- implémenter la discussion entre plusieurs clients (en utilisant `MVar`)
- gérer les déconnexions clients (en utilisant `finally`)

Vous pouvez éventuellement vous inspirer de [l'exemple de Jasper Van der Jeugt](https://jaspervdj.be/websockets/example/server.html).

* * *

<video preload="metadata" controls>
<source src="files/websockets/chat.mp4" type="video/mp4" />
![](files/websockets/chat.png)

</video>

