---
title: TP PFW, API REST
date: 2019-04-16
---

## Musicapi (api/musicapi-js et api/musicapi-hs)

- En reprenant l'exemple du cours sur l'API de musique, implémentez :
    - un serveur en Node
    - un client en Node
    - un client en HTML
    - un serveur en Haskell
    - un client en Haskell

- Testez les différentes combinaisons client/serveur.

* * *

<video preload="metadata" controls>
<source src="files/api/pfw_api_musicapi.mp4" type="video/mp4" />
![](files/api/pfw_api_musicapi.png)

</video>

## Bmxriders (api/bmxriders-js)

Le projet fourni contient un serveur Node qui lit des données dans un fichier
`bmxriders.json` et sert une API JSON (`api`) et une page web
(`index.html`).  À noter que l'API correspond directement au fichier
`bmxriders.json` donc on aurait pu ici servir directement ce fichier.

- Lancez le serveur et tester l'API avec `curl` et avec un navigateur.

- Complétez la page web de façon à afficher les vignettes correspondant aux
  données récupérées via l'API.

- Implémentez un serveur équivalent en Haskell. Votre serveur doit lire le
  fichier `bmxriders.json` dans une structure de données Haskell pour
  retourner les données via l'API.

* * *

<video preload="metadata" controls>
<source src="files/api/pfw_api_bmxriders.mp4" type="video/mp4" />
![](files/api/pfw_api_bmxriders.png)

</video>

## Client timezone

L'API Google timezone permet de récupérer la timezone à partir de coordonnées GPS :

```text
$ curl "https://maps.googleapis.com/maps/api/timezone/json?location=51,1.9&timestamp=0"

{
   "dstOffset" : 0,
   "rawOffset" : 3600,
   "status" : "OK",
   "timeZoneId" : "Europe/Paris",
   "timeZoneName" : "Central European Standard Time"
}
```

* * *

- Complétez la page web fournie de façon à interroger l'API timezone et à
  afficher le `timeZoneId` des coordonnées saisies.

- Implémentez un programme Haskell qui permet d'interroger l'API timezone. Pour
  cela, utilisez la fonction `tlsQuery` fournie et la fonction `decode` de
  Aeson.

* * *

<video preload="metadata" controls>
<source src="files/api/pfw_api_timezone.mp4" type="video/mp4" />
![](files/api/pfw_api_timezone.png)

</video>

## Client omdbapi 

- Dans un navigateur, allez à l'adresse http://www.omdbapi.com/?apikey=PlzBanMe&t=metropolis et regardez le format JSON utilisé.

- Complétez le client Haskell fourni de façon à récupérer le titre, l'année et le réalisateur d'un film donné. Indications :
    - le type Haskell ne correspond pas directement à l'API donc vous devrez instancier explicitement `FromJSON` (voir la [doc d'Aeson](http://hackage.haskell.org/package/aeson/docs/Data-Aeson.html))
    - l'API fournit l'année sous forme de texte; au final, on veut un entier mais dans un premier temps vous pouvez modifier le type Haskell

* * *

<video preload="metadata" controls>
<source src="files/api/pfw_api_omdbapi.mp4" type="video/mp4" />
![](files/api/pfw_api_omdbapi.png)

</video>

## API Github

Github fournit une API web permettant de récupérer de nombreuses informations à
propos des dépôts qu'il héberge
(voir la [documentation de l'API Github](https://developer.github.com/v3)).

Par exemple, la route `/users/:user` retourne les informations d'un utilisateur
à partir de son login :

```text
$ curl https://api.github.com/users/juliendehos
{
  "login": "juliendehos",
  "id": 11947756,
  ...
}
```

* * *

La route `/repositories/:id` retourne les informations d'un dépôt à partir de
son identifiant :

```text
$ curl https://api.github.com/repositories/171292423
{
  "id": 171292423,
  "name": "ELF",
  "full_name": "juliendehos/ELF",
  "owner": {
    "login": "juliendehos",
    "id": 11947756,
    ...
  },
  ...
}
```

* * *

La route `/users/:user/repos` retourne les projets d'un utilisateur donnée :

```text
$ curl https://api.github.com/users/juliendehos/repos
[
  {
    "id": 171292423,
    "name": "ELF",
    "full_name": "juliendehos/ELF",
    "owner": {
      "login": "juliendehos",
      "id": 11947756,
      ...
    },
    ...
  },
  {
    "id": 171289022,
    "name": "haskellweekly.github.io",
    "full_name": "juliendehos/haskellweekly.github.io",
    "private": false,
    "owner": {
      "login": "juliendehos",
      "id": 11947756,
      ...
    },
    ...
  },
  ...
}
 
```

* * *

Le code fourni pour cet exercice (`api/github`) contient un programme client qui interroge l'API
Github. Il contient également un serveur qui réimplémente partiellement cette
API et un client pour ce serveur. Pour l'instant, une seule route, sur les
trois présentées précédemment, est implémentée. En vous inspirant des exemples
`servant` de 
[ce dépôt](https://gitlab.com/juliendehos/lillefp-2019-isomorphic/tree/master/archives),
implémentez les deux routes restantes.

```text
Right (Just (User {id = 11947756, login = "juliendehos"}))

Right (Repo {id = 171292423, name = "ELF", full_name = "juliendehos/ELF", owner = User {id = 11947756, login = "juliendehos"}})

Repo {id = 171292423, name = "ELF", full_name = "juliendehos/ELF", owner = User {id = 11947756, login = "juliendehos"}}
Repo {id = 171289022, name = "haskellweekly.github.io", full_name = "juliendehos/haskellweekly.github.io", owner = User {id = 11947756, login = "juliendehos"}}
...
```

## Routage "isomorphe"

- Clonez [ce dépôt](https://gitlab.com/juliendehos/lillefp-2019-isomorphic),
lancez un `make` dans le dossier `heroes-1.0` et laissez compiler pendant 2h.

- En partant du projet `heroes-0.1` et en suivant les explications données dans
  [ces slides](https://juliendehos.gitlab.io/lillefp-2019-isomorphic/),
implémentez l'application proposée. 

- Remplacez le "pop hero" par une fonctionnalité qui permet de sélectionner le
  "hero" à enlever.

- Ajoutez une page permettant d'ajouter des "hero", côté client.

- Utilisez le système SSE (Server-Sent Event) pour que l'ajout d'un "hero" se
  répercute sur tous les clients.


