---
title: CM PFW, Frameworks frontend
date: 2019-04-16
---

# Généralités

## Rappels sur le dev web frontend

- idée : 
    - intégrer du code côté client (typiquement du JS dans du HTML/CSS)
    - voire même développer une « Single Page Application »
- intérêts : 
    - pages plus dynamiques (callback, DOM…)
    - moins de charge côté serveur
- mise en œuvre :
    - code client : HTML/CSS/JS
    - communication avec le serveur : requêtes AJAX, API REST/JSON
    - éventuellement : architecture MVC côté client
    - éventuellement : framework frontend (ember, angular, react, vue…)

## Le frontend en Haskell (et langages dérivés)

- concepts classiques :
    - MVC fonctionnel (Model-View-Controler)
    - FRP (Functional Reactive Programming)
- mises en œuvre classiques :
    - avec Haskell et son écosystème
    - dans un langage « inspiré d'Haskell » + un écosystème spécifique
- principal intérêt de la prog. fonctionnelle : pas d'erreur au runtime
- mais : écosystème moins fourni et moins mature que pour le backend

* * *

- concrètement :
    - Ghcjs : compilateur Haskell vers JS (https://github.com/ghcjs/ghcjs)
    - Miso : MVC en Haskell
    - Reflex : FRP en Haskell
    - Elm : MVC avec langage/écosystème spécifiques
    - Purescript : langage/écosystème spécifiques; générique (MVC/FRP, front/back)
- dev fullstack dans le même langage : possible en Haskell (et en Purescript)
  mais l'intérêt n'est pas toujours évident (en JS non plus d'ailleurs…)
- autres écosystèmes de dev frontend fonctionnel : Ocsigen (OCaml), ClojureScript…

## MVC (Model-View-Controler)

- architecture classique pour développer des interfaces utilisateurs
- définit la structure de l'application : données, affichage, mise à jour
- architectures alternatives : Flux, MVVM…
- très adapté au fonctionnel (immuabilité, typage statique…)

## FRP (Functional Reactive Programming)

- style de programmation pour gérer des flux de données asynchrones
- deux types de base :
    - « event » : événements discrets
    - « behavior » : comportement continu
- peut-être utilisé conjointement avec un MVC

## Elm

![](files/frontend/logo_elm.png)

* * *

- langage inspiré d'Haskell mais plus simple (pas de classe de type…)
- écosystème complet, conçu pour le dev web (XHR, Websocket, JSON…)
- MVC + virtual DOM
- réputé simple et performant
- a influencé le monde JS : Redux, Vue…
- site web : http://elm-lang.org

* * *

- The Elm Architecture (TEA) :

![](files/frontend/elm_architecture.png)

## Miso

![](files/frontend/logo_miso.png)

* * *

- framework frontend en Haskell
- compilation en JS via Ghcjs
- MVC + virtual DOM
- inspiré de Elm mais en Haskell pur
- intérêt : architecture MVC classique dans du Haskell classique (mais moins
  simple/mature/performant que Elm)
- site web : https://haskell-miso.org

## Reflex

![](files/frontend/logo_reflex.png)

* * *

- bibliothèque Haskell de FRP générique
- projets associés : reflex-dom, reflex-platform…
- compilation via Ghc ou Ghcjs
- peut produire des applis : web, natives, android, ios 
- quelques liens :
    - [site web](https://reflex-frp.org/)
    - [tutorial reflex-platform](https://github.com/reflex-frp/reflex-platform)
    - [tutorial reflex-dom](https://github.com/hansroland/reflex-dom-inbits/blob/master/tutorial.md)
    - [intro par le QFPL](https://blog.qfpl.io/projects/reflex/)
    - [quickref reflex](https://github.com/reflex-frp/reflex/blob/develop/Quickref.md)
    - [quickref reflex-dom](https://github.com/reflex-frp/reflex-dom/blob/3eb4740a4abb8beff4664f00ce54b8b7420e7fd4/Quickref.md)

## Purescript

![](files/frontend/logo_purs.png)

* * *

- langage très proche d'Haskell, conçu pour compiler en JS
- écosystème orienté pour le web (front + back)
- packages pour le frontend : thermite, halogen, pux…
- plus puissant/générique que Elm mais moins mature/performant sur son domaine spécifique
- site web : http://www.purescript.org/

# Exemple d'application

## Cahier des charges

- côté serveur :
    - pages web, images, css…
    -  api JSON (`api/square/:x`)
- côté client :
    - un input pour saisir le paramètre à envoyer à l'API
    - une zone de texte pour afficher le résultat renvoyé par l'API
- dans la vraie vie, on n'utiliserait peut-être pas un framework pour une appli aussi simple…
- dépôt de code : [fullstaskell2](https://gitlab.com/juliendehos/fullstaskell2)

* * *

<video preload="metadata" controls>
<source src="files/frontend/frontend.mp4" type="video/mp4" />
![](files/frontend/frontend.png)

</video>

## Common (Haskell)

```haskell
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
module Common where

import Data.Aeson (FromJSON, ToJSON)
import Data.Text (Text)
import GHC.Generics (Generic)

data Square = Square
    { squareX :: Double
    , squareX2 :: Double
    , squareInfo :: Text
    } deriving (Eq, Generic, Show)

instance FromJSON Square 
instance ToJSON Square

computeSquare :: Double -> Square
computeSquare x = Square { squareX = x, squareX2 = x*x, squareInfo = "from computeSquare" }
```

## Backend (Haskell)

```haskell
{-# LANGUAGE OverloadedStrings #-}
import qualified Clay as C
import Common
import Data.Maybe (fromMaybe)
import Data.Text.Lazy (toStrict, unpack)
import Lucid
import Network.HTTP.Types.Status (badRequest400)
import Network.Wai.Middleware.Cors (simpleCors)
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Network.Wai.Middleware.Static (static)
import Web.Scotty (get, html, json, middleware, param, scotty, status, text)
import System.Environment (lookupEnv)
import Text.Read (readMaybe)
```

* * *

```haskell
main :: IO ()
main = do
    portMayStr <- lookupEnv "PORT"
    let port = fromMaybe 3000 (portMayStr >>= readMaybe)
    scotty port $ do
        middleware logStdoutDev
        middleware simpleCors
        get "/" $ html $ renderText $ html_ $ do
            head_ $ style_ $ toStrict $ C.render $ do
                C.body C.? C.backgroundImage  (C.url "static/haskell.jpg")
            body_ $ do
                h1_ "Fullstaskell2"
                ul_ $ do
                    li_ $ a_ [href_ "api/square/42"] "api/square/42"
                    li_ $ a_ [href_ "frontend_js/index.html"] "frontend_js"
                    li_ $ a_ [href_ "frontend_miso/index.html"] "frontend_miso"
                    li_ $ a_ [href_ "frontend_reflex/index.html"] "frontend_reflex"
                    li_ $ a_ [href_ "frontend_elm/index.html"] "frontend_elm"
                    li_ $ a_ [href_ "frontend_purescript/index.html"] "frontend_purescript"
        get "/api/square/:x" $ do
            xMaybe <- readMaybe <$> unpack <$> param "x" 
            case xMaybe of Nothing -> text "not a number" >> status badRequest400
                           Just x  -> json $ computeSquare x
        middleware static
```

## Frontend (js)

* * *

```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <link rel="stylesheet" type="text/css" href="static/custom.css">
    </head>
    <body>
        <p> <a href="../"> home page </a> </p>
        <h1>Compute Square (frontend_js)</h1>
        <input id="my_x" onkeyup="my_update()" onkeypress="my_update()"/>
        <p id="my_p"> </p>
        <script>
            const my_x = document.getElementById('my_x');
            const my_p = document.getElementById('my_p');
            function my_update(evt) {
                const x = my_x.value;
                const url = `/api/square/${x}`;
                fetch(url)
                    .then(resp => resp.json()) 
                    .then(data => my_p.innerHTML = `${data.squareX}^2 = ${data.squareX2}`)
                    .catch(err => my_p.innerHTML = "");
            }
        </script>
    </body>
</html>
```

## Frontend (reflex)

* * *

```haskell
{-# LANGUAGE OverloadedStrings #-}
import Common (Square(..))
import Data.Default (def)
import Data.Maybe (maybe)
import Data.Monoid ((<>))
import Data.Text (pack, Text)
import Reflex (holdDyn)
import Reflex.Dom 
import Reflex.Dom.Xhr (decodeXhrResponse, performRequestAsync, XhrRequest(..))

main :: IO ()
main = mainWidget $ do 
    el "p" $ elAttr "a" ("href" =: "../") $ text "home page"
    el "h1" $ text "Compute Square (frontend_reflex)"
    myInput <- textInput def
    evStart <- getPostBuild
    let evs = [ () <$ _textInput_input myInput, evStart ]
        evCode = tagPromptlyDyn (value myInput) (leftmost evs)
    evResponse <- performRequestAsync $ reqSquare <$> evCode
    let evResult = ((maybe "" fmtSquare) . decodeXhrResponse) <$> evResponse
    el "p" $ dynText =<< holdDyn "" evResult 

reqSquare :: Text -> XhrRequest ()
reqSquare code = XhrRequest "GET" ("/api/square/" <> code) def

fmtSquare :: Square -> Text
fmtSquare (Square x x2 _) = pack $ show x ++ "^2 = " ++ show x2
```

## Frontend (miso)

```haskell
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
import           Common (Square (..))
import           Data.Aeson (decodeStrict)
import qualified JavaScript.Web.XMLHttpRequest as XHR
import           Miso
import qualified Miso.String as MS

main :: IO ()
main = startApp App 
    { model = Model Nothing
    , update = updateModel
    , view = viewModel
    , subs = []
    , events = defaultEvents
    , initialAction = NoOp
    , mountPoint = Nothing
    }

data Model = Model { modelSquare :: Maybe Square } deriving (Eq, Show)

data Action 
    = FetchSquare MS.MisoString 
    | SetSquare (Maybe Square) 
    | NoOp 
    deriving (Show, Eq)
```

* * *

```haskell
viewModel :: Model -> View Action
viewModel (Model info) = div_ []
    [ a_ [ href_ "../" ] [ text "home page" ]
    , h1_ [] [ text "Compute Square (frontend_miso)" ]
    , input_ [ onInput FetchSquare ] 
    , p_ [] $ case info of 
        Nothing -> []
        Just (Square x x2 _) -> [text (MS.toMisoString x), text "^2 = ", text (MS.toMisoString x2)]
    ]

updateModel :: Action -> Model -> Effect Action Model
updateModel (FetchSquare str) m = m <# (SetSquare <$> queryApi str)
updateModel (SetSquare ms) m = noEff m { modelSquare = ms }
updateModel NoOp m = noEff m

queryApi :: MS.MisoString -> IO (Maybe Square)
queryApi str = do
    let uri = MS.pack $ "/api/square/" ++ MS.fromMisoString str
        req = XHR.Request XHR.GET uri Nothing [] False XHR.NoData
    Just contents <- XHR.contents <$> XHR.xhrByteString req
    return $ decodeStrict contents
```

## Frontend (elm)

```haskell
module Main exposing (..)

import Html exposing (..)
import Html.Attributes exposing (href)
import Html.Events exposing (onClick, onInput)
import Http
import Json.Decode as Decode

main : Program Never Model Msg
main = Html.program
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    }

type alias Square = { x : Float, x2 : Float }

type alias Model = { maybeSquare : Maybe Square } 

type Msg = NewInput String | NewApiSquare (Result Http.Error Square)

init : (Model, Cmd Msg)
init = (Model Nothing, Cmd.none)

subscriptions : Model -> Sub Msg
subscriptions _ = Sub.none
```

* * *

```haskell
view model =
    div []
        [ p [] [ a [ href "../" ] [ text "home page" ] ]
        , h1 [] [ text "Compute Square (frontend_elm)" ]
        , input [ onInput NewInput ] []
        , p [] 
            [ text ( case model.maybeSquare of
                        Nothing -> ""
                        Just s -> toString s.x ++ "^2 = " ++ toString s.x2
                   )
            ]
        ]

update : Msg -> Model -> (Model, Cmd Msg)
update msg model = case msg of 
                        NewInput txt -> (model, getSquare txt)
                        NewApiSquare (Ok s) -> (Model (Just s), Cmd.none)
                        NewApiSquare (Err _) -> (Model Nothing, Cmd.none)

getSquare : String -> Cmd Msg
getSquare txt = Http.send NewApiSquare (Http.get ("/api/square/" ++ txt) decodeApiSquare)

decodeApiSquare : Decode.Decoder Square
decodeApiSquare = Decode.map2 Square
                    (Decode.field "squareX" Decode.float)
                    (Decode.field "squareX2" Decode.float)
```

## Frontend (purescript)

```haskell
module Main where

import Control.Monad.Aff (Aff)
import Control.Monad.Eff (Eff)
import Data.Argonaut (class DecodeJson, decodeJson, (.?))
import Data.Either (hush)
import Data.Maybe (Maybe(..))
import Halogen as H
import Halogen.Aff as HA
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Halogen.VDom.Driver (runUI)
import Network.HTTP.Affjax as AX
import Prelude
```

* * *

```haskell
main :: Eff (HA.HalogenEffects (ajax :: AX.AJAX)) Unit
main = HA.runHalogenAff do
  body <- HA.awaitBody
  runUI ui unit body

ui :: forall eff. H.Component HH.HTML Query Unit Void (Aff (ajax :: AX.AJAX | eff))
ui = H.component
    { initialState: const initialState
    , render
    , eval
    , receiver: const Nothing
    }

newtype Square = Square { x :: Number, x2 :: Number } 

instance decodeJsonTodo :: DecodeJson Square where
    decodeJson json = do
        obj <- decodeJson json
        x <- obj .? "squareX"
        x2 <- obj .? "squareX2"
        pure $ Square { x, x2 }

type State = { square :: Maybe Square }

data Query a = ReqApiSquare String a

initialState :: State
initialState = { square: Nothing }
```

* * *

```haskell
render :: State -> H.ComponentHTML Query
render st =
    HH.div []
        [ HH.p []
            [ HH.a 
                [ HP.href "../" ] 
                [ HH.text "home page" ]
            ]
        , HH.h1 [] [ HH.text "Compute Square (frontend_purescript)" ]
        , HH.input [ HE.onValueInput (HE.input ReqApiSquare) ]
        , HH.p []
            [ HH.text $ case st.square of
                  Nothing -> ""
                  Just (Square sq) -> show sq.x <> "^2 = " <> show sq.x2
            ]
        ]

eval :: forall eff. Query ~> H.ComponentDSL State Query Void (Aff (ajax :: AX.AJAX | eff))
eval (ReqApiSquare username next) = do
        H.modify (_ { square = Nothing })
        response <- H.liftAff $ AX.get ("/api/square/" <> username)
        let sq = hush $ decodeJson response.response 
        H.modify (_ { square = sq })
        pure next
```

