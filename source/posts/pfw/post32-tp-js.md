---
title: TP PFW, JavaScript et dev fullstack
date: 2019-04-16
---

## Application « fullstack » (ecmascript/bmxriders_pgnode)

- Le projet fourni contient un serveur Node qui sert une route `bmxriders` et
  des fichiers statiques. Testez le serveur. Modifiez la page
  `static/index0.html` de façon à récupérer les données `bmxriders` sur le
  serveur et à les afficher directement dans la page.

* * *

<video preload="metadata" controls>
<source src="files/js/pfw_ecmascript_bmx1.mp4" type="video/mp4" />
![](files/js/pfw_ecmascript_bmx1.png)

</video>

* * *

- Modifiez la page `static/index1.html` de façon à récupérer les données
  `bmxriders` et à les afficher sous forme de vignettes.

* * *

<video preload="metadata" controls>
<source src="files/js/pfw_ecmascript_bmx2.mp4" type="video/mp4" />
![](files/js/pfw_ecmascript_bmx2.png)

</video>

* * *

- Modifiez la page `static/index.html` de façon à récupérer les données
  `bmxriders` et à les afficher sous forme de vignettes catégorisées.

* * *

<video preload="metadata" controls>
<source src="files/js/pfw_ecmascript_bmx3.mp4" type="video/mp4" />
![](files/js/pfw_ecmascript_bmx3.png)

</video>

* * *

- Réimplémentez le serveur en Haskell.

## Serveur de webcam

Le projet fourni contient un serveur web en Haskell qui sert une page statique
`index-http.html` et une image `out.png` générée à la volée à partir d'une webcam
via OpenCV.

- Lancez un nix-shell avec la commande `nix-shell --cores 8` et commencez à
  regarder les questions suivantes le temps que la compilation se fasse.

- Branchez une webcam, lancez le server web et allez sur la page 
  `<http://localhost:3042).

- Modifiez la page `index-http.html` afin que l'image se mette à jour toutes
  les 200 ms.

* * *

- Regardez le code du serveur `camserver-http.hs`. Comment sont générées les
  images par rapport aux requêtes HTTP et au flux vidéo ? Que se passe-t-il s'il
  y a plusieurs clients en même temps ?

- Modifiez le serveur de façon à rendre la capture d'images et la gestion des
  requêtes indépendantes. Pour cela, stockez les données de l'image dans un
  `IORef` que vous lirez dans la fonction `runServer` et que vous écrirez
  dans une fonction `runCam` exécutée dans un thread via `forkIO`.

* * *

<video preload="metadata" controls>
<source src="files/js/pfw-ecmascript-camserver.mp4" type="video/mp4" />
![](files/js/pfw-ecmascript-camserver.png)

</video>


