---
title: CM PFW, Déploiement et DevOps
date: 2019-04-16
---

# Généralités

## Comment réaliser un logiciel efficacement ?

- génie logiciel : modélisation, cycles de développement, formalismes
- méthodes agiles : itérations courtes, pilotages
- devops : intégration de l'exploitation du logiciel

![](files/devops/agile_methodology.jpg)

## Présentation du DevOps

Organisation centrée sur la réalisation du logiciel, du codage (*development*)
jusqu'à son exploitation (*operations*).

- principes : collaboration, automatisation, valeur ajoutée, métriques, partage
- méthodes : méthodes agiles, intégration/déploiement continus (CI/CD)…
- outils : système de compilation, forges, IaaS/PaaS/SaaS, conteneurs logiciels…
- intérêts : agilité, "time to market"

* * *

![](files/devops/Devops-toolchain.svg){width="50%"}

## Exemples de mise en œuvre

- historiquement :
    - code + issues + releases sur *Github*
    - intégration continue avec *TravisCI*
    - conteneur sur *Dockerhub*, ou déploiement avec *Kubernetes* + cloud
- système intégré : forge/CI/registry sur *Gitlab* + cloud
- avec nix : hydra, nixops, disnix

# Quelques outils pour développer en Haskell

## Configuration de projet Haskell

- organisation d'un projet Haskell (lib + app) :

```text
mymath
|-- app
|   |-- Main.hs         -- application exécutable
|-- default.nix         -- configuration Nix
|-- mymath.cabal        -- configuration Cabal
|-- src                 -- bibliothèque interne
|   |-- Mymath.hs
|-- stack.yaml          -- configuration Stack
|-- test                -- tests automatisés
    |-- MymathSpec.hs
    |-- Spec.hs
```

* * *

- configuration Cabal de base (`mymath.cabal`) :

```haskell
name:                mymath
version:             0.1
license:             MIT
build-type:          Simple
cabal-version:       >=1.10

library
  hs-source-dirs:      src
  exposed-modules:     Mymath
  ghc-options:         -Wall -O2
  default-language:    Haskell2010
  build-depends:       base

executable mymath-server
  main-is:             Main.hs
  hs-source-dirs:      app
  ghc-options:         -Wall -O2
  default-language:    Haskell2010
  build-depends:       base, mymath, scotty, lucid, text
  …
```

* * *

- utilisation avec Cabal + Nix (`mymath.cabal` + `default.nix`) :

```haskell
$ nix-shell --run "cabal run mymath-server"
```

- utilisation avec Stack (`mymath.cabal` + `stack.yaml`) :

```haskell
$ stack --nix build
$ stack --nix exec mymath-server
```

## Tests automatisés (avec hspec)

- différentes bibliothèques de test : quickcheck, hspec, hunit…

- hspec : découverte automatique de tests

- dossier `test`, même structure que le dossier `src` à tester :

```text
…
|-- test
    |-- MymathSpec.hs
    |-- Spec.hs
```

* * *

- configuration Cabal (`mymath.cabal`) :

```haskell
test-suite spec
  main-is:             Spec.hs
  hs-source-dirs:      test
  type:                exitcode-stdio-1.0
  ghc-options:         -Wall
  default-language:    Haskell2010
  build-depends:       base, mymath, hspec
  other-modules:       MymathSpec
```

- fichier de découverte hspec (`test/Spec.hs`) :

```haskell
{-# OPTIONS_GHC -F -pgmF hspec-discover #-}
```

* * *

- fichier de module de tests (`test/MymathSpec.hs`) :

```haskell
module MymathSpec where

import Test.Hspec
import Mymath

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "mymath" $ do
    it "square 2 == 4" $ square (2::Int) `shouldBe` 4
    it "square 3 == 9" $ square (3::Int) `shouldBe` 9
    it "square (-2) == 4" $ square (-2::Int) `shouldBe` 4
```

* * *

- exécuter les tests avec Cabal + Nix :

```text
[nix-shell]$ cabal test

Running 1 test suites...
Test suite spec: RUNNING...
Test suite spec: PASS
Test suite logged to: dist/test/mymath-0.1-spec.log
1 of 1 test suites (1 of 1 test cases) passed.
```

* * *

- exécuter les tests avec Stack :

```text
$ stack --nix test

mymath-0.1: test (suite: spec)

Mymath
  mymath
    square 2 == 4
    square 3 == 9
    square (-2) == 4

Finished in 0.0001 seconds
3 examples, 0 failures

mymath-0.1: Test suite spec passed
```

## Génération de documentation

- [Haddock](https://www.haskell.org/haddock/)

- d'après le code source + commentaires (comme javadoc, doxygen…)

* * *

- exemple (`src/Mymath.hs`) :

```haskell
-- |
-- Module: Mymath
-- Copyright: (c) 2018 Julien Dehos
-- License: MIT
--
-- Some math functions.
--
-- > import Mymath
-- > square 3 :: Int
module Mymath where

-- |Compute the square of x, i.e. x*x.
square :: Num a => a -> a
square x = x*x
```

* * *

- génération avec Cabal (résultat dans `dist/doc/html`) : 

```text
[nix-shell]$ cabal haddock
```

- génération avec Stack (résultat dans `.stack-work/install/…`) : 

```text
$ stack --nix haddock --open mymath
```

* * *

![](files/devops/pfw_haddock.png)

# Intégration continue avec Gitlab

## Principe des jobs/stages/pipelines

- job : 
    - unité élémentaire d'intégration continue (tache à réaliser)
    - exemples de jobs : compilation d'un module, exécution d'un jeu de test…
- stage :
    - étape de la chaine d'intégration continue (groupe de jobs)
    - exemples de stages : build, test, pages, deploy…
    - les jobs d'un stage sont réalisés en parallèle
- pipeline :
    - chaine d'intégration continue (séquence de stages)
    - les stages du pipeline sont réalisés séquentiellement

## Mise en œuvre

- fichier `.gitlab-ci.yml`
- à partir d'une image Docker (dockerhub…)
- + configurations supplémentaires
- + description du pipeline d'intégration continue à réaliser
- pipeline déclenché lors des git push (voir le menu CI/CD dans l'interface gitlab)
- et plein d'autres possibilités : 
    - génération/déploiement de doc
    - déploiement sur PaaS/registry/Kubernetes
    - …

* * *

![](files/devops/pfw_pipeline.png)

* * *

![](files/devops/pfw_job_ci.png)

## Exemples de `.gitlab-ci.yml`

- à partir d'une image `haskell` :

* * *

```yaml
image: haskell:8.2.2  # si possible, adapter au resolver du stack.yaml

build: # job "build" pour vérifier la construction
    stage: build
    script:
        - stack build

test: # job "test" pour vérifier les tests
    stage: test
    script:
        - stack test

pages: # job "pages" pour générer la doc et la déployer vers https://login.gitlab.io/projet
    stage: build
    script: 
        - stack haddock
        - mkdir -p public
        - cp -R .stack-work/install/x86_64-linux/lts-11.9/8.2.2/doc/mymath-0.1/* public/
    artifacts:
        paths:
            - public
```

* * *

- à partir d'une image `nixos/nix` :

* * *

```yaml
image: nixos/nix

before_script:
    - nix-env -iA nixpkgs.cabal-install nixpkgs.git

build: # job "build" pour vérifier la construction
    stage: build
    script:
        - nix-shell --run "cabal --http-transport=plain-http build"

test: # job "test" pour vérifier les tests
    stage: test
    script:
        - nix-shell --run "cabal --http-transport=plain-http test"

pages: # job "pages" pour générer la doc et la déployer vers https://login.gitlab.io/projet
    stage: build
    script: 
        - nix-shell --run "cabal --http-transport=plain-http haddock"
        - mkdir -p public
        - cp -R dist/doc/html/mymath/* public/
    artifacts:
        paths:
            - public
```

# Déploiement sur Heroku

## IaaS, PaaS, SaaS

- « cloud » = service informatique externalisé

- différents niveaux de « cloud » :
    - IaaS (Infrastructure as a Service) : système d'exploitation
    - PaaS (Platform as a Service) : runtime d'applications + données 
    - SaaS (Software as a Service) : applications

* * *

![](files/devops/pfw_xaas_1.png)

* * *

![](files/devops/pfw_xaas_2.jpg){width="80%"}

* * *

![](files/devops/pfw_xaas_3.jpg)

## Docker : image, conteneur, registry

- docker : gestionnaire de conteneurs virtuels, basé sur LXC
- image : image de système logiciel
- conteneur : image en cours d'exécution
- registry : dépôt d'images

![](files/devops/pfw_docker_taxo_1.png)

* * *

- utilisations classiques :
    - récupérer une image pour exécuter une tache (CI Gitlab)
    - créer une image pour packager un logiciel (registry Gitlab, Dockerhub)
    - déployer un logiciel dans le cloud

![](files/devops/pfw_docker_taxo_2.png)

## Principe du déploiement sur Heroku

- PaaS :
    - applications Node, Ruby, Java, PHP, Python…
    - + buildpack (dont haskell)
    - base de données Postgresql, Redis…
- fonctionnement :
    - via git (direct, heroku-cli, github)
    - ou via des images docker

## Le client Heroku (heroku-cli)

- permet de gérer un déploiement d'application (création, déploiement, monitoring…)

- basé sur git

- si le paquet heroku-cli est cassé dans Nixos, l'installer à la main : 

```text
$ nix-env -iA nixos.nodejs-8_x nixos.nodePackages.node2nix nixos.postgresql
$ git clone https://github.com/heroku/cli.git heroku-cli
$ cd heroku-cli
$ git checkout 3b5eb335cf35163cf4b3abdc9526a3428eadbc1c
$ node2nix -8
$ nix-env -f . -iA package
```

## Déployer une application Haskell simple, avec heroku-cli

- mettre le code source de l'application dans un dépôt git (même local) 

- configurer le projet avec Stack

- ajouter un fichier `Procfile` indiquant la commande pour lancer l'application :

```yaml
web: mymath-server
```

- pour lancer l'application, Heroku exporte la variable d'environnement PORT
  puis lance la commande du `Procfile`

* * *

- lancer le déploiement sur Heroku :

```text
$ heroku login
$ heroku create --buildpack https://github.com/mfine/heroku-buildpack-stack.git
$ git push heroku master
$ heroku open
$ heroku logout
```

![](files/devops/pfw_heroku_2.png)

* * *

- éventuellement administrer l'application sur l'interface web de Heroku

![](files/devops/pfw_heroku.png)


## Déploiement continu (Gitlab + Heroku)

- créer une application sur Heroku (avec heroku-cli ou avec l'interface web)

- configurer un projet Gitlab avec CI et ajouter un job dans le pipeline :

```yaml
production:
    stage: deploy
    script:
        - git push https://heroku:$HEROKU_API_KEY@git.heroku.com/$HEROKU_APP_NAME.git HEAD:master
    only:
        - master
```

* * *

- sur Heroku, récupérer le nom de projet et l'API_Key (menu « Account settings ») :

![](files/devops/pfw_cd_1.png)

* * *

- sur Gitlab, configurer les variables HEROKU_API_KEY et HEROKU_APP_NAME 
  (menu Settings > CI/CD > Variables) :

![](files/devops/pfw_cd_2.png)

* * *

- sur le dépôt local, pusher vers Gitlab (vérifier le CI/CD Gitlab puis
  l'application sur Heroku) :

![](files/devops/pfw_cd_3.png)

## Déployer une application avec base de données

- créer une base, par exemple avec heroku-cli après le `heroku create` :

```text
$ heroku addons:create heroku-postgresql:hobby-dev
```

- provisionner la base (pas besoin d'administrer, uniquement créer et remplir
  les tables) :

```text
$ heroku psql -f bmxriders.sql
```

* * *

- page web pour monitorer la base :

![](files/devops/pfw_heroku_bd_1.png)

* * *

- page web pour monitorer l'application :

![](files/devops/pfw_heroku_bd_2.png)

* * *

- application déployée :

![](files/devops/pfw_heroku_bd_3.png)

* * *

- Heroku transmet à l'application les paramètres d'accès via la variable
  d'environnement DATABASE_URL

- on peut récupérer les paramètres d'accès et l'utiliser la base Heroku dans
  une application locale mais **attention aux paramètres d'accès en clair**

- pour éviter ça, on peut passer par heroku-cli :

```text
$ heroku login
$ PORT=3000 DATABASE_URL=`heroku config:get DATABASE_URL` nix-shell --run "cabal run"
```

# L'écosystème Nix

## Aparté 

Comment échouer avec une bonne idée ?

- trouver une idée originale en avance de 10 ans 
- implémenter des fonctionnalités puissantes et innovantes
- faire une thèse
- publier dans des conférences scientifiques
- utiliser un logo "recherché" (par ex. des lambdas qui forment un hexagone)

* * *

Comment réussir avec une mauvaise idée ?

- reprendre une idée existante pas trop bonne (pour éviter la concurrence)
- simplifier au maximum l'utilisation
- apprendre le marketing
- faire de la pub, négocier des partenariats, vendre du service
- utiliser un logo "cool" (par ex. une baleine rigolote, une pomme croquée...)

* * *

Le summum de la réussite :

- The Challenge of "Good Enough" Software (James Bach)
- Feature: A bug as described by the marketing department (Apple II Reference Manual)
- If majority is always right, let's eat shit... millions of flies can't be wrong (Waldemar Łysia)

## Machines virtuelles, conteneurs

![](files/devops/docker1.png){width="50%"}

![](files/devops/docker2.png){width="50%"}

## Principes et intérêts de Nix

- système de dérivations
- principes fonctionnels (sans effet de bord)
- léger, reproductible, composable, isolé
- logiciels + services
- paquet, conteneur (nix container), micro-service (disnix), système (nixops)

# Déployer avec Nixops

## Présentation de Nixops

- outil de déploiement basé sur Nix
- ressources : serveurs web, bases de données, webservices...
- cibles : Nixos, VirtualBox, Amazon EC2, Google Cloud Engine, Microsoft Azure...
- [documentation nixops](https://nixos.org/nixops/manual/)

## Exemple : déployer un serveur web

- `index.html` :

```html
<!DOCTYPE html>
<html>
  <body>
    <h1>My webpage</h1>
  </body>
</html>
```

* * *

- `default.nix` :

```nix
{ pkgs ? import <nixpkgs> {} } :
with pkgs;
stdenv.mkDerivation {
  name = "mywebpage";
  src = ./.;
  installPhase = ''
    mkdir $out
    cp index.html $out/
  '';
}
        
```

## Spécification nixops 

- décrit les ressources à déployer et la cible de déploiement (`nixops.nix`) :

* * *

```nix
{
  network.description = "my webpage";
  webserver = { config, pkgs, ... }: {

    # spécification logique
    networking.firewall.allowedTCPPorts = [ 80 ];
    services.httpd = {
      enable = true;
      adminAddr = "toto@example.org";
      documentRoot = import ./default.nix { inherit pkgs; };
    };

    # spécification physique
    deployment = {
      targetEnv = "virtualbox";
      virtualbox = {
        memorySize = 512; 
        vcpu = 1; 
        headless = true;
      };
    };
  };
}
```

## Construction, déploiement

- créer un nouveau déploiement (par exemple `mywebpage-vm`) à partir des
  spécifications :

```text
$ nixops create -d mywebpage-vm nixops.nix
created deployment ‘68e7061c-9e1f-11e7-9f3d-d1c616a96c9d’
68e7061c-9e1f-11e7-9f3d-d1c616a96c9d
```

- effectuer le déploiement (installation/configuration de la machine cible) :

```text
$ nixops deploy -d mywebpage-vm
webserver> creating VirtualBox VM...
...
webserver> activation finished successfully
mywebpage-vm> deployment finished successfully
```

## Liste des déploiements

```text
$ nixops list
+------     -+--------------+------------+------------+
| UUID       | Name         | # Machines |    Type    |
+------ ... -+--------------+------------+------------+
| 68e70      | mywebpage-vm |          1 | virtualbox |
+------     -+--------------+------------+------------+
```

## Monitoring

```text
$ nixops info -d mywebpage-vm
Network name: mywebpage-vm
Network UUID: 68e7061c-9e1f-11e7-9f3d-d1c616a96c9d
Network description: my webpage
...
+-----------+-----------------+-     -+----------------+
| Name      |      Status     |       | IP address     |
+-----------+-----------------+- ... -+----------------+
| webserver | Up / Up-to-date |       | 192.168.56.102 |
+-----------+-----------------+-     -+----------------+
```

## Résultat du déploiement

![](files/devops/nixops_mywebpage.png)

## Accès ssh

- se connecter en SSH à la VM 

```text
$ nixops ssh -d mywebpage-vm webserver

[root@webserver:~]# journalctl -u httpd
Sep 20 16:21:39 webserver systemd[1]: Starting Apache HTTPD...
Sep 20 16:21:40 webserver systemd[1]: Started Apache HTTPD.
```

- on peut également monter le système de fichier de la VM sur la machine locale
  avec `nixops mount` (nécessite le kernelModule `fuse` et le paquet
  `sshfs-fuse`)

## Arrêt, démarrage

- commandes `stop` et `start` :

```text
$ nixops stop -d mywebpage-vm 
webserver> shutting down... Connection to 192.168.56.102 closed by remote host.
webserver> [running]
webserver> [poweroff]
```

## Suppression

- commandes `destroy` et `delete` :

```text
$ nixops destroy -d mywebpage-vm
warning: are you sure you want to destroy VirtualBox VM ‘webserver’? (y/N) y
webserver> destroying VirtualBox VM...
webserver> 0%...10%...20%...30%...40%...50%...60%...70%...80%...90%...100%

$ nixops delete -d mywebpage-vm
```

- option `--all` pour supprimer tous les déploiements

## Configuration de VirtualBox (ou pas)

- dans les préférences générales, ajouter un réseau privé :

![](files/devops/vbox_params_1.png)

* * *

- configurer la carte réseau virtuelle :

![](files/devops/vbox_params_2.png)

* * *

- configurer le DHCP :

![](files/devops/vbox_params_3.png)

# Nix + Docker + Heroku

## Exemple de déploiement d'une appli web C++.

![](files/devops/pfw_hellomin.png)

* * *

- `hellomin.cpp` :

```cpp
#include <Wt/WApplication.h>
#include <Wt/WBreak.h>
#include <Wt/WContainerWidget.h>
#include <Wt/WLineEdit.h>
#include <Wt/WText.h>

using namespace std;
using namespace Wt;

struct App : WApplication {
    App(const WEnvironment& env) : WApplication(env) {
        auto myEdit = root()->addWidget(make_unique<WLineEdit>());
        root()->addWidget(make_unique<WBreak>());
        auto myText = root()->addWidget(make_unique<WText>());
        auto editFunc = [=]{ myText->setText(myEdit->text()); };
        myEdit->textInput().connect(editFunc);
    }
};

int main(int argc, char **argv) {
    auto mkApp = [](const WEnvironment& env) { return make_unique<App>(env); };
    return WRun(argc, argv, mkApp);
}
```

## Solution 1 (pas optimisée du tout)

- `default.nix` :

```nix
with import <nixpkgs> {}; 
stdenv.mkDerivation {
  name = "hellomin";
  src = ./.;
  buildInputs = [ wt ];
  buildPhase = "g++ -O2 -o hellomin hellomin.cpp -lwthttp -lwt";
  installPhase = ''
    mkdir -p $out/bin
    cp hellomin $out/bin/
  '';
}
```

* * *

- `Dockerfile` :

```dockerfile
FROM nixos/nix
RUN nix-channel --add https://nixos.org/channels/nixos-18.09 nixpkgs
RUN nix-channel --update
WORKDIR /tmp
ADD . /tmp/
RUN nix-env -f . -i
CMD hellomin --docroot . --http-address 0.0.0.0 --http-port $PORT
```

* * *

- construction et test en local :

```text
$ docker build -t hellomin:latest .
$ docker run --rm -it -e PORT=3000 -p 3000:3000 hellomin:latest
```

* * *

- déploiement sur Heroku :

```text
$ heroku container:login
$ heroku create ma-super-app-wt
$ heroku container:push web --app ma-super-app-wt
$ heroku container:release web --app ma-super-app-wt
$ heroku open --app ma-super-app-wt
```

## Solution 2 (mieux)

- `default.nix` :

```nix
{ pkgs ? import <nixpkgs> {} }: 
pkgs.stdenv.mkDerivation {
  name = "hellomin";
  src = ./.;
  buildInputs = [ pkgs.wt ];
  buildPhase = "g++ -O2 -o hellomin hellomin.cpp -lwt -lwthttp";
  installPhase = ''
    mkdir -p $out/bin
    cp hellomin $out/bin/
  '';
}
```

* * *

- `docker.nix` :

```dockerfile
{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.09.tar.gz") {} }:
let
  myapp = import ./default.nix { inherit pkgs; };
  entrypoint = pkgs.writeScript "entrypoint.sh" ''
    #!${pkgs.stdenv.shell}
    $@ --docroot . --http-address 0.0.0.0 --http-port $PORT
  '';
in
  pkgs.dockerTools.buildImage {
    name = "hellomin";
    config = {
      Entrypoint = [ entrypoint ];
      Cmd = [ "${myapp}/bin/hellomin" ];
    };
  }
```

* * *

- construction et test en local :

```text
$ nix-build --cores 8 docker.nix && docker load < result
$ docker run --rm -it -e PORT=3000 -p 3000:3000 hellomin:latest
```

* * *

- déploiement sur Heroku :

```text
$ heroku container:login
$ heroku create ma-super-app-wt
$ docker tag hellomin:latest registry.heroku.com/ma-super-app-wt/web
$ docker push registry.heroku.com/ma-super-app-wt/web
$ heroku container:release web --app ma-super-app-wt
$ heroku open --app ma-super-app-wt
```



