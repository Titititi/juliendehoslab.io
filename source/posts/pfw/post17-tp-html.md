---
title: TP PFW, HTML/CSS, bases de données
date: 2019-04-16
---

## Génération de HTML/CSS (html/hpack2)

On veut écrire un programme `hpack2` qui lit les lignes d’un fichier et les
affiche dans une page HTML, sous forme de liste.  Le nom du fichier d'entrée et
le nom du fichier HTML généré sont passé en argument de la ligne de commande.

<video preload="metadata" controls>
<source src="files/html/pfw_html_hpack2.mp4" type="video/mp4" />
![](files/html/pfw_html_hpack2.png)

</video>

* * *

- Ajoutez une configuration Cabal et une configuration Nix permettant de
  compiler le projet fourni.

- Modifiez le code fourni de façon à implémenter le programme demandé.

## Base de données SQLite (html/music)

- Provisionner une base SQLite dans un fichier `music.db`, en utilisant
  le fichier sql fourni.

- Dans le fichier `Main.hs`, implémentez les requêtes SQL demandées.

* * *

```text
[nix-shell]$ cabal run

*** id et nom des artistes ***
(0,"Radiohead")
(1,"Rage against the machine")
(2,"Ibrahim Maalouf")

*** id et nom de l'artiste 'Radiohead' ***
(0,"Radiohead")

*** tous les champs des titres dont le nom contient 'ust' et dont l'id > 1 ***
(3,1,"How I could just kill a man")

*** noms des titres (en utilisant le type Name) ***
Name "Paranoid android"
Name "Just"
Name "Take the power back"
Name "How I could just kill a man"
Name "La porte bonheur"

*** noms des titres (en utilisant une liste de T.Text) ***
["Paranoid android"]
["Just"]
["Take the power back"]
["How I could just kill a man"]
["La porte bonheur"]
```

## Base de données PostgreSQL (html/music-pg)

- Créez un projet `music-pg` qui réalise la même chose que le projet
  `music` mais en utilisant une base PostgreSQL.

## Base de données + HTML/CSS (html/music-html)

- Créez un projet `music-html` qui affiche la liste des paires
  titres-artistes de la base PostgreSQL réalisée dans le projet `music-pg`.
  Pour cela, définissez un type enregistrement, avec les instanciations de
  classes nécessaires.

- Modifiez votre code de façon à générer du code HTML/CSS dans un fichier.

* * *

![](files/html/pfw_html_music_html.png)

## Megalozon.com (html/megalozon)

On veut implémenter un site de e-commerce basique qui affiche des produits
regroupés en catégories. Les données sont fournies (images, script
PostgreSQL).

* * *

<video preload="metadata" controls>
<source src="files/html/pfw_html_megalozon.mp4" type="video/mp4" />
![](files/html/pfw_html_megalozon.png)

</video>

* * *

- Implémentez un programme `megalozon1` qui affiche les données de la base.  Indications :

    - Écrivez des types `Category` et `Product` pour modéliser les données.
    - Écrivez une fonction `getCategory` qui retourne les catégories de la base.
    - Écrivez une fonction `getProducts` qui retourne les produits d'une catégorie donnée.

* * *

```text
[nix-shell]$ cabal run megalozon1

Category { idCategory = 0, nameCategory = "yachts"
         , imageCategory = "img/yacht.jpg"}
Category { idCategory = 1, nameCategory = "supercars"
         , imageCategory = "img/supercar.jpg"}
Category { idCategory = 2, nameCategory = "majordomes"
         , imageCategory = "img/majordome.jpg"}
Product { idProduct = 3, categoryProduct = 1, nameProduct = "couletach"
        , priceProduct = 2000000, imageProduct = "img/couletach.jpg"}
Product { idProduct = 4, categoryProduct = 1, nameProduct = "supercrade"
        , priceProduct = 1000000, imageProduct = "img/supercrade.jpg"}
```

* * *

- Implémentez un programme `megalozon2` qui lit les données de la base et génère les pages statiques correspondantes. Indications :

    - Générez une page principale, pour lister les catégories, et une page par catégorie, pour lister ses produits.
    - Toutes les pages ont la même structure : un ensemble de tuiles où chaque tuile est composée d'une image, d'un texte et d'un lien. Implémentez une fonction permettant de générer ce type de page et appelez-la pour générer chacune des pages.

