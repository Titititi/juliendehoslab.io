---
title: Installer NixOS 
date: 2019-09-17
---

## Motivation

Pour les modules HPC et PFA de master, vous aurez besoin d'un certain
nombre de logiciels, compilés avec des options particulières et de gérer des
services systèmes. Pour cela, vous allez installer et utiliser une distribution
NixOS sur un disque-dur rackable.

## Installer le système de base

#. **Branchez le disque-dur prévu pour les TP**.
#. Bootez sur la clé USB contenant l'installeur Nixos (F12 pour
   sélectionner le device à booter). Si la machine bloque sur un message "disk
   ... slow" ou autre, alors éteignez la machine, débranchez/rebranchez le
   disque-dur et recommencez.
#. Une console root doit s'afficher. Si des notifications USB apparaissent
   aléatoirement, débranchez la souris le temps de l'installation.
#. Suivez les instructions données avec la [configuration fournie](https://gitlab.com/juliendehos/ulco-nixos).


## Gérer des paquets systèmes

**À faire en tant qu'utilisateur (non root)**

- chercher le paquet, par example `hello` :

```text
nix search hello
```

- pour installer/supprimer des paquets, modifier le fichier
  ``~/.config/nixpkgs/packages.nix`` puis :

```text
home-manager switch
```


## Utilisation dans des projets 

On peut configurer un projet en écrivant un fichier de configuration
`default.nix` ou `shell.nix` puis en lançant un :

```text
nix-shell
```

L'environnement se charge et vous pouvez travailler comme sur un système
classique. Entrez `exit` ou `ctrl-d` pour quitter.


