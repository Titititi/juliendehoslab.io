---
title: Installer Haskell Stack
date: 2020-02-25
notoc: notoc
---

- Installez :

```bash
curl -sSL https://get.haskellstack.org/stable/linux-x86_64-static.tar.gz | tar xvz -C ~/.local
echo PATH=$PATH:`find $HOME/.local -maxdepth 1 -name "stack*"` >> ~/.bashrc
mkdir -p ~/.stack/global-project
echo "packages: []" > ~/.stack/global-project/stack.yaml
echo "resolver: lts-12.26" >> ~/.stack/global-project/stack.yaml
echo "extra-deps:" >> ~/.stack/global-project/stack.yaml
echo "    - random-1.1" >> ~/.stack/global-project/stack.yaml
source ~/.bashrc
```

- Si machine perso, installez les dépendances :

```bash
sudo apt install -y g++ gcc libc6-dev libffi-dev libgmp-dev make xz-utils zlib1g-dev git gnupg netbase
```

- Testez l'installation :

```bash
stack new myproject --resolver=lts-12.26
cd myproject
stack run
```

