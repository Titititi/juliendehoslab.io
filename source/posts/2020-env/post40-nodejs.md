---
title: Installer Node.js
date: 2019-12-16
notoc: notoc
---

Si votre logithèque système ne propose pas Node.js, ou une version trop ancienne, 
utilisez [nvm](https://github.com/nvm-sh/nvm) comme décrit ci-dessous.

- Téléchargez et installez `nvm` :

```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.1/install.sh | bash
```

- Fermez puis réouvrez le terminal

- Installez et activez la version 12 de `Node.js` :

```bash
nvm install 12
nvm use node 12
```

- Configurez `npm` de façon à passer le proxy (vérifiez d'abord que vous avez configurer votre [accès par proxy](post10-proxy.html)) :

```bash
npm config set proxy $http_proxy
npm config set https-proxy $https_proxy
```


