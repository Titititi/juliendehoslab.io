---
title: Installer VSCode
date: 2020-02-25
---

## VSCode

- [Téléchargez VSCode](https://code.visualstudio.com/docs/?dv=linux64)

- Installez :

```bash
tar zxf ~/Téléchargements/code-stable-*.tar.gz -C ~/.local
echo PATH=$PATH:$HOME/.local/VSCode-linux-x64/bin >> ~/.bashrc
source ~/.bashrc
```

## Extension pour Haskell

- Si machine perso, installez les dépendances :

```bash
sudo apt install -y libtinfo-dev
```

- Installez ghcide :

```bash
git clone https://github.com/digital-asset/ghcide.git
cd ghcide
stack --stack-yaml stack84.yaml install
echo PATH=$PATH:$HOME/.local/bin >> ~/.bashrc
source ~/.bashrc
```

- Lancez `code` et installez l'extension `ghcide`.

