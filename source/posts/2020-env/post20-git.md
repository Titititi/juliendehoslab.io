---
title: Configurer git
date: 2019-04-14
---

## Paramètres de l'utilisateur

Indiquez votre nom et adresse mail (pour les messages de commits) :

```text
git config --global user.name "John Doe"
git config --global user.email doe@nimpe.org
```

## Mots de passe (optionnel)

Pour garder les mots de passe en cache, par exemple pendant 3h :

```text
git config --global credential.helper 'cache --timeout=10800'
```

## Éditeur par défaut

Pour écrire les messages de commits, `git` lance l'éditeur de texte par défaut.
Pour le spécifier, vous pouvez modifier le fichier `~/.bashrc` (puis
faire un `source ~/.bashrc`). Par exemple, pour utiliser `nano` :

```bash
export EDITOR=nano
```

## Synchroniser un dépôt forké avec son dépôt initial

Pour les TP, des dépôts vous sont fournis. Vous devez les forker pour pouvoir
travailler avec vos propres dépôts. Si un dépôt initial a évolué, vous pouvez 
resynchroniser votre dépôt forké en exécutant les commandes suivantes :

```text
git remote add upstream https://gitlab.com/juliendehos/<dépôt initial>

git fetch upstream

git merge upstream/master
```

