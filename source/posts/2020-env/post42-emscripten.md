---
title: Installer Emscripten
date: 2019-12-16
notoc: notoc
---

- Récupérez le script d'installation :

```bash
cd
git clone https://github.com/emscripten-core/emsdk.git
cd emsdk
```

- Installez la dernière version d'emscripten :

```bash
./emsdk install latest
./emsdk activate latest
```

- Configurez votre shell :

```bash
echo 'source ~/emsdk/emsdk_env.sh' >> ~/.bashrc
```

- Fermez et réouvrez le terminal pour prendre en compte les modifications.

Si besoin, voir la [doc d'emscripten](https://emscripten.org/docs/getting_started/downloads.html).


