---
title: Projet GL 2016-2017 (Chifoumi)
---

## Instructions

- travail en binôme
- 3 séances de 3h
- privilégiez la mise en pratique correcte des méthodes et outils vus au cours
  du module plutôt que d'essayer de tout implémenter n'importe comment


# Sujet

On veut développer un jeu de
[chifoumi](https://fr.wikipedia.org/wiki/Pierre-feuille-ciseaux)
(pierre-feuille-ciseaux) jouable contre l'ordinateur, en console ou via une 
interface graphique, et on veut pouvoir analyser les parties via des journaux
de logs.


## Interface console

On lance le programme en spécifiant le nom du joueur. Le programme demande de 
saisir le coup à jouer, calcule le coup de l'ordinateur, affiche le résultat
et passe au coup suivant. 

```text
$ ./chifoumi_cli.out toto
**** Chifoumi ***
move (rock, paper, scissors, quit): rock
draw (rock-rock)
move (rock, paper, scissors, quit): scissors
draw (scissors-scissors)
move (rock, paper, scissors, quit): quit
```


## Interface graphique

On lance le programme en spécifiant le nom du joueur. L'interface propose un
bouton pour chaque coup possible. Lorsque l'utilisateur clique sur un bouton,
le programme calcule le coup de l'ordinateur, affiche le résultat dans une 
boite de dialogue et passe au coup suivant.

![](files/2016-2017-gl/TP_projet_chifoumi_gui.png)


## Journaux de logs

Les programmes (console et graphique) enregistrent les résultats dans des
journaux de logs au format :

```text
<nom du joueur>;<resultat>;<coup du joueur>;<coup de l'ordinateur>
```

Par exemple :

```text
Log file created at: 2016/12/29 00:59:21
Running on machine: narges
Log line format: [IWEF]mmdd hh:mm:ss.uuuuuu threadid file:line] msg
I1229 00:59:21.699193 12606 chifoumi_cli.cpp:34] toto;draw;rock;rock
I1229 00:59:27.731173 12606 chifoumi_cli.cpp:34] toto;draw;scissors;scissors
```

## Analyse des parties de jeu

On veut pouvoir analyser les parties jouées, notamment les résultats des
différents joueurs et leurs coups joués, en traitant les logs avec des scripts.

Un premier script permet d'extraire les données des logs dans un fichier CSV :

```text
$ ./run/format_logs.sh build/ data.csv
$ cat data.csv 
toto;draw;rock;rock
toto;draw;scissors;scissors
...
```

* * *

Puis un script permet de calculer le graphe des résultats :

```text
$ ./run/plot_results.sh data.csv plot_results.png
```

![](files/2016-2017-gl/TP_projet_chifoumi_plot_results.svg){width="80%"}

* * *

Et idem pour le graphe des coups joués :

```text
$ ./run/plot_moves.sh data.csv plot_moves.png
```

![](files/2016-2017-gl/TP_projet_chifoumi_plot_moves.svg){width="80%"}


# Organisation

## Organisation générale

On propose de réaliser le projet en 4 jalons :

- jalon 1 : projet de base (avec un programme "helloworld")
- jalon 2 : interface console et IA
- jalon 3 : analyse des parties de jeu
- jalon 4 : interface graphique

* * *

Conseils pour commencer le projet :

- créez un dépôt gitlab et ajoutez-y les 4 jalons (milestones)
- réfléchissez sur papier :
    - aux spécifications générales du projet
    - à l'organisation générale (programmes, documentation, tests, scripts...)
    - à la conception générale (classes)

* * *

Conseils pour réaliser un jalon :

- spécifiez et ordonnez les étapes à réaliser puis ajoutez-les dans le projet gitlab (issues)
- pour chaque étape pensez également aux tests et à la documentation 
- committez/pushez régulièrement et mettez à jour les étapes réalisées 
- échangez les rôles régulièrement dans le binôme


## Indications pour le jalon 1

Mettez en place tous les outils nécessaires pour réaliser le projet, notamment :

- système de compilation
- logs
- tests unitaires
- documentation développeur
- manuel utilisateur

Cf les TP.


## Indications pour le jalon 2

Pas de difficultés particulières. Pensez à reprendre le [module Random du projet drunk_player](https://gitlab.com/juliendehos/L3_GL_etudiant/tree/master/TP_documentation/drunk_player/src/drunk_player).


## Indications pour le jalon 3

- implémentez les logs demandés dans le sujet 

- pour simplifier, vous pouvez nommer les fichiers de logs en commençant par
  "log_chifoumi"

- utilisez `find`, `tail` et `sed` pour récupérer les données et écrire un
  fichier CSV

* * *

- utilisez `awk` pour agréger les données du CSV (cf cette
  [page sur awk](http://www.theunixschool.com/2012/06/awk-10-examples-to-group-data-in-csv-or.html));
  par exemple pour récupérer le nombre de victoires par joueur :
 

```bash
awk -F ";" '{a[$1][$2]++;}END{for (i in a) \
    print i, \
    a[i]["win"] == "" ? 0 : a[i]["win"];}' ${CSV} > ${TMP}
```


* * *

- utilisez `gnuplot` pour plotter les données récupérées (cf cette
  [page sur gnuplot](http://gnuplot.sourceforge.net/demo/histograms.html)); par exemple pour
  afficher les victoires par joueur :

```bash
gnuplot -e "set out '${PNG}'; \
    set terminal png size 720,405; \
    set title 'Results'; \
    set key inside right top vertical Right invert noenhanced nobox; \
    set style data histogram; \
    set style histogram rowstacked; \
    set style fill solid border -1; \
    set boxwidth 0.5; \
    plot '${TMP}' using 2:xtic(1) title 'win'" 
```

## Indications pour le jalon 4

Débrouillez-vous.


