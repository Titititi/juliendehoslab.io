---
title: PF TP7 - Mini-projet 1
date: 2020-02-25
notoc: notoc
---

## HS Paint

Implémentez un logiciel de dessin basique. Votre logiciel doit permettre de
dessiner à la souris en cliquant sur le bouton gauche et de nettoyer la zone de
dessin en cliquant sur le bouton droit.  S'il vous reste du temps, vous pouvez
ajouter un réglage de la brosse, avec aperçu (couleur et taille).

<video preload="metadata" controls>
<source src="files/hs-paint.mp4" type="video/mp4" />
![](files/hs-paint.png){width="50%"}

</video>

* * *

Vous pouvez partir du code de base suivant :

```haskell
{-# LANGUAGE OverloadedStrings #-}

import           GI.Cairo.Render
import           GI.Cairo.Render.Connector (renderWithContext)
import qualified GI.Gdk as Gdk
import qualified GI.Gtk as Gtk
import           GI.Gtk.Objects.Widget

handleDraw :: Gtk.DrawingArea -> Render Bool
handleDraw canvas = do
    moveTo 0 0
    lineTo 200 100
    stroke
    return True

handlePress :: Gdk.EventButton -> IO Bool
handlePress b = do
    Gtk.mainQuit
    return True
```

* * *

```haskell
main :: IO ()
main = do
    _ <- Gtk.init Nothing
    window <- Gtk.windowNew Gtk.WindowTypeToplevel 
    _ <- Gtk.onWidgetDestroy window Gtk.mainQuit

    canvas <- Gtk.drawingAreaNew
    _ <- Gtk.onWidgetDraw canvas $ renderWithContext $ handleDraw canvas
    Gtk.containerAdd window canvas 

    Gtk.widgetSetEvents canvas [ Gdk.EventMaskButtonPressMask ]
    _ <- onWidgetButtonPressEvent canvas handlePress 

    Gtk.widgetShowAll window
    Gtk.main
```

