---
title: PF TP2 - Conditions, pattern matching
date: 2020-02-25
---

# Conditions et gardes

## Conditions simples

- Écrivez une fonction `formaterParite` qui prend un `Int` et retourne un
  `String` indiquant sa parité. Utilisez un `if-then-else`.

```text
$ stack ghci conditions.hs

*Main> formaterParite 21
"impair"

*Main> formaterParite 42
"pair"
```

* * *

- Écrivez une fonction `formaterSigne` qui prend un `Int` et retourne un
  `String` indiquant si le nombre et négatif, nul ou positif. Utilisez un
  `if-then-else`.


## Gardes simples

- Écrivez des fonctions équivalentes aux fonctions `formaterParite` et
  `formaterSigne` précédentes mais en utilisant des gardes.


## Conditions et gardes locaux

- Écrivez une fonction `borneEtFormate1` qui prend un `Double` et affiche un
  message qui borne ce nombre dans $[0, 1]$. Utilisez un `if-then-else` dans un
  `where`.

```text
$ stack runghc borne-et-formate.hs
-1.2 -> 0.0
0.2 -> 0.2
2.2 -> 1.0
```

* * *

- Écrivez une fonction `borneEtFormate2` équivalente mais qui utilise des
  gardes (dans un `where`).


# Filtrage par motif

## Case-of simple

- Écrivez une fonction `fibo` implémentant Fibonacci avec un `case-of`.

```text
$ stack ghci case-of.hs

*Main> fibo 9
34
*Main> fibo 10
55
*Main> fibo 11
89
```

## Pattern matching simple

- Écrivez une fonction `fibo` implémentant Fibonacci avec un filtrage par motif.

```text
$ stack ghci pattern-matching.hs

*Main> fibo 9
34
*Main> fibo 10
55
*Main> fibo 11
89
```

## Case-of et pattern matching locaux

- Écrivez une fonction `formateNul1` qui prend un `Int` et retourne un `String`
  indiquant si le nombre est nul ou non nul. Utilisez une fonction dans un
  `where` avec un `case-of`.

```text
$ stack ghci formate-nul.hs

*Main> formateNul1 0
"0 est nul"

*Main> formateNul1 42
"42 est non nul"
```

* * *

- Écrivez une fonction `formateNul2` équivalente mais avec un filtrage par
  motif (via une fonction dans un `where`).


# Maybe

## readMaybe

- Écrivez un programme qui saisit une ligne et essaie d'en récupérer un `Int`.
  Indication : utilisez la fonction
  [readMaybe](https://hackage.haskell.org/package/base/docs/Text-Read.html#v:readMaybe).

```text
$ stack runghc read-maybe.hs
42
Just 42

$ stack runghc read-maybe.hs
foobar
Nothing
```

## safeSqrt

- Écrivez une fonction `safeSqrt` qui prend un `Double` et retourne peut-être
  un `Double`.

```text
$ stack ghci safeSqrt.hs

*Main> safeSqrt 16
Just 4.0

*Main> safeSqrt (-16)
Nothing
```

* * *

- Écrivez une fonction `formatedSqrt` qui prend un `Double` et retourne un
  `String` indiquant sa racine carrée ou un message d'erreur.

```text
$ stack runghc safeSqrt.hs
sqrt(16.0) = 4.0
sqrt(-16.0) is not defined
```


# Récapitulatif

## XOR

- Écrivez 5 implémentations du XOR :

    1. avec une expression booléenne
    1. avec un `if-then-else`
    1. avec un `case-of`
    1. avec des gardes
    1. avec un filtrage par motif

- Écrivez une fonction `testXor` qui prend une fonction et teste si les
  différents cas du XOR sont bien respectés.

## Guessing game

- Copiez le "guessing game" du TP précédent. Gérez les saisies invalides.
  Remplacez tous les `if-then-else` par des gardes ou par du filtrage par
  motif.

```text
$ stack runghc guessing-game.hs
Type a number (10 tries): 50
Too small!
Type a number (9 tries): foobar
Type a number (9 tries): 75
Too small!
...
```

## Dichotomie

- Écrivez un programme qui recherche le zéro d'une fonction $\mathbb{R}
  \rightarrow \mathbb{R}$ croissante, dans un intervalle donné et pour une
  précision $\epsilon$ donnée.

- Testez avec $f_1(x) = 2x - 84$ et $f_2(x) = x^2 - 42$, dans $[0, 100]$ et
  pour $\epsilon = 0.001$.

```text
$ stack runghc dicho.hs 
41.99981689453125
6.48040771484375
```

