---
title: PF - Présentation du module
date: 2020-02-25
---

## Objectifs du module

- avoir un peu de culture générale sur la programmation fonctionnelle
- utiliser un langage fonctionnel (Haskell)
- programmer selon une approche fonctionnelle

## Pré-requis 

- algorithmique

## Organisation

- 3h de CM

- 24h de TP :
    - TP1 - Découverte d'Haskell
    - TP2 - Conditions, pattern matching
    - TP3 - Liste, tuple, Maybe
    - TP4 - Fonctions récursives **+ DS1 (noté)**
    - TP5 - Traitements de liste, listes en compréhension
    - TP6 - Fonctions d'ordres supérieurs **+ DS2 (noté)**
    - TP7 - Mini-projet 1
    - TP8 - **Mini-projet 2 (noté, TPs précédents autorisés)**

## Travaux pratiques

- TP sous Linux.
- [Installez Haskell Stack](../2020-env/post45-stack.html).
- [Installez VSCode](../2020-env/post30-vscode.html).
- Et faites-vous un dépôt gitlab pour versionner vos TP. 

