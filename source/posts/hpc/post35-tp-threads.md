---
title: TP HPC, Threads
date: 2019-04-15
---

## Introduction

Le but de ce TP est d'utiliser les threads C++11 pour mettre en oeuvre des
calculs parallèles sans accès concurrent.  Le calcul à paralléliser est le
calcul de la célèbre "suite de Fibonacci modulo 42", définie par :

$$F^{42}(n) = 
\left\{
    \begin{array}{l}
        n \text{ si } n \in \{0, 1\} \\
        F^{42}(n-1) + F^{42}(n-2) \mod 42 \text{ sinon }
    \end{array}
    \right.$$

Cette suite est très utile pour... illustrer un TP de HPC sur les threads C++11.

* * *

> **Important :**
> 
> - Si ce n'est pas déjà fait, forkez/clonez le 
> [dépôt de code du module HPC](https://gitlab.com/juliendehos/M_HPC_etudiant). 
> - Travaillez dans le dossier `hpcThreads`.
> - Exécutez vos programmes avec un `nix-shell --run`.

## Découverte du projet et des threads C++ 

- Regardez comment est organisé le projet : fichiers `setup.py`, `cpp/Hello.cpp`, `cpp/Wrapper.cpp` et `scripts/hpcThreadsHello.py`.

- Testez le programme :

```text
$ nix-shell --run hpcThreadsHello.py 
Bonjour, je suis le thread 140079549934464
```

- Complétez le fichier `cpp/Hello.cpp` de façon à créer deux threads qui
  affichent leur identifiant, et à attendre la fin des threads avant de
  terminer le programme principal.

## Implémentation séquentielle 

Le programme fourni, calcule les $N$ premiers termes de la suite
$F^{42}$.  Pour l'instant, chaque terme est calculé séquentiellement (et
indépendamment...).

- Regardez l'implémentation séquentielle dans les fichiers `cpp/Fibo.cpp` et `scripts/hpcThreadsFiboTest.py`.
 
- Testez l'exécution.

```text
$ nix-shell --run "hpcThreadsFiboTest.py 20"
fiboSequentiel:
 [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 13, 5, 18, 23, 41, 22, 21, 1, 22, 23]
```

## Parallélisation sur deux threads

- Complétez la fonction `fiboBlocs` pour implémenter une parallélisation sur deux blocs. Si besoin, ajoutez une fonction auxiliaire (cf `calculerTout`).

- Modifiez le script `hpcThreadsFiboTest.py` et testez.

```text
$ nix-shell --run "hpcThreadsFiboTest.py 20"
fiboSequentiel:
 [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 13, 5, 18, 23, 41, 22, 21, 1, 22, 23]
fiboBlocs:
 [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 13, 5, 18, 23, 41, 22, 21, 1, 22, 23]
```

* * *

- Modifiez le script `hpcThreadsFiboTime.py` et regardez les temps de calcul.

```text
$ nix-shell --run "hpcThreadsFiboTime.py 30000"
sequentiel: 1.8934495449066162 s
blocs: 1.3450028896331787 s
```

## Équilibrage de charge

- Complétez la fonction `fiboCyclique2` de façon à mieux équilibrer la charge de calcul.

- Modifiez les scripts et testez.

## Paramétrage du nombre de threads

- Complétez la fonction `fiboCycliqueN` de façon à paralléliser sur un nombre de threads donné en paramètre. Pour cela, vous utiliserez un tableau de threads pour répartir les calculs (en équilibrant la charge...).

- Modifiez les scripts et testez.

## Analyse des performances

- Complétez la fonction `fiboCycliqueNFake` de façon à reprendre `fiboCycliqueN` mais en ignorant le résultat (pour mesurer le temps de calcul uniquement).

- Regardez et lancez le script `hpcThreadsFiboPlot.py`. 

- Testez le scripts et expliquez les graphiques obtenus.

* * *

![Temps d'exécution sur un processeur 6 coeurs hyper-threading.](files/tp-threads/out_times.png)

* * *

![Accélération sur un processeur 6 coeurs hyper-threading.](files/tp-threads/out_speedups.png)

* * *

![Passage à l'échelle sur un processeur 6 coeurs hyper-threading.](files/tp-threads/out_scalability.png)

