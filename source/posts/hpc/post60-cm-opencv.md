---
title: CM HPC, OpenCV
date: 2019-04-16
---

# Généralités 

## Difficultés du HPC

- problème à résoudre : problème complexe, nombreuses données
- méthode de résolution : complexité algorithmique, vitesse de convergence,
  stabilité, robustesse, précision
- technologie : difficulté d'implémentation, temps d'exécution, coût

$\rightarrow$ difficultés récurrentes

$\rightarrow$ outils classiques

## Outils classiques

- bibliothèques "bas-niveau" : blas, lapack, eigen, armadillo, numpy...
- bibliothèques spécialisées à un domaine : opencv, cgal, tensorflow...
- frameworks généralistes : matlab/octave, R, julia, sagemath...
- objectifs :
    - implémentation interne efficace : fortran, C, (C++), SIMD, SMP, GPU...
    - utilisation simple via des langages haut-niveau (C++, python, lua...) ou
      dédiés (matlab, R...)

## Présentation d'OpenCV

- bibliothèque open-source de vision artificielle (Computer Vision)
- avec traitement/analyse d'images, calibrage caméra, machine learning,
  interfaces graphiques... 
- portable : linux, windows, mac, android, ios, BSD...
- codé en C++ avec bindings python, java, matlab...
- peut utiliser SIMD, SMP, CUDA, OpenCL...
- nombreux utilisateurs industriels et académiques
- <https://docs.opencv.org>

## Mise en oeuvre (C++)

- bibliothèque classique :
    - installer la bibliothèque (souvent présente dans les paquets officiels)
    - include l'entête dans le code source
    - ajouter la bibliothèque à la compilation (configurations cmake et
      pkg-config disponibles)
- utilisation :
    - quelques classes de base : `cv::Mat`, `cv::Point`, `cv::Size`,
      `cv::Scalar` ...
    - nombreux modules et algorithmes

## Mise en oeuvre (Python)

- bibliothèque Pypi classique :
    - Nixos : paquet `nixos.python3Packages.opencv3`
    - autres OS : `virtualenv`, puis `pip install opencv-python` 
    - importer cv2 dans les scripts : `import cv2 as cv`
    - éventuellement importer aussi numpy : `import numpy as np`
- utilisation : très proche du C++
- particularités :
    - `numpy.ndarray` au lieu de `cv::Mat`
    - tuple au lieu de `cv::Size`, `cv::Point`...
    - image de sortie en valeur de retour plutôt que paramètre en référence

# Notion de matrice 

## La classe `cv::Mat` (C++)

```cpp
cv::Mat M1(2, 3, CV_64F);      // crée une matrice de double
assert(M1.rows == 2);          // nombre de lignes
assert(M1.cols == 3);          // nombre de colonnes
assert(M1.type() == CV_64F);   // type des coefficients
M1.at<double>(0, 2) = 42.0;    // accès à un élément
std::cout << M1 << std::endl;  // affichage

cv::Mat M2(2, 3, CV_8U);                     // matrice d'unsigned char
cv::Mat M3 = cv::Mat::zeros(3, 3, CV_64F);   // initialise les coefs à 0
cv::Mat M4 = cv::Mat::eye(3, 3, CV_64F);     // matrice identité

cv::Mat M5 = M1;           // M5 et M1 référence la même matrice
M1.copyTo(M5);             // M5 référence une copie de M1
cv::Mat M6 = M1.clone();   // M6 référence une copie de M1

cv::Mat V1 = (cv::Mat_<float>(1, 3) << 1, 2, 3);  // crée et initialise un vecteur
cv::Mat V2(1, 3, CV_32F);                         // idem en 2 instructions
V2 << 4, 5, 6;
cv::Mat V3 = 2.f * V1 + V1.dot(V1);             // quelques opérations sur les vecteurs
```


## Les tableaux nD avec `numpy.ndarray` (Python)

```python
M1 = np.ndarray((2, 3), dtype='d') # crée une matrice de double
assert((2,3) == M1.shape)          # nb lignes, nb colonnes
assert('float64' == M1.dtype)      # type des coefficients
M1[0,2] = 42                       # accès à un élément
print(M1)                          # affichage

M2 = np.empty((2, 3), 'uint8')     # matrice d'unsigned char
M3 = np.zeros((3, 3), 'float64')   # doubles, initialisés à 0
M4 = np.eye(3, dtype='d')          # matrice identité

M5 = M1                    # M5 et M1 référence la même matrice
M6 = M1.copy()             # M6 référence une copie de M1

V1 = np.asarray([1, 2, 3])      # crée et initialise un vecteur
V3 = 2.0 * V1 + np.dot(V1, V1)  # quelques opérations
```

## Utilisation des matrices/images

> **Attention, c'est à vous de gérer correctement vos matrices/images :**
> 
> - type des coefficients ?
> - nombre de canaux de couleur ?
> - valeurs dans $[0,1]$ ? dans $[0,255]$ ?
> - couleur en BGR ou en RGB ?
> - référence ou copie mémoire ?
> - compatibilité avec les différentes fonctions OpenCV ou Numpy ?

## Import/export  (Python)

- au format numpy :

```python
numpy.save('times', times)
# ...
times = numpy.load('times.npy')
```

- au format text :

```python
numpy.savetxt('times.csv', times)
# ...
times = numpy.loadtxt('times.csv')
```

- avec pandas :

```python
df.to_csv('times.csv')
# ...
df = pandas.read_csv('times.csv')
```

# Traitement d'images et de vidéos

## Gestion d'images couleurs (Python)

```python
# image 4 canaux
img1 = cv.ndarray((720, 1280, 4), 'uint8')

# modifie un pixel
img1[20, 20, :] = (255, 0, 0, 255)

# dessine un rectangle
p0 = (100, 200)
p1 = (300, 300)
couleur = (0, 0, 255, 255)
cv.rectangle(img1, p0, p1, couleur)
```

## Lecture/écriture de fichiers images (Python)

```python
# lit une image BGR (ou pas) dans d'un fichier
img2 = cv.imread("kermorvan.png")
assert(img2.shape[2] == 3)
assert(img2.dtype == 'uint8')

# convertit l'image en niveaux de gris (1 canal)
gray = cv.cvtColor(img2, cv.COLOR_BGR2GRAY)

# écrit l'image en niveau de gris dans un fichier
cv.imwrite("kermorvan_gray.png", gray)
```

## Interface graphique utilisateur (Python)

```python
img3 = cv.imread("kermorvan.png")

# affiche l'image
cv.imshow("image kermorvan.png", img3)

# lit le clavier en boucle et quitte si "esc"
while True:
    k = cv.waitKey() & 0xFF
    if (k == 27):
      break
```

## Interface graphique utilisateur (Python + Matplotlib)

```python
# mettre ces 4 lignes au début et dans cet ordre
import matplotlib as mpl
mpl.use('TkAgg')
from matplotlib import pyplot as plt
import cv2 as cv

imgInput = cv.imread("kermorvan.png")
imgInput = cv.cvtColor(imgInput, cv.COLOR_BGR2RGB)
plt.imshow(imgInput)
plt.xticks([])
plt.yticks([])
plt.show()
```

## Interface graphique utilisateur avec trackbar (Python)

```python
imgInput = cv.imread("kermorvan.png") / 255.0

# fonction de rappel pour le trackbar (capture imgInput)
def update_win(x):
    img = imgInput * x / 100
    cv.imshow('kermorvan', img)

# affiche l'image + trackbar
cv.namedWindow('kermorvan')
cv.createTrackbar('mytrackbar', 'kermorvan', 0, 100, update_win)
cv.setTrackbarPos('mytrackbar', 'kermorvan', 50)

while True:
    if cv.waitKey() == 27:
        break
```

## Flux vidéo avec affichage (Python)

```python
cap = cv.VideoCapture(0)           # lit depuis une caméra
#cap = cv.VideoCapture("bmx.mkv")  # lit depuis un fichier

while(cap.isOpened()):
    ret, frame = cap.read()
    if not ret:
        break

    cv.imshow('bmx', frame)
    if cv.waitKey(30) == 27:
        break
```

## Flux vidéo avec sortie fichier (Python)

```python
# flux d'entrée (camera)
cap = cv.VideoCapture(0)

# flux de sortie (fichier)
fourcc = cv.VideoWriter_fourcc(*'MPEG')
fps = cap.get(cv.CAP_PROP_FPS)
width = int(cap.get(cv.CAP_PROP_FRAME_WIDTH))
height = int(cap.get(cv.CAP_PROP_FRAME_HEIGHT))
out = cv.VideoWriter('output.avi', fourcc, fps, (width, height))

while cap.isOpened():
    ret, frame = cap.read()
    if not ret:
        break
    out.write(frame)
    cv.imshow('frame', frame)
    if cv.waitKey(1) & 0xFF == ord('q'):
        break
```

## Nombreux algorithmes

Par exemple, pour le filtrage d'images :

![](files/cm-opencv/fonctions_filtrage.png)

# Calcul parallèle avec OpenCV

## Fonctionnalités 

- SIMD/SMP $\rightarrow$ options de compilation d'OpenCV
- CUDA $\rightarrow$ modules dédiés
- OpenCL $\rightarrow$ Transparent-API

## Transparent-API OpenCL

- disponible depuis OpenCV 3
- classe `cv::UMat`  (Unified)
- équivalente à `cv::Mat` (mais pas toutes les fonctionnalités)
- via OpenCL (GPU/CPU)
- avec transferts mémoires automatiques

## Utilisation de la T-API (Python)

```python
# lit un fichier image dans le GPU :
img1 = cv.imread("kermorvan.png")  # charge l'image dans la RAM CPU
assert(img1.size != 0)
imgGpu1 = cv.UMat(img1)            # transfert l'image sur le GPU

# fait des calculs sur le GPU
imgGpu2 = cv.cvtColor(imgGpu1, cv.COLOR_BGR2GRAY)
_, imgGpu2 = cv.threshold(imgGpu2, 127, 255, cv.THRESH_BINARY)

# écrit et affiche l'image modifiée (peut-être après copie en RAM CPU)
cv.imwrite("kermorvan_seuille.png", imgGpu2)
cv.imshow("kermorvan seuille", imgGpu2)

while True:
    k = cv.waitKey(30)
    if k == 27:
        break
```

