---
title: Présentation du module de HPC
date: 2019-04-15
---

## Objectifs

- avoir un peu de culture générale sur le calcul haute performance
- utiliser quelques outils classiques de HPC 
- savoir quand et comment le faire (ou pas)

## Pré-requis (ou pas)

- notions d'algo
- système Unix, Nixos
- C++, Python

## Organisation

- 15h de séances encadrées :
    #. Généralités sur le HPC, rappels C++/Python, threads
    #. OpenMP
    #. MPI
    #. OpenCV
    #. OpenCL

## Évaluation

- contrôle continu 
- annales : 
    - [2015-2016 session 1](files/presentation/M_HPC_EX1_2015-2016.pdf)
    - [2015-2016 session 2](files/presentation/M_HPC_EX2_2015-2016.pdf)

## Travaux pratiques

- TP sous Nixos. Voir la [page consacrée à Nixos](../env/post30-nixos.html).
- Forkez le [dépôt git fourni](https://gitlab.com/juliendehos/M_HPC_etudiant). 
- Le depôt contient des configs Nix.

