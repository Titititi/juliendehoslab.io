---
title: CM HPC, Langages et logiciels
date: 2019-04-15
---

# Généralités

## Langages bas-niveau

- Fortran, C, C++
- performances maximales en calcul pur
- mais développement plus long

## Langages haut-niveau

- Python, R, Matlab, JavaScript...
- développement rapide, nombreux outils
- mais moins performants en calcul pur

## Pour le HPC

- calculs intensifs dans un langage bas-niveau
- interfacés dans un langage haut-niveau
- et pleins d'outils plus ou moins spécifiques 
- mise en place efficace difficile : matériel, compilation, déploiement...
- le combo C++/Python est classique : tensorflow, opencv, CNTK...
- récemment : langage Julia (haut-niveau et performant)

# Rappels de C++

## Objectifs

- rappeler quelques notions de C++ utilisées dans les TP 
- n'aborde pas les classes, templates...
- site de référence sur le C++ : <http://en.cppreference.com>

# Notions de base

## Fonctions

- habituellement en C++, on sépare la déclaration (signature) de la définition
  (corps) 
- pour ces TP vous pouvez écrire la définition en même temps que la déclaration
  (à la java)

```cpp
// déclare et définit une fonction
float maSuperFonction(float x) {
    return 42.f * x;
}

int main() {
    // appelle la fonction définie précédemment
    maSuperFonction(13.37f);
    return 0;
}
```

## Fonction main

- fonction principale d'un code C++ (point d'entrée)
- prend en argument les paramètres de la ligne de commande 
- retourne un entier

* * *

```cpp
#include <iostream>  // pour les affichages
int main(int argc, char ** argv) {
    // teste le nombre de paramètres donnés dans la ligne de commande
    if (argc != 3) {   
        // affiche un message d'usage
        std::cout << "usage: " << argv[0] << " <age> <taille>" << std::endl;
        // termine avec le code -1 (erreur)
        return -1;
    }
    // récupère les paramètres et les affiche
    const int age = std::stoi(argv[1]);         
    const float taille = std::stof(argv[2]);
    std::cout << "Vous avez " << age << " ans et mesurez " 
              << taille << "m." << std::endl;
    // termine avec le code 0 (succès)
    return 0;
}
```

## Références

- pointeurs simplifiés
- utiles notamment pour passer des paramètres sans copie intermédiaire.


* * *

```cpp
// prend une référence vers un tableau
void afficherTableau(const std::vector<int> & T) {
    // affiche les éléments du tableau
    for (int e : T)
        std::cout << e << ' ';
    std::cout << std::endl;
}
int main() {
    // crée un tableau à 200 éléments de valeur 0
    std::vector<int> v1(200, 0);
    // appelle la fonction sans copie intermédiaire du tableau
    afficherTableau(v1);
    return 0;
}
```

# Bibliothèque standard (STL)

## Entrées/sorties

- pour les entrées/sorties, la STL définit quelques classes, opérateurs et objets

```cpp
#include <iostream>             // pour cin et cout
int main() {
    std::cout << "Entrez a:";   // affiche un texte à l'écran
    int a;
    std::cin >> a;              // saisit un entier au clavier
    std::cout << "Vous avez entré a=" << a << std::endl;
    return 0;
}
```

## Chaînes de caractères

- classe `string`
- compatible avec les chaînes C
- et plein de fonctionnalités

```cpp
#include <string>
#include <iostream>
int main() {
    std::string s1 = "to";          // initialise une chaîne de caractères
    std::string s2 = s1 + s1;       // concaténation
    std::cout << s2 << std::endl;   // affichage
    const char * p = s1.c_str();    // accès à la chaîne C correspondante
    return 0;
}
```

## Fichiers

- classes `ifstream` et `ofstream` 
- et opérateurs d'entrée/sortie

```cpp
#include <fstream>
int main() {
    // crée un fichier "toto.txt" en écriture
    std::ofstream ofs("toto.txt");
    // écrit "toto42" dans le fichier
    ofs << "toto" << 42 << std::endl;
    return 0;
    // le fichier est fermé automatiquement en fin de portée
}
```

* * *

```cpp
#include <fstream>
int main() {
    // ouvre un fichier "toto.txt" en lecture
    std::ifstream ifs("toto.txt");
    // lit le fichier ligne par ligne
    std::string line;
    while (std::getline(ifs, line)) {
        // affiche les lignes à l'écran
        std::cout << line << std::endl;
    }
    return 0;
    // le fichier est fermé automatiquement en fin de portée
}
```

## Tableaux dynamiques

- classe `vector`
- tableau à accès aléatoire avec gestion mémoire et taille dynamique

* * *

```cpp
#include <vector>
#include <iostream>
int main () {
    // crée un tableau avec 3 valeurs données
    std::vector<int> v1 {1, 2, 4};
    // crée un tableau à 12 éléments de valeurs 0
    std::vector<int> v2(12, 0);
    // accés aléatoire
    v1[2] = 3;
    // ajout en fin de tableau -> réallocation
    v1.push_back(4);
    // parcourt le tableau, version old-school
    for (unsigned i=0; i<v1.size(); i++)
        std::cout << v1[i] << std::endl;
    // parcourt le tableau, version C++11
    for (int e : v1)
        std::cout << e << std::endl;
    return 0;
}
```

* * *

> **Attention :**
>
> La gestion mémoire des tableaux dynamiques peut amener à des réallocations +
> recopies. Si possible, toujours préallouer l'espace mémoire nécessaire.

## Générateurs pseudo-aléatoires 

- **Ne pas utiliser le** `rand()` **de la bibliothèque C !!!**
- Le C++11 propose des générateurs et des distributions plus performants et
  avec des meilleures propriétés statistiques.

```cpp
#include <random>
#include <iostream>
int main() {
    // crée un générateur initialisé aléatoirement
    std::mt19937 engine(std::random_device{}());
    // distribution uniforme d'entiers
    std::uniform_int_distribution<int> distribution(0, 255);
    // génère un premier nombre de la suite
    std::cout << distribution(engine) << std::endl;
    // génère un second nombre de la suite
    std::cout << distribution(engine) << std::endl;
    return 0;
}
```

## Mesure du temps d'exécution 

```cpp
#include <chrono>
...
int main() {
    ...
    auto startTime = std::chrono::system_clock::now();
    
    ...    // code à chronométrer

    auto endTime = std::chrono::system_clock::now();

    std::chrono::duration<double> nbSeconds = endTime - startTime;

    std::cout << "temps de calcul = " << nbSeconds.count() << std::endl;
    ...
}
```

# Python

## Rappels 

- heu... non, débrouillez-vous
- voir 
  l'[initiation au langage Python](http://www-lisic.univ-littoral.fr/~teytaud/files/Cours/Apprentissage/tutoPython.pdf)
  du cours d'apprentissage artificiel
- juste : pas besoin de Virtualenv avec Nix

# Systèmes de build

## Code fourni

- forker/cloner le [dépôt de code du module HPC](https://gitlab.com/juliendehos/M_HPC_etudiant)
- à chaque séance de TP correspond un dossier, avec une config Nix

## Projet pur C++ 

- dans la vraie vie : autotools, cmake, meson, bazel...
- pour les TP : make (fichier `Makefile`) 
    - 1 cpp = 1 programme
    - à lancer dans un nix-shell

* * *

```text
$ cd hpcMpiCpp

$ nix-shell

[nix-shell]$ make

g++ -std=c++11 -Wall -Wextra -fopenmp -O2 -o hpcMpiCppHello1.out hpcMpiCppHello1.cpp

g++ -std=c++11 -Wall -Wextra -fopenmp -O2 -o hpcMpiCppHello2.out hpcMpiCppHello2.cpp
...

[nix-shell]$ ./hpcMpiCppHello1.out
...
```

## Projet pur Python

- dans la vraie vie, nombreux outils : bibliothèques, système de build/déploiement, packaging...
- pour les TP : setuptools (fichier `setup.py`), matplotlib, numpy, pandas...

```text
$ cd hpcMpi

$ nix-shell

[nix-shell]$ ./scripts/hpcMpiImageSeq.py 10
10 1 1.103224277496338
```

## Projet C++/Python

- systèmes d'interfaçage : libpython, swig, boost-python, pybind11
- 2 approches : 
    - dans un projet C++, exporter une interface Python
    - dans un projet Python, inclure une extension C++
- pour les TP: setuptools + pybind11 (fichiers `setup.py` et `cpp/Wrapper.cpp`)

```text
$ cd hpcThreads

$ nix-shell --run "hpcThreadsHello.py"
Bonjour, je suis le thread 140011813428096
...
```

