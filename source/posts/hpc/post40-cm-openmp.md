---
title: CM HPC, OpenMP
date: 2019-04-15
---

## Présentation d'OpenMP

- "Multi-Processing"
- standard d'API C/C++ et fortran
- pour la programmation parallèle avec mémoire partagée (SMP)
- via des directives de compilation et une bibliothèque de fonctions
- implémenté dans beaucoup de compilateurs
- http://www.openmp.org

## Avantages/inconvénients

- avantages :
    - standard répandu
    - simple à utiliser
    - aide du compilateur
    - code proche du code séquentiel
- inconvénients :
    - limité au SMP
    - performances parfois difficiles à analyser

## Modèle d'exécution

- modèle "fork and join"
- un thread master
- chaque section parallèle est forkée sur des nouveaux threads
- join en fin de section parallèle 
- possibilité de spécifier des barrières de synchro, réduction...

## Modèle mémoire

- mémoire partagée par tous les threads
- possibilité de déclarer les données privées ou partagées
- gestion automatique des transferts mémoires

## Mise en oeuvre

- définir les sections parallèles avec des directives OpenMP
- si besoin, inclure `<omp.h>` et utiliser la bibliothèque OpenMP
- compiler avec l'option `-fopenmp` (gcc)
- exécuter comme un programme classique

## Exemple de base

```cpp
// toto.cpp
#pragma omp parallel for
for (int i=0; i<1000; i++)
    b[i] = 2*a[i];
```

- compilation :

```text
$ g++ -O2 -fopenmp toto.cpp 
```

- exécution :

```text
$ ./a.out
```

## Définir une section parallèle

- directive `parallel`
- exemple de code :

```cpp
#pragma omp parallel
{       
    // fork : tous les coeurs exécutent cette section en parallèle
    std::cout << "Hello world !" << std::endl;
}
// join : seul le thread master continue l'exécution
```

- exemple d'exécution (machine à 4 coeurs) :

```text
$ ./a.out
Hello world !Hello world !
Hello world !

Hello world !
```

## Spécifier le nombre de threads

- par défaut, OpenMP répartit sur tous les coeurs disponibles

- spécifier le nombre de threads à la compilation (clause `num_thread`) :

```cpp
#pragma omp parallel num_threads(2)
{       
    // ...
}
```

- spécifier le nombre de threads à l'exécution (variable d'environnement
  `OMP_NUM_THREADS`) :

```text
$ OMP_NUM_THREADS=2 ./a.out
```

## Paralléliser une boucle

- directive `for`

- exemple :

```cpp
#pragma omp parallel
{
    #pragma omp for
    for (int i=0; i<1000; i++)
        b[i] = 2*a[i];
}
```

- autre syntaxe possible :

```cpp
#pragma omp parallel for
for (int i=0; i<1000; i++)
    b[i] = 2*a[i];
```

## Spécifier la répartition des calculs

- clause `schedule`
- types de répartition possibles : `static`, `dynamic`...
- possibilité de spécifier la taille des segments répartis
- par défaut : un segment par thread, répartition statique équitable

```cpp
#pragma omp parallel for schedule(static, 400) num_threads(2)
for (int i=0; i<1000; i++)
    b[i] = 2*a[i];
// thread 0 : segments [0, 399] et [800, 999]
// thread 1 : segment [400, 799] 
```


## Minimiser le surcoût de la parallélisation

- implémentation bof :

```cpp
#pragma omp parallel for    // fork
for (int i=0; i<1000; i++)
    b[i] = 2*a[i];          // join
#pragma omp parallel for    // fork (nouveaux threads)
for (int i=0; i<1000; i++)
    c[i] = 3*a[i];          // join
```

* * *

- implémentation mieux :

```cpp
#pragma omp parallel        // fork
{
    #pragma omp for         // première répartition 
    for (int i=0; i<1000; i++)
        b[i] = 2*a[i];
    #pragma omp for         // seconde répartition (mêmes threads)
    for (int i=0; i<1000; i++)
        c[i] = 3*a[i];
}                           // join
```

## Barrière de synchronisation

- synchronisation implicite :

```cpp
#pragma omp parallel
{
    #pragma omp for 
    for (int i=0; i<1000; i++)
        b[i] = 2*a[i];
    // synchronisation implicite
    #pragma omp for 
    for (int i=0; i<1000; i++)
        c[i] = b[999-i];
    // synchronisation implicite
}
```

- synchronisation explicite avec la directive `barrier`
- **les synchro nuisent aux performances et au passage à l'échelle** (mais
  peuvent être nécessaires pour la validité du calcul...)

## Éviter les synchronisations

- clause `nowait` :

```cpp
#pragma omp parallel
{
    #pragma omp for nowait
    for (int i=0; i<1000; i++)
        b[i] = 2*a[i];
    // pas de synchronisation 
    #pragma omp for 
    for (int i=0; i<1000; i++)
        c[i] = 3*a[i];
    // synchronisation implicite
}
```


- **attention à ce que le calcul reste valide** (par exemple, ça ne
  fonctionnerait pas sur l'exemple précédent)

## Données partagées ou privées

- clauses `shared`, `private`...
- partagé = la donnée peut être utilisée par tous les threads
- privé = la donnée est copiée et chaque thread utilise sa copie
- par défaut, suit la même logique que la portée de déclaration :

```cpp
std::vector<int> b(1000);       // b est partagé par les threads
#pragma omp parallel
{
    #pragma omp for
    for (int i=0; i<1000; i++)  // i est privé
        b[i] = 2*a[i];
}
```

## Réduction

- calculer un résultat à partir d'un ensemble de données
- donnée résultat partagée + clause `reduction` :

```cpp
int somme = 0;  // donnée résultat (en mémoire partagée)
#pragma omp parallel reduction(+:somme) 
// demande de calculer la réduction de somme par l'opérateur +
{
    #pragma omp for 
    for (int i=0; i<1000; i++)
        b[i] = 2*a[i];
    #pragma omp for
    for (int i=0; i<1000; i++)
        somme += b[i];  // chaque thread calcule sa somme
}  // calcul de la somme finale
```

## Bibliothèque OpenMP

- fonctions annexes définies dans le standard OpenMP
- selon la fonction, à utiliser dans le master ou dans une section parallèle
- inclure `<omp.h>`

* * *

| fonction               | description                           |
|------------------------|---------------------------------------|
| `omp_set_num_threads`  | spécifie le nb de threads à utiliser 
| `omp_get_num_threads`  | nb de threads de la section parallèle 
| `omp_get_max_threads`  | nb de threads max demandés 
| `omp_get_thread_num`   | numéro du thread  
| `omp_get_num_procs`    | nombre de coeurs 
| `omp_get_wtime`        | horloge 

## Bibliothèque OpenMP (exemple)

- code :

```cpp
double t0 = omp_get_wtime(); 
#pragma omp parallel 
{
    int i = omp_get_thread_num();
    int s = omp_get_num_threads();
    std::cout << "thread " << i << " of " << s << std::endl;
}
double t1 = omp_get_wtime();
std::cout << "time = " << t1 - t0 << std::endl;
```


- exécution :

```text
thread thread 01 of  of 22

time = 0.000384692
```

