---
title: CM HPC, MPI
date: 2019-04-15
---

## Présentation de MPI

- "Message Passing Interface"
- standard de protocole
- pour la programmation parallèle avec mémoire distribuée  (et aussi
  programmation concurrente)
- via une bibliothèque de fonctions et un environnement d'exécution
- nombreuses implémentations : mpich, openmpi...
- disponible pour de nombreux langages : C, C++, fortran, python...
- [Livermore National Laboratory](https://computing.llnl.gov/tutorials/mpi)
- [MPI forum](http://www.mpi-forum.org)
- doc mpi4py : [tutorial](https://mpi4py.readthedocs.io/en/stable)

## Avantages/inconvénients

- avantages :
    - standard répandu
    - passage à l'échelle
    - performances relativement faciles à analyser
- inconvénients :
    - apprentissage pas immédiat
    - ne préserve pas le code séquentiel

## Modèle d'exécution

- pas de modèle imposé 
- algorithmique distribuée : un même code lancé par l'environnement d'exécution
  sur des noeuds interconnectés
- en programmation parallèle, on implémente généralement un modèle "fork and
  join" (un noeud master + des noeuds de calculs)

## Modèle mémoire

- ZE point clé de la programmation parallèle à mémoire distribuée
- mémoire distribuée : chaque noeud a sa propre mémoire et les échanges sont
  gérés explicitement 
- physiquement : topologie en maillage, anneau, hypercube...  (connexion
  complète $\Rightarrow O(N^2) \Rightarrow$ souvent irréalisable)
- virtuellement (avec MPI) : accès point-à-point + accès collectifs
- en pratique : exploiter les processeurs sans saturer l'interconnexion

## Accès collectifs MPI

![](files/cm-mpi/collective_comm.png)

## Mise en oeuvre

- écrire un code distribué utilisant la bibliothèque MPI
- compiler avec les options qui vont bien
- exécuter sur les noeuds via l'environnement d'exécution

## Exemple de base (en C++)

```cpp
// toto.cpp
#include <iostream>
#include <mpi.h>
int main(int argc, char ** argv) {
    // initialise MPI
    MPI_Init(&argc, &argv);
    int worldRank;
    MPI_Comm_rank(MPI_COMM_WORLD, &worldRank);
    int worldSize;
    MPI_Comm_size(MPI_COMM_WORLD, &worldSize);
    // chaque noeud affiche un message en parallèle
    std::cout << "I am process #" << worldRank << " of " << worldSize << std::endl; 
    // termine les instructions MPI
    MPI_Finalize();
    return 0;
}
```

* * *

- compilation :

```text
$ mpic++ -O2 toto.cpp
```

- exécution :

```text
$ mpirun -n 2 ./a.out
I am process #0 of 2
I am process #1 of 2
```


## Exemple de base (en Python)

```python
# toto.py
#!/usr/bin/env python3

from mpi4py import MPI

comm = MPI.COMM_WORLD
worldRank = comm.Get_rank()
worldSize = comm.Get_size()
print("I am process #{} of {}".format(worldRank, worldSize))
```

- pas de compilation mais mettre les droits d'exécution pour le fichier

- exécution :

```text
$ mpirun -n 2 ./toto.py
I am process #0 of 2
I am process #1 of 2
```

## Modèle "fork and join" en MPI

- solution 1 : séparer code maitre / code esclave

```python
comm = MPI.COMM_WORLD
worldRank = comm.Get_rank()
if worldRank == 0:
    # code MPI spécifique au noeud maitre
    ...
else:
    # code MPI spécifique aux noeuds esclaves
    ...
```

* * *

- solution 2 : code générique + section spécifique 

```python
comm = MPI.COMM_WORLD
worldRank = comm.Get_rank()
# code MPI générique maitre/esclave
...
if worldRank == 0:
    # section spécifique au noeud maitre
    ...
```

## Communication point-à-point (en C++)

```cpp
if (worldRank != 0) {
    // les noeuds esclaves envoient un message au noeud master
    char outMsg[] = "hello";
    MPI_Send(outMsg, sizeof(outMsg), MPI_CHAR, 0, 0, MPI_COMM_WORLD);         
}
else {
    // le noeud master reçoit et affiche les messages des noeuds esclaves
    for (int i=1; i<worldSize; i++) {
        char inMsg[50];
        MPI_Status status;
        MPI_Recv(&inMsg, sizeof(inMsg), MPI_CHAR, MPI_ANY_SOURCE, 
                 MPI_ANY_TAG, MPI_COMM_WORLD, &status); 
        std::cout << "Process #" << status.MPI_SOURCE << " sent: " << inMsg << std::endl;
    }
}
```

## Communication point-à-point (en Python)

```python
if worldRank != 0:
    # les noeuds esclaves envoient un message au noeud master
    msg = "hello"
    comm.send(msg, dest=0)
else:
    # le noeud master reçoit et affiche les messages des noeuds esclaves
    for _ in range(1, worldSize):
        st = MPI.Status()
        msg = comm.recv(status=st)
        print("Process #{} sent: {}".format(st.source, msg))
```

## Transfert de données (C++, code maitre/esclaves)

```cpp
if (worldRank == 0) {  
    // le noeud master initialise les données d'entrée
    std::vector<int> allData;
    for (unsigned i=0; i<worldSize; i++)
        allData.push_back(i);
    // le noeud master répartit les données sur tous les noeuds
    int nodeData;
    MPI_Scatter(allData.data(), 1, MPI_INT, &nodeData, 1, MPI_INT, 0, MPI_COMM_WORLD);
    // le noeud master effectue le calcul sur sa donnée
    double nodeResult = sqrt(double(nodeData));
    // le noeud master rappartrit les résultats de tous les noeuds
    std::vector<double> allResults(worldSize);
    MPI_Gather(&nodeResult, 1, MPI_DOUBLE, allResults.data(), 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    // le noeud master affiche les résultats
    for (double d : allResults) 
        std::cout << d << std::endl;
}
...
```

* * *

```cpp
...
else {
    // les noeuds esclaves récupèrent leur donnée
    int nodeData;
    MPI_Scatter(nullptr, 1, MPI_INT, &nodeData, 1, MPI_INT, 0, MPI_COMM_WORLD);
    // chaque noeud esclave effectue le calcul sur sa donnée
    double nodeResult = sqrt(double(nodeData));
    // les noeuds esclaves envoient leur résultat au noeud master
    MPI_Gather(&nodeResult, 1, MPI_DOUBLE, nullptr, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
}
```

## Transfert de données (C++, code générique)

```cpp
// le noeud master initialise les données d'entrée
std::vector<int> allData;
if (worldRank == 0) {  
    for (unsigned i=0; i<worldSize; i++)
        allData.push_back(i);
}

// les données sont réparties sur tous les noeuds
int nodeData;
MPI_Scatter(allData.data(), 1, MPI_INT, &nodeData, 1, MPI_INT, 0, MPI_COMM_WORLD);

// chaque noeud effectue le calcul sur sa donnée
double nodeResult = sqrt(double(nodeData));

// les résultats sont rappatriés sur le noeud master
std::vector<double> allResults(worldSize);
MPI_Gather(&nodeResult, 1, MPI_DOUBLE, allResults.data(), 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

// le noeud master affiche les résultats
if (worldRank == 0) {  
    for (double d : allResults) 
        std::cout << d << std::endl;
}
```

## Transfert de données (Python, code maitre/esclaves)

```python
if worldRank == 0:
    # le noeud master initialise les données d'entrée
    all_data = numpy.arange(worldSize, dtype='i')
    # le noeud master répartit les données sur tous les noeuds
    node_data = numpy.empty(1, dtype='i')
    comm.Scatter(all_data, node_data)
    # le noeud master effectue le calcul sur sa donnée
    node_result = numpy.sqrt(float(node_data))
    # le noeud master rappartrit les résultats de tous les noeuds
    all_results = numpy.empty(worldSize, dtype='d')
    comm.Gather(node_result, all_results)
    # le noeud master affiche les résultats
    print(all_results)
else:
    # les noeuds esclaves récupèrent leur donnée
    node_data = numpy.empty(1, dtype='i')
    comm.Scatter(None, node_data)
    # chaque noeud esclave effectue le calcul sur sa donnée
    node_result = numpy.sqrt(float(node_data))
    # les noeuds esclaves envoient leur résultat au noeud master
    comm.Gather(node_result, None)
```

## Transfert de données (Python, code générique)

```python
# le noeud master initialise les données d'entrée et de sortie
all_data = None
all_results = None
if worldRank == 0:
    all_data = numpy.arange(worldSize, dtype='i')
    all_results = numpy.empty(worldSize, dtype='d')

# les données sont réparties sur tous les noeuds
node_data = numpy.empty(1, dtype='i')
comm.Scatter(all_data, node_data)

# chaque noeud effectue le calcul sur sa donnée
node_result = numpy.sqrt(float(node_data))

# les résultats sont rappatriés sur le noeud master
comm.Gather(node_result, all_results)

# le noeud master affiche les résultats
if worldRank == 0:
    print(all_results)
```

## Réduction (C++)

```cpp
// chaque noeud calcule sa donnée d'entrée
int nodeData = worldRank;

// chaque noeud effectue le calcul sur sa donnée
int nodeResult = nodeData*nodeData;

// calcule la somme des résultats de tous les noeuds
int allResults;
MPI_Reduce(&nodeResult, &allResults, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

// le noeud master affiche les résultats
if (worldRank == 0) {  
    std::cout << allResults << std::endl;
}
```

## Réduction (Python)

```python
node_data = worldRank
node_result = numpy.empty(1, 'i')
node_result[0] = node_data*node_data
all_results = numpy.empty(1, 'i')
comm.Reduce(node_result, all_results, op=MPI.SUM)

if worldRank == 0:
    print(all_results[0])
```

## Mesure du temps d'exécution (C++)

```cpp
if (worldRank == 0)  {
    double t0 = MPI_Wtime();
    ...
    double t1 = MPI_Wtime();
    std::cout << "time = " << t1 - t0 << std::endl;
}
```

## Mesure du temps d'exécution (Python)

```python
if worldRank == 0:
   t0 = MPI.Wtime()
    ...
    t1 = MPI.Wtime()
    print("time =", t1 - t0)
```

