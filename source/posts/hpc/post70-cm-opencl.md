---
title: CM HPC, OpenCL
date: 2019-04-16
---

# Généralités

## Contexte 

- GPGPU : 
    - General-Purpose computation on Graphics Processing Units 
    - utiliser des GPU pour faire tous types de calculs (graphiques ou non)
- grande diversité des moyens/outils de calcul :
    - matériels : CPU, GPU, FPGA, DSP...
    - logiciels : OpenGL, Cuda, OpenCL, OpenACC, OpenCV...
- tendance actuelle vers le calcul hétérogène :
    - utiliser tout le matériel disponible
    - soit en combinant des logiciels spécifiques à chaque type de matériel
    - soit en utilisant un outil générique (OpenCL, OpenACC)

Ce cours aborde principalement la programmation GPU en OpenCL (version 1.2 avec
API C++).

## Motivation d'OpenCL : calcul hétérogène

- CPU de plus en plus parallèles (coeurs, instructions vectorielles...)
- GPU de plus en plus génériques (pipeline graphique $\rightarrow$ processeur
  de flux)
- convergence des applications mais programmation très différente
- objectif : avoir un outil générique de programmation

## Description d'OpenCL

- framework de programmation de systèmes hétérogènes (code utilisable sur
  différents systèmes ou utilisant différents systèmes)
- standard industriel (Apple, AMD, Nvidia, Intel, IBM...)
- spécifications ouvertes (Khronos Group)
- portable (Linux, Windows... supercalculateurs, appareils mobiles...)
- répandu (logiciels, outils de HPC de plus haut-niveau)
- pas vraiment d'alternatives : Cuda (propriétaire, GPU Nvidia), OpenACC
  ("OpenMP hétérogène", en développement)

## Composition du framework OpenCL

- langage de kernel, inspiré du C99, pour implémenter les calculs
- API platforme en C pour connaître et initialiser le matériel disponible
- API runtime en C pour lancer et contrôler les calculs
- wrapper C++ (officiel) et python (non officiel)

## Quelques liens 

- le site du Khronos Group :
    - spécifications d'OpenCL et du wrapper C++
    - aide-mémoire (refcards)
- les sites d'AMD et de Nvidia :
    - documentations plus spécifiques à leurs matériels

# Formalismes d'OpenCL

## Principe d'un calcul avec OpenCL

- définir un domaine de calcul
- exécuter un noyau de calcul en chaque point du domaine

* * *

- calcul séquentiel classique :

```cpp
// fonction de calcul
void mul42(const float * input, float * output, int i) 
    { output[i] = 42.f * input[i]; }
// calcul sur tout le domaine
for (int i=0; i<N; i++) 
    mul42(input, output, i);
```

* * *

- calcul parellèle OpenCL :

```c
// noyau de calcul à exécuter sur le device
__kernel void mul42(__global const float * input, __global float * output) {
  int gid = get_global_id(0);
  output[gid] = 42.f * input[gid];
}
```

```cpp
// calcul sur tout le domaine, initié par le host
cl::Kernel kernel(program, "mul42");
queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(N), cl::NullRange);
```

## Modèle de plateforme

- `host` : matériel qui exécute l'application principale (CPU)
- `compute device` : matériel réalisant les calculs (CPU, GPU...)
- `compute unit` : unité de calcul d'un device (coeur CPU, multi-processeur
  GPU)
- `processing element` : élément de calcul d'un compute unit (ligne SIMD CPU,
  processeur de flux GPU)
- `platform` : ensemble de matériel géré par le runtime OpenCL (host +
  devices)

![](files/cm-opencl/opencl_platform.png)

## Modèle d'exécution (depuis le host)

- `kernel` : noyau de calcul élémentaire, sur un device
- `program` : ensemble de kernels et de fonctions, sur un device
- `memory object` : 
    - `buffer` : tableau de données 1D générique
    - `image` : tableau de données 2D ou 3D spécialisé 
- `command queue` : file de commandes permettant de contrôler un device
  (transferts mémoires, kernels, synchro...) 
- `context` : environnement du host (devices + programs + memory objects +
  queues)

* * *

![](files/cm-opencl/opencl_execution.png)

## Modèle d'exécution (depuis le device)

- `work-item` : exécution d'un kernel, pour un point du domaine, sur un ou
  plusieurs processing elements
- `work-group` : groupe de work-items, sur un même compute unit
- `ND-range` : domaine de calcul (1D, 2D ou 3D)

$\rightarrow$ un calcul (kernel) est réalisé en chaque point (work-item) du
domaine (ND-range)

$\rightarrow$ définir des groupes de work-items (work-groups) permet
d'accélérer les performances (partage de transferts mémoires, synchronisation
de work-items...)

## Modèle d'exécution (systèmes de coordonnées)

- `ND-range` : 
    - taille $G$ (nombre total de work-items)
    - offset $F$
- `work-group` : 
    - taille $S$ (nombre de work-items par work-group)
    - indice $w \in [0, G/S)$ (du work-group dans le ND-range)
- `work-item` : 
    - indice local $s \in [0, S)$ (du work-item dans le work-group) 
    - indice global $g \in [0, G)$ (du work-item dans le ND-range) 
    - donc $g = w \times S + s + F$

$\rightarrow$ en 1D, 2D ou 3D...

* * *

![](files/cm-opencl/opencl_ndrange.png)

## Modèle de mémoire

- plusieurs types de mémoire sur le device :
    - `global/constant` : commune à tous les work-items 
    - `local` : partagée uniquement par les work-items du work-group
    - `private` : spécifique au work-item
- $+$ la mémoire du host (non accessible depuis le device)
- transferts à la charge du programmeur : host $\leftrightarrow$ global
  $\leftrightarrow$ local

* * *

![](files/cm-opencl/PhysicalMemory_CL_System_2x.png)

* * *

![](files/cm-opencl/opencl_memory.png)

# Programmation parallèle avec OpenCL

## Introduction

- ici, on suppose OpenCL 1.2 avec wrapper C++
- $+$ le fichier 
  [opencHelp.hpp](https://gitlab.com/juliendehos/M_HPC_etudiant/blob/master/hpcOpencl/opencHelp.hpp) 
  fourni pour le TP (sélection du device, codes d'erreur, chargement du code
  d'un kernel depuis un fichier...)

## Exemple 

- problème :
    - multiplier les éléments d'un tableau de réels par 42
    - en entrée : un tableau 
    - en sortie : un tableau de même taille 

* * *

- code séquentiel classique :

```cpp
int main(int argc, char ** argv) {
    // get input data
    std::vector<float> inputData;
    ...
    // initialize some other data for computation
    int dataSize = inputData.size();
    std::vector<float> outputData(dataSize);
    // compute
    for (int k=0; k<dataSize; k++)
        outputData[k] = 42.f * inputData[k];
    // do something with results
    ...
    return 0;
}
```

* * *

- code kernel équivalent :

```c
// mul42.cl
__kernel void mul42(__global const float * input, 
                    __global float * output)
{ 
    int gid = get_global_id(0);
    output[gid] = 42.f * input[gid]; 
} 
```

## Organisation classique d'un code OpenCL

- récupérer les données d'entrées
- effectuer le calcul avec OpenCL :
    - initialiser l'API OpenCL
    - initialiser un context vers un device
    - initialiser une file de commandes
    - initialiser des buffers sur le device
    - initialiser un program sur le device
    - exécuter un kernel sur le device
    - récupérer les résultats du device
- sortir les données résultats

## Programmation du host : initialiser l'API OpenCL 

- bibliothèque C/C++ classique $\rightarrow$ include, lib...
- ici : `opencHelp.hpp`
- en C++, gestion d'erreurs via des exceptions

* * *

```cpp
// mul42.cpp
#define __CL_ENABLE_EXCEPTIONS
#include "opencHelp.hpp"
int main(int argc, char ** argv) {
  ...
  // compute
  try {
    // code using OpenCL API
    ...
  }
  catch (cl::Error error) {
    std::cerr << "Error: " << error.what() << " -> " 
              << opencHelp::errorToString(error.err())
              << std::endl;
    exit(-1);
  }
  ...
}
```

## Programmation du host : initialiser un context sur un device 

- choisir une plateforme puis un device de cette plateforme 
- créer un context vers le device

$\rightarrow$ le context permet d'accéder au device pour créer des
program, buffer...

$\rightarrow$ fonction `opencHelp::printPlatformsAndDevices`


```cpp
// mul42.cpp
// code using OpenCL API
// initialize device, context 
cl::Device device = opencHelp::getDevice(iPlatform, iDevice);
cl::Context context(device);
```

## Programmation du host : initialiser une file de commandes 

- créer une file à partir d'un context et d'un device
- permet de contrôler le device : transferts, kernels, synchro
- file in-order ou out-of-order
- commande bloquante ou non
- attente de fin de file

```cpp
// mul42.cpp
// initialize command queue
cl::CommandQueue queue(context, device);
```

## Programmation du host : initialiser des buffers sur le device 

- allouer un buffer sur le device via le context
- transférer des données du host au device via la file
- plusieurs types de buffer : lecture, écriture, lecture/écriture...
- transfert mémoire bloquant ou non

```cpp
// mul42.cpp
// create and write buffers
int dataFullSize = dataSize*sizeof(float);
cl::Buffer inputBuffer(context, CL_MEM_READ_ONLY, dataFullSize);
cl::Buffer outputBuffer(context, CL_MEM_WRITE_ONLY, dataFullSize);
queue.enqueueWriteBuffer(inputBuffer, false, 0, dataFullSize, inputData.data());
```

## Programmation du host : initialiser un program sur le device 

- créer le programme d'un code source, sur le device via le context 
- transfère le code source sur le device qui le compile ensuite
- i.e. le programme device est compilé à l'exécution du programme host

```cpp
// mul42.cpp
// create and build program
std::string kernelSource = opencHelp::readKernelFile("mul42.cl");
cl::Program program = cl::Program(context, kernelSource, true);
```

## Programmation du host : exécuter un kernel sur le device 

- sélectionner une fonction `kernel` du programme device
- régler ses paramètres
- demander son exécution via la file de commandes

$\rightarrow$ le code source du program peut contenir plusieurs kernels

$\rightarrow$ la commande d'exécution spécifie :
    - l'offset du domaine de calcul
    - la taille globale du domaine (nombre de work-items)
    - la taille locale du domaine (nombre de work-items par work-groups)
    - la taille globale doit être un multiple de la taille locale

* * *
 
```cpp
// mul42.cpp
// setup and launch kernel
cl::Kernel kernel(program, "mul42"); // kernel "mul42" in file "mul42.cl"
kernel.setArg(0, inputBuffer);
kernel.setArg(1, outputBuffer);
queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(dataSize), cl::NullRange); 
                                  // no offset, global size == dataSize, local size == 1
queue.finish();
```

## Programmation du host : récupérer les résultats du device 

- récupérer un buffer du device sur le host

$\rightarrow$ transfert mémoire similaire au précédent mais dans l'autre sens

```cpp
// mul42.cpp
// read results from buffers 
queue.enqueueReadBuffer(outputBuffer, true, 0, dataFullSize, outputData.data());
```

## Programmation du device 

- écrire la fonction kernel réalisant le calcul

$\rightarrow$ le nom et les paramètres doivent correspondre à ceux utilisés par
le host

$\rightarrow$ le kernel est exécuté sur chaque work-item du domaine

$\rightarrow$ code source à écrire dans langage de kernel d'OpenCL

```c
// mul42.cl
__kernel void mul42(__global const float * input, __global float * output)
{ 
    int gid = get_global_id(0);
    output[gid] = 42.f * input[gid]; 
} 
```

# Langage de kernel d'OpenCL 

## Généralités 

- langage pour le code source à envoyer au device
- inspiré du C99
- restrictions :
    - pas de récursivité
    - pas de pointeur de fonction
- ajouts :
    - types de données vectoriels
    - qualificateurs
    - fonctions prédéfinies
- système d'extensions (e.g. printf, double, image 3D...)

* * *

```c
float superFonction(float4 v) {
return 42.f * dot(v, v);
}
__kernel void superNoyau(__global const float4 * input, __global float * output) { 
int gid = get_global_id(0);
output[gid] = superFonction(input[gid]); 
} 
```

## Qualificateurs 

- de fonction : `__kernel`
- d'espace mémoire : `__global`, `__local`, `__private`
- d'accès mémoire, d'attribut...

```c
__kernel void superNoyau(__global const float4 * input, __global float * output) { 
... 
```

## Types de données 

- scalaires :
    - int, unsigned int, float...
    - bool, half

- vectoriels :
    - int2, int4, int8, uint2, uint4, uint8
    - float2, float4, float8...

- tableaux statiques :

```c
int4 tab[12];
```

## Opérateurs 

- arithmétiques, booléens, comparaisons...
- calcul membre-à-membre pour les vecteurs
- accès aux membres d'un vecteur (swizzling) :

```c
float4 u = float4(1, 2, 3, 4);
float2 v;
v.yx = float2(u.x+u.y, u.z+u.w);
```

- conversions implicites ou explicites :

```c
float4 f;
char4 c1 = convert_char4(f);      // conversion explicite
char4 c2 = convert_char4_sat(f);  // conversion avec saturation
char4 c3 = convert_char4_rte(f);  // conversion au plus proche 
```

## Fonctions prédéfinies : fonctions mathématiques

- générales : min, max, clamp, floor, round, abs...
- fonctions usuelles : cos, sqrt, log, pow...
- fonctions géométriques : cross, dot, length, radians, degrees...

## Fonctions prédéfinies : fonctions d'accès au domaine

| fonction                         | description                     |
|----------------------------------|---------------------------------|
| `uint get_work_dim()`            | Number of dimensions in use 
| `size_t get_global_size(uint)`   | Number of global work-items 
| `size_t get_global_id(uint)`     | Global work-item ID value 
| `size_t get_local_size(uint)`    | Number of local work-items 
| `size_t get_local_id(uint)`      | Local work-item ID 
| `size_t get_num_groups(uint)`    | Number of work-groups 
| `size_t get_group_id(uint)`      | Returns the work-group ID 
| `size_t get_global_offset(uint)` | Returns global offset 

$\rightarrow$ en paramètre la dimension du ND-range voulue (0, 1 ou 2)

## Fonctions prédéfinies : fonctions de synchronisation

- fonctions :
    - `barrier` : synchronisation des work-items
    - `mem_fence` : synchronisation mémoire
- drapeaux :
    - `CLK_LOCAL_MEM_FENCE` 
    - `CLK_GLOBAL_MEM_FENCE` 

# Optimisation des performances 

## Généralités

- OpenCL : portabilité du code mais pas forcément des performances
- chaque matériel a ses particularités
- pour une efficacité max, prendre en compte ces particularités

## Matériel de calcul : principales différences CPU/GPU

- CPU : faible débit, faible latence, prédiction de branchement
- GPU : grand débit, grande latence, parallélisme massif

![](files/cm-opencl/architecture_gpu_cpu.png)

## Matériel de calcul : GPU NVIDIA (extrait de clinfo)

```text
Platform Name:                                NVIDIA CUDA
Device Type:                                  CL_DEVICE_TYPE_GPU
Max compute units:                            1
Max work items dimensions:                    3
  Max work items[0]:                          1024
  Max work items[1]:                          1024
  Max work items[2]:                          64
Max work group size:                          1024
Preferred vector width float:                 1
Native vector width float:                    1
Global memory size:                           1073020928
Local memory size:                            49152
Kernel Preferred work group size multiple:    32
Name:                                         Quadro K600
Vendor:                                       NVIDIA Corporation
Device OpenCL C version:                      OpenCL C 1.1 
```

## Matériel de calcul : GPU AMD (extrait clinfo)

```text
Platform Name:                                AMD Accelerated Parallel Processing
Device Type:                                  CL_DEVICE_TYPE_GPU
Max compute units:                            2
Max work items dimensions:                    3
  Max work items[0]:                          256
  Max work items[1]:                          256
  Max work items[2]:                          256
Max work group size:                          256
Preferred vector width float:                 4
Native vector width float:                    4
Global memory size:                           536870912
Local memory size:                            32768
Kernel Preferred work group size multiple:    64
Name:                                         Caicos
Vendor:                                       Advanced Micro Devices, Inc.
Device OpenCL C version:                      OpenCL C 1.2 
```

## Matériel de calcul : CPU Intel (extrait clinfo)

```text
Platform Name:                                AMD Accelerated Parallel Processing
Device Type:                                  CL_DEVICE_TYPE_CPU
Max compute units:                            2
Max work items dimensions:                    3
  Max work items[0]:                          1024
  Max work items[1]:                          1024
  Max work items[2]:                          1024
Max work group size:                          1024
Preferred vector width float:                 4
Native vector width float:                    4
Global memory size:                           6204669952
Local memory size:                            32768
Kernel Preferred work group size multiple:    1
Name:                                         Intel Core2 Duo CPU E8400 @ 3.00GHz
Vendor:                                       GenuineIntel
Device OpenCL C version:                      OpenCL C 1.2 
```

## Optimisation des performances sur GPU

- méthode classique : 
    - identifier les facteurs limitants
    - implémenter une
    - optimisation
    - vérifier correction
    - mesurer le gain...

- particularités sur GPU :
    - mesures de performances
    - gestion mémoire
    - répartition du domaine de calcul
    - optimisation des instructions
    - optimisation du flux d'instructions

## Mesure des performances

- temps d'exécution :
    - mesure CPU (attention aux appels non-bloquants)
    - mesure GPU (file de commande en mode profiling)

- bande passante :
    - mesure des transferts mémoires
    - comparer valeur théorique et valeur mesurée $\rightarrow$ indique si
      bonne gestion des accès mémoires
    - BP théorique : calculable d'après les caractéristiques matérielles
      (fréquence mémoire, taille du bus...)
    - BP mesurée : $(B_r + B_w)/t$ (quantités transférées lecture+écriture /
      temps)

## Gestion mémoire

- souvent grande influence sur les performances
- différentes mémoires avec quantités/débits différents
- accès par blocs, alignements, banques mémoires
- mémoire cache

* * *

![](files/cm-opencl/gpuMemoryHierarchy_2x.png)

## Gestion mémoire : mémoire globale

- minimiser les transferts host $\leftrightarrow$ device (réutilisation
  de buffers...)
- minimiser les accès à la mémoire globale (mémoire locale, cache...)
- accès par bloc (GPU AMD : memory channel)
- accès contigus (GPU NVIDIA : coalescing)

## Gestion mémoire : mémoire locale

- mémoire partagée par les work-items d'un même work-group
- accès plus rapide qu'à la mémoire globale
- permet de réordonner les accès pour les rendre contigus
- mais : bank conflicts, parfois cache déjà très efficace

* * *

```cpp
// toto.cpp
kernel.setArg( 2, cl::Local(nbWIperWG * sizeof(cl_float)) );
```

```c
// toto.cl
__kernel void mul42sum(__global const float * input, __global float * output, 
                       __local float * tmp) { 
    int s = get_local_id(0);
    int g = get_global_id(0);
    tmp[s] = 42.f * input[g];      // each WI computes its mul42 in local memory
    barrier(CLK_LOCAL_MEM_FENCE);  // wait for all WI of the WG
    if (s == 0) {                  // the first WI of the WG computes final sum
        float sum = 0;
        for (uint i = 0; i < get_local_size(0); i++) sum += tmp[i];
        output[g] = sum;
    }
} 
```

## Répartition du domaine de calcul

- découper le domaine en plein de work-items :
    - permet d'occuper tous les processeurs de flux du GPU
    - permet d'amortir les latences mémoires

- utiliser la taille de work-group recommandée :
    - meilleure répartition des calculs
    - meilleure gestion des accès mémoires

## Optimisation des instructions

- éviter la division entière et le modulo
- utiliser les fonctions mathématiques précablées (`native_sqrt`...)
- utiliser les types vecteurs (GPU AMD, CPU SSE)

## Optimisation du flux d'instructions

- éviter les barrières de synchronisation (maximiser l'occupation GPU)
- éviter les instructions de branchement (minimiser la divergence des chemins
  d'exécution)
- dérouler les petites boucles

