---
title: TP HPC, OpenCL
date: 2019-04-16
---

## Introduction

Le but de ce TP est d'utiliser OpenCL (version 1.2, API C++) pour réaliser des
calculs d'algèbre linéaire ou de traitement d'images.  Le TP aborde les notions
de base d'OpenCL (périphérique de calcul, noyau de calcul, transfert mémoire,
répartition des calculs) ainsi que quelques techniques d'optimisation (accès
mémoire contigus, mémoire locale...).

Si besoin, pensez à consulter les 
[spécifications d'OpenCL](https://www.khronos.org/registry/cl/specs/opencl-1.2.pdf) et de 
[son API C++](https://www.khronos.org/registry/cl/specs/opencl-cplusplus-1.2.pdf).

## Matériel de calcul disponible

- En utilisant le programme `clinfo` ou le fichier `OpenCLInfo.cpp`,
  affichez et regardez le matériel reconnu par OpenCL sur votre machine
  (platforms, devices, compute units, work-items, mémoire globale, mémoire
  locale...).

## SAXPY (Single-precision Alpha X Plus Y)

Le fichier `TP_OpenCL_saxpy_1.cpp` implémente le calcul sur CPU du vecteur
$\mathbf{z} = \alpha \mathbf{x} + \mathbf{y}$ où $\mathbf{x}$ et
$\mathbf{y}$ sont des vecteurs et $\alpha$ un scalaire.

- Regardez et comprenez les fichiers `TP_OpenCL_saxpy_1.cpp` et
  `opencHelp.hpp` fournis. Compilez et exécutez.

- Implementez le calcul en OpenCL (avec la répartition simple où un work-item
  calcule un coefficient).  Exécutez sur différents devices et comparez tous
  les temps d'exécution (sur GPU, pensez à exécuter plusieurs fois de suite
  pour être sûr que la bête est bien réveillée).

* * *

- Écrivez une autre implémentation du calcul en utilisant des work-groups de
  256 work-items et en calculant l'id des work-items à la main (sans utiliser
  `get_global_id`).  Ajoutez un affichage par le work-item 420 (dans le
  noyau) et vérifiez que le device est capable de réaliser des affichages (dans
  le programme hôte).  **Écrivez le code de ce noyau dans un fichier séparé
  !!!**

```text
$ ./TP_OpenCL_saxpy_3.out 1 0 2000
iPlatform 0: AMD Accelerated Parallel Processing
iDevice 0: CL_DEVICE_TYPE_CPU; GenuineIntel; Intel(R) Core(TM) i5-4430 CPU 
iPlatform 1: NVIDIA CUDA
iDevice 0: CL_DEVICE_TYPE_GPU; NVIDIA Corporation; Quadro K600
Using iPlatform 1
Using iDevice 0
Oh no, this device can't printf!.
```

* * *

```text
$ ./TP_OpenCL_saxpy_3.out 0 0 2000
iPlatform 0: AMD Accelerated Parallel Processing
  iDevice 0: CL_DEVICE_TYPE_CPU; GenuineIntel; Intel(R) Core(TM) i5-4430 CPU 
iPlatform 1: NVIDIA CUDA
  iDevice 0: CL_DEVICE_TYPE_GPU; NVIDIA Corporation; Quadro K600
Using iPlatform 0
Using iDevice 0
**** Hi, this is work-item 420 speaking *****
  global_size   = 2048
  global_id     = 420
  local_size    = 256
  local_id      = 164
  num_groups    = 8
  group_id      = 1
  global_offset = 0
**** So long, and thanks for all the fish *****
Time: 0.260678 s
Results: passed
```

* * *

- Écrivez une autre implémentation du calcul en utilisant 4 work-groups de 64
  work-items chacun.  Le noyau devra donc s'arranger pour traiter toutes les
  données.  Indication : passez la taille des données en paramètre du noyau. 

## Transposition d'images

Le fichier `TP_OpenCL_imagetranspo_1.cpp` implémente une transposition
d'image en utilisant la bibliothèque OpenCV (CPU).

- Implémentez cette transposition en OpenCL, où un work-item calcule un pixel
  de l'image résultat.  Testez sur le front-flip en bmx.

![Image initiale.](files/tp-opencl/bmx_front_flip.jpg)

* * *

![Image transposée.](files/tp-opencl/bmx_front_flip_trans.jpg)

## Amélioration d'images

Le fichier `TP_OpenCL_imageproc_1.cpp` implémente un traitement d'image en
utilisant la bibliothèque OpenCV (CPU).  Ce traitement est composé d'un
réhaussement de contour par différence de filtres moyenneurs et d'une
augmentation de contraste basique (transformation linéaire des niveaux).

- Regardez/comprenez le code fourni. Testez sur le bottom-turn de Jason à
  Teahupoo.

- Implémentez ce traitement en OpenCL, où un work-item calcule un pixel de
  l'image résultat.

- Proposez une implémentation plus efficace (par exemple, où le noyau de calcul
  préconvertit les pixels nécessaires en `float4`). Qu'en concluez-vous sur
  la mise en oeuvre d'OpenCL?

* * *

![Image initiale.](files/tp-opencl/polakow_small.png)

* * *

![Image traitée.](files/tp-opencl/polakow_small_proc.png)

## Multiplication matrice-vecteur

Le fichier `TP_OpenCL_matvecmul_1.cpp` implémente le calcul CPU du produit
matrice-vecteur.

- Regardez/comprenez/testez le code fourni.

- Implémentez le calcul en OpenCL, où un work-item calcule un coefficient du
  vecteur résultat.

* * *

- Implémentez le calcul en OpenCL avec accès contigus, où tous les work-items
  d'un work-group sont utilisés pour calculer un coefficient du vecteur
  résultat.  Votre implémentation doit utiliser automatiquement autant de
  work-items que permis par le matériel.

- Implémentez le calcul en OpenCL avec accès contigus et calcul de réduction
  avec une première passe parallèle sur 8 work-items.

- Pour quelles tailles de matrice les implémentations OpenCL GPU sont-elles
  plus intéressantes ? Pourquoi ?

