---
title: Instructions pour les L3
date: 2020-03-16
notoc: notoc
---

Les cours et TP de C++ et de Génie Logiciel auront lieu pendant les créneaux
prévus mais à distance, via le serveur Discord qui vous a été communiqué.


## PRÉPARATIONS À FAIRE AVANT LES COURS/TP

- Discord :
    - Vérifiez que vous arrivez à vous connecter.
    - Prévoyez un casque ou des écouteurs pour les canaux vocaux

- Git :
    - Vérifiez que vous avez un compte sur Gitlab.
    - Vérifiez que vous avez forké les dépôts fournis pour les TP.
    - Si vos dépots sont en privé, ajoutez-y vos encadrants de TP avec le rôle
      `reporter`.
    - Entrainez-vous aux commandes git de base (add, commit, push).

- Logiciels de TP :
    - Si vous avez déjà un système Linux (de type Debian/Ubuntu), vous pouvez
      continuer à l'utiliser mais vérifiez que vous avez installé les logiciels
      demandés pour le
      [module de C++](../cpp/post05-cpp-presentation.html#travaux-pratiques)
      et pour le [module de GL](../gl/post05-presentation.html#travaux-pratiques).
    - Sinon, vous pouvez utiliser l'image Virtual Box fournie :
        - [Téléchargez l'image fournie](http://www-lisic.univ-littoral.fr/~dehos/pub/debian.ova) (environ 4 Go).
        - [Installez Virtual Box](https://www.virtualbox.org/wiki/Downloads).
        - Lancez Virtual Box et importez l'image (menu `File` puis `Import
          Appliance...`).
        - Lancez la machine virtuelle (VM) et connectez-vous (utilisateur
          `toto`, mot de passe `toto`).
        - Toujours dans la VM, clonez vos dépôts git et
          [configurez vos paramètres gitlab](../env/post20-git.html#param%C3%A8tres-de-lutilisateur).
        - Normalement tous les logiciels sont déjà installés dans cette VM.


## Déroulement des TP

- Connectez-vous au canal Discord de votre groupe, à l'heure prévue, et suivez
  les instructions données par votre encadrant.
- Committez et pushez régulièrement vos fichiers (à la fin de chaque exercice
  ou de chaque "grosse question").
- Envoyez un message Discord à votre encadrant pour poser une question ou
  demander une vérification de votre travail (pensez à mettre un lien vers vos
  fichiers sur gitlab). Votre encadrant vous répondra personnellement ou sur le
  canal du groupe.
- Vous pouvez également utiliser le canal du groupe et vous entraider avec vos
  camarades mais vous devez travailler par vous-même; vous n'apprendrez rien en
  faisant du copier-coller sans réfléchir.


## Déroulement des CM

- Regardez la vidéo du cours sur ce
  [canal peertube](https://video.ploud.fr/video-channels/ulcovid19/videos),
  de préférence avant le créneau prévu.
- Posez vos questions sur le canal Discord, lors du créneau prévu.


