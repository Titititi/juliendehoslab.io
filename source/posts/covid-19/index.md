---
title: COVID-19, enseignements à distance
---

>    Ces enseignements à distance reposent sur les services suivants :
>
>    - [Discord](https://discordapp.com/), pour le service de chat;
>    - [Gitlab](https://about.gitlab.com/), pour l'hébergement et la gestion des
>      dépôts git;
>    - [Ploud Video France](https://video.ploud.fr/about/instance) pour
>      l'hébergement des vidéos;
>    - [Heroku](https://www.heroku.com/) pour l'hébergement de 
>      [l'application de partage d'écran](https://gitlab.com/juliendehos/covideo19).
>    
>    Et si vous voulez consacrer un peu de votre CPU ou GPU à la recherche :
>    
>    - [Folding@home takes up the fight against COVID-19 / 2019-nCoV](https://foldingathome.org/2020/02/27/foldinghome-takes-up-the-fight-against-covid-19-2019-ncov/)
>    - [Fight Covid-19 with Folding@home and NixOS](https://discourse.nixos.org/t/fight-covid-19-with-folding-home-and-nixos/6202/)

