---
title: Instructions pour les M2
date: 2020-03-24
notoc: notoc
---

Le cours de Programmation Fonctionnelle pour le Web aura lieu pendant les
créneaux prévus mais à distance, via le serveur Discord qui vous a été
communiqué.


## PRÉPARATIONS À FAIRE AVANT LES COURS/TP

- Discord :
    - Vérifiez que vous arrivez à vous connecter.
    - Prévoyez un casque ou des écouteurs pour les canaux vocaux

- Gitlab :
    - Vérifiez que vous avez un compte sur [Gitlab](https://about.gitlab.com/).
    - Forkez le dépôt fourni pour les TP.
    - Si votre dépot est en privé, ajoutez-y votre encadrant de TP avec le rôle
      `reporter`.
    - Entrainez-vous aux commandes git de base (add, commit, push) si vous ne
      les maitrisez pas déjà.

- Heroku :
    - Vérifiez que vous avez un compte sur [Heroku](https://www.heroku.com/).

- Logiciels de TP :
    - [Téléchargez l'image NixOS](http://www-lisic.univ-littoral.fr/~dehos/pub/nixos.ova) (environ 4 Go).
    - [Installez Virtual Box](https://www.virtualbox.org/wiki/Downloads).
    - Lancez Virtual Box et importez l'image (menu `File` puis `Import
      Appliance...`).
    - Lancez la machine virtuelle (VM) et connectez-vous (utilisateur
      `toto`, mot de passe `toto`).
    - Toujours dans la VM, clonez votre dépôt git et
      [configurez vos paramètres gitlab](../env/post20-git.html#param%C3%A8tres-de-lutilisateur).

- Vidéo de cours :
    - Chaque séance est accompagnée d'une vidéo qui présente les principales
      notions. Voir ce [canal peertube](https://video.ploud.fr/videos/watch/playlist/a5e42d0e-d2a1-4f90-af58-0be8cb412547?videoId=3f5e012b-8d05-4676-aa08-0c84e1c9bffa).


## Déroulement des séances

- Le support de cours est [Prog fonctionnelle avancée (M1
  Info)](../2020-pfa/index.html). Il a été adapté à l'enseignement à
  distance et à la refonte des programmes de la prochaine rentrée mais les
  notions abordées sont à peu près les mêmes.
- Connectez-vous au canal Discord à l'heure prévue, et suivez
  les instructions données par votre encadrant.
- Committez et pushez régulièrement vos fichiers.
- Envoyez un message Discord à votre encadrant pour poser une question ou
  demander une vérification de votre travail (pensez à mettre un lien vers vos
  fichiers sur gitlab). Votre encadrant vous répondra personnellement ou sur le
  canal du groupe.
- Vous pouvez également utiliser le canal du groupe et vous entraider avec vos
  camarades mais vous devez travailler par vous-même; vous n'apprendrez rien en
  faisant du copier-coller sans réfléchir.

