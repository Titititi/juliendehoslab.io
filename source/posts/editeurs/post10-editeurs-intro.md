---
title: Introduction aux éditeurs de texte
date: 2019-04-14
---

## Motivation

Cette page a pour objectif de vous aider à apprendre à utiliser efficacement un
éditeur de texte avancé. En effet, en tant qu'informaticien, vous avez très
souvent du texte à écrire ou à modifier, par exemple pour :

- éditer un fichier de configuration
- écrire du code, un rapport en latex, un email…
- dessiner une vache en ASCII art

* * *

```text
 ___________________________ 
< c'est vraiment pas drôle >
 --------------------------- 
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
```

## Choix de l'éditeur

### Éditeurs de texte

Pour écrire ou modifier du texte, vous pouvez utiliser l'un des très nombreux
[éditeurs de texte](https://en.wikipedia.org/wiki/Comparison_of_text_editors)
disponibles (notepad++, sublime text, geany…). Ces éditeurs permettent
notamment de :

- naviguer dans le texte (par caractère, mot, phrase, paragraphe…)
- écrire et modifier du texte (insertion, complétion, sélection,
  copier-coller…)
- colorer du code source, afficher les numéros de ligne…

### Environnements de développement intégrés

Il existe également de nombreux 
[environnements de développement intégrés](https://en.wikipedia.org/wiki/Comparison_of_integrated_development_environments)
(eclipse, visual studio, xcode…), plus spécialisés dans l'édition de code
source. Ces IDE permettent notamment de :

- lancer les compilation, exécution et débogage
- naviguer dans le code, le ré-usiner…

### Critères de choix
 
Tous ces outils demandent un apprentissage plus ou moins important pour être
utilisés efficacement. Donc, dans un premier temps, il vaut mieux en choisir un
et essayer de bien le maitriser plutôt que d'en tester 42 sans finalement
savoir vraiment les utiliser. Pour faire ce choix, on peut retenir les
critères suivants :

* * *

- éditeur de texte généraliste avec au moins les fonctionnalités de base d'un
  IDE et si possible paramétrable et extensible (plug-ins)
- ne pas être trop dégueu esthétiquement et pouvoir être lancé en mode 
  graphique et en mode texte (pour les connexions ssh notamment)
- être rapide à lancer
- être disponible sous différents systèmes (unix, windows…)
- être bien documenté et avoir une bonne communauté d'utilisateurs
- être gratuit et si possible open-source

* * *

Et là tout de suite le choix se restreint à, à peu près, 
[vim](https://fr.wikipedia.org/wiki/Vim) et 
[emacs](https://fr.wikipedia.org/wiki/Emacs). Je vous rassure tout de suite, je
ne vais pas relancer la 
[guerre des éditeurs](https://fr.wikipedia.org/wiki/Guerre_d%27%C3%A9diteurs)
puisqu'il est communément admis par moi-même que c'est vim le plus meilleur.

* * *

Malheureusement, comme mon chef ne veut pas admettre que c'est très pratique de
taper `<esc>:%s/toto/tata/g<ret>` pour remplacer "toto" par "tata", on va donc
également considérer emacs (où il faut faire `M-< M-% toto RET tata RET !`, ce
qui est effectivement nettement plus simple). En vrai, ces deux éditeurs
nécessitent un certain apprentissage pour être vraiment efficaces mais emacs
permet au moins de survivre sans cet apprentissage.

* * *

![](files/curves.jpg)

## Notions de dactylographie

Pour éditer du texte efficacement, il est indispensable de savoir taper sans
regarder le clavier. Il existe des logiciels permettant
de s'entraîner à cette « frappe à l'aveugle », par exemple
[klavaro](http://klavaro.sourceforge.net) ou
[ktouch](https://edu.kde.org/applications/all/ktouch).

* * *

Notez également que les dispositions de clavier classiques (AZERTY, QWERTY…)
sont intrinsèquement inefficaces et qu'il en existe des plus adaptées à une
frappe rapide comme 
[dvorak](https://fr.wikipedia.org/wiki/Disposition_Dvorak) ou 
[bépo](http://bepo.fr/wiki/Accueil) (sous Debian la commande `setxkbmap fr
bepo` permet de passer en bépo, et `setxkbmap fr` de revenir en azerty…).

* * *

Quelques conseils pour une frappe efficace :

- privilégiez la fiabilité à la vitesse. Évitez notamment de taper à mach 12
  mais d'avoir systématiquement à revenir corriger des fautes.

- ayez une bonne position de travail en général et un bon placement des mains
  en particulier. Par exemple sur un clavier AZERTY : index gauche sur la
  touche `f`, index droit sur la touche `j` (fiez-vous aux repères) et les
  autres doigts sur les touches voisines. 

- chaque touche doit être utilisée avec un doigt bien particulier, par exemple
  les touches `5/r/f/v` s'utilisent avec l'index gauche, `4/e/d/c` avec le
  majeur gauche, `8/u/j/,` avec l'index droit…

* * *

- essayez de garder au maximum la position de référence : bougez uniquement le
  doigt correspondant à la touche voulue, et ramenez-le à sa position de
  référence après la frappe. De même, évitez la souris et les flêches de
  direction.

- ne regardez pas le clavier pour trouver une touche. Si vous ne
  connaissez pas bien votre clavier, imprimez la disposition sur une feuille de
  papier, placez cette feuille au bas de l'écran et jettez-y un oeil en cas de
  besoin.

- pour les touches de contrôle (ctrl, alt, shift), utilisez les deux mains (une
  pour la touche de contrôle, une pour la touche complémentaire).  Évitez les
  contorsions à une main.

