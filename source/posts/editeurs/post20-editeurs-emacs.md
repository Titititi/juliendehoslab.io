---
title: Emacs
date: 2019-04-14
---

# Généralités

## Présentation

Emacs est un… heu… bon en fait personne ne sait vraiment ce qu'est emacs 
mais certains l'utilisent pour éditer du texte. Comme diraient les vilains
capitalistes du lobby franc-maçonniste des éditeurs d'éditeurs et d'IDE
propriétaires : "bouh emacs, c'est nul, c'est vieux, c'est moche et ça a un
petit kiki". 

* * *

Et comble de malhonnêteté, ils vous montreraient ça :

![](files/emacs-tui.png){width="50%"}

* * *

ou encore ça :

![](files/emacs-xray.png)

* * *

\pagebreak

voire même ça :

![](files/emacs-stallman.png)

* * *

\pagebreak

alors qu'en fait, emacs ça peut être ça :

![](files/emacs-text.png)

* * *

\pagebreak

et même ça :

![](files/emacs-ide.png){width="90%"}

* * *

[GNU Emacs](https://www.gnu.org/software/emacs) permet d'éditer du texte de
façon efficace. Il est relativement léger, disponible sur de nombreux systèmes,
open-source, paramétrable, extensible…  Il dispose de plug-ins permettant de
le rendre proche d'un IDE.  Enfin, il s'intègre bien avec les systèmes de type
unix.

* * *

## Démonstration

<video preload="metadata" controls>
<source src="files/emacs-demo.mp4" type="video/mp4">
![](files/emacs-demo.png)

</video>

* * *

## Installation sous debian et autres linux/unix

Sous [debian](http://www.debian.org) et sous de nombreux systèmes linux/unix,
emacs est disponible dans les paquets standards. Sur les machines de la fac,
emacs est déjà installé mais si vous voulez l'installer sur votre machine
perso, ouvrez une console et exécutez :

```text
sudo apt-get install emacs git global
```

Pour installer ma super config qui déchire trop tout :

```text
rm -rf ~/.emacs ~/.emacs.d
git clone https://gitlab.com/juliendehos/emacs_config ~/.emacs.d
```

* * *

> **Attention:**
>
>   Sur les machines de la fac, vous devez configurer 
>   l'[accès par proxy](../env/post10-proxy.html#title).

* * *

Emacs s'intègre bien au système. Par exemple, certains raccourcis-claviers
d'emacs sont disponibles dans la console bash. De même, vous pouvez spécifier
emacs comme éditeur par défaut (par exemple pour les messages de commit
git) en ajoutant la ligne suivante dans votre `~/.bashrc` :

```bash
export EDITOR=emacs
```

* * *

## Installation sous Windows

Il existe différentes façons 
d'[installer emacs sous Windows](http://www.gnu.org/software/emacs/manual/html_mono/efaq-w32.html).
Celle que je recommande tient en deux étapes :

#. installer Debian à la place de Windows
#. se reporter à la section précédente

# Guide de survie

Cette section présente le strict minimum nécessaire pour vous en sortir avec
emacs.  Ainsi, vous pourrez l'utiliser comme n'importe quel éditeur de
texte basique, même si vous ne voulez pas apprendre par coeur les 900
raccourcis-claviers et 10 000 commandes permettant de maitriser emacs comme un
dieu.

Pour apprendre emacs, n'essayez pas de retenir trop de raccourcis d'un coup.
Essayez plutôt d'en apprendre quelques-uns et de prendre l'habitude de les
utiliser avant d'en aborder de nouveaux. Et relisez les aides-mémoires
régulièrement.

* * *

## Lancer emacs

Pour lancer emacs, je vous recommande d'ouvrir une console, d'aller dans le
dossier qui vous intéresse à coup de `cd`, de `ls` et de `mkdir`, puis
d'exécuter :

```text
emacs &
```

* * *

Vous pouvez également lancer emacs en spécifiant des fichiers à ouvrir ou à
créer, par exemple :

```text
emacs toto.txt &
```

![](files/emacs-lancement.png)

Une fois dans emacs, vous pouvez taper du texte et naviguer avec les flêches du
clavier ou avec la molette de la souris.

* * *

## Raccourcis-claviers

L'un des intérêts d'emacs est de pouvoir lancer de nombreuses fonctionnalités
grâce à des commandes ou raccourcis claviers. Lorsqu'on commence à taper un
raccourci, celui-ci s'affiche en bas de la fenêtre selon la convention
suivante.

* * *

| symbole | touche clavier correspondante                             |
|---------|-----------------------------------------------------------|
| `M`     | `<meta>` (souvent, il s'agit de la touche `<alt>`)
| `C`     | `<ctrl>`
| `DEL`   | `<delete>`, parfois appelé `<backspace>`
| `ESC`   | `<escape>`
| `RET`   | `<return>`
| `SPC`   | `<space>`
| `TAB`   | `<tab>`

* * *

![](files/emacs-raccourci.png)

Les touches `<alt>` et `<ctrl>` sont toujours combinées à une autre touche
clavier. Par exemple, pour réaliser le raccourci `C-g`, il faut appuyer sur
la touche `<ctrl>` et la tenir appuyée, puis appuyer sur la touche `g`,
puis relacher la touche `<ctrl>` (autre notation : `<ctrl>+g`). 

* * *

Autre exemple, pour réaliser `C-x C-c`, il faut faire `<ctrl>+x` puis
`<ctrl>+c`. Ici, on est pas obligé de relacher la touche `<ctrl>` entre les
deux combinaisons : on peut directement appuyer sur la touche `<ctrl>` et la
tenir appuyée, puis appuyer sur la touche `x`, puis appuyer sur la touche
`c`, puis relacher la touche `<ctrl>`.

Autre exemple, pour réaliser `C-x k`, il faut faire `<ctrl>+x` puis `k`.
Ici, il faut donc relacher la touche `<ctrl>` avant d'appuyer sur la touche
`k`.

## Obtenir de l'aide, annuler, quitter

|           |                                              |
|-----------|----------------------------------------------|
| `C-g`     | annuler le raccourci ou la commande en cours
| `C-_`     | annuler l'action précédente 
| `C-x C-c` | quitter emacs
| `C-h`     | lancer l'aide

> **Attention:**
>
> Le raccourci à connaître absolument est `C-g`. Il permet d'annuler le
> raccourci ou la commande en cours.

## Gérer les fichiers

Emacs charge et manipule les fichiers via des buffers.

|            |                                                    |
|------------|----------------------------------------------------|
| `C-x C-f`  | ouvrir un fichier dans un buffer
| `C-x k`    | fermer un buffer
| `C-x C-s`  | enregistrer le buffer courant
| `C-x s`    | enregistrer tous les buffers
| `C-x C-w`  | enregistrer le buffer courant sous un autre nom
| `C-x b`    | changer de buffer

## Copier-coller du texte

Emacs dispose de raccourcis pour [sélectionner du texte](#sélectionner-du-texte)
mais on peut également le faire à la souris. Un texte sélectionné peut ensuite
être coupé ou copié.

|        |                       |
|--------|-----------------------|
| `C-w`  | couper la sélection
| `M-w`  | copier la sélection
| `C-y`  | coller

Notez que sous linux/unix, on peut généralement sélectionner/copier avec le
bouton gauche de la souris puis coller avec de bouton milieu.

## Rechercher et remplacer

|                |                                                             |
|----------------|-------------------------------------------------------------|
| `C-s <m> RET`  | rechercher le mot `<m>` 
| `C-r`          | rechercher vers le début du fichier
| `M-%`          | remplacer
| `C-M-s`        | rechercher une expression régulière vers la fin du fichier
| `C-M-r`        | rechercher une expression régulière vers le début du fichier

Taper `C-s` plusieurs fois pour réutiliser le mot de la recherche précédente
ou pour l'occurrence suivante.

## Divers

|                    |                                                             |
|--------------------|-------------------------------------------------------------|
| `M-/`              | complèter le mot courant en s'inspirant des mots du buffer
| `M-g M-g <n> RET`  | aller à la ligne `<n>`

# Fonctionnalités un peu plus avancées

Pour utiliser emacs efficacement, il est généralement conseillé de tout faire
au clavier (c'est-à-dire sans la souris et même sans les flêches de direction).
En effet, les raccourcis-claviers rendent cette méthode efficace et compatible
avec les bonnes pratiques de [dactylographie](post10-editeurs-intro.html#notions-de-dactylographie).

## Déplacer le curseur

| déplacer d'un(e)…  | vers l'arrière | vers l'avant  |
|--------------------|----------------|---------------|
| caractère          | `C-b`          | `C-f`
| mot                | `M-b`          | `M-f`
| ligne              | `C-p`          | `C-n`
| début/fin de ligne | `C-a`          | `C-e`
| phrase             | `M-a`          | `M-e`
| paragraphe         | `M-{`          | `M-}`
| buffer             | `M-<`          | `M->`

## Sélectionner du texte

|            |                                                            |
|------------|------------------------------------------------------------|
| `C-SPC`    | poser une marque (permet de [copier ou couper du texte](#copier-coller-du-texte))
| `C-x C-x`  | échanger la marque et la position courante
| `M-h`      | sélectionner tout le paragraphe
| `C-x h`    | sélectionner le buffer entier

## Effacer du texte

|            |                                           |
|------------|-------------------------------------------|
| `C-d`      | effacer le caractère courant
| `M-d`      | effacer le mot à partir du curseur
| `M-DEL`    | effacer le mot précédent
| `C-k`      | effacer la ligne à partir du curseur
| `M-z <c>`  | effacer jusqu'à la prochaine occurrence de `<c>` (`<c>` compris)

## Formatage

|                          |                                                   |
|--------------------------|---------------------------------------------------|
| `M-c`                    | mettre en majuscules le premier caractère d'un mot
| `M-u`                    | mettre le mot en majuscules
| `M-l`                    | mettre le mot en minuscules
| `M-q`                    | reformater le paragraphe
| `TAB`                    | indenter la ligne ou la sélection
| `M-^`                    | joindre à la ligne précédente
| `M-x flyspell-mode RET`  | activer/désactiver le correcteur d'orthographe
| `C-x RET l`              | spécifier la langue
| `C-x RET f`              | spécifier l'encodage

## Multi-fenêtrage

|          |                                           |
|----------|-------------------------------------------|
| `C-x 0`  | fermer la fenêtre courante
| `C-x 1`  | fermer toutes les fenêtres sauf la fenêtre courante
| `C-x 2`  | diviser la fenêtre courante en 2, horizontalement
| `C-x 3`  | diviser la fenêtre courante en 2, verticalement
| `C-x o`  | passer à une autre fenêtre
| `C-l`    | centrer la fenêtre courante sur le curseur
| `C-v`    | faire défiler l'écran vers la fin du fichier
| `M-v`    | faire défiler l'écran vers le début du fichier

## Registres

Les registres permettent de faire plusieurs copier-coller en même temps.
Ils sont nommés par une lettre ou un chiffre.

|                |                                           |
|----------------|-------------------------------------------|
| `C-x r s <c>`  | sauver la région dans le registre `<c>`
| `C-x r i <c>`  | insérer le contenu du registre `<c>`

## Macros

|                       |                                           |
|-----------------------|-------------------------------------------|
| `C-u <n> <commande>`  | répèter `<n>` fois la commande
| `C-u <n> <c>`         | répèter `<n>` fois le caractère `<c>`
| `C-x (`               | commencer l'enregistrement d'une macro
| `C-x )`               | terminer l'enregistrement d'une macro
| `C-x e`               | exécuter la dernière macro enregistrée

## Personnalisation

Le dossier `~/.emacs.d` permet de personnaliser emacs. Il est lu à chaque
démarrage du logiciel.

* * *

La [configuration fournie](#installation-sous-debian-et-autres-linuxunix) ajoute, entre autres,
quelques raccourcis :

|          |                                           |
|----------|-------------------------------------------|
| `<f1>`   | activer/désactiver le correcteur d'orthographe
| `<f2>`   | passer au buffer précédent
| `<f3>`   | passer au buffer suivant
| `<f4>`   | aller à l'erreur de compilation précédente
| `<f5>`   | aller à l'erreur de compilation suivante
| `<f6>`   | activer/désactiver ECB
| `<f7>`   | compiler
| `<f8>`   | activer/désactiver GDB (débogueur)
| `<f9>`   | aller à la définition du symbole courant 
| `<f10>`  | revenir à la position précédente 
| `<f11>`  | recréer l'index de navigation de code

## Documentation

Emacs est beaucoup documenté, notamment :

* l'aide en ligne, accessible depuis emacs par `C-h`
* [la documentation officielle](https://www.gnu.org/software/emacs/manual/html_node/emacs/index.html) (prévoir de l'aspirine)
* [l'aide-mémoire officiel](https://www.gnu.org/software/emacs/refcards/pdf/fr-refcard.pdf) 
* [le site des tuteurs de l'ENS](http://www.tuteurs.ens.fr/unix/editeurs/emacs.html) (certaines pages sont un peu anciennes mais toujours valides)

# Édition de code source

Pour éditer du code, emacs propose quelques fonctionnalités intéressantes que
l'on trouve habituellement dans les IDE.  La [configuration fournie](#installation-sous-debian-et-autres-linuxunix) contient quelques paramétrages et packages facilitant
leur utilisation. 

## Formater du code

Emacs s'adapte automatiquement au type de fichier en cours d'édition (notamment
pour la coloration syntaxique, les commentaires, l'indentation…).  Les
raccourcis-claviers donnés ici fonctionnent pour les modes C et C++ mais
peuvent également fonctionner pour d'autres modes. 

* * *

|                             |                                           |
|-----------------------------|-------------------------------------------|
| `TAB`                       | indenter la sélection ou la ligne courante
| `M-;`                       | ajouter un commentaire
| `M-x comment-region RET`    | commenter la sélection 
| `C-c C-c`                   | commenter la sélection (avec la [configuration fournie](#installation-sous-debian-et-autres-linuxunix))
| `M-x uncomment-region RET`  | décommenter la sélection 
| `C-c c`                     | décommenter la sélection (avec la configuration fournie)

## Compiler 

|                          |                                           |
|--------------------------|-------------------------------------------|
| `M-x compile RET`        | lancer la compilation (`<f7>` avec la [configuration fournie](#installation-sous-debian-et-autres-linuxunix))
| `M-x next-error RET`     | aller à l'erreur suivante (`<f5>` avec la configuration fournie)
| `M-x previous-error RET` | aller à l'erreur précédente (`<f4>` avec la configuration fournie)
| `M-!`                    | exécuter une commande shell depuis emacs
| `M-x shell`              | ouvrir un shell dans emacs

* * *

![](files/emacs-compilation.png)

## Déboguer

Ne pas oublier de compiler avec l'option `-g`.

|                |                                           |
|----------------|-------------------------------------------|
| `M-x gdb RET`  | lancer gdb
| `<f8>`         | activer/désactiver GDB (débogueur)

* * *

![](files/emacs-debug1.png)

* * *

![](files/emacs-debug2.png)

## Travailler sur un projet avec ECB

[ECB](http://ecb.sourceforge.net) permet d'accéder aux fichiers et symboles
d'un projet plus facilement. Pensez bien à lancer emacs depuis de dossier
principal de votre projet.

|                           |                                           |
|---------------------------|-------------------------------------------|
| `M-x ecb-minor-mode RET`  | activer/désactiver ECB
| `<f6>`                    | activer/désactiver ECB (avec la [configuration fournie](#installation-sous-debian-et-autres-linuxunix))

* * *

![](files/emacs-ecb.png){width="90%"}

## Complétion et navigation avec company et ggtags

Les packages [auto-complete](https://github.com/auto-complete/auto-complete) et 
[semantic](http://www.gnu.org/software/emacs/manual/html_node/semantic/index.html)
permettent de compléter automatiquement les symboles pendant la frappe. Cette
complétion est "sémantique", c'est-à-dire que seul du code valide est proposé.
La complétion "statistique" (c'est-à-dire les mots du buffer pouvant
correspondre) est toujours disponible via `M-/`.

* * *

|           |                                           |
|-----------|-------------------------------------------|
| `TAB`     | sélectionner la complétion
| `<f9>`    | aller à la définition d'un symbole (avec la [configuration fournie](#installation-sous-debian-et-autres-linuxunix))
| `<f10>`   | revenir à la position précédente (avec la [configuration fournie](#installation-sous-debian-et-autres-linuxunix))
| `<f11>`   | recréer l'index de navigation (avec la [configuration fournie](#installation-sous-debian-et-autres-linuxunix))

* * *

![](files/emacs-autocomplete.png)

