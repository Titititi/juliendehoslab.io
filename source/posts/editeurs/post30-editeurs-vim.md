---
title: Vim
date: 2019-04-14
---

> **Attention:**
>
> Cette page n'est pas une documentation complète <br> de vim ni un nième
> tutoriel. Elle a uniquement pour <br> but d'expliquer quelques principes de
> base de <br> vim, de présenter une configuration à peu près <br> utilisable
> et de donner des liens vers des <br> documentations classiques.

# Généralités

## Présentation

[Vim](http://www.vim.org) est un éditeur de texte qui se veut hautement
personnalisable et très efficace pour éditer du texte. En réalité, c'est plutôt
un logiciel pour masochiste : tu souffres 3 mois pour l'apprendre et ensuite tu
re-souffres à chaque fois que tu dois t'en passer.

* * *

Un peu d'humour de geek :

![](files/vim-exit.png)

* * *

Houlala comme c'est drôle. Une autre :

![](files/vim-nano.png)

* * *

\newpage

Et pour ceux qui n'auraient pas encore vomi, un screenshot de config moche :

![](files/vim-capture.png)

* * *

Bon, une vraie config maintenant :

![](files/vim-capture-config.png)

* * *

\newpage

Et pour une poignée de plugins :

![](files/vim-ide.png)

* * *

Bien qu'il soit difficile d'avoir des statistiques fiables sur l'utilisation
des IDE et éditeurs de texte, vim semble encore relativement utilisé, 
[notamment par les développeurs](http://stackoverflow.com/research/developer-survey-2015#tech-editor):

![](files/vim-stats.png)

* * *

Par ailleurs, de nombreux logiciels utilisent des
raccourcis-claviers de vim (hjkl…) ou 
ont des plugins simulant vim : 
[eclipse](http://vrapper.sourceforge.net/home), 
[intellij idea](https://www.jetbrains.com/help/idea/2016.3/using-intellij-idea-as-the-vim-editor.html), 
[visual studio](https://github.com/jaredpar/VsVim), 
[firefox](http://www.vimperator.org/vimperator) …

## Démonstration

<video preload="metadata" controls>
<source src="files/vim-demo.mp4" type="video/mp4" />
![](files/vim-demo.png)

</video>

## Installation sous debian et autres linux/unix

Sous [debian](http://www.debian.org) et sous de nombreux systèmes linux/unix,
vim est disponible dans les paquets standards. Généralement, plusieurs
implémentations sont proposées : mode texte (vim), mode fenêtré (gvim),
avec ou sans support python…

Sur les machines de la fac, vim et gvim sont déjà installés mais si vous voulez
les installer sur votre machine perso, ouvrez une console et exécutez :

```
sudo apt-get install vim vim-gtk
```

* * *

> **Attention:**
>
> La configuration par défaut de vim est déjà très utilisable mais **pour
> être vraiment efficace, vous devez configurer votre vim** (fichier
> `.vimrc`, plugins): [exemple de config vim](https://gitlab.com/juliendehos/vim_config) 
> (sur les machines de la fac, pensez à 
> [configurer les paramètres du proxy](../env/post10-proxy.html#title).

* * *

Utilisez de préférence gvim. Gvim s'ouvre dans une nouvelle fenêtre (ce qui
permet de ne pas bloquer un terminal) et gère mieux le copier-coller en
interaction avec le système.  

## Installation sous Windows

Hahaha, très drôle…

Bon ok, techniquement c'est possible mais a priori si vous voulez utiliser vim,
c'est que vous n'êtes pas allergique au clavier donc ce serait dommage de ne
pas utiliser également un unix avec un shell.

# Les modes de vim

Vim propose plusieurs modes de fonctionnement (édition, insertion, visuel…).
Chaque mode réagit différemment au clavier (par exemple appuyer sur `j`
insère un "j" en mode insertion, déplace le curseur en mode édition et modifie
la sélection courante en mode visuel).

> **Attention:**
>
> Le mode principal de vim est le mode édition. La touche `Esc` permet de
> revenir dans le mode édition.

## Mode édition

Comme expliqué précédemment, le mode édition est le mode principal et est
accessible via la touche `Esc`. Ce mode permet de déplacer le curseur, de
couper/copier/coller du texte, d'annuler des actions, de passer dans d'autres
modes…

* * *

<video preload="metadata" controls>
<source src="files/vim-mode-edition.mp4" type="video/mp4" />
![](files/vim-mode-edition.png)

</video>

## Mode insertion

Le mode insertion permet d'écrire du texte. Dans ce mode, tout ce qui est tapé
au clavier est inséré dans le texte, hormis les touches `Esc`, flêches et
quelques combinaisons (`Ctrl…`).

Quelques actions permettant de passer dans le mode insertion (depuis le mode édition) :

|     |                                                          |
|-----|----------------------------------------------------------|
| `i` | passe en mode insertion avant le curseur                 |
| `a` | passe en mode insertion après le curseur                 |
| `O` | insère une ligne avant le curseur et passe en insertion  |
| `o` | insère une ligne après le curseur et passe en insertion  |
| `s` | supprime le caractère courant et passe en insertion      |
| `S` | vide la ligne courante et passe en insertion             |

* * *

<video preload="metadata" controls>
<source src="files/vim-mode-insertion.mp4" type="video/mp4" />
![](files/vim-mode-insertion.png)

</video>

## Mode visuel

Le mode visuel permet de sélectionner du texte de façon visuelle.  Il existe
également un mode visuel par bloc, qui permet de sélectionner un bloc 2D de
texte.

|          |                                         |
|----------|-----------------------------------------|
| `v`      | passe en mode visuel
| `Ctrl+v` | passe en mode visuel par bloc

* * *

<video preload="metadata" controls>
<source src="files/vim-mode-visuel.mp4" type="video/mp4" />
![](files/vim-mode-visuel.png)

</video>

## Mode commande

Le mode commande permet d'exécuter des commandes complexes (par exemple,
remplacer du texte…) ou qui ne sont pas directement en rapport avec le texte
courant (par exemple, ouvrir un fichier, changer de buffer, compiler, quitter
vim…). Une commande commence par le caractère `:`.

* * *

<video preload="metadata" controls>
<source src="files/vim-mode-commande.mp4" type="video/mp4" />
![](files/vim-mode-commande.png)

</video>

# La grammaire vim

## Action

Vim fonctionne par action. Par exemple, "passer en mode insertion puis taper du
texte puis revenir en mode édition" est une action. Une action peut être
annulée, refaite, répétée.

|     |                                        |
|-----|----------------------------------------|
| `u` | annuler la dernière action 
| `r` | refaire la dernière action annulée
| `.` | répéter la dernière action

* * *

Une action peut être plus complexe, selon la grammaire `<répétitions> <opérateur> <mouvement>`.  Par exemple, l'action "4 fois supprimer (d) un mot (w)" s'écrit `4dw`.

Répéter l'opérateur applique l'action sur la ligne courante. Par exemple, `dd` supprime la ligne courante.

## Mouvements

|     |                                    |
|-----|------------------------------------|
| `h`  | un caractère vers la gauche
| `l`  | un caractère vers la droite
| `k`  | une ligne au dessus
| `j`  | une ligne en dessous
| `b`  | mot précédent
| `w`  | mot suivant
| `e`  | fin du mot
| `(`  | début de la phrase
| `)`  | fin de la phrase
| `{`  | début du paragraphe
| `}`  | fin du paragraphe
| `gg` | début du fichier
| `G`  | fin du fichier
| `^`  | début de la ligne
| `$`  | fin de la ligne

## Opérateurs

|      |                                        |
|------|----------------------------------------|
| `d`  | supprimer (delete)
| `s`  | remplacer (substitute)
| `y`  | copier (yank)
| `p`  | coller après le curseur (paste)
| `P`  | coller avant le curseur
| `x`  | supprimer un caractère

# Fonctionnalités de base

## Commandes de base (quitter, ouvrir un fichier, compiler…)

|                   |                                                      |
|-------------------|------------------------------------------------------|
| `:q`              | quitter                                              |
| `:q!`             | quitter sans enregistrer                             |
| `:e`              | ouvrir un fichier                                    |
| `:w`              | enregistrer le fichier courant                       |
| `:make`           | compiler                                             | 
| `:set makeprg=…`  | définir la commande de compilation (make par défaut) |
| `:!…`             | exécuter une commande shell                          |

* * *

<video preload="metadata" controls>
<source src="files/vim-demo-base.mp4" type="video/mp4" />
![](files/vim-demo-base.png)

</video>

## Naviguer dans le fichier

|           |                                              |
|-----------|----------------------------------------------|
| `{`       | aller au début du paragraphe 
| `}`       | aller à la fin du paragraphe
| `Ctrl+f`  | faire défiler vers le début du fichier
| `Ctrl+b`  | faire défiler vers la fin du fichier
| `%`       | aller au symbôle appairé (`()`, `{}` …)
| `42G`     | aller à la ligne 42

* * *

<video preload="metadata" controls>
<source src="files/vim-demo-navigation.mp4" type="video/mp4" />
![](files/vim-demo-navigation.png)

</video>

## Indenter du code

|       |                             |
|-------|-----------------------------|
| `==`  | indenter automatiquement
| `<<`  | décaler vers la gauche
| `>>`  | décaler vers la droite

* * *

<video preload="metadata" controls>
<source src="files/vim-demo-indentation.mp4" type="video/mp4" />
![](files/vim-demo-indentation.png)

</video>

## Copier-coller du texte

|       |                                                                       |
|-------|-----------------------------------------------------------------------|
| `d`   | couper
| `y`   | copier
| `dd`  | couper la ligne courante
| `yy`  | copier la ligne courante
| `di(` | supprimer le texte à l'intérieur des parenthéses englobant le curseur
| `ci(` | changer le texte à l'intérieur des parenthéses englobant le curseur
| `dap` | couper le texte du paragraphe englobant le curseur

* * *

<video preload="metadata" controls>
<source src="files/vim-demo-copier.mp4" type="video/mp4" />
![](files/vim-demo-copier.png)

</video>

## Rechercher et remplacer

|                  |                                                      |
|------------------|------------------------------------------------------|
| `/`              | recherche un motif vers la fin du fichier
| `n`              | recherche l'occurrence suivante
| `N`              | recherche l'occurrence précédente
| `*`              | recherche le mot situé sous le curseur, vers la fin
| `:%s/…/…/g`  | remplace un motif par un autre dans tout le fichier

* * *

<video preload="metadata" controls>
<source src="files/vim-demo-recherche.mp4" type="video/mp4" />
![](files/vim-demo-recherche.png)

</video>

# Personnalisation

## Mise en oeuvre

Le fichier de configuration principal est `~/.vimrc`. Les plugins sont placés
dans le dossier `~/.vim`. Vim dispose de son propre langage de script
(vimscript) et peut également supporter les langages perl, lua, python.

Il existe de nombreux [plugins pour vim](http://vimawesome.com). Voir aussi
la [config vim proposée](https://gitlab.com/juliendehos/vim_config).

* * *

> **Attention:**
>
> Configurez votre vim le plus simplement et progressivement possible.
> Évitez notamment d'ajouter des options de configuration ou des plugins que
> vous ne comprenez pas. 

## Configuration fournie

Avec la configuration fournie, l'analyseur de code considère les fichiers des
dossiers `.` et `src`. Donc, si vous éditez du code, ouvrez vim dans le
dossier contenant votre code ou un dossier `src`.

|                           |                                                          |
|---------------------------|----------------------------------------------------------|
| `F2`, `F3`                | buffer précédent/suivant
| `F4`, `F5`                | erreur de compilation précédente/suivante
| `F6`                      | ouvre/ferme le navigateur de fichiers (nerdtree)
| `F7`                      | ouvre/ferme le navigateur de symboles (tagbar)
| `F10`                     | active/désactive la correction orthographique en français
| `F11`                     | active/désactive la correction orthographique en anglais
| `F8`, `F9`                | erreur d'orthographe précédente/suivante
| `F1`                      | suggestions de correction orthographique
| `:BD`                     | ferme un buffer sans fermer la fenêtre
| `Space Space w`           | navigation avec easy-motion
| `Space Space e`           | navigation avec easy-motion (fins de mot)
| flêches                   | redimensionne la fenêtre courante
| `Ctrl+h`, `Ctrl+l`        | passe sur la fenêtre gauche/droite
| `Ctrl+k`, `Ctrl+j`        | passe sur la fenêtre au-dessus/en-dessous

* * *

<video preload="metadata" controls>
<source src="files/vim-demo-personnalisation.mp4" type="video/mp4" />
![](files/vim-demo-personnalisation.png)

</video>

# Pour aller plus loin…

## Autres fonctionnalités intéressantes

- completion
- marqueurs
- macros
- …

## Documentations classiques

- vimtutor : le tutoriel de base accessible en tapant `vimtutor` dans un shell
- [interactive vim tutorial](http://www.openvim.com/) : un tutorial vim interactif (si, si)
- [learn vim in y minutes](https://learnxinyminutes.com/docs/vim/) : 
  un résumé concis des fonctionnalités principales
- [vimdocs](http://www.vim.org/docs.php) : les docs officielles (pour les insomniaques)

