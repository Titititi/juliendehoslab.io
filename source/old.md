---
title: Anciens cours 
notoc: notoc
---


## Anciens cours

- [Programmation fonctionnelle (Master Info)](posts/pf/index.html)
- [Programmation fonctionnelle pour le web (Master Info)](posts/pfw/index.html)
- [Calcul haute performance (Master Info)](posts/hpc/index.html)
- [Éditeurs de texte](posts/editeurs/index.html)
- [Environnement de travail](posts/env/index.html)


## TODO

- [2020 env](posts/2020-env/index.html)
- [2020 gl1](posts/2020-gl1/index.html)
- [2020 gl2](posts/2020-gl2/index.html)

