{-# LANGUAGE OverloadedStrings #-}

import Clay
import Data.Text.Lazy.IO (putStr)
import Prelude hiding (putStr, div)

main :: IO ()
main = putStr $ renderWith compact [] $ do
-- main = putStr $ renderWith pretty [] $ do
-- margin: top right bottom left

    -- main styles

    hr ? border none nil white

    -- video <> img ? do
    --     display block
    --     marginLeft auto
    --     marginRight auto

    -- section |> p |> img <> p |> img <> figure |> img ? do
    --     backgroundColor white
    --     marginTop (px 10)

    section |> p |> img <> p |> img <> figure |> img ? do
        border solid (px 1) "#ddd"
        backgroundColor white
        marginTop (px 10)

    html ? do
        padding nil (px 5) nil (px 5)
        margin auto auto auto auto
        maxWidth (px 900)
        width (pct 100)
        background ("#222"::Color)
        color white
        fontFamily ["Helvetica", "sans-serif"] [sansSerif]
        fontSize (pt 14)

    ul ? listStyleType none

    li # ".post" ? do
        display flex
        alignItems center

    ul |> li # before ? do
        content (stringContent "-")
        position absolute
        marginLeft (em (-1))

    a ? do
        color "#6b6"
        textDecoration none

    (h1 <> h2 <> h3) ? a ? do
        color "#ddd"
        textDecoration none

    h1 ? fontSize (em 2)
    h2 ? fontSize (em 1.5)
    h3 ? fontSize (em 1.2)

    (h1 <> h2 <> h3) ? 
        marginTop (px 40)

    (td <> h1 <> h2 <> h3) |> a |> img ?
        display inline

    table <> th <> td ? do
        border solid (px 1) "#ddd"
        borderCollapse collapse
        padding (px 5) (px 5) (px 5) (px 5)

    table ?
        marginBottom (px 20)

    "#mytopic" <> "#mysite" ? do
        color "#ddd"
        fontWeight bold
        textDecoration none

    "#mysite" ?
        fontSize (em 1.5)

    "#mydate" ? 
        textAlign (alignSide sideRight)

    -- search box

{-
    "#searchbox" ? do
        overflow hidden
        margin nil nil nil nil
        padding nil nil nil nil
        maxWidth (px 200)
        height (px 40)
        border none nil white
        verticalAlign vAlignTop
-}

    -- header/footer

    table # ".myfloat" ? do 
        border none nil white
        width (pct 100)

    td # ".myfloat-left" <> td # ".myfloat-center" <> td # ".myfloat-right" ? do
        border solid nil white
        padding (px 5) (px 5) nil (px 5)
        margin (px 5) (px 5) nil (px 5)
        width (pct 33)

    header |> (td # ".myfloat-left" <> td # ".myfloat-center" <> td # ".myfloat-right") ?
        width auto

    ".myfloat-left" ? textAlign (alignSide sideLeft)

    ".myfloat-center" ? textAlign (alignSide sideCenter)

    ".myfloat-right" ? textAlign (alignSide sideRight)

    main_ ? do
        borderTop solid (px 1) white
        borderBottom solid (px 1) white

    main_ <> header <> footer ? 
        padding (px 5) (px 5) (px 5) (px 5)

    "#title" ? do
        fontVariant smallCaps
        textAlign center

    img # ".icon" ? do
        border solid nil black
        margin (px 5) (px 5) nil (px 5) 
        width (px 32)

    img # ".icon-inline" ? do
        border solid nil black
        margin (px 5) nil nil (px 10)
        width (px 24)

    -- sidebar

    "#sidebar-container" ? 
        width (pct 100)

    "#sidebar-content" ? do
        verticalAlign vAlignTop
        width (pct 100)

    "#sidebar-toc" ? do
        verticalAlign vAlignTop
        float floatRight
        maxWidth (px 250)
        backgroundColor "#111"
        margin (px 10) (px 10) (px 10) (px 10)
        padding (px 0) (px 10) (px 0) (px 10)
        border solid (px 1) "#ddd"

    "#sidebar-toc" ? ul ? do
        margin (px 10) nil (px 10) nil
        paddingLeft (px 20)

    -- div |> "#sidebar-toc" # before ? do
    --     content (stringContent "Table of contents")
    --     display block

    -- source code

    pre # ".text" <> pre # ".sourceCode" <> pre |> div # ".sourceCode" ? do
        border solid (px 1) "#ddd"
        backgroundColor "#111"
        padding (px 10) (px 10) (px 10) (px 10)
        overflow auto

    code ? 
        fontSize (pt 12)

    (p <> li) ? code ? 
        color "#dd8"
        -- backgroundColor "#111"

    -- block

    blockquote |> p ? margin (px 0) (px 0) (px 0) (px 0)

    blockquote ? do
        display inlineBlock
        margin (px 20) (px 20) (px 20) (px 20)
        padding (px 10) (px 10) (px 10) (px 10)
        color "#ddd"
        backgroundColor "#422"
        border solid (px 1) "#ddd"

